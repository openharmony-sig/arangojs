export function querystringify(obj: Record<string, any>) {
  console.log(`arangojs-当前对象-----${JSON.stringify(obj)}`)
  const params = new URLSearchParams();
  for (const [key, value] of Object.entries(obj)) {
    if (value === undefined) continue;
    if (!Array.isArray(value)) {
      params.append(key, value + "");// oh的URLSearchParams不支持除了字符之外的类型
    } else {
      for (const item of value) {
        params.append(key, item + "");// oh的URLSearchParams不支持除了字符之外的类型
      }
    }
  }
  return String(params);
}
