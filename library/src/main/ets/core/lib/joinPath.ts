/**
 * Wrapper around the browser-compatible implementation of the path module.
 *
 * @packageDocumentation
 * @internal
 */

import { join } from "path";

/**
 * @internal
 */
export const joinPath = join;
