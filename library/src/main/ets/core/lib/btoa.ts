/**
 * Node.js implementation of browser `btoa` function.
 *
 * @packageDocumentation
 * @internal
 */

/**
 * @internal
 */
// TODO 使用openharmony @ohos.buffer 实现
import buffer from '@ohos.buffer';
export function base64Encode(str: string) {
  return buffer.from(str).toString("base64");
}
