/*
  * Copyright (c) 2023 Huawei Device Co., Ltd.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
    *
  * http://www.apache.org/licenses/LICENSE-2.0
    *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
import http from "@ohos.net.http";
import { ArangojsResponse } from "../lib/request.node";

/**
 * 参数转换
 * @param options
 */
function transformOtherInputToOhos(input) {
  let {  url, body, method, headers, expectBinary } =
    input;
  return {
    url,
    method: http.RequestMethod[method],
    header: typeof headers === "object" ? headers : null,
    extraData: body || {},
    expectDataType: expectBinary
      ? http.HttpDataType.ARRAY_BUFFER
      : http.HttpDataType.STRING,
  };
}

/**
 * 返回值转换
 * @param output
 */
function transformOhosOutputToOther(
  output: http.HttpResponse
): ArangojsResponse | { headers: Record<string, any> } {
  return {
    statusCode: output.responseCode,
    headers: output.header as unknown as any,
    body: output.result,
  };
}

export default function (options, cb) {
  const { url, method, header, extraData, expectDataType } =
    transformOtherInputToOhos(options);
  let httpRequest = http.createHttp();
  httpRequest.request(
    url,
    {
      method,
      header,
      extraData,
      expectDataType,
    },
    (err, data) => {
      if (!err) {
        cb(null, transformOhosOutputToOther(data));
      } else {
        cb(err);
      }
    }
  );
}
