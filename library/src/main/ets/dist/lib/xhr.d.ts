/**
 * Wrapper around the `xhr` module for HTTP(S) requests in the browser.
 *
 * @packageDocumentation
 * @internal
 */
import { ClientRequest } from "http";
import { Errback } from "./errback";
/**
 * @internal
 */
declare const _default: (options: any, cb: Errback<any>) => ClientRequest;
export default _default;
//# sourceMappingURL=xhr.d.ts.map