/**
 * Utility function for omitting properties by key.
 *
 * @packageDocumentation
 * @internal
 */
/**
 * @internal
 */
export declare function omit<T extends {}>(obj: T, keys: (keyof T)[]): T;
//# sourceMappingURL=omit.d.ts.map