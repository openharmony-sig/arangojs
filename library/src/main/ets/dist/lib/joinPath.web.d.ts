/**
 * Wrapper around the browser-specific implementation of the path module.
 *
 * @packageDocumentation
 * @internal
 */
/**
 * @internal
 */
export declare function joinPath(...path: string[]): string;
//# sourceMappingURL=joinPath.web.d.ts.map