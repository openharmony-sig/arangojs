/**
 * Utility function for constructing a multipart form in Node.js.
 *
 * @packageDocumentation
 * @internal
 */
/// <reference types="node" />
import { Headers } from "../connection";
/**
 * @internal
 */
export type Fields = {
    [key: string]: any;
};
/**
 * @internal
 */
export interface MultipartRequest {
    headers?: Headers;
    body: Buffer;
}
/**
 * @internal
 */
export declare function toForm(fields: Fields): Promise<MultipartRequest>;
//# sourceMappingURL=multipart.d.ts.map