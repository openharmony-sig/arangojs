/**
 * Wrapper around browser `btoa` function to allow substituting a
 * Node.js-specific implementation.
 *
 * @packageDocumentation
 * @internal
 */
/**
 * @internal
 */
export declare function base64Encode(str: string): string;
//# sourceMappingURL=btoa.web.d.ts.map