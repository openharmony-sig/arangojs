/**
 * Utility function for constructing a multipart form in the browser.
 *
 * @packageDocumentation
 * @internal
 */
import { Errback } from "./errback";
import { Fields, MultipartRequest } from "./multipart";
/**
 * @internal
 */
export declare function toForm(fields: Fields, callback: Errback<MultipartRequest>): void;
//# sourceMappingURL=multipart.web.d.ts.map