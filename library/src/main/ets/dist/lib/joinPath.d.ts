/**
 * Wrapper around the browser-compatible implementation of the path module.
 *
 * @packageDocumentation
 * @internal
 */
/**
 * @internal
 */
export declare const joinPath: (...paths: string[]) => string;
//# sourceMappingURL=joinPath.d.ts.map