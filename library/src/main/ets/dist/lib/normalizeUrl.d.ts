/**
 * Utility function for normalizing URLs.
 *
 * @packageDocumentation
 * @internal
 */
/**
 * @internal
 */
export declare function normalizeUrl(url: string): string;
//# sourceMappingURL=normalizeUrl.d.ts.map