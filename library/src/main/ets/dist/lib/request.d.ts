/**
 * Wrapper for the Node.js implementation of the HTTP(S) request function.
 *
 * In the future this module could conditionally provide VST support.
 *
 * @packageDocumentation
 * @internal
 */
export { ArangojsResponse, ArangojsError, RequestFunction } from "./request.node";
export * from "../ohos/request.ohos";
//# sourceMappingURL=request.d.ts.map