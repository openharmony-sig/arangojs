/**
 * Node.js implementation of browser `btoa` function.
 *
 * @packageDocumentation
 * @internal
 */
export declare function base64Encode(str: string): any;
//# sourceMappingURL=btoa.d.ts.map