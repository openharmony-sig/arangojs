/**
 * Error codes handled by arangojs.
 *
 * See also [ArangoDB error documentation](https://www.arangodb.com/docs/stable/appendix-error-codes.html).
 *
 * @packageDocumentation
 * @internal
 */
export declare const TRANSACTION_NOT_FOUND = 10;
export declare const ERROR_ARANGO_MAINTENANCE_MODE = 503;
export declare const ERROR_ARANGO_CONFLICT = 1200;
export declare const ANALYZER_NOT_FOUND = 1202;
export declare const DOCUMENT_NOT_FOUND = 1202;
export declare const COLLECTION_NOT_FOUND = 1203;
export declare const VIEW_NOT_FOUND = 1203;
export declare const DATABASE_NOT_FOUND = 1228;
export declare const GRAPH_NOT_FOUND = 1924;
//# sourceMappingURL=codes.d.ts.map