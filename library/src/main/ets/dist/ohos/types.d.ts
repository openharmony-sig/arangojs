/// <reference types="node" />
import { ClientRequest, IncomingMessage } from "http";
export interface OhosOptions {
}
export type Headers = Record<string, string>;
export type RequestOptions = {
    method: string;
    url: {
        pathname: string;
        search?: string;
    };
    headers: Headers;
    body: any;
    expectBinary: boolean;
    timeout?: number;
};
export type Errback<T = never> = (err: Error | null, result?: T) => void;
export interface ArangojsResponse extends IncomingMessage {
    request: ClientRequest;
    body?: any;
    arangojsHostUrl?: string;
}
//# sourceMappingURL=types.d.ts.map