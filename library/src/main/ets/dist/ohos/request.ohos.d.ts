import { RequestInterceptors } from "../connection";
import { OhosOptions } from "./types";
import { Errback } from "../lib/errback";
import { RequestOptions, ArangojsResponse } from "../lib/request.node";
export declare const isBrowser = true;
export declare function createRequest(baseUrl: string, agentOptions: OhosOptions & RequestInterceptors, agent?: any): ({ method, url: reqUrl, headers, body, timeout, expectBinary }: RequestOptions, cb: Errback<ArangojsResponse>) => void;
//# sourceMappingURL=request.ohos.d.ts.map