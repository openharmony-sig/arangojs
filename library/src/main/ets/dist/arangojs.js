import buffer$1 from '@ohos.buffer';
import t$1 from '@ohos.hilog';
import e$1 from '@ohos.process';
import r$1 from '@ohos.util';
import require$$0$1 from '@ohos.url';
import http from '@ohos.net.http';

/**
 * ```ts
 * import type { ArangoError, HttpError } from "arangojs/error";
 * ```
 *
 * The "error" module provides types and interfaces for TypeScript related
 * to arangojs error handling.
 *
 * @packageDocumentation
 */
const messages = {
    0: "Network Error",
    304: "Not Modified",
    400: "Bad Request",
    401: "Unauthorized",
    402: "Payment Required",
    403: "Forbidden",
    404: "Not Found",
    405: "Method Not Allowed",
    406: "Not Acceptable",
    407: "Proxy Authentication Required",
    408: "Request Timeout",
    409: "Conflict",
    410: "Gone",
    411: "Length Required",
    412: "Precondition Failed",
    413: "Payload Too Large",
    414: "Request-URI Too Long",
    415: "Unsupported Media Type",
    416: "Requested Range Not Satisfiable",
    417: "Expectation Failed",
    418: "I'm a teapot",
    421: "Misdirected Request",
    422: "Unprocessable Entity",
    423: "Locked",
    424: "Failed Dependency",
    426: "Upgrade Required",
    428: "Precondition Required",
    429: "Too Many Requests",
    431: "Request Header Fields Too Large",
    444: "Connection Closed Without Response",
    451: "Unavailable For Legal Reasons",
    499: "Client Closed Request",
    500: "Internal Server Error",
    501: "Not Implemented",
    502: "Bad Gateway",
    503: "Service Unavailable",
    504: "Gateway Timeout",
    505: "HTTP Version Not Supported",
    506: "Variant Also Negotiates",
    507: "Insufficient Storage",
    508: "Loop Detected",
    510: "Not Extended",
    511: "Network Authentication Required",
    599: "Network Connect Timeout Error",
};
const nativeErrorKeys = [
    "fileName",
    "lineNumber",
    "columnNumber",
    "stack",
    "description",
    "number",
];
/**
 * Indicates whether the given value represents an {@link ArangoError}.
 *
 * @param error - A value that might be an `ArangoError`.
 */
function isArangoError(error) {
    return Boolean(error && error.isArangoError);
}
/**
 * Indicates whether the given value represents an ArangoDB error response.
 *
 * @internal
 */
function isArangoErrorResponse(body) {
    return (body &&
        body.hasOwnProperty("error") &&
        body.hasOwnProperty("code") &&
        body.hasOwnProperty("errorMessage") &&
        body.hasOwnProperty("errorNum"));
}
/**
 * Indicates whether the given value represents a Node.js `SystemError`.
 */
function isSystemError(err) {
    return (Object.getPrototypeOf(err) === Error.prototype &&
        err.hasOwnProperty("code") &&
        err.hasOwnProperty("errno") &&
        err.hasOwnProperty("syscall"));
}
/**
 * Represents an error returned by ArangoDB.
 */
class ArangoError extends Error {
    /**
     * @internal
     */
    constructor(response) {
        super();
        this.name = "ArangoError";
        this.response = response;
        this.message = response.body.errorMessage;
        this.errorNum = response.body.errorNum;
        this.code = response.body.code;
        const err = new Error(this.message);
        err.name = this.name;
        for (const key of nativeErrorKeys) {
            if (err[key])
                this[key] = err[key];
        }
    }
    /**
     * @internal
     *
     * Indicates that this object represents an ArangoDB error.
     */
    get isArangoError() {
        return true;
    }
    toJSON() {
        return {
            error: true,
            errorMessage: this.message,
            errorNum: this.errorNum,
            code: this.code,
        };
    }
}
/**
 * Represents a plain HTTP error response.
 */
class HttpError extends Error {
    /**
     * @internal
     */
    constructor(response) {
        super();
        this.name = "HttpError";
        this.response = response;
        this.code = response.statusCode || 500;
        this.message = messages[this.code] || messages[500];
        const err = new Error(this.message);
        err.name = this.name;
        for (const key of nativeErrorKeys) {
            if (err[key])
                this[key] = err[key];
        }
    }
    toJSON() {
        return {
            error: true,
            code: this.code,
        };
    }
}

/**
 * Error codes handled by arangojs.
 *
 * See also [ArangoDB error documentation](https://www.arangodb.com/docs/stable/appendix-error-codes.html).
 *
 * @packageDocumentation
 * @internal
 */
const TRANSACTION_NOT_FOUND = 10;
const ERROR_ARANGO_MAINTENANCE_MODE = 503;
const ERROR_ARANGO_CONFLICT = 1200;
const ANALYZER_NOT_FOUND = 1202;
const DOCUMENT_NOT_FOUND = 1202;
const COLLECTION_NOT_FOUND = 1203;
const VIEW_NOT_FOUND = 1203;
const DATABASE_NOT_FOUND = 1228;
const GRAPH_NOT_FOUND = 1924;

/**
 * Indicates whether the given value represents an {@link Analyzer}.
 *
 * @param analyzer - A value that might be an Analyzer.
 */
function isArangoAnalyzer(analyzer) {
    return Boolean(analyzer && analyzer.isArangoAnalyzer);
}
/**
 * Represents an Analyzer in a {@link database.Database}.
 */
class Analyzer {
    /**
     * @internal
     */
    constructor(db, name) {
        this._db = db;
        this._name = name.normalize("NFC");
    }
    /**
     * @internal
     *
     * Indicates that this object represents an ArangoDB Analyzer.
     */
    get isArangoAnalyzer() {
        return true;
    }
    /**
     * Name of this Analyzer.
     *
     * See also {@link database.Database}.
     */
    get name() {
        return this._name;
    }
    /**
     * Checks whether the Analyzer exists.
     *
     * @example
     * ```js
     * const db = new Database();
     * const analyzer = db.analyzer("some-analyzer");
     * const result = await analyzer.exists();
     * // result indicates whether the Analyzer exists
     * ```
     */
    async exists() {
        try {
            await this.get();
            return true;
        }
        catch (err) {
            if (isArangoError(err) && err.errorNum === ANALYZER_NOT_FOUND) {
                return false;
            }
            throw err;
        }
    }
    /**
     * Retrieves the Analyzer definition for the Analyzer.
     *
     * @example
     * ```js
     * const db = new Database();
     * const analyzer = db.analyzer("some-analyzer");
     * const definition = await analyzer.get();
     * // definition contains the Analyzer definition
     * ```
     */
    get() {
        return this._db.request({
            path: `/_api/analyzer/${encodeURIComponent(this._name)}`,
        });
    }
    /**
     * Creates a new Analyzer with the given `options` and the instance's name.
     *
     * See also {@link database.Database#createAnalyzer}.
     *
     * @param options - Options for creating the Analyzer.
     *
     * @example
     * ```js
     * const db = new Database();
     * const analyzer = db.analyzer("potatoes");
     * await analyzer.create({ type: "identity" });
     * // the identity Analyzer "potatoes" now exists
     * ```
     */
    create(options) {
        return this._db.request({
            method: "POST",
            path: "/_api/analyzer",
            body: { name: this._name, ...options },
        });
    }
    /**
     * Deletes the Analyzer from the database.
     *
     * @param force - Whether the Analyzer should still be deleted even if it
     * is currently in use.
     *
     * @example
     * ```js
     * const db = new Database();
     * const analyzer = db.analyzer("some-analyzer");
     * await analyzer.drop();
     * // the Analyzer "some-analyzer" no longer exists
     * ```
     */
    drop(force = false) {
        return this._db.request({
            method: "DELETE",
            path: `/_api/analyzer/${encodeURIComponent(this._name)}`,
            qs: { force },
        });
    }
}

var dist = {};

var LinkedList$1 = {};

var LinkedListItem$1 = {};

Object.defineProperty(LinkedListItem$1, "__esModule", { value: true });
/**
 * Represents an Item within LinkedList.
 * An item holds a value and the links to other LinkedListItem's
 * LinkedListItem's can only be attached behind.
 * Theirfor, to add one before, before has to add one behind.
 */
class LinkedListItem {
    /**
     * @param value Value to be held
     * @param unlinkCleanup Function to run on unlink() call. Usually used by LinkedList to fix first and last pointers and reduce length.
     */
    constructor(value, 
    /**
     *
     */
    unlinkCleanup) {
        this.value = value;
        this.unlinkCleanup = unlinkCleanup;
    }
    /**
     * This will link given LinkListItem behind this item.
     * If there's already a LinkedListItem linked behind, it will be relinked accordingly
     * @param item LinkListItem to be inserted behind this one
     */
    insertBehind(item) {
        item.insertBefore(this);
        if (this.behind) {
            let itemChainEnd = item;
            while (itemChainEnd.behind)
                itemChainEnd = itemChainEnd.behind;
            this.behind.insertBefore(itemChainEnd);
            itemChainEnd.insertBehind(this.behind);
        }
        this.behind = item;
    }
    /**
     * Unlinks this LinkedListItem and calls unlinkCleanup
     * @param unchain If true, additionally removes the reference to the item before and behind
     * @see LinkedListItem#unlinkCleanup
     */
    unlink(unchain = false) {
        if (this.before)
            this.before.behind = this.behind;
        if (this.behind) {
            this.behind.before = this.before;
        }
        if (this.unlinkCleanup) {
            this.unlinkCleanup(this);
        }
        this.unlinkCleanup = undefined;
        if (unchain) {
            this.before = this.behind = undefined;
        }
    }
    /**
     * Item given will be inserted before this item.
     * unlinkCleanup will be copied if neccessary.
     * This function is protected, because LinkedListItem's can only be attached behind.
     *
     * @param before
     * @see insertBehind
     */
    insertBefore(before) {
        this.before = before;
        if (!this.unlinkCleanup) {
            this.unlinkCleanup = before.unlinkCleanup;
        }
    }
}
LinkedListItem$1.LinkedListItem = LinkedListItem;

Object.defineProperty(LinkedList$1, "__esModule", { value: true });
const LinkedListItem_1 = LinkedListItem$1;
/**
 * Implements a linked list structure
 * @typeparam T Type of values within this LinkedList
 */
class LinkedList {
    /**
     * @param values Values to be added upfront into list
     */
    constructor(values) {
        /**
         * Current length of this LinkedList.
         * Note that this does not work anymore if you for some reason add your own LinkedListItems to LinkedList by hand
         */
        this.length = 0;
        /**
         * Given to own LinkedListItem's for following jobs regarding an unlink:
         * - If item is first item, set the next item as first item
         * - If item is last item, set the previous item as last item
         * - Decrease length
         * @param item Item that has been unlinked
         */
        this.unlinkCleanup = (item) => {
            if (this.first === item) {
                this.first = this.first.behind;
            }
            if (this.last === item) {
                this.last = this.last.before;
            }
            this.length--;
        };
        if (values) {
            if (values instanceof LinkedList)
                values = values.values();
            for (const value of values) {
                this.push(value);
            }
        }
    }
    /**
     * Clears this LinkedList.
     * The default complexity is O(1), because it only removes links to the first and last item and resets the length.
     * Note that if any LinkedListItem is still referenced outside the LinkedList, their before and behind fields might
     * still reference the chain, not freeing space.
     * You can set the unchain parameter to true, so every item in the linked list will be unchained,
     * meaning all references to before and behind items will be removed.
     * This increases complexity to O(n), but removes accidental outside references to the full chain.
     * @param unchain If `true`, remove link info from every item. Changes complexity to O(n)!
     */
    clear(unchain = false) {
        if (unchain) {
            while (this.first) {
                this.first.unlink(true);
            }
        }
        this.first = this.last = undefined;
        this.length = 0;
    }
    /**
     * As Array#every() given callback is called for every element until one call returns falsy or all elements had been processed
     * @returns `false` if there was a falsy response from the callback, `true` if all elements have been processed "falselesly"
     * @see Array#every
     */
    every(callback, thisArg) {
        if (thisArg) {
            callback = callback.bind(thisArg);
        }
        for (const item of this.keys()) {
            if (!callback(item.value, item, this)) {
                return false;
            }
        }
        return true;
    }
    /**
     * Filters values into a new LinkedList
     * @param callback decides wether given element should be part of new LinkedList
     * @see Array#filter
     */
    filter(callback, thisArg) {
        if (thisArg) {
            callback = callback.bind(thisArg);
        }
        const newList = new LinkedList();
        for (const [item, value] of this) {
            if (callback(value, item, this)) {
                newList.push(value);
            }
        }
        return newList;
    }
    /**
     * Returns value for which given callback returns truthy
     * @param callback runs for every value in LinkedList. If it returns truthy, current value is returned.
     * @see Array#find
     */
    find(callback, thisArg) {
        if (thisArg) {
            callback = callback.bind(thisArg);
        }
        for (const [item, value] of this) {
            if (callback(value, item, this)) {
                return value;
            }
        }
    }
    /**
     * Returns the LinkedListItem for which given callback returns truthy
     * @param callback runs for every LinkedListItem in LinkedList. If it returns truthy, current LinkedListItem is returned.
     * @see Array#findIndex
     */
    findItem(callback, thisArg) {
        if (thisArg) {
            callback = callback.bind(thisArg);
        }
        for (const [item, value] of this) {
            if (callback(value, item, this)) {
                return item;
            }
        }
    }
    /**
     * Iterates this LinkedList's items and values
     * @param callback Gets every value in LinkedList once with corresponding LinkedListItem and LinkedList
     * @param thisArg If given, callback will be bound here
     * @see Array#forEach
     */
    forEach(callback, thisArg) {
        if (thisArg) {
            callback = callback.bind(thisArg);
        }
        for (const [item, value] of this) {
            callback(value, item, this);
        }
    }
    /**
     * Checks if value can be found within LinkedList, starting from fromIndex, if given.
     * @param value value to be found in this
     * @param fromIndex Starting index. Supports negative values for which `this.size - 1 + fromIndex` will be used as starting point.
     * @returns true if value could be found in LinkedList (respecting fromIndex), false otherwhise
     * @see Array#includes
     */
    includes(value, fromIndex = 0) {
        let current = this.getItemByIndex(fromIndex);
        while (current) {
            if (current.value === value) {
                return true;
            }
            current = current.behind;
        }
        return false;
    }
    /**
     * Searches forward for given value and returns the first corresponding LinkedListItem found
     * @param searchedValue Value to be found
     * @param fromIndex Index to start from
     * @see Array#indexOf
     */
    itemOf(searchedValue, fromIndex = 0) {
        let current = this.getItemByIndex(fromIndex);
        while (current) {
            if (current.value === searchedValue) {
                return current;
            }
            current = current.behind;
        }
        return;
    }
    /**
     * Searches backwards for given value and returns the first corresponding LinkedListItem found
     * @param searchedValue Value to be found
     * @param fromIndex Index to start from
     * @see Array#indexOf
     */
    lastItemOf(searchedValue, fromIndex = -1) {
        let current = this.getItemByIndex(fromIndex);
        while (current) {
            if (current.value === searchedValue) {
                return current;
            }
            current = current.before;
        }
        return;
    }
    /**
     * Creates a new LinkedList with each of its itesm representing the output of the callback with each item in current LinkedList.
     * @param callback Gets value, LinkedListeItem and LinkedList. The response will be used as value in the new LinkedList
     * @param thisArg If given, callback is bound to thisArg
     * @see Array#map
     */
    map(callback, thisArg) {
        if (thisArg) {
            callback = callback.bind(thisArg);
        }
        const newList = new LinkedList();
        for (const [item, value] of this) {
            newList.push(callback(value, item, this));
        }
        return newList;
    }
    reduce(callback, initialValue) {
        let current = this.first;
        if (!current) {
            if (!initialValue) {
                throw new TypeError("Empty accumulator on empty LinkedList is not allowed.");
            }
            return initialValue;
        }
        if (initialValue === undefined) {
            initialValue = current.value;
            if (!current.behind) {
                return initialValue;
            }
            current = current.behind;
        }
        do {
            initialValue = callback(initialValue, current.value, current, this);
            current = current.behind;
        } while (current);
        return initialValue;
    }
    reduceRight(callback, initialValue) {
        let current = this.last;
        if (!current) {
            if (!initialValue) {
                throw new TypeError("Empty accumulator on empty LinkedList is not allowed.");
            }
            return initialValue;
        }
        // let accumulator: V | T;
        if (initialValue === undefined) {
            initialValue = current.value;
            if (!current.before) {
                return initialValue;
            }
            current = current.before;
        }
        do {
            initialValue = callback(initialValue, current.value, current, this);
            current = current.before;
        } while (current);
        return initialValue;
    }
    /**
     * Runs callback for every entry and returns true immediately if call of callback returns truthy.
     * @param callback called for every element. If response is truthy, iteration
     * @param thisArg If set, callback is bound to this
     * @returns `true` once a callback call returns truthy, `false` if none returned truthy.
     */
    some(callback, thisArg) {
        if (thisArg) {
            callback = callback.bind(thisArg);
        }
        for (const [item, value] of this) {
            if (callback(value, item, this)) {
                return true;
            }
        }
        return false;
    }
    /**
     * Joins values within this by given separator. Uses Array#join directly.
     * @param separator separator to be used
     * @see Array#join
     */
    join(separator) {
        return [...this.values()].join(separator);
    }
    /**
     * Concats given values and returns a new LinkedList with all given values.
     * If LinkedList's are given, they will be spread.
     * @param others Other values or lists to be concat'ed together
     * @see Array#concat
     */
    concat(...others) {
        const newList = new LinkedList(this);
        for (const other of others) {
            if (other instanceof LinkedList) {
                newList.push(...other.values());
            }
            else {
                newList.push(other);
            }
        }
        return newList;
    }
    /**
     * Removes the last LinkedListItem and returns its inner value
     */
    pop() {
        if (!this.last) {
            return;
        }
        const item = this.last;
        item.unlink();
        return item.value;
    }
    /**
     * Adds given values on the end of this LinkedList
     * @param values Values to be added
     */
    push(...values) {
        for (const value of values) {
            const item = new LinkedListItem_1.LinkedListItem(value, this.unlinkCleanup);
            if (!this.first || !this.last) {
                this.first = this.last = item;
            }
            else {
                this.last.insertBehind(item);
                this.last = item;
            }
            this.length++;
        }
        return this.length;
    }
    /**
     * Adds given values to the beginning of this LinkedList
     * @param values Values to be added
     */
    unshift(...values) {
        for (const value of values) {
            const item = new LinkedListItem_1.LinkedListItem(value, this.unlinkCleanup);
            if (!this.last || !this.first) {
                this.first = this.last = item;
            }
            else {
                item.insertBehind(this.first);
                this.first = item;
            }
            this.length++;
        }
        return this.length;
    }
    /**
     * Removes first occurrence of value found.
     * @param value value to remove from LinkedList
     */
    remove(value) {
        for (const item of this.keys()) {
            if (item.value === value) {
                item.unlink();
                return true;
            }
        }
        return false;
    }
    /**
     * Removes every occurrance of value within this.
     * @param value value to remove from LinkedList
     */
    removeAllOccurrences(value) {
        let foundSomethingToDelete = false;
        for (const item of this.keys()) {
            if (item.value === value) {
                item.unlink();
                foundSomethingToDelete = true;
            }
        }
        return foundSomethingToDelete;
    }
    /**
     * Returns and removes first element from LinkedList
     */
    shift() {
        if (!this.first) {
            return;
        }
        const item = this.first;
        item.unlink();
        return item.value;
    }
    /**
     * Returns LinkedListItem and value for every entry of this LinkedList
     */
    *[Symbol.iterator]() {
        let current = this.first;
        if (!current) {
            return;
        }
        do {
            yield [current, current.value];
            current = current.behind;
        } while (current);
    }
    /**
     * Returns LinkedListItem and value for every entry of this LinkedList
     * @see LinkedList#Symbol.iterator
     */
    entries() {
        return this[Symbol.iterator]();
    }
    /**
     * Iterates the LinkedListItem's of this LinkedList
     */
    *keys() {
        let current = this.first;
        if (!current) {
            return;
        }
        do {
            yield current;
            current = current.behind;
        } while (current);
    }
    /**
     * Returns a value for every entry of this LinkedList
     */
    *values() {
        let current = this.first;
        if (!current) {
            return;
        }
        do {
            yield current.value;
            current = current.behind;
        } while (current);
    }
    /**
     * Returns the item by given index.
     * Supports negative values and will return the item at `LinkedList.size - 1 + index` in that case.
     * @param index Index of item to get from list
     */
    getItemByIndex(index) {
        if (index === undefined) {
            throw new Error("index must be a number!");
        }
        if (!this.first) {
            return;
        }
        let current;
        if (index > 0) {
            current = this.first;
            while (current && index--) {
                current = current.behind;
            }
        }
        else if (index < 0) {
            current = this.last;
            while (current && ++index) {
                current = current.before;
            }
        }
        else {
            return this.first;
        }
        return current;
    }
}
LinkedList$1.LinkedList = LinkedList;

(function (exports) {
	function __export(m) {
	    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
	}
	Object.defineProperty(exports, "__esModule", { value: true });
	__export(LinkedList$1);
	__export(LinkedListItem$1);
	
} (dist));

/**
 * ```ts
 * import type { ArrayCursor, BatchedArrayCursor } from "arangojs/cursor";
 * ```
 *
 * The "cursor" module provides cursor-related interfaces for TypeScript.
 *
 * @packageDocumentation
 */
/**
 * The `BatchedArrayCursor` provides a batch-wise API to an {@link ArrayCursor}.
 *
 * When using TypeScript, cursors can be cast to a specific item type in order
 * to increase type safety.
 *
 * @param T - Type to use for each item. Defaults to `any`.
 *
 * @example
 * ```ts
 * const db = new Database();
 * const query = aql`FOR x IN 1..5 RETURN x`;
 * const cursor = await db.query(query) as ArrayCursor<number>;
 * const batches = cursor.batches;
 * ```
 *
 * @example
 * ```js
 * const db = new Database();
 * const query = aql`FOR x IN 1..10000 RETURN x`;
 * const cursor = await db.query(query, { batchSize: 10 });
 * for await (const batch of cursor.batches) {
 *   // Process all values in a batch in parallel
 *   await Promise.all(batch.map(
 *     value => asyncProcessValue(value)
 *   ));
 * }
 * ```
 */
class BatchedArrayCursor {
    /**
     * @internal
     */
    constructor(db, body, hostUrl, allowDirtyRead) {
        const batches = new dist.LinkedList(body.result.length ? [new dist.LinkedList(body.result)] : []);
        this._db = db;
        this._batches = batches;
        this._id = body.id;
        this._hasMore = Boolean(body.id && body.hasMore);
        this._hostUrl = hostUrl;
        this._count = body.count;
        this._extra = body.extra;
        this._allowDirtyRead = allowDirtyRead;
        this._itemsCursor = new ArrayCursor(this, {
            get isEmpty() {
                return !batches.length;
            },
            more: () => this._more(),
            shift: () => {
                let batch = batches.first?.value;
                while (batch && !batch.length) {
                    batches.shift();
                    batch = batches.first?.value;
                }
                if (!batch)
                    return undefined;
                const value = batch.shift();
                if (!batch.length)
                    batches.shift();
                return value;
            },
        });
    }
    async _more() {
        if (!this.hasMore)
            return;
        const body = await this._db.request({
            method: "PUT",
            path: `/_api/cursor/${encodeURIComponent(this._id)}`,
            hostUrl: this._hostUrl,
            allowDirtyRead: this._allowDirtyRead,
        });
        this._batches.push(new dist.LinkedList(body.result));
        this._hasMore = body.hasMore;
    }
    /**
     * An {@link ArrayCursor} providing item-wise access to the cursor result set.
     *
     * See also {@link ArrayCursor#batches}.
     */
    get items() {
        return this._itemsCursor;
    }
    /**
     * Additional information about the cursor.
     */
    get extra() {
        return this._extra;
    }
    /**
     * Total number of documents in the query result. Only available if the
     * `count` option was used.
     */
    get count() {
        return this._count;
    }
    /**
     * Whether the cursor has any remaining batches that haven't yet been
     * fetched. If set to `false`, all batches have been fetched and no
     * additional requests to the server will be made when consuming any
     * remaining batches from this cursor.
     */
    get hasMore() {
        return this._hasMore;
    }
    /**
     * Whether the cursor has more batches. If set to `false`, the cursor has
     * already been depleted and contains no more batches.
     */
    get hasNext() {
        return this.hasMore || Boolean(this._batches.length);
    }
    /**
     * Enables use with `for await` to deplete the cursor by asynchronously
     * yielding every batch in the cursor's remaining result set.
     *
     * **Note**: If the result set spans multiple batches, any remaining batches
     * will only be fetched on demand. Depending on the cursor's TTL and the
     * processing speed, this may result in the server discarding the cursor
     * before it is fully depleted.
     *
     * @example
     * ```js
     * const cursor = await db.query(aql`
     *   FOR user IN users
     *   FILTER user.isActive
     *   RETURN user
     * `);
     * for await (const users of cursor.batches) {
     *   for (const user of users) {
     *     console.log(user.email, user.isAdmin);
     *   }
     * }
     * ```
     */
    async *[Symbol.asyncIterator]() {
        while (this.hasNext) {
            yield this.next();
        }
        return undefined;
    }
    /**
     * Loads all remaining batches from the server.
     *
     * **Warning**: This may impact memory use when working with very large
     * query result sets.
     *
     * @example
     * ```js
     * const cursor = await db.query(
     *   aql`FOR x IN 1..5 RETURN x`,
     *   { batchSize: 1 }
     * );
     * console.log(cursor.hasMore); // true
     * await cursor.batches.loadAll();
     * console.log(cursor.hasMore); // false
     * console.log(cursor.hasNext); // true
     * for await (const item of cursor) {
     *   console.log(item);
     *   // No server roundtrips necessary any more
     * }
     * ```
     */
    async loadAll() {
        while (this._hasMore) {
            await this._more();
        }
    }
    /**
     * Depletes the cursor, then returns an array containing all batches in the
     * cursor's remaining result list.
     *
     * @example
     * ```js
     * const cursor = await db.query(
     *   aql`FOR x IN 1..5 RETURN x`,
     *   { batchSize: 2 }
     * );
     * const result = await cursor.batches.all(); // [[1, 2], [3, 4], [5]]
     * console.log(cursor.hasNext); // false
     * ```
     */
    async all() {
        return this.map((batch) => batch);
    }
    /**
     * Advances the cursor and returns all remaining values in the cursor's
     * current batch. If the current batch has already been exhausted, fetches
     * the next batch from the server and returns it, or `undefined` if the
     * cursor has been depleted.
     *
     * **Note**: If the result set spans multiple batches, any remaining batches
     * will only be fetched on demand. Depending on the cursor's TTL and the
     * processing speed, this may result in the server discarding the cursor
     * before it is fully depleted.
     *
     * @example
     * ```js
     * const cursor = await db.query(
     *   aql`FOR i IN 1..10 RETURN i`,
     *   { batchSize: 5 }
     * );
     * const firstBatch = await cursor.batches.next(); // [1, 2, 3, 4, 5]
     * await cursor.next(); // 6
     * const lastBatch = await cursor.batches.next(); // [7, 8, 9, 10]
     * console.log(cursor.hasNext); // false
     * ```
     */
    async next() {
        while (!this._batches.length && this.hasNext) {
            await this._more();
        }
        if (!this._batches.length) {
            return undefined;
        }
        const batch = this._batches.shift();
        if (!batch)
            return undefined;
        const values = [...batch.values()];
        batch.clear(true);
        return values;
    }
    /**
     * Advances the cursor by applying the `callback` function to each item in
     * the cursor's remaining result list until the cursor is depleted or
     * `callback` returns the exact value `false`. Returns a promise that
     * evalues to `true` unless the function returned `false`.
     *
     * **Note**: If the result set spans multiple batches, any remaining batches
     * will only be fetched on demand. Depending on the cursor's TTL and the
     * processing speed, this may result in the server discarding the cursor
     * before it is fully depleted.
     *
     * See also:
     * [`Array.prototype.forEach`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach).
     *
     * @param callback - Function to execute on each element.
     *
     * @example
     * ```js
     * const cursor = await db.query(
     *   aql`FOR x IN 1..5 RETURN x`,
     *   { batchSize: 2 }
     * );
     * const result = await cursor.batches.forEach((currentBatch) => {
     *   for (const value of currentBatch) {
     *     console.log(value);
     *   }
     * });
     * console.log(result) // true
     * console.log(cursor.hasNext); // false
     * ```
     *
     * @example
     * ```js
     * const cursor = await db.query(
     *   aql`FOR x IN 1..5 RETURN x`,
     *   { batchSize: 2 }
     * );
     * const result = await cursor.batches.forEach((currentBatch) => {
     *   for (const value of currentBatch) {
     *     console.log(value);
     *   }
     *   return false; // stop after the first batch
     * });
     * console.log(result); // false
     * console.log(cursor.hasNext); // true
     * ```
     */
    async forEach(callback) {
        let index = 0;
        while (this.hasNext) {
            const currentBatch = await this.next();
            const result = callback(currentBatch, index, this);
            index++;
            if (result === false)
                return result;
            if (this.hasNext)
                await this._more();
        }
        return true;
    }
    /**
     * Depletes the cursor by applying the `callback` function to each batch in
     * the cursor's remaining result list. Returns an array containing the
     * return values of `callback` for each batch.
     *
     * **Note**: This creates an array of all return values, which may impact
     * memory use when working with very large query result sets. Consider using
     * {@link BatchedArrayCursor#forEach}, {@link BatchedArrayCursor#reduce} or
     * {@link BatchedArrayCursor#flatMap} instead.
     *
     * See also:
     * [`Array.prototype.map`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map).
     *
     * @param R - Return type of the `callback` function.
     * @param callback - Function to execute on each element.
     *
     * @example
     * ```js
     * const cursor = await db.query(
     *   aql`FOR x IN 1..5 RETURN x`,
     *   { batchSize: 2 }
     * );
     * const squares = await cursor.batches.map((currentBatch) => {
     *   return currentBatch.map((value) => value ** 2);
     * });
     * console.log(squares); // [[1, 4], [9, 16], [25]]
     * console.log(cursor.hasNext); // false
     * ```
     */
    async map(callback) {
        let index = 0;
        const result = [];
        while (this.hasNext) {
            const currentBatch = await this.next();
            result.push(callback(currentBatch, index, this));
            index++;
        }
        return result;
    }
    /**
     * Depletes the cursor by applying the `callback` function to each batch in
     * the cursor's remaining result list. Returns an array containing the
     * return values of `callback` for each batch, flattened to a depth of 1.
     *
     * **Note**: If the result set spans multiple batches, any remaining batches
     * will only be fetched on demand. Depending on the cursor's TTL and the
     * processing speed, this may result in the server discarding the cursor
     * before it is fully depleted.
     *
     * See also:
     * [`Array.prototype.flatMap`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/flatMap).
     *
     * @param R - Return type of the `callback` function.
     * @param callback - Function to execute on each element.
     *
     * @example
     * ```js
     * const cursor = await db.query(
     *   aql`FOR x IN 1..5 RETURN x`,
     *   { batchSize: 2 }
     * );
     * const squares = await cursor.batches.flatMap((currentBatch) => {
     *   return currentBatch.map((value) => value ** 2);
     * });
     * console.log(squares); // [1, 1, 2, 4, 3, 9, 4, 16, 5, 25]
     * console.log(cursor.hasNext); // false
     * ```
     *
     * @example
     * ```js
     * const cursor = await db.query(
     *   aql`FOR x IN 1..5 RETURN x`,
     *   { batchSize: 1 }
     * );
     * const odds = await cursor.batches.flatMap((currentBatch) => {
     *   if (currentBatch[0] % 2 === 0) {
     *     return []; // empty array flattens into nothing
     *   }
     *   return currentBatch;
     * });
     * console.logs(odds); // [1, 3, 5]
     * ```
     */
    async flatMap(callback) {
        let index = 0;
        const result = [];
        while (this.hasNext) {
            const currentBatch = await this.next();
            const value = callback(currentBatch, index, this);
            if (Array.isArray(value)) {
                result.push(...value);
            }
            else {
                result.push(value);
            }
            index++;
        }
        return result;
    }
    async reduce(reducer, initialValue) {
        let index = 0;
        if (!this.hasNext)
            return initialValue;
        if (initialValue === undefined) {
            initialValue = (await this.next());
            index += 1;
        }
        let value = initialValue;
        while (this.hasNext) {
            const currentBatch = await this.next();
            value = reducer(value, currentBatch, index, this);
            index++;
        }
        return value;
    }
    /**
     * Drains the cursor and frees up associated database resources.
     *
     * This method has no effect if all batches have already been consumed.
     *
     * @example
     * ```js
     * const cursor1 = await db.query(aql`FOR x IN 1..5 RETURN x`);
     * console.log(cursor1.hasMore); // false
     * await cursor1.kill(); // no effect
     *
     * const cursor2 = await db.query(
     *   aql`FOR x IN 1..5 RETURN x`,
     *   { batchSize: 2 }
     * );
     * console.log(cursor2.hasMore); // true
     * await cursor2.kill(); // cursor is depleted
     * ```
     */
    async kill() {
        if (this._batches.length) {
            for (const batch of this._batches.values()) {
                batch.clear();
            }
            this._batches.clear();
        }
        if (!this.hasNext)
            return undefined;
        return this._db.request({
            method: "DELETE",
            path: `/_api/cursor/${encodeURIComponent(this._id)}`,
        }, () => {
            this._hasMore = false;
            return undefined;
        });
    }
}
/**
 * The `ArrayCursor` type represents a cursor returned from a
 * {@link database.Database#query}.
 *
 * When using TypeScript, cursors can be cast to a specific item type in order
 * to increase type safety.
 *
 * See also {@link BatchedArrayCursor}.
 *
 * @param T - Type to use for each item. Defaults to `any`.
 *
 * @example
 * ```ts
 * const db = new Database();
 * const query = aql`FOR x IN 1..5 RETURN x`;
 * const result = await db.query(query) as ArrayCursor<number>;
 * ```
 *
 * @example
 * ```js
 * const db = new Database();
 * const query = aql`FOR x IN 1..10 RETURN x`;
 * const cursor = await db.query(query);
 * for await (const value of cursor) {
 *   // Process each value asynchronously
 *   await processValue(value);
 * }
 * ```
 */
class ArrayCursor {
    /**
     * @internal
     */
    constructor(batchedCursor, view) {
        this._batches = batchedCursor;
        this._view = view;
    }
    /**
     * A {@link BatchedArrayCursor} providing batch-wise access to the cursor
     * result set.
     *
     * See also {@link BatchedArrayCursor#items}.
     */
    get batches() {
        return this._batches;
    }
    /**
     * Additional information about the cursor.
     */
    get extra() {
        return this.batches.extra;
    }
    /**
     * Total number of documents in the query result. Only available if the
     * `count` option was used.
     */
    get count() {
        return this.batches.count;
    }
    /**
     * Whether the cursor has more values. If set to `false`, the cursor has
     * already been depleted and contains no more items.
     */
    get hasNext() {
        return this.batches.hasNext;
    }
    /**
     * Enables use with `for await` to deplete the cursor by asynchronously
     * yielding every value in the cursor's remaining result set.
     *
     * **Note**: If the result set spans multiple batches, any remaining batches
     * will only be fetched on demand. Depending on the cursor's TTL and the
     * processing speed, this may result in the server discarding the cursor
     * before it is fully depleted.
     *
     * @example
     * ```js
     * const cursor = await db.query(aql`
     *   FOR user IN users
     *   FILTER user.isActive
     *   RETURN user
     * `);
     * for await (const user of cursor) {
     *   console.log(user.email, user.isAdmin);
     * }
     * ```
     */
    async *[Symbol.asyncIterator]() {
        while (this.hasNext) {
            yield this.next();
        }
        return undefined;
    }
    /**
     * Depletes the cursor, then returns an array containing all values in the
     * cursor's remaining result list.
     *
     * @example
     * ```js
     * const cursor = await db.query(aql`FOR x IN 1..5 RETURN x`);
     * const result = await cursor.all(); // [1, 2, 3, 4, 5]
     * console.log(cursor.hasNext); // false
     * ```
     */
    async all() {
        return this.batches.flatMap((v) => v);
    }
    /**
     * Advances the cursor and returns the next value in the cursor's remaining
     * result list, or `undefined` if the cursor has been depleted.
     *
     * **Note**: If the result set spans multiple batches, any remaining batches
     * will only be fetched on demand. Depending on the cursor's TTL and the
     * processing speed, this may result in the server discarding the cursor
     * before it is fully depleted.
     *
     * @example
     * ```js
     * const cursor = await db.query(aql`FOR x IN 1..3 RETURN x`);
     * const one = await cursor.next(); // 1
     * const two = await cursor.next(); // 2
     * const three = await cursor.next(); // 3
     * const empty = await cursor.next(); // undefined
     * ```
     */
    async next() {
        while (this._view.isEmpty && this.batches.hasMore) {
            await this._view.more();
        }
        if (this._view.isEmpty) {
            return undefined;
        }
        return this._view.shift();
    }
    /**
     * Advances the cursor by applying the `callback` function to each item in
     * the cursor's remaining result list until the cursor is depleted or
     * `callback` returns the exact value `false`. Returns a promise that
     * evalues to `true` unless the function returned `false`.
     *
     * **Note**: If the result set spans multiple batches, any remaining batches
     * will only be fetched on demand. Depending on the cursor's TTL and the
     * processing speed, this may result in the server discarding the cursor
     * before it is fully depleted.
     *
     * See also:
     * [`Array.prototype.forEach`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach).
     *
     * @param callback - Function to execute on each element.
     *
     * @example
     * ```js
     * const cursor = await db.query(aql`FOR x IN 1..5 RETURN x`);
     * const result = await cursor.forEach((currentValue) => {
     *   console.log(currentValue);
     * });
     * console.log(result) // true
     * console.log(cursor.hasNext); // false
     * ```
     *
     * @example
     * ```js
     * const cursor = await db.query(aql`FOR x IN 1..5 RETURN x`);
     * const result = await cursor.forEach((currentValue) => {
     *   console.log(currentValue);
     *   return false; // stop after the first item
     * });
     * console.log(result); // false
     * console.log(cursor.hasNext); // true
     * ```
     */
    async forEach(callback) {
        let index = 0;
        while (this.hasNext) {
            const value = await this.next();
            const result = callback(value, index, this);
            index++;
            if (result === false)
                return result;
        }
        return true;
    }
    /**
     * Depletes the cursor by applying the `callback` function to each item in
     * the cursor's remaining result list. Returns an array containing the
     * return values of `callback` for each item.
     *
     * **Note**: This creates an array of all return values, which may impact
     * memory use when working with very large query result sets. Consider using
     * {@link ArrayCursor#forEach}, {@link ArrayCursor#reduce} or
     * {@link ArrayCursor#flatMap} instead.
     *
     * See also:
     * [`Array.prototype.map`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map).
     *
     * @param R - Return type of the `callback` function.
     * @param callback - Function to execute on each element.
     *
     * @example
     * ```js
     * const cursor = await db.query(aql`FOR x IN 1..5 RETURN x`);
     * const squares = await cursor.map((currentValue) => {
     *   return currentValue ** 2;
     * });
     * console.log(squares); // [1, 4, 9, 16, 25]
     * console.log(cursor.hasNext); // false
     * ```
     */
    async map(callback) {
        let index = 0;
        const result = [];
        while (this.hasNext) {
            const value = await this.next();
            result.push(callback(value, index, this));
            index++;
        }
        return result;
    }
    /**
     * Depletes the cursor by applying the `callback` function to each item in
     * the cursor's remaining result list. Returns an array containing the
     * return values of `callback` for each item, flattened to a depth of 1.
     *
     * **Note**: If the result set spans multiple batches, any remaining batches
     * will only be fetched on demand. Depending on the cursor's TTL and the
     * processing speed, this may result in the server discarding the cursor
     * before it is fully depleted.
     *
     * See also:
     * [`Array.prototype.flatMap`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/flatMap).
     *
     * @param R - Return type of the `callback` function.
     * @param callback - Function to execute on each element.
     *
     * @example
     * ```js
     * const cursor = await db.query(aql`FOR x IN 1..5 RETURN x`);
     * const squares = await cursor.flatMap((currentValue) => {
     *   return [currentValue, currentValue ** 2];
     * });
     * console.log(squares); // [1, 1, 2, 4, 3, 9, 4, 16, 5, 25]
     * console.log(cursor.hasNext); // false
     * ```
     *
     * @example
     * ```js
     * const cursor = await db.query(aql`FOR x IN 1..5 RETURN x`);
     * const odds = await cursor.flatMap((currentValue) => {
     *   if (currentValue % 2 === 0) {
     *     return []; // empty array flattens into nothing
     *   }
     *   return currentValue; // or [currentValue]
     * });
     * console.logs(odds); // [1, 3, 5]
     * ```
     */
    async flatMap(callback) {
        let index = 0;
        const result = [];
        while (this.hasNext) {
            const value = await this.next();
            const item = callback(value, index, this);
            if (Array.isArray(item)) {
                result.push(...item);
            }
            else {
                result.push(item);
            }
            index++;
        }
        return result;
    }
    async reduce(reducer, initialValue) {
        let index = 0;
        if (!this.hasNext)
            return initialValue;
        if (initialValue === undefined) {
            const value = (await this.next());
            initialValue = value;
            index += 1;
        }
        let value = initialValue;
        while (this.hasNext) {
            const item = await this.next();
            value = reducer(value, item, index, this);
            index++;
        }
        return value;
    }
    /**
     * Kills the cursor and frees up associated database resources.
     *
     * This method has no effect if all batches have already been fetched.
     *
     * @example
     * ```js
     * const cursor1 = await db.query(aql`FOR x IN 1..5 RETURN x`);
     * console.log(cursor1.hasMore); // false
     * await cursor1.kill(); // no effect
     *
     * const cursor2 = await db.query(
     *   aql`FOR x IN 1..5 RETURN x`,
     *   { batchSize: 2 }
     * );
     * console.log(cursor2.hasMore); // true
     * await cursor2.kill(); // cursor is depleted
     * ```
     */
    async kill() {
        return this.batches.kill();
    }
}

/**
 * ```ts
 * import type { Document, Edge } from "arangojs/documents";
 * ```
 *
 * The "documents" module provides document/edge related types for TypeScript.
 *
 * @packageDocumentation
 */
/**
 * @internal
 */
function _documentHandle(selector, collectionName, strict = true) {
    if (typeof selector !== "string") {
        if (selector._id) {
            return _documentHandle(selector._id, collectionName);
        }
        if (selector._key) {
            return _documentHandle(selector._key, collectionName);
        }
        throw new Error("Document handle must be a string or an object with a _key or _id attribute");
    }
    if (selector.includes("/")) {
        const [head, ...tail] = selector.split("/");
        const normalizedHead = head.normalize("NFC");
        if (strict && normalizedHead !== collectionName) {
            throw new Error(`Document ID "${selector}" does not match collection name "${collectionName}"`);
        }
        return [normalizedHead, ...tail].join("/");
    }
    return `${collectionName}/${selector}`;
}

/**
 * ```ts
 * import type {
 *   FulltextIndex,
 *   GeoIndex,
 *   PersistentIndex,
 *   PrimaryIndex,
 *   TtlIndex,
 *   ZkdIndex,
 * } from "arangojs/indexes";
 * ```
 *
 * The "indexes" module provides index-related types for TypeScript.
 *
 * @packageDocumentation
 */
/**
 * @internal
 */
function _indexHandle(selector, collectionName) {
    if (typeof selector !== "string") {
        if (selector.id) {
            return _indexHandle(selector.id, collectionName);
        }
        throw new Error("Index handle must be a string or an object with an id attribute");
    }
    if (selector.includes("/")) {
        const [head, ...tail] = selector.split("/");
        const normalizedHead = head.normalize("NFC");
        if (normalizedHead !== collectionName) {
            throw new Error(`Index ID "${selector}" does not match collection name "${collectionName}"`);
        }
        selector = tail.join("/").normalize("NFC");
        return [normalizedHead, selector].join("/");
    }
    return `${collectionName}/${String(selector).normalize("NFC")}`;
}

/**
 * ```ts
 * import type {
 *   DocumentCollection,
 *   EdgeCollection,
 * } from "arangojs/collection";
 * ```
 *
 * The "collection" module provides collection related types and interfaces
 * for TypeScript.
 *
 * @packageDocumentation
 */
/**
 * Indicates whether the given value represents an {@link ArangoCollection}.
 *
 * @param collection - A value that might be a collection.
 */
function isArangoCollection(collection) {
    return Boolean(collection && collection.isArangoCollection);
}
/**
 * Coerces the given collection name or {@link ArangoCollection} object to
 * a string representing the collection name.
 *
 * @param collection - Collection name or {@link ArangoCollection} object.
 */
function collectionToString(collection) {
    if (isArangoCollection(collection)) {
        return String(collection.name);
    }
    else
        return String(collection).normalize("NFC");
}
/**
 * Integer values indicating the collection type.
 */
var CollectionType;
(function (CollectionType) {
    CollectionType[CollectionType["DOCUMENT_COLLECTION"] = 2] = "DOCUMENT_COLLECTION";
    CollectionType[CollectionType["EDGE_COLLECTION"] = 3] = "EDGE_COLLECTION";
})(CollectionType || (CollectionType = {}));
/**
 * Integer values indicating the collection loading status.
 */
var CollectionStatus;
(function (CollectionStatus) {
    CollectionStatus[CollectionStatus["NEWBORN"] = 1] = "NEWBORN";
    CollectionStatus[CollectionStatus["UNLOADED"] = 2] = "UNLOADED";
    CollectionStatus[CollectionStatus["LOADED"] = 3] = "LOADED";
    CollectionStatus[CollectionStatus["UNLOADING"] = 4] = "UNLOADING";
    CollectionStatus[CollectionStatus["DELETED"] = 5] = "DELETED";
    CollectionStatus[CollectionStatus["LOADING"] = 6] = "LOADING";
})(CollectionStatus || (CollectionStatus = {}));
/**
 * @internal
 */
class Collection {
    //#endregion
    /**
     * @internal
     */
    constructor(db, name) {
        this._name = name.normalize("NFC");
        this._db = db;
    }
    //#region metadata
    get isArangoCollection() {
        return true;
    }
    get name() {
        return this._name;
    }
    get() {
        return this._db.request({
            path: `/_api/collection/${encodeURIComponent(this._name)}`,
        });
    }
    async exists() {
        try {
            await this.get();
            return true;
        }
        catch (err) {
            if (isArangoError(err) && err.errorNum === COLLECTION_NOT_FOUND) {
                return false;
            }
            throw err;
        }
    }
    create(options = {}) {
        const { waitForSyncReplication = undefined, enforceReplicationFactor = undefined, ...opts } = options;
        if (opts.computedValues) {
            opts.computedValues = opts.computedValues.map((computedValue) => {
                if (isAqlLiteral(computedValue.expression)) {
                    return {
                        ...computedValue,
                        expression: computedValue.expression.toAQL(),
                    };
                }
                if (isAqlQuery(computedValue.expression)) {
                    return {
                        ...computedValue,
                        expression: computedValue.expression.query,
                    };
                }
                return computedValue;
            });
        }
        const qs = {};
        if (typeof waitForSyncReplication === "boolean") {
            qs.waitForSyncReplication = waitForSyncReplication ? 1 : 0;
        }
        if (typeof enforceReplicationFactor === "boolean") {
            qs.enforceReplicationFactor = enforceReplicationFactor ? 1 : 0;
        }
        return this._db.request({
            method: "POST",
            path: "/_api/collection",
            qs,
            body: {
                ...opts,
                name: this._name,
            },
        });
    }
    properties(properties) {
        if (!properties) {
            return this._db.request({
                path: `/_api/collection/${encodeURIComponent(this._name)}/properties`,
            });
        }
        return this._db.request({
            method: "PUT",
            path: `/_api/collection/${encodeURIComponent(this._name)}/properties`,
            body: properties,
        });
    }
    count() {
        return this._db.request({
            path: `/_api/collection/${encodeURIComponent(this._name)}/count`,
        });
    }
    async recalculateCount() {
        return this._db.request({
            method: "PUT",
            path: `/_api/collection/${encodeURIComponent(this._name)}/recalculateCount`,
        }, (res) => res.body.result);
    }
    figures(details = false) {
        return this._db.request({
            path: `/_api/collection/${encodeURIComponent(this._name)}/figures`,
            qs: { details },
        });
    }
    revision() {
        return this._db.request({
            path: `/_api/collection/${encodeURIComponent(this._name)}/revision`,
        });
    }
    checksum(options) {
        return this._db.request({
            path: `/_api/collection/${encodeURIComponent(this._name)}/checksum`,
            qs: options,
        });
    }
    async loadIndexes() {
        return this._db.request({
            method: "PUT",
            path: `/_api/collection/${encodeURIComponent(this._name)}/loadIndexesIntoMemory`,
        }, (res) => res.body.result);
    }
    async rename(newName) {
        const result = await this._db.renameCollection(this._name, newName);
        this._name = newName.normalize("NFC");
        return result;
    }
    truncate() {
        return this._db.request({
            method: "PUT",
            path: `/_api/collection/${this._name}/truncate`,
        });
    }
    drop(options) {
        return this._db.request({
            method: "DELETE",
            path: `/_api/collection/${encodeURIComponent(this._name)}`,
            qs: options,
        });
    }
    //#endregion
    //#region crud
    getResponsibleShard(document) {
        return this._db.request({
            method: "PUT",
            path: `/_api/collection/${encodeURIComponent(this._name)}/responsibleShard`,
            body: document,
        }, (res) => res.body.shardId);
    }
    documentId(selector) {
        return _documentHandle(selector, this._name);
    }
    async documentExists(selector, options = {}) {
        const { ifMatch = undefined, ifNoneMatch = undefined } = options;
        const headers = {};
        if (ifMatch)
            headers["if-match"] = ifMatch;
        if (ifNoneMatch)
            headers["if-none-match"] = ifNoneMatch;
        try {
            return await this._db.request({
                method: "HEAD",
                path: `/_api/document/${encodeURI(_documentHandle(selector, this._name))}`,
                headers,
            }, (res) => {
                if (ifNoneMatch && res.statusCode === 304) {
                    throw new HttpError(res);
                }
                return true;
            });
        }
        catch (err) {
            if (err.code === 404) {
                return false;
            }
            throw err;
        }
    }
    documents(selectors, options = {}) {
        const { allowDirtyRead = undefined } = options;
        return this._db.request({
            method: "PUT",
            path: `/_api/document/${encodeURIComponent(this._name)}`,
            qs: { onlyget: true },
            allowDirtyRead,
            body: selectors,
        });
    }
    async document(selector, options = {}) {
        if (typeof options === "boolean") {
            options = { graceful: options };
        }
        const { allowDirtyRead = undefined, graceful = false, ifMatch = undefined, ifNoneMatch = undefined, } = options;
        const headers = {};
        if (ifMatch)
            headers["if-match"] = ifMatch;
        if (ifNoneMatch)
            headers["if-none-match"] = ifNoneMatch;
        const result = this._db.request({
            path: `/_api/document/${encodeURI(_documentHandle(selector, this._name))}`,
            headers,
            allowDirtyRead,
        }, (res) => {
            if (ifNoneMatch && res.statusCode === 304) {
                throw new HttpError(res);
            }
            return res.body;
        });
        if (!graceful)
            return result;
        try {
            return await result;
        }
        catch (err) {
            if (isArangoError(err) && err.errorNum === DOCUMENT_NOT_FOUND) {
                return null;
            }
            throw err;
        }
    }
    save(data, options) {
        return this._db.request({
            method: "POST",
            path: `/_api/document/${encodeURIComponent(this._name)}`,
            body: data,
            qs: options,
        }, (res) => (options?.silent ? undefined : res.body));
    }
    saveAll(data, options) {
        return this._db.request({
            method: "POST",
            path: `/_api/document/${encodeURIComponent(this._name)}`,
            body: data,
            qs: options,
        }, (res) => (options?.silent ? undefined : res.body));
    }
    replace(selector, newData, options = {}) {
        const { ifMatch = undefined, ...opts } = options;
        const headers = {};
        if (ifMatch)
            headers["if-match"] = ifMatch;
        return this._db.request({
            method: "PUT",
            path: `/_api/document/${encodeURI(_documentHandle(selector, this._name))}`,
            headers,
            body: newData,
            qs: opts,
        }, (res) => (options?.silent ? undefined : res.body));
    }
    replaceAll(newData, options) {
        return this._db.request({
            method: "PUT",
            path: `/_api/document/${encodeURIComponent(this._name)}`,
            body: newData,
            qs: options,
        }, (res) => (options?.silent ? undefined : res.body));
    }
    update(selector, newData, options = {}) {
        const { ifMatch = undefined, ...opts } = options;
        const headers = {};
        if (ifMatch)
            headers["if-match"] = ifMatch;
        return this._db.request({
            method: "PATCH",
            path: `/_api/document/${encodeURI(_documentHandle(selector, this._name))}`,
            headers,
            body: newData,
            qs: opts,
        }, (res) => (options?.silent ? undefined : res.body));
    }
    updateAll(newData, options) {
        return this._db.request({
            method: "PATCH",
            path: `/_api/document/${encodeURIComponent(this._name)}`,
            body: newData,
            qs: options,
        }, (res) => (options?.silent ? undefined : res.body));
    }
    remove(selector, options = {}) {
        const { ifMatch = undefined, ...opts } = options;
        const headers = {};
        if (ifMatch)
            headers["if-match"] = ifMatch;
        return this._db.request({
            method: "DELETE",
            path: `/_api/document/${encodeURI(_documentHandle(selector, this._name))}`,
            headers,
            qs: opts,
        }, (res) => (options?.silent ? undefined : res.body));
    }
    removeAll(selectors, options) {
        return this._db.request({
            method: "DELETE",
            path: `/_api/document/${encodeURIComponent(this._name)}`,
            body: selectors,
            qs: options,
        }, (res) => (options?.silent ? undefined : res.body));
    }
    import(data, options = {}) {
        const qs = { ...options, collection: this._name };
        if (Array.isArray(data)) {
            qs.type = Array.isArray(data[0]) ? undefined : "documents";
            const lines = data;
            data = lines.map((line) => JSON.stringify(line)).join("\r\n") + "\r\n";
        }
        return this._db.request({
            method: "POST",
            path: "/_api/import",
            body: data,
            isBinary: true,
            qs,
        });
    }
    //#endregion
    //#region edges
    _edges(selector, options, direction) {
        const { allowDirtyRead = undefined } = options;
        return this._db.request({
            path: `/_api/edges/${encodeURIComponent(this._name)}`,
            allowDirtyRead,
            qs: {
                direction,
                vertex: _documentHandle(selector, this._name, false),
            },
        });
    }
    edges(vertex, options) {
        return this._edges(vertex, options);
    }
    inEdges(vertex, options) {
        return this._edges(vertex, options, "in");
    }
    outEdges(vertex, options) {
        return this._edges(vertex, options, "out");
    }
    traversal(startVertex, options) {
        return this._db.request({
            method: "POST",
            path: "/_api/traversal",
            body: {
                ...options,
                startVertex,
                edgeCollection: this._name,
            },
        }, (res) => res.body.result);
    }
    //#endregion
    //#region simple queries
    list(type = "id") {
        return this._db.request({
            method: "PUT",
            path: "/_api/simple/all-keys",
            body: { type, collection: this._name },
        }, (res) => new BatchedArrayCursor(this._db, res.body, res.arangojsHostUrl).items);
    }
    all(options) {
        return this._db.request({
            method: "PUT",
            path: "/_api/simple/all",
            body: {
                ...options,
                collection: this._name,
            },
        }, (res) => new BatchedArrayCursor(this._db, res.body, res.arangojsHostUrl).items);
    }
    any() {
        return this._db.request({
            method: "PUT",
            path: "/_api/simple/any",
            body: { collection: this._name },
        }, (res) => res.body.document);
    }
    byExample(example, options) {
        return this._db.request({
            method: "PUT",
            path: "/_api/simple/by-example",
            body: {
                ...options,
                example,
                collection: this._name,
            },
        }, (res) => new BatchedArrayCursor(this._db, res.body, res.arangojsHostUrl).items);
    }
    firstExample(example) {
        return this._db.request({
            method: "PUT",
            path: "/_api/simple/first-example",
            body: {
                example,
                collection: this._name,
            },
        }, (res) => res.body.document);
    }
    removeByExample(example, options) {
        return this._db.request({
            method: "PUT",
            path: "/_api/simple/remove-by-example",
            body: {
                ...options,
                example,
                collection: this._name,
            },
        });
    }
    replaceByExample(example, newValue, options) {
        return this._db.request({
            method: "PUT",
            path: "/_api/simple/replace-by-example",
            body: {
                ...options,
                example,
                newValue,
                collection: this._name,
            },
        });
    }
    updateByExample(example, newValue, options) {
        return this._db.request({
            method: "PUT",
            path: "/_api/simple/update-by-example",
            body: {
                ...options,
                example,
                newValue,
                collection: this._name,
            },
        });
    }
    lookupByKeys(keys) {
        return this._db.request({
            method: "PUT",
            path: "/_api/simple/lookup-by-keys",
            body: {
                keys,
                collection: this._name,
            },
        }, (res) => res.body.documents);
    }
    removeByKeys(keys, options) {
        return this._db.request({
            method: "PUT",
            path: "/_api/simple/remove-by-keys",
            body: {
                options: options,
                keys,
                collection: this._name,
            },
        });
    }
    //#endregion
    //#region indexes
    indexes() {
        return this._db.request({
            path: "/_api/index",
            qs: { collection: this._name },
        }, (res) => res.body.indexes);
    }
    index(selector) {
        return this._db.request({
            path: `/_api/index/${encodeURI(_indexHandle(selector, this._name))}`,
        });
    }
    ensureIndex(options) {
        const opts = { ...options };
        if (opts.name) {
            opts.name = opts.name.normalize("NFC");
        }
        return this._db.request({
            method: "POST",
            path: "/_api/index",
            body: options,
            qs: { collection: this._name },
        });
    }
    dropIndex(selector) {
        return this._db.request({
            method: "DELETE",
            path: `/_api/index/${encodeURI(_indexHandle(selector, this._name))}`,
        });
    }
    fulltext(attribute, query, { index, ...options } = {}) {
        return this._db.request({
            method: "PUT",
            path: "/_api/simple/fulltext",
            body: {
                ...options,
                index: index ? _indexHandle(index, this._name) : undefined,
                attribute,
                query,
                collection: this._name,
            },
        }, (res) => new BatchedArrayCursor(this._db, res.body, res.arangojsHostUrl).items);
    }
    compact() {
        return this._db.request({
            method: "PUT",
            path: `/_api/collection/${this._name}/compact`,
        }, (res) => res.body);
    }
}

/**
 * ```ts
 * import type {
 *   Graph,
 *   GraphVertexCollection,
 *   GraphEdgeCollection,
 * } from "arangojs/graph";
 * ```
 *
 * The "graph" module provides graph related types and interfaces
 * for TypeScript.
 *
 * @packageDocumentation
 */
/**
 * Indicates whether the given value represents a {@link graph.Graph}.
 *
 * @param graph - A value that might be a Graph.
 */
function isArangoGraph(graph) {
    return Boolean(graph && graph.isArangoGraph);
}
/**
 * @internal
 */
function mungeGharialResponse(body, prop) {
    const { new: newDoc, old: oldDoc, [prop]: doc, ...meta } = body;
    const result = { ...meta, ...doc };
    if (typeof newDoc !== "undefined")
        result.new = newDoc;
    if (typeof oldDoc !== "undefined")
        result.old = oldDoc;
    return result;
}
/**
 * @internal
 */
function coerceEdgeDefinition(options) {
    const edgeDefinition = {};
    edgeDefinition.collection = collectionToString(options.collection);
    edgeDefinition.from = Array.isArray(options.from)
        ? options.from.map(collectionToString)
        : [collectionToString(options.from)];
    edgeDefinition.to = Array.isArray(options.to)
        ? options.to.map(collectionToString)
        : [collectionToString(options.to)];
    return edgeDefinition;
}
/**
 * Represents a {@link collection.DocumentCollection} of vertices in a {@link graph.Graph}.
 *
 * @param T - Type to use for document data. Defaults to `any`.
 */
class GraphVertexCollection {
    /**
     * @internal
     */
    constructor(db, name, graph) {
        this._db = db;
        this._collection = db.collection(name);
        this._name = this._collection.name;
        this._graph = graph;
    }
    /**
     * @internal
     *
     * Indicates that this object represents an ArangoDB collection.
     */
    get isArangoCollection() {
        return true;
    }
    /**
     * Name of the collection.
     */
    get name() {
        return this._name;
    }
    /**
     * A {@link collection.DocumentCollection} instance for this vertex collection.
     */
    get collection() {
        return this._collection;
    }
    /**
     * The {@link graph.Graph} instance this vertex collection is bound to.
     */
    get graph() {
        return this._graph;
    }
    /**
     * Checks whether a vertex matching the given key or id exists in this
     * collection.
     *
     * Throws an exception when passed a vertex or `_id` from a different
     * collection.
     *
     * @param selector - Document `_key`, `_id` or object with either of those
     * properties (e.g. a vertex from this collection).
     *
     * @example
     * ```js
     * const graph = db.graph("some-graph");
     * const collection = graph.vertexCollection("vertices");
     * const exists = await collection.vertexExists("abc123");
     * if (!exists) {
     *   console.log("Vertex does not exist");
     * }
     * ```
     */
    async vertexExists(selector) {
        try {
            return await this._db.request({
                method: "HEAD",
                path: `/_api/gharial/${encodeURIComponent(this.graph.name)}/vertex/${encodeURI(_documentHandle(selector, this._name))}`,
            }, () => true);
        }
        catch (err) {
            if (err.code === 404) {
                return false;
            }
            throw err;
        }
    }
    async vertex(selector, options = {}) {
        if (typeof options === "boolean") {
            options = { graceful: options };
        }
        const { allowDirtyRead = undefined, graceful = false, rev, ...qs } = options;
        const headers = {};
        if (rev)
            headers["if-match"] = rev;
        const result = this._db.request({
            path: `/_api/gharial/${encodeURIComponent(this.graph.name)}/vertex/${encodeURI(_documentHandle(selector, this._name))}`,
            headers,
            qs,
            allowDirtyRead,
        }, (res) => res.body.vertex);
        if (!graceful)
            return result;
        try {
            return await result;
        }
        catch (err) {
            if (isArangoError(err) && err.errorNum === DOCUMENT_NOT_FOUND) {
                return null;
            }
            throw err;
        }
    }
    save(data, options) {
        return this._db.request({
            method: "POST",
            path: `/_api/gharial/${encodeURIComponent(this.graph.name)}/vertex/${encodeURIComponent(this._name)}`,
            body: data,
            qs: options,
        }, (res) => mungeGharialResponse(res.body, "vertex"));
    }
    replace(selector, newValue, options = {}) {
        if (typeof options === "string") {
            options = { rev: options };
        }
        const { rev, ...qs } = options;
        const headers = {};
        if (rev)
            headers["if-match"] = rev;
        return this._db.request({
            method: "PUT",
            path: `/_api/gharial/${encodeURIComponent(this.graph.name)}/vertex/${encodeURI(_documentHandle(selector, this._name))}`,
            body: newValue,
            qs,
            headers,
        }, (res) => mungeGharialResponse(res.body, "vertex"));
    }
    update(selector, newValue, options = {}) {
        if (typeof options === "string") {
            options = { rev: options };
        }
        const headers = {};
        const { rev, ...qs } = options;
        if (rev)
            headers["if-match"] = rev;
        return this._db.request({
            method: "PATCH",
            path: `/_api/gharial/${encodeURIComponent(this.graph.name)}/vertex/${encodeURI(_documentHandle(selector, this._name))}`,
            body: newValue,
            qs,
            headers,
        }, (res) => mungeGharialResponse(res.body, "vertex"));
    }
    remove(selector, options = {}) {
        if (typeof options === "string") {
            options = { rev: options };
        }
        const headers = {};
        const { rev, ...qs } = options;
        if (rev)
            headers["if-match"] = rev;
        return this._db.request({
            method: "DELETE",
            path: `/_api/gharial/${encodeURIComponent(this.graph.name)}/vertex/${encodeURI(_documentHandle(selector, this._name))}`,
            qs,
            headers,
        }, (res) => mungeGharialResponse(res.body, "removed"));
    }
}
/**
 * Represents a {@link collection.EdgeCollection} of edges in a {@link graph.Graph}.
 *
 * @param T - Type to use for document data. Defaults to `any`.
 */
class GraphEdgeCollection {
    /**
     * @internal
     */
    constructor(db, name, graph) {
        this._db = db;
        this._collection = db.collection(name);
        this._name = this._collection.name;
        this._graph = graph;
    }
    /**
     * @internal
     *
     * Indicates that this object represents an ArangoDB collection.
     */
    get isArangoCollection() {
        return true;
    }
    /**
     * Name of the collection.
     */
    get name() {
        return this._name;
    }
    /**
     * A {@link collection.EdgeCollection} instance for this edge collection.
     */
    get collection() {
        return this._collection;
    }
    /**
     * The {@link graph.Graph} instance this edge collection is bound to.
     */
    get graph() {
        return this._graph;
    }
    /**
     * Checks whether a edge matching the given key or id exists in this
     * collection.
     *
     * Throws an exception when passed a edge or `_id` from a different
     * collection.
     *
     * @param selector - Document `_key`, `_id` or object with either of those
     * properties (e.g. a edge from this collection).
     *
     * @example
     * ```js
     * const graph = db.graph("some-graph");
     * const collection = graph.edgeCollection("friends")
     * const exists = await collection.edgeExists("abc123");
     * if (!exists) {
     *   console.log("Edge does not exist");
     * }
     * ```
     */
    async edgeExists(selector) {
        try {
            return await this._db.request({
                method: "HEAD",
                path: `/_api/gharial/${encodeURIComponent(this.graph.name)}/edge/${encodeURI(_documentHandle(selector, this._name))}`,
            }, () => true);
        }
        catch (err) {
            if (err.code === 404) {
                return false;
            }
            throw err;
        }
    }
    async edge(selector, options = {}) {
        if (typeof options === "boolean") {
            options = { graceful: options };
        }
        const { allowDirtyRead = undefined, graceful = false, rev, ...qs } = options;
        const result = this._db.request({
            path: `/_api/gharial/${encodeURIComponent(this.graph.name)}/edge/${encodeURI(_documentHandle(selector, this._name))}`,
            qs,
            allowDirtyRead,
        }, (res) => res.body.edge);
        if (!graceful)
            return result;
        try {
            return await result;
        }
        catch (err) {
            if (isArangoError(err) && err.errorNum === DOCUMENT_NOT_FOUND) {
                return null;
            }
            throw err;
        }
    }
    save(data, options) {
        return this._db.request({
            method: "POST",
            path: `/_api/gharial/${encodeURIComponent(this.graph.name)}/edge/${encodeURIComponent(this._name)}`,
            body: data,
            qs: options,
        }, (res) => mungeGharialResponse(res.body, "edge"));
    }
    replace(selector, newValue, options = {}) {
        if (typeof options === "string") {
            options = { rev: options };
        }
        const { rev, ...qs } = options;
        const headers = {};
        if (rev)
            headers["if-match"] = rev;
        return this._db.request({
            method: "PUT",
            path: `/_api/gharial/${encodeURIComponent(this.graph.name)}/edge/${encodeURI(_documentHandle(selector, this._name))}`,
            body: newValue,
            qs,
            headers,
        }, (res) => mungeGharialResponse(res.body, "edge"));
    }
    update(selector, newValue, options = {}) {
        if (typeof options === "string") {
            options = { rev: options };
        }
        const { rev, ...qs } = options;
        const headers = {};
        if (rev)
            headers["if-match"] = rev;
        return this._db.request({
            method: "PATCH",
            path: `/_api/gharial/${encodeURIComponent(this.graph.name)}/edge/${encodeURI(_documentHandle(selector, this._name))}`,
            body: newValue,
            qs,
            headers,
        }, (res) => mungeGharialResponse(res.body, "edge"));
    }
    remove(selector, options = {}) {
        if (typeof options === "string") {
            options = { rev: options };
        }
        const { rev, ...qs } = options;
        const headers = {};
        if (rev)
            headers["if-match"] = rev;
        return this._db.request({
            method: "DELETE",
            path: `/_api/gharial/${encodeURIComponent(this.graph.name)}/edge/${encodeURI(_documentHandle(selector, this._name))}`,
            qs,
            headers,
        }, (res) => mungeGharialResponse(res.body, "removed"));
    }
}
/**
 * Represents a graph in a {@link database.Database}.
 */
class Graph {
    /**
     * @internal
     */
    constructor(db, name) {
        this._name = name.normalize("NFC");
        this._db = db;
    }
    /**
     * @internal
     *
     * Indicates that this object represents an ArangoDB Graph.
     */
    get isArangoGraph() {
        return true;
    }
    /**
     * Name of the graph.
     */
    get name() {
        return this._name;
    }
    /**
     * Checks whether the graph exists.
     *
     * @example
     * ```js
     * const db = new Database();
     * const graph = db.graph("some-graph");
     * const result = await graph.exists();
     * // result indicates whether the graph exists
     * ```
     */
    async exists() {
        try {
            await this.get();
            return true;
        }
        catch (err) {
            if (isArangoError(err) && err.errorNum === GRAPH_NOT_FOUND) {
                return false;
            }
            throw err;
        }
    }
    /**
     * Retrieves general information about the graph.
     *
     * @example
     * ```js
     * const db = new Database();
     * const graph = db.graph("some-graph");
     * const data = await graph.get();
     * // data contains general information about the graph
     * ```
     */
    get() {
        return this._db.request({ path: `/_api/gharial/${encodeURIComponent(this._name)}` }, (res) => res.body.graph);
    }
    /**
     * Creates a graph with the given `edgeDefinitions` and `options` for this
     * graph's name.
     *
     * @param edgeDefinitions - Definitions for the relations of the graph.
     * @param options - Options for creating the graph.
     *
     * @example
     * ```js
     * const db = new Database();
     * const graph = db.graph("some-graph");
     * const info = await graph.create([
     *   {
     *     collection: "edges",
     *     from: ["start-vertices"],
     *     to: ["end-vertices"],
     *   },
     * ]);
     * // graph now exists
     * ```
     */
    create(edgeDefinitions, options = {}) {
        const { orphanCollections, satellites, waitForSync, isSmart, isDisjoint, ...opts } = options;
        return this._db.request({
            method: "POST",
            path: "/_api/gharial",
            body: {
                orphanCollections: orphanCollections &&
                    (Array.isArray(orphanCollections)
                        ? orphanCollections.map(collectionToString)
                        : [collectionToString(orphanCollections)]),
                edgeDefinitions: edgeDefinitions.map(coerceEdgeDefinition),
                isSmart,
                isDisjoint,
                name: this._name,
                options: { ...opts, satellites: satellites?.map(collectionToString) },
            },
            qs: { waitForSync },
        }, (res) => res.body.graph);
    }
    /**
     * Deletes the graph from the database.
     *
     * @param dropCollections - If set to `true`, the collections associated with
     * the graph will also be deleted.
     *
     * @example
     * ```js
     * const db = new Database();
     * const graph = db.graph("some-graph");
     * await graph.drop();
     * // the graph "some-graph" no longer exists
     * ```
     */
    drop(dropCollections = false) {
        return this._db.request({
            method: "DELETE",
            path: `/_api/gharial/${encodeURIComponent(this._name)}`,
            qs: { dropCollections },
        }, (res) => res.body.removed);
    }
    /**
     * Returns a {@link graph.GraphVertexCollection} instance for the given collection
     * name representing the collection in this graph.
     *
     * @param T - Type to use for document data. Defaults to `any`.
     * @param collection - Name of the vertex collection.
     */
    vertexCollection(collection) {
        return new GraphVertexCollection(this._db, collectionToString(collection), this);
    }
    /**
     * Fetches all vertex collections of this graph from the database and returns
     * an array of their names.
     *
     * See also {@link graph.Graph#vertexCollections}.
     *
     * @example
     * ```js
     * const db = new Database();
     * const graph = db.graph("some-graph");
     * const info = await graph.create([
     *   {
     *     collection: "edges",
     *     from: ["start-vertices"],
     *     to: ["end-vertices"],
     *   },
     * ]);
     * const vertexCollectionNames = await graph.listVertexCollections();
     * // ["start-vertices", "end-vertices"]
     * ```
     */
    listVertexCollections() {
        return this._db.request({ path: `/_api/gharial/${encodeURIComponent(this._name)}/vertex` }, (res) => res.body.collections);
    }
    /**
     * Fetches all vertex collections of this graph from the database and returns
     * an array of {@link graph.GraphVertexCollection} instances.
     *
     * See also {@link graph.Graph#listVertexCollections}.
     *
     * @example
     * ```js
     * const db = new Database();
     * const graph = db.graph("some-graph");
     * const info = await graph.create([
     *   {
     *     collection: "edges",
     *     from: ["start-vertices"],
     *     to: ["end-vertices"],
     *   },
     * ]);
     * const vertexCollections = await graph.vertexCollections();
     * for (const vertexCollection of vertexCollections) {
     *   console.log(vertexCollection.name);
     *   // "start-vertices"
     *   // "end-vertices"
     * }
     * ```
     */
    async vertexCollections() {
        const names = await this.listVertexCollections();
        return names.map((name) => new GraphVertexCollection(this._db, name, this));
    }
    /**
     * Adds the given collection to this graph as a vertex collection.
     *
     * @param collection - Collection to add to the graph.
     *
     * @example
     * ```js
     * const db = new Database();
     * const graph = db.graph("some-graph");
     * await graph.addVertexCollection("more-vertices");
     * // The collection "more-vertices" has been added to the graph
     * const extra = db.collection("extra-vertices");
     * await graph.addVertexCollection(extra);
     * // The collection "extra-vertices" has been added to the graph
     * ```
     */
    addVertexCollection(collection, options = {}) {
        const { satellites, ...opts } = options;
        return this._db.request({
            method: "POST",
            path: `/_api/gharial/${encodeURIComponent(this._name)}/vertex`,
            body: {
                collection: collectionToString(collection),
                options: { ...opts, satellites: satellites?.map(collectionToString) },
            },
        }, (res) => res.body.graph);
    }
    /**
     * Removes the given collection from this graph as a vertex collection.
     *
     * @param collection - Collection to remove from the graph.
     * @param dropCollection - If set to `true`, the collection will also be
     * deleted from the database.
     *
     * @example
     * ```js
     * const db = new Database();
     * const graph = db.graph("some-graph");
     * const info = await graph.create([
     *   {
     *     collection: "edges",
     *     from: ["start-vertices"],
     *     to: ["end-vertices"],
     *   },
     * ]);
     * await graph.removeVertexCollection("start-vertices");
     * // The collection "start-vertices" is no longer part of the graph.
     * ```
     */
    removeVertexCollection(collection, dropCollection = false) {
        return this._db.request({
            method: "DELETE",
            path: `/_api/gharial/${encodeURIComponent(this._name)}/vertex/${encodeURIComponent(collectionToString(collection))}`,
            qs: {
                dropCollection,
            },
        }, (res) => res.body.graph);
    }
    /**
     * Returns a {@link graph.GraphEdgeCollection} instance for the given collection
     * name representing the collection in this graph.
     *
     * @param T - Type to use for document data. Defaults to `any`.
     * @param collection - Name of the edge collection.
     *
     * @example
     * ```js
     * const db = new Database();
     * const graph = db.graph("some-graph");
     * const info = await graph.create([
     *   {
     *     collection: "edges",
     *     from: ["start-vertices"],
     *     to: ["end-vertices"],
     *   },
     * ]);
     * const graphEdgeCollection = graph.edgeCollection("edges");
     * // Access the underlying EdgeCollection API:
     * const edgeCollection = graphEdgeCollection.collection;
     * ```
     */
    edgeCollection(collection) {
        return new GraphEdgeCollection(this._db, collectionToString(collection), this);
    }
    /**
     * Fetches all edge collections of this graph from the database and returns
     * an array of their names.
     *
     * See also {@link graph.Graph#edgeCollections}.
     *
     * @example
     * ```js
     * const db = new Database();
     * const graph = db.graph("some-graph");
     * const info = await graph.create([
     *   {
     *     collection: "edges",
     *     from: ["start-vertices"],
     *     to: ["end-vertices"],
     *   },
     * ]);
     * const edgeCollectionNames = await graph.listEdgeCollections();
     * // ["edges"]
     * ```
     */
    listEdgeCollections() {
        return this._db.request({ path: `/_api/gharial/${encodeURIComponent(this._name)}/edge` }, (res) => res.body.collections);
    }
    /**
     * Fetches all edge collections of this graph from the database and returns
     * an array of {@link graph.GraphEdgeCollection} instances.
     *
     * See also {@link graph.Graph#listEdgeCollections}.
     *
     * @example
     * ```js
     * const db = new Database();
     * const graph = db.graph("some-graph");
     * const info = await graph.create([
     *   {
     *     collection: "edges",
     *     from: ["start-vertices"],
     *     to: ["end-vertices"],
     *   },
     * ]);
     * const graphEdgeCollections = await graph.edgeCollections();
     * for (const collection of graphEdgeCollection) {
     *   console.log(collection.name);
     *   // "edges"
     * }
     * ```
     */
    async edgeCollections() {
        const names = await this.listEdgeCollections();
        return names.map((name) => new GraphEdgeCollection(this._db, name, this));
    }
    /**
     * Adds an edge definition to this graph.
     *
     * @param edgeDefinition - Definition of a relation in this graph.
     *
     * @example
     * ```js
     * const db = new Database();
     * const graph = db.graph("some-graph");
     * await graph.addEdgeDefinition({
     *   collection: "edges",
     *   from: ["start-vertices"],
     *   to: ["end-vertices"],
     * });
     * // The edge definition has been added to the graph
     * ```
     */
    addEdgeDefinition(edgeDefinition, options = {}) {
        const { satellites, ...opts } = options;
        return this._db.request({
            method: "POST",
            path: `/_api/gharial/${encodeURIComponent(this._name)}/edge`,
            body: {
                ...coerceEdgeDefinition(edgeDefinition),
                options: { ...opts, satellites: satellites?.map(collectionToString) },
            },
        }, (res) => res.body.graph);
    }
    replaceEdgeDefinition(collectionOrEdgeDefinitionOptions, edgeDefinitionOrOptions, options = {}) {
        let collection = collectionOrEdgeDefinitionOptions;
        let edgeDefinition = edgeDefinitionOrOptions;
        if (edgeDefinitionOrOptions &&
            !edgeDefinitionOrOptions.hasOwnProperty("collection")) {
            options = edgeDefinitionOrOptions;
            edgeDefinitionOrOptions = undefined;
        }
        if (!edgeDefinitionOrOptions) {
            edgeDefinition =
                collectionOrEdgeDefinitionOptions;
            collection = edgeDefinition.collection;
        }
        const { satellites, ...opts } = options;
        return this._db.request({
            method: "PUT",
            path: `/_api/gharial/${encodeURIComponent(this._name)}/edge/${encodeURIComponent(collectionToString(collection))}`,
            body: {
                ...coerceEdgeDefinition(edgeDefinition),
                options: { ...opts, satellites: satellites?.map(collectionToString) },
            },
        }, (res) => res.body.graph);
    }
    /**
     * Removes the edge definition for the given edge collection from this graph.
     *
     * @param collection - Edge collection for which to remove the definition.
     * @param dropCollection - If set to `true`, the collection will also be
     * deleted from the database.
     *
     * @example
     * ```js
     * const db = new Database();
     * const graph = db.graph("some-graph");
     * const info = await graph.create([
     *   {
     *     collection: "edges",
     *     from: ["start-vertices"],
     *     to: ["end-vertices"],
     *   },
     * ]);
     * await graph.removeEdgeDefinition("edges");
     * // The edge definition for "edges" has been replaced
     * ```
     */
    removeEdgeDefinition(collection, dropCollection = false) {
        return this._db.request({
            method: "DELETE",
            path: `/_api/gharial/${encodeURIComponent(this._name)}/edge/${encodeURIComponent(collectionToString(collection))}`,
            qs: {
                dropCollection,
            },
        }, (res) => res.body.graph);
    }
    /**
     * Performs a traversal starting from the given `startVertex` and following
     * edges contained in this graph.
     *
     * See also {@link collection.EdgeCollection#traversal}.
     *
     * @param startVertex - Document `_id` of a vertex in this graph.
     * @param options - Options for performing the traversal.
     *
     * @deprecated Simple Queries have been deprecated in ArangoDB 3.4 and can be
     * replaced with AQL queries.
     *
     * @example
     * ```js
     * const db = new Database();
     * const graph = db.graph("my-graph");
     * const collection = graph.edgeCollection("edges").collection;
     * await collection.import([
     *   ["_key", "_from", "_to"],
     *   ["x", "vertices/a", "vertices/b"],
     *   ["y", "vertices/b", "vertices/c"],
     *   ["z", "vertices/c", "vertices/d"],
     * ]);
     * const startVertex = "vertices/a";
     * const cursor = await db.query(aql`
     *   FOR vertex IN OUTBOUND ${startVertex} GRAPH ${graph}
     *   RETURN vertex._key
     * `);
     * const result = await cursor.all();
     * console.log(result); // ["a", "b", "c", "d"]
     * ```
     */
    traversal(startVertex, options) {
        return this._db.request({
            method: "POST",
            path: `/_api/traversal`,
            body: {
                ...options,
                startVertex,
                graphName: this._name,
            },
        }, (res) => res.body.result);
    }
}

/**
 * Indicates whether the given value represents a {@link View}.
 *
 * @param view - A value that might be a View.
 */
function isArangoView(view) {
    return Boolean(view && view.isArangoView);
}
/**
 * Represents a View in a {@link database.Database}.
 */
class View {
    /**
     * @internal
     */
    constructor(db, name) {
        this._db = db;
        this._name = name.normalize("NFC");
    }
    /**
     * @internal
     *
     * Indicates that this object represents an ArangoDB View.
     */
    get isArangoView() {
        return true;
    }
    /**
     * Name of the View.
     */
    get name() {
        return this._name;
    }
    /**
     * Retrieves general information about the View.
     *
     * @example
     * ```js
     * const db = new Database();
     * const view = db.view("some-view");
     * const data = await view.get();
     * // data contains general information about the View
     * ```
     */
    get() {
        return this._db.request({
            path: `/_api/view/${encodeURIComponent(this._name)}`,
        });
    }
    /**
     * Checks whether the View exists.
     *
     * @example
     * ```js
     * const db = new Database();
     * const view = db.view("some-view");
     * const exists = await view.exists();
     * console.log(exists); // indicates whether the View exists
     * ```
     */
    async exists() {
        try {
            await this.get();
            return true;
        }
        catch (err) {
            if (isArangoError(err) && err.errorNum === VIEW_NOT_FOUND) {
                return false;
            }
            throw err;
        }
    }
    /**
     * Creates a View with the given `options` and the instance's name.
     *
     * See also {@link database.Database#createView}.
     *
     * @example
     * ```js
     * const db = new Database();
     * const view = db.view("potatoes");
     * await view.create();
     * // the ArangoSearch View "potatoes" now exists
     * ```
     */
    create(options) {
        return this._db.request({
            method: "POST",
            path: "/_api/view",
            body: {
                ...options,
                name: this._name,
            },
        });
    }
    /**
     * Renames the View and updates the instance's `name` to `newName`.
     *
     * Additionally removes the instance from the {@link database.Database}'s internal
     * cache.
     *
     * **Note**: Renaming Views may not be supported when ArangoDB is
     * running in a cluster configuration.
     *
     * @param newName - The new name of the View.
     *
     * @example
     * ```js
     * const db = new Database();
     * const view1 = db.view("some-view");
     * await view1.rename("other-view");
     * const view2 = db.view("some-view");
     * const view3 = db.view("other-view");
     * // Note all three View instances are different objects but
     * // view1 and view3 represent the same ArangoDB view!
     * ```
     */
    async rename(newName) {
        const result = this._db.renameView(this._name, newName);
        this._name = newName.normalize("NFC");
        return result;
    }
    /**
     * Retrieves the View's properties.
     *
     * @example
     * ```js
     * const db = new Database();
     * const view = db.view("some-view");
     * const data = await view.properties();
     * // data contains the View's properties
     * ```
     */
    properties() {
        return this._db.request({
            path: `/_api/view/${encodeURIComponent(this._name)}/properties`,
        });
    }
    /**
     * Updates the properties of the View.
     *
     * @param properties - Properties of the View to update.
     *
     * @example
     * ```js
     * const db = new Database();
     * const view = db.view("some-view");
     * const result = await view.updateProperties({
     *   consolidationIntervalMsec: 234
     * });
     * console.log(result.consolidationIntervalMsec); // 234
     * ```
     */
    updateProperties(properties) {
        return this._db.request({
            method: "PATCH",
            path: `/_api/view/${encodeURIComponent(this._name)}/properties`,
            body: properties ?? {},
        });
    }
    /**
     * Replaces the properties of the View.
     *
     * @param properties - New properties of the View.
     *
     * @example
     * ```js
     * const db = new Database();
     * const view = db.view("some-view");
     * const result = await view.replaceProperties({
     *   consolidationIntervalMsec: 234
     * });
     * console.log(result.consolidationIntervalMsec); // 234
     * ```
     */
    replaceProperties(properties) {
        return this._db.request({
            method: "PUT",
            path: `/_api/view/${encodeURIComponent(this._name)}/properties`,
            body: properties ?? {},
        });
    }
    /**
     * Deletes the View from the database.
     *
     * @example
     *
     * ```js
     * const db = new Database();
     * const view = db.view("some-view");
     * await view.drop();
     * // the View "some-view" no longer exists
     * ```
     */
    drop() {
        return this._db.request({
            method: "DELETE",
            path: `/_api/view/${encodeURIComponent(this._name)}`,
        }, (res) => res.body.result);
    }
}

/**
 * ```js
 * import { aql } from "arangojs/aql";
 * ```
 *
 * The "aql" module provides the {@link aql} template string handler and
 * helper functions, as well as associated types and interfaces for TypeScript.
 *
 * The aql function and namespace is also re-exported by the "index" module.
 *
 * @packageDocumentation
 */
/**
 * Indicates whether the given value is an {@link AqlQuery}.
 *
 * @param query - A value that might be an `AqlQuery`.
 */
function isAqlQuery(query) {
    return Boolean(query && typeof query.query === "string" && query.bindVars);
}
/**
 * Indicates whether the given value is a {@link GeneratedAqlQuery}.
 *
 * @param query - A value that might be a `GeneratedAqlQuery`.
 *
 * @internal
 */
function isGeneratedAqlQuery(query) {
    return isAqlQuery(query) && typeof query._source === "function";
}
/**
 * Indicates whether the given value is an {@link AqlLiteral}.
 *
 * @param literal - A value that might be an `AqlLiteral`.
 */
function isAqlLiteral(literal) {
    return Boolean(literal && typeof literal.toAQL === "function");
}
/**
 * Template string handler (template tag) for AQL queries.
 *
 * The `aql` tag can be used to write complex AQL queries as multi-line strings
 * without having to worry about `bindVars` and the distinction between
 * collections and regular parameters.
 *
 * Tagged template strings will return an {@link AqlQuery} object with
 * `query` and `bindVars` attributes reflecting any interpolated values.
 *
 * Any {@link collection.ArangoCollection} instance used in a query string will
 * be recognized as a collection reference and generate an AQL collection bind
 * parameter instead of a regular AQL value bind parameter.
 *
 * **Note**: you should always use the `aql` template tag when writing
 * dynamic AQL queries instead of using untagged (normal) template strings.
 * Untagged template strings will inline any interpolated values and return
 * a plain string as result. The `aql` template tag will only inline references
 * to the interpolated values and produce an AQL query object containing both
 * the query and the values. This prevents most injection attacks when using
 * untrusted values in dynamic queries.
 *
 * @example
 * ```js
 * // Some user-supplied string that may be malicious
 * const untrustedValue = req.body.email;
 *
 * // Without aql tag: BAD! DO NOT DO THIS!
 * const badQuery = `
 *   FOR user IN users
 *   FILTER user.email == "${untrustedValue}"
 *   RETURN user
 * `;
 * // e.g. if untrustedValue is '" || user.admin == true || "':
 * // Query:
 * //   FOR user IN users
 * //   FILTER user.email == "" || user.admin == true || ""
 * //   RETURN user
 *
 * // With the aql tag: GOOD! MUCH SAFER!
 * const betterQuery = aql`
 *   FOR user IN users
 *   FILTER user.email == ${untrustedValue}
 *   RETURN user
 * `;
 * // Query:
 * //   FOR user IN users
 * //   FILTER user.email == @value0
 * //   RETURN user
 * // Bind parameters:
 * //   value0 -> untrustedValue
 * ```
 *
 * @example
 * ```js
 * const collection = db.collection("some-collection");
 * const minValue = 23;
 * const result = await db.query(aql`
 *   FOR d IN ${collection}
 *   FILTER d.num > ${minValue}
 *   RETURN d
 * `);
 *
 * // Equivalent raw query object
 * const result2 = await db.query({
 *   query: `
 *     FOR d IN @@collection
 *     FILTER d.num > @minValue
 *     RETURN d
 *   `,
 *   bindVars: {
 *     "@collection": collection.name,
 *     minValue: minValue
 *   }
 * });
 * ```
 *
 * @example
 * ```js
 * const collection = db.collection("some-collection");
 * const color = "green";
 * const filter = aql`FILTER d.color == ${color}'`;
 * const result = await db.query(aql`
 *   FOR d IN ${collection}
 *   ${filter}
 *   RETURN d
 * `);
 * ```
 */
function aql(templateStrings, ...args) {
    const strings = [...templateStrings];
    const bindVars = {};
    const bindValues = [];
    let query = strings[0];
    for (let i = 0; i < args.length; i++) {
        const rawValue = args[i];
        let value = rawValue;
        if (isGeneratedAqlQuery(rawValue)) {
            const src = rawValue._source();
            if (src.args.length) {
                query += src.strings[0];
                args.splice(i, 1, ...src.args);
                strings.splice(i, 2, strings[i] + src.strings[0], ...src.strings.slice(1, src.args.length), src.strings[src.args.length] + strings[i + 1]);
            }
            else {
                query += rawValue.query + strings[i + 1];
                args.splice(i, 1);
                strings.splice(i, 2, strings[i] + rawValue.query + strings[i + 1]);
            }
            i -= 1;
            continue;
        }
        if (rawValue === undefined) {
            query += strings[i + 1];
            continue;
        }
        if (isAqlLiteral(rawValue)) {
            query += `${rawValue.toAQL()}${strings[i + 1]}`;
            continue;
        }
        const index = bindValues.indexOf(rawValue);
        const isKnown = index !== -1;
        let name = `value${isKnown ? index : bindValues.length}`;
        if (isArangoCollection(rawValue) ||
            isArangoGraph(rawValue) ||
            isArangoView(rawValue) ||
            isArangoAnalyzer(rawValue)) {
            name = `@${name}`;
            value = rawValue.name;
        }
        if (!isKnown) {
            bindValues.push(rawValue);
            bindVars[name] = value;
        }
        query += `@${name}${strings[i + 1]}`;
    }
    return {
        query,
        bindVars,
        _source: () => ({ strings, args }),
    };
}

/**
 * Node.js implementation of browser `btoa` function.
 *
 * @packageDocumentation
 * @internal
 */
function base64Encode(str) {
    return buffer$1.from(str).toString("base64");
}

/**
 * Utility function for normalizing URLs.
 *
 * @packageDocumentation
 * @internal
 */
/**
 * @internal
 */
function normalizeUrl(url) {
    const raw = url.match(/^(tcp|ssl|tls)((?::|\+).+)/);
    if (raw)
        url = (raw[1] === "tcp" ? "http" : "https") + raw[2];
    const unix = url.match(/^(?:(http|https)\+)?unix:\/\/(\/.+)/);
    if (unix)
        url = `${unix[1] || "http"}://unix:${unix[2]}`;
    return url;
}

function _typeof$2(obj) {
  "@babel/helpers - typeof";

  return _typeof$2 = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) {
    return typeof obj;
  } : function (obj) {
    return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
  }, _typeof$2(obj);
}
function _classCallCheck$1(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}
function _defineProperties$1(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, _toPropertyKey$1(descriptor.key), descriptor);
  }
}
function _createClass$1(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties$1(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties$1(Constructor, staticProps);
  Object.defineProperty(Constructor, "prototype", {
    writable: false
  });
  return Constructor;
}
function _inherits$1(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }
  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  Object.defineProperty(subClass, "prototype", {
    writable: false
  });
  if (superClass) _setPrototypeOf$1(subClass, superClass);
}
function _getPrototypeOf$1(o) {
  _getPrototypeOf$1 = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf$1(o);
}
function _setPrototypeOf$1(o, p) {
  _setPrototypeOf$1 = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };
  return _setPrototypeOf$1(o, p);
}
function _isNativeReflectConstruct$1() {
  if (typeof Reflect === "undefined" || !Reflect.construct) return false;
  if (Reflect.construct.sham) return false;
  if (typeof Proxy === "function") return true;
  try {
    Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
    return true;
  } catch (e) {
    return false;
  }
}
function _assertThisInitialized$1(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }
  return self;
}
function _possibleConstructorReturn$1(self, call) {
  if (call && (typeof call === "object" || typeof call === "function")) {
    return call;
  } else if (call !== void 0) {
    throw new TypeError("Derived constructors may only return object or undefined");
  }
  return _assertThisInitialized$1(self);
}
function _createSuper$1(Derived) {
  var hasNativeReflectConstruct = _isNativeReflectConstruct$1();
  return function _createSuperInternal() {
    var Super = _getPrototypeOf$1(Derived),
      result;
    if (hasNativeReflectConstruct) {
      var NewTarget = _getPrototypeOf$1(this).constructor;
      result = Reflect.construct(Super, arguments, NewTarget);
    } else {
      result = Super.apply(this, arguments);
    }
    return _possibleConstructorReturn$1(this, result);
  };
}
function _toPrimitive$1(input, hint) {
  if (typeof input !== "object" || input === null) return input;
  var prim = input[Symbol.toPrimitive];
  if (prim !== undefined) {
    var res = prim.call(input, hint || "default");
    if (typeof res !== "object") return res;
    throw new TypeError("@@toPrimitive must return a primitive value.");
  }
  return (hint === "string" ? String : Number)(input);
}
function _toPropertyKey$1(arg) {
  var key = _toPrimitive$1(arg, "string");
  return typeof key === "symbol" ? key : String(key);
}

function getDefaultExportFromCjs$2 (x) {
	return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x['default'] : x;
}

var buffer = {};

var base64Js = {};

base64Js.byteLength = byteLength;
base64Js.toByteArray = toByteArray;
base64Js.fromByteArray = fromByteArray;
var lookup = [];
var revLookup = [];
var Arr = typeof Uint8Array !== "undefined" ? Uint8Array : Array;
var code = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
for (var i$3 = 0, len = code.length; i$3 < len; ++i$3) {
  lookup[i$3] = code[i$3];
  revLookup[code.charCodeAt(i$3)] = i$3;
}

// Support decoding URL-safe base64 strings, as Node.js does.
// See: https://en.wikipedia.org/wiki/Base64#URL_applications
revLookup["-".charCodeAt(0)] = 62;
revLookup["_".charCodeAt(0)] = 63;
function getLens(b64) {
  var len = b64.length;
  if (len % 4 > 0) {
    throw new Error("Invalid string. Length must be a multiple of 4");
  }

  // Trim off extra bytes after placeholder bytes are found
  // See: https://github.com/beatgammit/base64-js/issues/42
  var validLen = b64.indexOf("=");
  if (validLen === -1) validLen = len;
  var placeHoldersLen = validLen === len ? 0 : 4 - validLen % 4;
  return [validLen, placeHoldersLen];
}

// base64 is 4/3 + up to two characters of the original data
function byteLength(b64) {
  var lens = getLens(b64);
  var validLen = lens[0];
  var placeHoldersLen = lens[1];
  return (validLen + placeHoldersLen) * 3 / 4 - placeHoldersLen;
}
function _byteLength(b64, validLen, placeHoldersLen) {
  return (validLen + placeHoldersLen) * 3 / 4 - placeHoldersLen;
}
function toByteArray(b64) {
  var tmp;
  var lens = getLens(b64);
  var validLen = lens[0];
  var placeHoldersLen = lens[1];
  var arr = new Arr(_byteLength(b64, validLen, placeHoldersLen));
  var curByte = 0;

  // if there are placeholders, only get up to the last complete 4 chars
  var len = placeHoldersLen > 0 ? validLen - 4 : validLen;
  var i;
  for (i = 0; i < len; i += 4) {
    tmp = revLookup[b64.charCodeAt(i)] << 18 | revLookup[b64.charCodeAt(i + 1)] << 12 | revLookup[b64.charCodeAt(i + 2)] << 6 | revLookup[b64.charCodeAt(i + 3)];
    arr[curByte++] = tmp >> 16 & 0xff;
    arr[curByte++] = tmp >> 8 & 0xff;
    arr[curByte++] = tmp & 0xff;
  }
  if (placeHoldersLen === 2) {
    tmp = revLookup[b64.charCodeAt(i)] << 2 | revLookup[b64.charCodeAt(i + 1)] >> 4;
    arr[curByte++] = tmp & 0xff;
  }
  if (placeHoldersLen === 1) {
    tmp = revLookup[b64.charCodeAt(i)] << 10 | revLookup[b64.charCodeAt(i + 1)] << 4 | revLookup[b64.charCodeAt(i + 2)] >> 2;
    arr[curByte++] = tmp >> 8 & 0xff;
    arr[curByte++] = tmp & 0xff;
  }
  return arr;
}
function tripletToBase64(num) {
  return lookup[num >> 18 & 0x3f] + lookup[num >> 12 & 0x3f] + lookup[num >> 6 & 0x3f] + lookup[num & 0x3f];
}
function encodeChunk(uint8, start, end) {
  var tmp;
  var output = [];
  for (var i = start; i < end; i += 3) {
    tmp = (uint8[i] << 16 & 0xff0000) + (uint8[i + 1] << 8 & 0xff00) + (uint8[i + 2] & 0xff);
    output.push(tripletToBase64(tmp));
  }
  return output.join("");
}
function fromByteArray(uint8) {
  var tmp;
  var len = uint8.length;
  var extraBytes = len % 3; // if we have 1 byte left, pad 2 bytes
  var parts = [];
  var maxChunkLength = 16383; // must be multiple of 3

  // go through the array every three bytes, we'll deal with trailing stuff later
  for (var i = 0, len2 = len - extraBytes; i < len2; i += maxChunkLength) {
    parts.push(encodeChunk(uint8, i, i + maxChunkLength > len2 ? len2 : i + maxChunkLength));
  }

  // pad the end with zeros, but make sure to not forget the extra bytes
  if (extraBytes === 1) {
    tmp = uint8[len - 1];
    parts.push(lookup[tmp >> 2] + lookup[tmp << 4 & 0x3f] + "==");
  } else if (extraBytes === 2) {
    tmp = (uint8[len - 2] << 8) + uint8[len - 1];
    parts.push(lookup[tmp >> 10] + lookup[tmp >> 4 & 0x3f] + lookup[tmp << 2 & 0x3f] + "=");
  }
  return parts.join("");
}

var ieee754 = {};

/*! ieee754. BSD-3-Clause License. Feross Aboukhadijeh <https://feross.org/opensource> */
ieee754.read = function (buffer, offset, isLE, mLen, nBytes) {
  var e, m;
  var eLen = nBytes * 8 - mLen - 1;
  var eMax = (1 << eLen) - 1;
  var eBias = eMax >> 1;
  var nBits = -7;
  var i = isLE ? nBytes - 1 : 0;
  var d = isLE ? -1 : 1;
  var s = buffer[offset + i];
  i += d;
  e = s & (1 << -nBits) - 1;
  s >>= -nBits;
  nBits += eLen;
  while (nBits > 0) {
    e = e * 256 + buffer[offset + i];
    i += d;
    nBits -= 8;
  }
  m = e & (1 << -nBits) - 1;
  e >>= -nBits;
  nBits += mLen;
  while (nBits > 0) {
    m = m * 256 + buffer[offset + i];
    i += d;
    nBits -= 8;
  }
  if (e === 0) {
    e = 1 - eBias;
  } else if (e === eMax) {
    return m ? NaN : (s ? -1 : 1) * Infinity;
  } else {
    m = m + Math.pow(2, mLen);
    e = e - eBias;
  }
  return (s ? -1 : 1) * m * Math.pow(2, e - mLen);
};
ieee754.write = function (buffer, value, offset, isLE, mLen, nBytes) {
  var e, m, c;
  var eLen = nBytes * 8 - mLen - 1;
  var eMax = (1 << eLen) - 1;
  var eBias = eMax >> 1;
  var rt = mLen === 23 ? Math.pow(2, -24) - Math.pow(2, -77) : 0;
  var i = isLE ? 0 : nBytes - 1;
  var d = isLE ? 1 : -1;
  var s = value < 0 || value === 0 && 1 / value < 0 ? 1 : 0;
  value = Math.abs(value);
  if (isNaN(value) || value === Infinity) {
    m = isNaN(value) ? 1 : 0;
    e = eMax;
  } else {
    e = Math.floor(Math.log(value) / Math.LN2);
    if (value * (c = Math.pow(2, -e)) < 1) {
      e--;
      c *= 2;
    }
    if (e + eBias >= 1) {
      value += rt / c;
    } else {
      value += rt * Math.pow(2, 1 - eBias);
    }
    if (value * c >= 2) {
      e++;
      c /= 2;
    }
    if (e + eBias >= eMax) {
      m = 0;
      e = eMax;
    } else if (e + eBias >= 1) {
      m = (value * c - 1) * Math.pow(2, mLen);
      e = e + eBias;
    } else {
      m = value * Math.pow(2, eBias - 1) * Math.pow(2, mLen);
      e = 0;
    }
  }
  while (mLen >= 8) {
    buffer[offset + i] = m & 0xff;
    i += d;
    m /= 256;
    mLen -= 8;
  }
  e = e << mLen | m;
  eLen += mLen;
  while (eLen > 0) {
    buffer[offset + i] = e & 0xff;
    i += d;
    e /= 256;
    eLen -= 8;
  }
  buffer[offset + i - d] |= s * 128;
};

(function (exports) {

  var base64 = base64Js;
  var ieee754$1 = ieee754;
  var customInspectSymbol = typeof Symbol === "function" && typeof Symbol["for"] === "function" // eslint-disable-line dot-notation
  ? Symbol["for"]("nodejs.util.inspect.custom") // eslint-disable-line dot-notation
  : null;
  exports.Buffer = Buffer;
  exports.SlowBuffer = SlowBuffer;
  exports.INSPECT_MAX_BYTES = 50;
  var K_MAX_LENGTH = 0x7fffffff;
  exports.kMaxLength = K_MAX_LENGTH;

  /**
   * If `Buffer.TYPED_ARRAY_SUPPORT`:
   *   === true    Use Uint8Array implementation (fastest)
   *   === false   Print warning and recommend using `buffer` v4.x which has an Object
   *               implementation (most compatible, even IE6)
   *
   * Browsers that support typed arrays are IE 10+, Firefox 4+, Chrome 7+, Safari 5.1+,
   * Opera 11.6+, iOS 4.2+.
   *
   * We report that the browser does not support typed arrays if the are not subclassable
   * using __proto__. Firefox 4-29 lacks support for adding new properties to `Uint8Array`
   * (See: https://bugzilla.mozilla.org/show_bug.cgi?id=695438). IE 10 lacks support
   * for __proto__ and has a buggy typed array implementation.
   */
  Buffer.TYPED_ARRAY_SUPPORT = typedArraySupport();
  if (!Buffer.TYPED_ARRAY_SUPPORT && typeof console !== "undefined" && typeof console.error === "function") {
    console.error("This browser lacks typed array (Uint8Array) support which is required by " + "`buffer` v5.x. Use `buffer` v4.x if you require old browser support.");
  }
  function typedArraySupport() {
    // Can typed array instances can be augmented?
    try {
      var arr = new Uint8Array(1);
      var proto = {
        foo: function foo() {
          return 42;
        }
      };
      Object.setPrototypeOf(proto, Uint8Array.prototype);
      Object.setPrototypeOf(arr, proto);
      return arr.foo() === 42;
    } catch (e) {
      return false;
    }
  }
  Object.defineProperty(Buffer.prototype, "parent", {
    enumerable: true,
    get: function get() {
      if (!Buffer.isBuffer(this)) return undefined;
      return this.buffer;
    }
  });
  Object.defineProperty(Buffer.prototype, "offset", {
    enumerable: true,
    get: function get() {
      if (!Buffer.isBuffer(this)) return undefined;
      return this.byteOffset;
    }
  });
  function createBuffer(length) {
    if (length > K_MAX_LENGTH) {
      throw new RangeError('The value "' + length + '" is invalid for option "size"');
    }
    // Return an augmented `Uint8Array` instance
    var buf = new Uint8Array(length);
    Object.setPrototypeOf(buf, Buffer.prototype);
    return buf;
  }

  /**
   * The Buffer constructor returns instances of `Uint8Array` that have their
   * prototype changed to `Buffer.prototype`. Furthermore, `Buffer` is a subclass of
   * `Uint8Array`, so the returned instances will have all the node `Buffer` methods
   * and the `Uint8Array` methods. Square bracket notation works as expected -- it
   * returns a single octet.
   *
   * The `Uint8Array` prototype remains unmodified.
   */

  function Buffer(arg, encodingOrOffset, length) {
    // Common case.
    if (typeof arg === "number") {
      if (typeof encodingOrOffset === "string") {
        throw new TypeError('The "string" argument must be of type string. Received type number');
      }
      return allocUnsafe(arg);
    }
    return from(arg, encodingOrOffset, length);
  }
  Buffer.poolSize = 8192; // not used by this implementation

  function from(value, encodingOrOffset, length) {
    if (typeof value === "string") {
      return fromString(value, encodingOrOffset);
    }
    if (ArrayBuffer.isView(value)) {
      return fromArrayView(value);
    }
    if (value == null) {
      throw new TypeError("The first argument must be one of type string, Buffer, ArrayBuffer, Array, " + "or Array-like Object. Received type " + _typeof$2(value));
    }
    if (isInstance(value, ArrayBuffer) || value && isInstance(value.buffer, ArrayBuffer)) {
      return fromArrayBuffer(value, encodingOrOffset, length);
    }
    if (typeof SharedArrayBuffer !== "undefined" && (isInstance(value, SharedArrayBuffer) || value && isInstance(value.buffer, SharedArrayBuffer))) {
      return fromArrayBuffer(value, encodingOrOffset, length);
    }
    if (typeof value === "number") {
      throw new TypeError('The "value" argument must not be of type number. Received type number');
    }
    var valueOf = value.valueOf && value.valueOf();
    if (valueOf != null && valueOf !== value) {
      return Buffer.from(valueOf, encodingOrOffset, length);
    }
    var b = fromObject(value);
    if (b) return b;
    if (typeof Symbol !== "undefined" && Symbol.toPrimitive != null && typeof value[Symbol.toPrimitive] === "function") {
      return Buffer.from(value[Symbol.toPrimitive]("string"), encodingOrOffset, length);
    }
    throw new TypeError("The first argument must be one of type string, Buffer, ArrayBuffer, Array, " + "or Array-like Object. Received type " + _typeof$2(value));
  }

  /**
   * Functionally equivalent to Buffer(arg, encoding) but throws a TypeError
   * if value is a number.
   * Buffer.from(str[, encoding])
   * Buffer.from(array)
   * Buffer.from(buffer)
   * Buffer.from(arrayBuffer[, byteOffset[, length]])
   **/
  Buffer.from = function (value, encodingOrOffset, length) {
    return from(value, encodingOrOffset, length);
  };

  // Note: Change prototype *after* Buffer.from is defined to workaround Chrome bug:
  // https://github.com/feross/buffer/pull/148
  Object.setPrototypeOf(Buffer.prototype, Uint8Array.prototype);
  Object.setPrototypeOf(Buffer, Uint8Array);
  function assertSize(size) {
    if (typeof size !== "number") {
      throw new TypeError('"size" argument must be of type number');
    } else if (size < 0) {
      throw new RangeError('The value "' + size + '" is invalid for option "size"');
    }
  }
  function alloc(size, fill, encoding) {
    assertSize(size);
    if (size <= 0) {
      return createBuffer(size);
    }
    if (fill !== undefined) {
      // Only pay attention to encoding if it's a string. This
      // prevents accidentally sending in a number that would
      // be interpreted as a start offset.
      return typeof encoding === "string" ? createBuffer(size).fill(fill, encoding) : createBuffer(size).fill(fill);
    }
    return createBuffer(size);
  }

  /**
   * Creates a new filled Buffer instance.
   * alloc(size[, fill[, encoding]])
   **/
  Buffer.alloc = function (size, fill, encoding) {
    return alloc(size, fill, encoding);
  };
  function allocUnsafe(size) {
    assertSize(size);
    return createBuffer(size < 0 ? 0 : checked(size) | 0);
  }

  /**
   * Equivalent to Buffer(num), by default creates a non-zero-filled Buffer instance.
   * */
  Buffer.allocUnsafe = function (size) {
    return allocUnsafe(size);
  };
  /**
   * Equivalent to SlowBuffer(num), by default creates a non-zero-filled Buffer instance.
   */
  Buffer.allocUnsafeSlow = function (size) {
    return allocUnsafe(size);
  };
  function fromString(string, encoding) {
    if (typeof encoding !== "string" || encoding === "") {
      encoding = "utf8";
    }
    if (!Buffer.isEncoding(encoding)) {
      throw new TypeError("Unknown encoding: " + encoding);
    }
    var length = byteLength(string, encoding) | 0;
    var buf = createBuffer(length);
    var actual = buf.write(string, encoding);
    if (actual !== length) {
      // Writing a hex string, for example, that contains invalid characters will
      // cause everything after the first invalid character to be ignored. (e.g.
      // 'abxxcd' will be treated as 'ab')
      buf = buf.slice(0, actual);
    }
    return buf;
  }
  function fromArrayLike(array) {
    var length = array.length < 0 ? 0 : checked(array.length) | 0;
    var buf = createBuffer(length);
    for (var i = 0; i < length; i += 1) {
      buf[i] = array[i] & 255;
    }
    return buf;
  }
  function fromArrayView(arrayView) {
    if (isInstance(arrayView, Uint8Array)) {
      var copy = new Uint8Array(arrayView);
      return fromArrayBuffer(copy.buffer, copy.byteOffset, copy.byteLength);
    }
    return fromArrayLike(arrayView);
  }
  function fromArrayBuffer(array, byteOffset, length) {
    if (byteOffset < 0 || array.byteLength < byteOffset) {
      throw new RangeError('"offset" is outside of buffer bounds');
    }
    if (array.byteLength < byteOffset + (length || 0)) {
      throw new RangeError('"length" is outside of buffer bounds');
    }
    var buf;
    if (byteOffset === undefined && length === undefined) {
      buf = new Uint8Array(array);
    } else if (length === undefined) {
      buf = new Uint8Array(array, byteOffset);
    } else {
      buf = new Uint8Array(array, byteOffset, length);
    }

    // Return an augmented `Uint8Array` instance
    Object.setPrototypeOf(buf, Buffer.prototype);
    return buf;
  }
  function fromObject(obj) {
    if (Buffer.isBuffer(obj)) {
      var len = checked(obj.length) | 0;
      var buf = createBuffer(len);
      if (buf.length === 0) {
        return buf;
      }
      obj.copy(buf, 0, 0, len);
      return buf;
    }
    if (obj.length !== undefined) {
      if (typeof obj.length !== "number" || numberIsNaN(obj.length)) {
        return createBuffer(0);
      }
      return fromArrayLike(obj);
    }
    if (obj.type === "Buffer" && Array.isArray(obj.data)) {
      return fromArrayLike(obj.data);
    }
  }
  function checked(length) {
    // Note: cannot use `length < K_MAX_LENGTH` here because that fails when
    // length is NaN (which is otherwise coerced to zero.)
    if (length >= K_MAX_LENGTH) {
      throw new RangeError("Attempt to allocate Buffer larger than maximum " + "size: 0x" + K_MAX_LENGTH.toString(16) + " bytes");
    }
    return length | 0;
  }
  function SlowBuffer(length) {
    if (+length != length) {
      // eslint-disable-line eqeqeq
      length = 0;
    }
    return Buffer.alloc(+length);
  }
  Buffer.isBuffer = function isBuffer(b) {
    return b != null && b._isBuffer === true && b !== Buffer.prototype; // so Buffer.isBuffer(Buffer.prototype) will be false
  };

  Buffer.compare = function compare(a, b) {
    if (isInstance(a, Uint8Array)) a = Buffer.from(a, a.offset, a.byteLength);
    if (isInstance(b, Uint8Array)) b = Buffer.from(b, b.offset, b.byteLength);
    if (!Buffer.isBuffer(a) || !Buffer.isBuffer(b)) {
      throw new TypeError('The "buf1", "buf2" arguments must be one of type Buffer or Uint8Array');
    }
    if (a === b) return 0;
    var x = a.length;
    var y = b.length;
    for (var i = 0, len = Math.min(x, y); i < len; ++i) {
      if (a[i] !== b[i]) {
        x = a[i];
        y = b[i];
        break;
      }
    }
    if (x < y) return -1;
    if (y < x) return 1;
    return 0;
  };
  Buffer.isEncoding = function isEncoding(encoding) {
    switch (String(encoding).toLowerCase()) {
      case "hex":
      case "utf8":
      case "utf-8":
      case "ascii":
      case "latin1":
      case "binary":
      case "base64":
      case "ucs2":
      case "ucs-2":
      case "utf16le":
      case "utf-16le":
        return true;
      default:
        return false;
    }
  };
  Buffer.concat = function concat(list, length) {
    if (!Array.isArray(list)) {
      throw new TypeError('"list" argument must be an Array of Buffers');
    }
    if (list.length === 0) {
      return Buffer.alloc(0);
    }
    var i;
    if (length === undefined) {
      length = 0;
      for (i = 0; i < list.length; ++i) {
        length += list[i].length;
      }
    }
    var buffer = Buffer.allocUnsafe(length);
    var pos = 0;
    for (i = 0; i < list.length; ++i) {
      var buf = list[i];
      if (isInstance(buf, Uint8Array)) {
        if (pos + buf.length > buffer.length) {
          if (!Buffer.isBuffer(buf)) {
            buf = Buffer.from(buf.buffer, buf.byteOffset, buf.byteLength);
          }
          buf.copy(buffer, pos);
        } else {
          Uint8Array.prototype.set.call(buffer, buf, pos);
        }
      } else if (!Buffer.isBuffer(buf)) {
        throw new TypeError('"list" argument must be an Array of Buffers');
      } else {
        buf.copy(buffer, pos);
      }
      pos += buf.length;
    }
    return buffer;
  };
  function byteLength(string, encoding) {
    if (Buffer.isBuffer(string)) {
      return string.length;
    }
    if (ArrayBuffer.isView(string) || isInstance(string, ArrayBuffer)) {
      return string.byteLength;
    }
    if (typeof string !== "string") {
      throw new TypeError('The "string" argument must be one of type string, Buffer, or ArrayBuffer. ' + "Received type " + _typeof$2(string));
    }
    var len = string.length;
    var mustMatch = arguments.length > 2 && arguments[2] === true;
    if (!mustMatch && len === 0) return 0;

    // Use a for loop to avoid recursion
    var loweredCase = false;
    for (;;) {
      switch (encoding) {
        case "ascii":
        case "latin1":
        case "binary":
          return len;
        case "utf8":
        case "utf-8":
          return utf8ToBytes(string).length;
        case "ucs2":
        case "ucs-2":
        case "utf16le":
        case "utf-16le":
          return len * 2;
        case "hex":
          return len >>> 1;
        case "base64":
          return base64ToBytes(string).length;
        default:
          if (loweredCase) {
            return mustMatch ? -1 : utf8ToBytes(string).length; // assume utf8
          }

          encoding = ("" + encoding).toLowerCase();
          loweredCase = true;
      }
    }
  }
  Buffer.byteLength = byteLength;
  function slowToString(encoding, start, end) {
    var loweredCase = false;

    // No need to verify that "this.length <= MAX_UINT32" since it's a read-only
    // property of a typed array.

    // This behaves neither like String nor Uint8Array in that we set start/end
    // to their upper/lower bounds if the value passed is out of range.
    // undefined is handled specially as per ECMA-262 6th Edition,
    // Section 13.3.3.7 Runtime Semantics: KeyedBindingInitialization.
    if (start === undefined || start < 0) {
      start = 0;
    }
    // Return early if start > this.length. Done here to prevent potential uint32
    // coercion fail below.
    if (start > this.length) {
      return "";
    }
    if (end === undefined || end > this.length) {
      end = this.length;
    }
    if (end <= 0) {
      return "";
    }

    // Force coercion to uint32. This will also coerce falsey/NaN values to 0.
    end >>>= 0;
    start >>>= 0;
    if (end <= start) {
      return "";
    }
    if (!encoding) encoding = "utf8";
    while (true) {
      switch (encoding) {
        case "hex":
          return hexSlice(this, start, end);
        case "utf8":
        case "utf-8":
          return utf8Slice(this, start, end);
        case "ascii":
          return asciiSlice(this, start, end);
        case "latin1":
        case "binary":
          return latin1Slice(this, start, end);
        case "base64":
          return base64Slice(this, start, end);
        case "ucs2":
        case "ucs-2":
        case "utf16le":
        case "utf-16le":
          return utf16leSlice(this, start, end);
        default:
          if (loweredCase) throw new TypeError("Unknown encoding: " + encoding);
          encoding = (encoding + "").toLowerCase();
          loweredCase = true;
      }
    }
  }

  // This property is used by `Buffer.isBuffer` (and the `is-buffer` npm package)
  // to detect a Buffer instance. It's not possible to use `instanceof Buffer`
  // reliably in a browserify context because there could be multiple different
  // copies of the 'buffer' package in use. This method works even for Buffer
  // instances that were created from another copy of the `buffer` package.
  // See: https://github.com/feross/buffer/issues/154
  Buffer.prototype._isBuffer = true;
  function swap(b, n, m) {
    var i = b[n];
    b[n] = b[m];
    b[m] = i;
  }
  Buffer.prototype.swap16 = function swap16() {
    var len = this.length;
    if (len % 2 !== 0) {
      throw new RangeError("Buffer size must be a multiple of 16-bits");
    }
    for (var i = 0; i < len; i += 2) {
      swap(this, i, i + 1);
    }
    return this;
  };
  Buffer.prototype.swap32 = function swap32() {
    var len = this.length;
    if (len % 4 !== 0) {
      throw new RangeError("Buffer size must be a multiple of 32-bits");
    }
    for (var i = 0; i < len; i += 4) {
      swap(this, i, i + 3);
      swap(this, i + 1, i + 2);
    }
    return this;
  };
  Buffer.prototype.swap64 = function swap64() {
    var len = this.length;
    if (len % 8 !== 0) {
      throw new RangeError("Buffer size must be a multiple of 64-bits");
    }
    for (var i = 0; i < len; i += 8) {
      swap(this, i, i + 7);
      swap(this, i + 1, i + 6);
      swap(this, i + 2, i + 5);
      swap(this, i + 3, i + 4);
    }
    return this;
  };
  Buffer.prototype.toString = function toString() {
    var length = this.length;
    if (length === 0) return "";
    if (arguments.length === 0) return utf8Slice(this, 0, length);
    return slowToString.apply(this, arguments);
  };
  Buffer.prototype.toLocaleString = Buffer.prototype.toString;
  Buffer.prototype.equals = function equals(b) {
    if (!Buffer.isBuffer(b)) throw new TypeError("Argument must be a Buffer");
    if (this === b) return true;
    return Buffer.compare(this, b) === 0;
  };
  Buffer.prototype.inspect = function inspect() {
    var str = "";
    var max = exports.INSPECT_MAX_BYTES;
    str = this.toString("hex", 0, max).replace(/(.{2})/g, "$1 ").trim();
    if (this.length > max) str += " ... ";
    return "<Buffer " + str + ">";
  };
  if (customInspectSymbol) {
    Buffer.prototype[customInspectSymbol] = Buffer.prototype.inspect;
  }
  Buffer.prototype.compare = function compare(target, start, end, thisStart, thisEnd) {
    if (isInstance(target, Uint8Array)) {
      target = Buffer.from(target, target.offset, target.byteLength);
    }
    if (!Buffer.isBuffer(target)) {
      throw new TypeError('The "target" argument must be one of type Buffer or Uint8Array. ' + "Received type " + _typeof$2(target));
    }
    if (start === undefined) {
      start = 0;
    }
    if (end === undefined) {
      end = target ? target.length : 0;
    }
    if (thisStart === undefined) {
      thisStart = 0;
    }
    if (thisEnd === undefined) {
      thisEnd = this.length;
    }
    if (start < 0 || end > target.length || thisStart < 0 || thisEnd > this.length) {
      throw new RangeError("out of range index");
    }
    if (thisStart >= thisEnd && start >= end) {
      return 0;
    }
    if (thisStart >= thisEnd) {
      return -1;
    }
    if (start >= end) {
      return 1;
    }
    start >>>= 0;
    end >>>= 0;
    thisStart >>>= 0;
    thisEnd >>>= 0;
    if (this === target) return 0;
    var x = thisEnd - thisStart;
    var y = end - start;
    var len = Math.min(x, y);
    var thisCopy = this.slice(thisStart, thisEnd);
    var targetCopy = target.slice(start, end);
    for (var i = 0; i < len; ++i) {
      if (thisCopy[i] !== targetCopy[i]) {
        x = thisCopy[i];
        y = targetCopy[i];
        break;
      }
    }
    if (x < y) return -1;
    if (y < x) return 1;
    return 0;
  };

  // Finds either the first index of `val` in `buffer` at offset >= `byteOffset`,
  // OR the last index of `val` in `buffer` at offset <= `byteOffset`.
  //
  // Arguments:
  // - buffer - a Buffer to search
  // - val - a string, Buffer, or number
  // - byteOffset - an index into `buffer`; will be clamped to an int32
  // - encoding - an optional encoding, relevant is val is a string
  // - dir - true for indexOf, false for lastIndexOf
  function bidirectionalIndexOf(buffer, val, byteOffset, encoding, dir) {
    // Empty buffer means no match
    if (buffer.length === 0) return -1;

    // Normalize byteOffset
    if (typeof byteOffset === "string") {
      encoding = byteOffset;
      byteOffset = 0;
    } else if (byteOffset > 0x7fffffff) {
      byteOffset = 0x7fffffff;
    } else if (byteOffset < -0x80000000) {
      byteOffset = -0x80000000;
    }
    byteOffset = +byteOffset; // Coerce to Number.
    if (numberIsNaN(byteOffset)) {
      // byteOffset: it it's undefined, null, NaN, "foo", etc, search whole buffer
      byteOffset = dir ? 0 : buffer.length - 1;
    }

    // Normalize byteOffset: negative offsets start from the end of the buffer
    if (byteOffset < 0) byteOffset = buffer.length + byteOffset;
    if (byteOffset >= buffer.length) {
      if (dir) return -1;else byteOffset = buffer.length - 1;
    } else if (byteOffset < 0) {
      if (dir) byteOffset = 0;else return -1;
    }

    // Normalize val
    if (typeof val === "string") {
      val = Buffer.from(val, encoding);
    }

    // Finally, search either indexOf (if dir is true) or lastIndexOf
    if (Buffer.isBuffer(val)) {
      // Special case: looking for empty string/buffer always fails
      if (val.length === 0) {
        return -1;
      }
      return arrayIndexOf(buffer, val, byteOffset, encoding, dir);
    } else if (typeof val === "number") {
      val = val & 0xff; // Search for a byte value [0-255]
      if (typeof Uint8Array.prototype.indexOf === "function") {
        if (dir) {
          return Uint8Array.prototype.indexOf.call(buffer, val, byteOffset);
        } else {
          return Uint8Array.prototype.lastIndexOf.call(buffer, val, byteOffset);
        }
      }
      return arrayIndexOf(buffer, [val], byteOffset, encoding, dir);
    }
    throw new TypeError("val must be string, number or Buffer");
  }
  function arrayIndexOf(arr, val, byteOffset, encoding, dir) {
    var indexSize = 1;
    var arrLength = arr.length;
    var valLength = val.length;
    if (encoding !== undefined) {
      encoding = String(encoding).toLowerCase();
      if (encoding === "ucs2" || encoding === "ucs-2" || encoding === "utf16le" || encoding === "utf-16le") {
        if (arr.length < 2 || val.length < 2) {
          return -1;
        }
        indexSize = 2;
        arrLength /= 2;
        valLength /= 2;
        byteOffset /= 2;
      }
    }
    function read(buf, i) {
      if (indexSize === 1) {
        return buf[i];
      } else {
        return buf.readUInt16BE(i * indexSize);
      }
    }
    var i;
    if (dir) {
      var foundIndex = -1;
      for (i = byteOffset; i < arrLength; i++) {
        if (read(arr, i) === read(val, foundIndex === -1 ? 0 : i - foundIndex)) {
          if (foundIndex === -1) foundIndex = i;
          if (i - foundIndex + 1 === valLength) return foundIndex * indexSize;
        } else {
          if (foundIndex !== -1) i -= i - foundIndex;
          foundIndex = -1;
        }
      }
    } else {
      if (byteOffset + valLength > arrLength) byteOffset = arrLength - valLength;
      for (i = byteOffset; i >= 0; i--) {
        var found = true;
        for (var j = 0; j < valLength; j++) {
          if (read(arr, i + j) !== read(val, j)) {
            found = false;
            break;
          }
        }
        if (found) return i;
      }
    }
    return -1;
  }
  Buffer.prototype.includes = function includes(val, byteOffset, encoding) {
    return this.indexOf(val, byteOffset, encoding) !== -1;
  };
  Buffer.prototype.indexOf = function indexOf(val, byteOffset, encoding) {
    return bidirectionalIndexOf(this, val, byteOffset, encoding, true);
  };
  Buffer.prototype.lastIndexOf = function lastIndexOf(val, byteOffset, encoding) {
    return bidirectionalIndexOf(this, val, byteOffset, encoding, false);
  };
  function hexWrite(buf, string, offset, length) {
    offset = Number(offset) || 0;
    var remaining = buf.length - offset;
    if (!length) {
      length = remaining;
    } else {
      length = Number(length);
      if (length > remaining) {
        length = remaining;
      }
    }
    var strLen = string.length;
    if (length > strLen / 2) {
      length = strLen / 2;
    }
    var i;
    for (i = 0; i < length; ++i) {
      var parsed = parseInt(string.substr(i * 2, 2), 16);
      if (numberIsNaN(parsed)) return i;
      buf[offset + i] = parsed;
    }
    return i;
  }
  function utf8Write(buf, string, offset, length) {
    return blitBuffer(utf8ToBytes(string, buf.length - offset), buf, offset, length);
  }
  function asciiWrite(buf, string, offset, length) {
    return blitBuffer(asciiToBytes(string), buf, offset, length);
  }
  function base64Write(buf, string, offset, length) {
    return blitBuffer(base64ToBytes(string), buf, offset, length);
  }
  function ucs2Write(buf, string, offset, length) {
    return blitBuffer(utf16leToBytes(string, buf.length - offset), buf, offset, length);
  }
  Buffer.prototype.write = function write(string, offset, length, encoding) {
    // Buffer#write(string)
    if (offset === undefined) {
      encoding = "utf8";
      length = this.length;
      offset = 0;
      // Buffer#write(string, encoding)
    } else if (length === undefined && typeof offset === "string") {
      encoding = offset;
      length = this.length;
      offset = 0;
      // Buffer#write(string, offset[, length][, encoding])
    } else if (isFinite(offset)) {
      offset = offset >>> 0;
      if (isFinite(length)) {
        length = length >>> 0;
        if (encoding === undefined) encoding = "utf8";
      } else {
        encoding = length;
        length = undefined;
      }
    } else {
      throw new Error("Buffer.write(string, encoding, offset[, length]) is no longer supported");
    }
    var remaining = this.length - offset;
    if (length === undefined || length > remaining) length = remaining;
    if (string.length > 0 && (length < 0 || offset < 0) || offset > this.length) {
      throw new RangeError("Attempt to write outside buffer bounds");
    }
    if (!encoding) encoding = "utf8";
    var loweredCase = false;
    for (;;) {
      switch (encoding) {
        case "hex":
          return hexWrite(this, string, offset, length);
        case "utf8":
        case "utf-8":
          return utf8Write(this, string, offset, length);
        case "ascii":
        case "latin1":
        case "binary":
          return asciiWrite(this, string, offset, length);
        case "base64":
          // Warning: maxLength not taken into account in base64Write
          return base64Write(this, string, offset, length);
        case "ucs2":
        case "ucs-2":
        case "utf16le":
        case "utf-16le":
          return ucs2Write(this, string, offset, length);
        default:
          if (loweredCase) throw new TypeError("Unknown encoding: " + encoding);
          encoding = ("" + encoding).toLowerCase();
          loweredCase = true;
      }
    }
  };
  Buffer.prototype.toJSON = function toJSON() {
    return {
      type: "Buffer",
      data: Array.prototype.slice.call(this._arr || this, 0)
    };
  };
  function base64Slice(buf, start, end) {
    if (start === 0 && end === buf.length) {
      return base64.fromByteArray(buf);
    } else {
      return base64.fromByteArray(buf.slice(start, end));
    }
  }
  function utf8Slice(buf, start, end) {
    end = Math.min(buf.length, end);
    var res = [];
    var i = start;
    while (i < end) {
      var firstByte = buf[i];
      var codePoint = null;
      var bytesPerSequence = firstByte > 0xef ? 4 : firstByte > 0xdf ? 3 : firstByte > 0xbf ? 2 : 1;
      if (i + bytesPerSequence <= end) {
        var secondByte = void 0,
          thirdByte = void 0,
          fourthByte = void 0,
          tempCodePoint = void 0;
        switch (bytesPerSequence) {
          case 1:
            if (firstByte < 0x80) {
              codePoint = firstByte;
            }
            break;
          case 2:
            secondByte = buf[i + 1];
            if ((secondByte & 0xc0) === 0x80) {
              tempCodePoint = (firstByte & 0x1f) << 0x6 | secondByte & 0x3f;
              if (tempCodePoint > 0x7f) {
                codePoint = tempCodePoint;
              }
            }
            break;
          case 3:
            secondByte = buf[i + 1];
            thirdByte = buf[i + 2];
            if ((secondByte & 0xc0) === 0x80 && (thirdByte & 0xc0) === 0x80) {
              tempCodePoint = (firstByte & 0xf) << 0xc | (secondByte & 0x3f) << 0x6 | thirdByte & 0x3f;
              if (tempCodePoint > 0x7ff && (tempCodePoint < 0xd800 || tempCodePoint > 0xdfff)) {
                codePoint = tempCodePoint;
              }
            }
            break;
          case 4:
            secondByte = buf[i + 1];
            thirdByte = buf[i + 2];
            fourthByte = buf[i + 3];
            if ((secondByte & 0xc0) === 0x80 && (thirdByte & 0xc0) === 0x80 && (fourthByte & 0xc0) === 0x80) {
              tempCodePoint = (firstByte & 0xf) << 0x12 | (secondByte & 0x3f) << 0xc | (thirdByte & 0x3f) << 0x6 | fourthByte & 0x3f;
              if (tempCodePoint > 0xffff && tempCodePoint < 0x110000) {
                codePoint = tempCodePoint;
              }
            }
        }
      }
      if (codePoint === null) {
        // we did not generate a valid codePoint so insert a
        // replacement char (U+FFFD) and advance only 1 byte
        codePoint = 0xfffd;
        bytesPerSequence = 1;
      } else if (codePoint > 0xffff) {
        // encode to utf16 (surrogate pair dance)
        codePoint -= 0x10000;
        res.push(codePoint >>> 10 & 0x3ff | 0xd800);
        codePoint = 0xdc00 | codePoint & 0x3ff;
      }
      res.push(codePoint);
      i += bytesPerSequence;
    }
    return decodeCodePointsArray(res);
  }

  // Based on http://stackoverflow.com/a/22747272/680742, the browser with
  // the lowest limit is Chrome, with 0x10000 args.
  // We go 1 magnitude less, for safety
  var MAX_ARGUMENTS_LENGTH = 0x1000;
  function decodeCodePointsArray(codePoints) {
    var len = codePoints.length;
    if (len <= MAX_ARGUMENTS_LENGTH) {
      return String.fromCharCode.apply(String, codePoints); // avoid extra slice()
    }

    // Decode in chunks to avoid "call stack size exceeded".
    var res = "";
    var i = 0;
    while (i < len) {
      res += String.fromCharCode.apply(String, codePoints.slice(i, i += MAX_ARGUMENTS_LENGTH));
    }
    return res;
  }
  function asciiSlice(buf, start, end) {
    var ret = "";
    end = Math.min(buf.length, end);
    for (var i = start; i < end; ++i) {
      ret += String.fromCharCode(buf[i] & 0x7f);
    }
    return ret;
  }
  function latin1Slice(buf, start, end) {
    var ret = "";
    end = Math.min(buf.length, end);
    for (var i = start; i < end; ++i) {
      ret += String.fromCharCode(buf[i]);
    }
    return ret;
  }
  function hexSlice(buf, start, end) {
    var len = buf.length;
    if (!start || start < 0) start = 0;
    if (!end || end < 0 || end > len) end = len;
    var out = "";
    for (var i = start; i < end; ++i) {
      out += hexSliceLookupTable[buf[i]];
    }
    return out;
  }
  function utf16leSlice(buf, start, end) {
    var bytes = buf.slice(start, end);
    var res = "";
    // If bytes.length is odd, the last 8 bits must be ignored (same as node.js)
    for (var i = 0; i < bytes.length - 1; i += 2) {
      res += String.fromCharCode(bytes[i] + bytes[i + 1] * 256);
    }
    return res;
  }
  Buffer.prototype.slice = function slice(start, end) {
    var len = this.length;
    start = ~~start;
    end = end === undefined ? len : ~~end;
    if (start < 0) {
      start += len;
      if (start < 0) start = 0;
    } else if (start > len) {
      start = len;
    }
    if (end < 0) {
      end += len;
      if (end < 0) end = 0;
    } else if (end > len) {
      end = len;
    }
    if (end < start) end = start;
    var newBuf = this.subarray(start, end);
    // Return an augmented `Uint8Array` instance
    Object.setPrototypeOf(newBuf, Buffer.prototype);
    return newBuf;
  };

  /*
   * Need to make sure that buffer isn't trying to write out of bounds.
   */
  function checkOffset(offset, ext, length) {
    if (offset % 1 !== 0 || offset < 0) throw new RangeError("offset is not uint");
    if (offset + ext > length) throw new RangeError("Trying to access beyond buffer length");
  }
  Buffer.prototype.readUintLE = Buffer.prototype.readUIntLE = function readUIntLE(offset, byteLength, noAssert) {
    offset = offset >>> 0;
    byteLength = byteLength >>> 0;
    if (!noAssert) checkOffset(offset, byteLength, this.length);
    var val = this[offset];
    var mul = 1;
    var i = 0;
    while (++i < byteLength && (mul *= 0x100)) {
      val += this[offset + i] * mul;
    }
    return val;
  };
  Buffer.prototype.readUintBE = Buffer.prototype.readUIntBE = function readUIntBE(offset, byteLength, noAssert) {
    offset = offset >>> 0;
    byteLength = byteLength >>> 0;
    if (!noAssert) {
      checkOffset(offset, byteLength, this.length);
    }
    var val = this[offset + --byteLength];
    var mul = 1;
    while (byteLength > 0 && (mul *= 0x100)) {
      val += this[offset + --byteLength] * mul;
    }
    return val;
  };
  Buffer.prototype.readUint8 = Buffer.prototype.readUInt8 = function readUInt8(offset, noAssert) {
    offset = offset >>> 0;
    if (!noAssert) checkOffset(offset, 1, this.length);
    return this[offset];
  };
  Buffer.prototype.readUint16LE = Buffer.prototype.readUInt16LE = function readUInt16LE(offset, noAssert) {
    offset = offset >>> 0;
    if (!noAssert) checkOffset(offset, 2, this.length);
    return this[offset] | this[offset + 1] << 8;
  };
  Buffer.prototype.readUint16BE = Buffer.prototype.readUInt16BE = function readUInt16BE(offset, noAssert) {
    offset = offset >>> 0;
    if (!noAssert) checkOffset(offset, 2, this.length);
    return this[offset] << 8 | this[offset + 1];
  };
  Buffer.prototype.readUint32LE = Buffer.prototype.readUInt32LE = function readUInt32LE(offset, noAssert) {
    offset = offset >>> 0;
    if (!noAssert) checkOffset(offset, 4, this.length);
    return (this[offset] | this[offset + 1] << 8 | this[offset + 2] << 16) + this[offset + 3] * 0x1000000;
  };
  Buffer.prototype.readUint32BE = Buffer.prototype.readUInt32BE = function readUInt32BE(offset, noAssert) {
    offset = offset >>> 0;
    if (!noAssert) checkOffset(offset, 4, this.length);
    return this[offset] * 0x1000000 + (this[offset + 1] << 16 | this[offset + 2] << 8 | this[offset + 3]);
  };
  Buffer.prototype.readBigUInt64LE = defineBigIntMethod(function readBigUInt64LE(offset) {
    offset = offset >>> 0;
    validateNumber(offset, "offset");
    var first = this[offset];
    var last = this[offset + 7];
    if (first === undefined || last === undefined) {
      boundsError(offset, this.length - 8);
    }
    var lo = first + this[++offset] * Math.pow(2, 8) + this[++offset] * Math.pow(2, 16) + this[++offset] * Math.pow(2, 24);
    var hi = this[++offset] + this[++offset] * Math.pow(2, 8) + this[++offset] * Math.pow(2, 16) + last * Math.pow(2, 24);
    return BigInt(lo) + (BigInt(hi) << BigInt(32));
  });
  Buffer.prototype.readBigUInt64BE = defineBigIntMethod(function readBigUInt64BE(offset) {
    offset = offset >>> 0;
    validateNumber(offset, "offset");
    var first = this[offset];
    var last = this[offset + 7];
    if (first === undefined || last === undefined) {
      boundsError(offset, this.length - 8);
    }
    var hi = first * Math.pow(2, 24) + this[++offset] * Math.pow(2, 16) + this[++offset] * Math.pow(2, 8) + this[++offset];
    var lo = this[++offset] * Math.pow(2, 24) + this[++offset] * Math.pow(2, 16) + this[++offset] * Math.pow(2, 8) + last;
    return (BigInt(hi) << BigInt(32)) + BigInt(lo);
  });
  Buffer.prototype.readIntLE = function readIntLE(offset, byteLength, noAssert) {
    offset = offset >>> 0;
    byteLength = byteLength >>> 0;
    if (!noAssert) checkOffset(offset, byteLength, this.length);
    var val = this[offset];
    var mul = 1;
    var i = 0;
    while (++i < byteLength && (mul *= 0x100)) {
      val += this[offset + i] * mul;
    }
    mul *= 0x80;
    if (val >= mul) val -= Math.pow(2, 8 * byteLength);
    return val;
  };
  Buffer.prototype.readIntBE = function readIntBE(offset, byteLength, noAssert) {
    offset = offset >>> 0;
    byteLength = byteLength >>> 0;
    if (!noAssert) checkOffset(offset, byteLength, this.length);
    var i = byteLength;
    var mul = 1;
    var val = this[offset + --i];
    while (i > 0 && (mul *= 0x100)) {
      val += this[offset + --i] * mul;
    }
    mul *= 0x80;
    if (val >= mul) val -= Math.pow(2, 8 * byteLength);
    return val;
  };
  Buffer.prototype.readInt8 = function readInt8(offset, noAssert) {
    offset = offset >>> 0;
    if (!noAssert) checkOffset(offset, 1, this.length);
    if (!(this[offset] & 0x80)) return this[offset];
    return (0xff - this[offset] + 1) * -1;
  };
  Buffer.prototype.readInt16LE = function readInt16LE(offset, noAssert) {
    offset = offset >>> 0;
    if (!noAssert) checkOffset(offset, 2, this.length);
    var val = this[offset] | this[offset + 1] << 8;
    return val & 0x8000 ? val | 0xffff0000 : val;
  };
  Buffer.prototype.readInt16BE = function readInt16BE(offset, noAssert) {
    offset = offset >>> 0;
    if (!noAssert) checkOffset(offset, 2, this.length);
    var val = this[offset + 1] | this[offset] << 8;
    return val & 0x8000 ? val | 0xffff0000 : val;
  };
  Buffer.prototype.readInt32LE = function readInt32LE(offset, noAssert) {
    offset = offset >>> 0;
    if (!noAssert) checkOffset(offset, 4, this.length);
    return this[offset] | this[offset + 1] << 8 | this[offset + 2] << 16 | this[offset + 3] << 24;
  };
  Buffer.prototype.readInt32BE = function readInt32BE(offset, noAssert) {
    offset = offset >>> 0;
    if (!noAssert) checkOffset(offset, 4, this.length);
    return this[offset] << 24 | this[offset + 1] << 16 | this[offset + 2] << 8 | this[offset + 3];
  };
  Buffer.prototype.readBigInt64LE = defineBigIntMethod(function readBigInt64LE(offset) {
    offset = offset >>> 0;
    validateNumber(offset, "offset");
    var first = this[offset];
    var last = this[offset + 7];
    if (first === undefined || last === undefined) {
      boundsError(offset, this.length - 8);
    }
    var val = this[offset + 4] + this[offset + 5] * Math.pow(2, 8) + this[offset + 6] * Math.pow(2, 16) + (last << 24); // Overflow

    return (BigInt(val) << BigInt(32)) + BigInt(first + this[++offset] * Math.pow(2, 8) + this[++offset] * Math.pow(2, 16) + this[++offset] * Math.pow(2, 24));
  });
  Buffer.prototype.readBigInt64BE = defineBigIntMethod(function readBigInt64BE(offset) {
    offset = offset >>> 0;
    validateNumber(offset, "offset");
    var first = this[offset];
    var last = this[offset + 7];
    if (first === undefined || last === undefined) {
      boundsError(offset, this.length - 8);
    }
    var val = (first << 24) +
    // Overflow
    this[++offset] * Math.pow(2, 16) + this[++offset] * Math.pow(2, 8) + this[++offset];
    return (BigInt(val) << BigInt(32)) + BigInt(this[++offset] * Math.pow(2, 24) + this[++offset] * Math.pow(2, 16) + this[++offset] * Math.pow(2, 8) + last);
  });
  Buffer.prototype.readFloatLE = function readFloatLE(offset, noAssert) {
    offset = offset >>> 0;
    if (!noAssert) checkOffset(offset, 4, this.length);
    return ieee754$1.read(this, offset, true, 23, 4);
  };
  Buffer.prototype.readFloatBE = function readFloatBE(offset, noAssert) {
    offset = offset >>> 0;
    if (!noAssert) checkOffset(offset, 4, this.length);
    return ieee754$1.read(this, offset, false, 23, 4);
  };
  Buffer.prototype.readDoubleLE = function readDoubleLE(offset, noAssert) {
    offset = offset >>> 0;
    if (!noAssert) checkOffset(offset, 8, this.length);
    return ieee754$1.read(this, offset, true, 52, 8);
  };
  Buffer.prototype.readDoubleBE = function readDoubleBE(offset, noAssert) {
    offset = offset >>> 0;
    if (!noAssert) checkOffset(offset, 8, this.length);
    return ieee754$1.read(this, offset, false, 52, 8);
  };
  function checkInt(buf, value, offset, ext, max, min) {
    if (!Buffer.isBuffer(buf)) throw new TypeError('"buffer" argument must be a Buffer instance');
    if (value > max || value < min) throw new RangeError('"value" argument is out of bounds');
    if (offset + ext > buf.length) throw new RangeError("Index out of range");
  }
  Buffer.prototype.writeUintLE = Buffer.prototype.writeUIntLE = function writeUIntLE(value, offset, byteLength, noAssert) {
    value = +value;
    offset = offset >>> 0;
    byteLength = byteLength >>> 0;
    if (!noAssert) {
      var maxBytes = Math.pow(2, 8 * byteLength) - 1;
      checkInt(this, value, offset, byteLength, maxBytes, 0);
    }
    var mul = 1;
    var i = 0;
    this[offset] = value & 0xff;
    while (++i < byteLength && (mul *= 0x100)) {
      this[offset + i] = value / mul & 0xff;
    }
    return offset + byteLength;
  };
  Buffer.prototype.writeUintBE = Buffer.prototype.writeUIntBE = function writeUIntBE(value, offset, byteLength, noAssert) {
    value = +value;
    offset = offset >>> 0;
    byteLength = byteLength >>> 0;
    if (!noAssert) {
      var maxBytes = Math.pow(2, 8 * byteLength) - 1;
      checkInt(this, value, offset, byteLength, maxBytes, 0);
    }
    var i = byteLength - 1;
    var mul = 1;
    this[offset + i] = value & 0xff;
    while (--i >= 0 && (mul *= 0x100)) {
      this[offset + i] = value / mul & 0xff;
    }
    return offset + byteLength;
  };
  Buffer.prototype.writeUint8 = Buffer.prototype.writeUInt8 = function writeUInt8(value, offset, noAssert) {
    value = +value;
    offset = offset >>> 0;
    if (!noAssert) checkInt(this, value, offset, 1, 0xff, 0);
    this[offset] = value & 0xff;
    return offset + 1;
  };
  Buffer.prototype.writeUint16LE = Buffer.prototype.writeUInt16LE = function writeUInt16LE(value, offset, noAssert) {
    value = +value;
    offset = offset >>> 0;
    if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0);
    this[offset] = value & 0xff;
    this[offset + 1] = value >>> 8;
    return offset + 2;
  };
  Buffer.prototype.writeUint16BE = Buffer.prototype.writeUInt16BE = function writeUInt16BE(value, offset, noAssert) {
    value = +value;
    offset = offset >>> 0;
    if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0);
    this[offset] = value >>> 8;
    this[offset + 1] = value & 0xff;
    return offset + 2;
  };
  Buffer.prototype.writeUint32LE = Buffer.prototype.writeUInt32LE = function writeUInt32LE(value, offset, noAssert) {
    value = +value;
    offset = offset >>> 0;
    if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0);
    this[offset + 3] = value >>> 24;
    this[offset + 2] = value >>> 16;
    this[offset + 1] = value >>> 8;
    this[offset] = value & 0xff;
    return offset + 4;
  };
  Buffer.prototype.writeUint32BE = Buffer.prototype.writeUInt32BE = function writeUInt32BE(value, offset, noAssert) {
    value = +value;
    offset = offset >>> 0;
    if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0);
    this[offset] = value >>> 24;
    this[offset + 1] = value >>> 16;
    this[offset + 2] = value >>> 8;
    this[offset + 3] = value & 0xff;
    return offset + 4;
  };
  function wrtBigUInt64LE(buf, value, offset, min, max) {
    checkIntBI(value, min, max, buf, offset, 7);
    var lo = Number(value & BigInt(0xffffffff));
    buf[offset++] = lo;
    lo = lo >> 8;
    buf[offset++] = lo;
    lo = lo >> 8;
    buf[offset++] = lo;
    lo = lo >> 8;
    buf[offset++] = lo;
    var hi = Number(value >> BigInt(32) & BigInt(0xffffffff));
    buf[offset++] = hi;
    hi = hi >> 8;
    buf[offset++] = hi;
    hi = hi >> 8;
    buf[offset++] = hi;
    hi = hi >> 8;
    buf[offset++] = hi;
    return offset;
  }
  function wrtBigUInt64BE(buf, value, offset, min, max) {
    checkIntBI(value, min, max, buf, offset, 7);
    var lo = Number(value & BigInt(0xffffffff));
    buf[offset + 7] = lo;
    lo = lo >> 8;
    buf[offset + 6] = lo;
    lo = lo >> 8;
    buf[offset + 5] = lo;
    lo = lo >> 8;
    buf[offset + 4] = lo;
    var hi = Number(value >> BigInt(32) & BigInt(0xffffffff));
    buf[offset + 3] = hi;
    hi = hi >> 8;
    buf[offset + 2] = hi;
    hi = hi >> 8;
    buf[offset + 1] = hi;
    hi = hi >> 8;
    buf[offset] = hi;
    return offset + 8;
  }
  Buffer.prototype.writeBigUInt64LE = defineBigIntMethod(function writeBigUInt64LE(value) {
    var offset = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
    return wrtBigUInt64LE(this, value, offset, BigInt(0), BigInt("0xffffffffffffffff"));
  });
  Buffer.prototype.writeBigUInt64BE = defineBigIntMethod(function writeBigUInt64BE(value) {
    var offset = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
    return wrtBigUInt64BE(this, value, offset, BigInt(0), BigInt("0xffffffffffffffff"));
  });
  Buffer.prototype.writeIntLE = function writeIntLE(value, offset, byteLength, noAssert) {
    value = +value;
    offset = offset >>> 0;
    if (!noAssert) {
      var limit = Math.pow(2, 8 * byteLength - 1);
      checkInt(this, value, offset, byteLength, limit - 1, -limit);
    }
    var i = 0;
    var mul = 1;
    var sub = 0;
    this[offset] = value & 0xff;
    while (++i < byteLength && (mul *= 0x100)) {
      if (value < 0 && sub === 0 && this[offset + i - 1] !== 0) {
        sub = 1;
      }
      this[offset + i] = (value / mul >> 0) - sub & 0xff;
    }
    return offset + byteLength;
  };
  Buffer.prototype.writeIntBE = function writeIntBE(value, offset, byteLength, noAssert) {
    value = +value;
    offset = offset >>> 0;
    if (!noAssert) {
      var limit = Math.pow(2, 8 * byteLength - 1);
      checkInt(this, value, offset, byteLength, limit - 1, -limit);
    }
    var i = byteLength - 1;
    var mul = 1;
    var sub = 0;
    this[offset + i] = value & 0xff;
    while (--i >= 0 && (mul *= 0x100)) {
      if (value < 0 && sub === 0 && this[offset + i + 1] !== 0) {
        sub = 1;
      }
      this[offset + i] = (value / mul >> 0) - sub & 0xff;
    }
    return offset + byteLength;
  };
  Buffer.prototype.writeInt8 = function writeInt8(value, offset, noAssert) {
    value = +value;
    offset = offset >>> 0;
    if (!noAssert) checkInt(this, value, offset, 1, 0x7f, -0x80);
    if (value < 0) value = 0xff + value + 1;
    this[offset] = value & 0xff;
    return offset + 1;
  };
  Buffer.prototype.writeInt16LE = function writeInt16LE(value, offset, noAssert) {
    value = +value;
    offset = offset >>> 0;
    if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000);
    this[offset] = value & 0xff;
    this[offset + 1] = value >>> 8;
    return offset + 2;
  };
  Buffer.prototype.writeInt16BE = function writeInt16BE(value, offset, noAssert) {
    value = +value;
    offset = offset >>> 0;
    if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000);
    this[offset] = value >>> 8;
    this[offset + 1] = value & 0xff;
    return offset + 2;
  };
  Buffer.prototype.writeInt32LE = function writeInt32LE(value, offset, noAssert) {
    value = +value;
    offset = offset >>> 0;
    if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000);
    this[offset] = value & 0xff;
    this[offset + 1] = value >>> 8;
    this[offset + 2] = value >>> 16;
    this[offset + 3] = value >>> 24;
    return offset + 4;
  };
  Buffer.prototype.writeInt32BE = function writeInt32BE(value, offset, noAssert) {
    value = +value;
    offset = offset >>> 0;
    if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000);
    if (value < 0) value = 0xffffffff + value + 1;
    this[offset] = value >>> 24;
    this[offset + 1] = value >>> 16;
    this[offset + 2] = value >>> 8;
    this[offset + 3] = value & 0xff;
    return offset + 4;
  };
  Buffer.prototype.writeBigInt64LE = defineBigIntMethod(function writeBigInt64LE(value) {
    var offset = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
    return wrtBigUInt64LE(this, value, offset, -BigInt("0x8000000000000000"), BigInt("0x7fffffffffffffff"));
  });
  Buffer.prototype.writeBigInt64BE = defineBigIntMethod(function writeBigInt64BE(value) {
    var offset = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
    return wrtBigUInt64BE(this, value, offset, -BigInt("0x8000000000000000"), BigInt("0x7fffffffffffffff"));
  });
  function checkIEEE754(buf, value, offset, ext, max, min) {
    if (offset + ext > buf.length) throw new RangeError("Index out of range");
    if (offset < 0) throw new RangeError("Index out of range");
  }
  function writeFloat(buf, value, offset, littleEndian, noAssert) {
    value = +value;
    offset = offset >>> 0;
    if (!noAssert) {
      checkIEEE754(buf, value, offset, 4);
    }
    ieee754$1.write(buf, value, offset, littleEndian, 23, 4);
    return offset + 4;
  }
  Buffer.prototype.writeFloatLE = function writeFloatLE(value, offset, noAssert) {
    return writeFloat(this, value, offset, true, noAssert);
  };
  Buffer.prototype.writeFloatBE = function writeFloatBE(value, offset, noAssert) {
    return writeFloat(this, value, offset, false, noAssert);
  };
  function writeDouble(buf, value, offset, littleEndian, noAssert) {
    value = +value;
    offset = offset >>> 0;
    if (!noAssert) {
      checkIEEE754(buf, value, offset, 8);
    }
    ieee754$1.write(buf, value, offset, littleEndian, 52, 8);
    return offset + 8;
  }
  Buffer.prototype.writeDoubleLE = function writeDoubleLE(value, offset, noAssert) {
    return writeDouble(this, value, offset, true, noAssert);
  };
  Buffer.prototype.writeDoubleBE = function writeDoubleBE(value, offset, noAssert) {
    return writeDouble(this, value, offset, false, noAssert);
  };

  // copy(targetBuffer, targetStart=0, sourceStart=0, sourceEnd=buffer.length)
  Buffer.prototype.copy = function copy(target, targetStart, start, end) {
    if (!Buffer.isBuffer(target)) throw new TypeError("argument should be a Buffer");
    if (!start) start = 0;
    if (!end && end !== 0) end = this.length;
    if (targetStart >= target.length) targetStart = target.length;
    if (!targetStart) targetStart = 0;
    if (end > 0 && end < start) end = start;

    // Copy 0 bytes; we're done
    if (end === start) return 0;
    if (target.length === 0 || this.length === 0) return 0;

    // Fatal error conditions
    if (targetStart < 0) {
      throw new RangeError("targetStart out of bounds");
    }
    if (start < 0 || start >= this.length) throw new RangeError("Index out of range");
    if (end < 0) throw new RangeError("sourceEnd out of bounds");

    // Are we oob?
    if (end > this.length) end = this.length;
    if (target.length - targetStart < end - start) {
      end = target.length - targetStart + start;
    }
    var len = end - start;
    if (this === target && typeof Uint8Array.prototype.copyWithin === "function") {
      // Use built-in when available, missing from IE11
      this.copyWithin(targetStart, start, end);
    } else {
      Uint8Array.prototype.set.call(target, this.subarray(start, end), targetStart);
    }
    return len;
  };

  // Usage:
  //    buffer.fill(number[, offset[, end]])
  //    buffer.fill(buffer[, offset[, end]])
  //    buffer.fill(string[, offset[, end]][, encoding])
  Buffer.prototype.fill = function fill(val, start, end, encoding) {
    // Handle string cases:
    if (typeof val === "string") {
      if (typeof start === "string") {
        encoding = start;
        start = 0;
        end = this.length;
      } else if (typeof end === "string") {
        encoding = end;
        end = this.length;
      }
      if (encoding !== undefined && typeof encoding !== "string") {
        throw new TypeError("encoding must be a string");
      }
      if (typeof encoding === "string" && !Buffer.isEncoding(encoding)) {
        throw new TypeError("Unknown encoding: " + encoding);
      }
      if (val.length === 1) {
        var code = val.charCodeAt(0);
        if (encoding === "utf8" && code < 128 || encoding === "latin1") {
          // Fast path: If `val` fits into a single byte, use that numeric value.
          val = code;
        }
      }
    } else if (typeof val === "number") {
      val = val & 255;
    } else if (typeof val === "boolean") {
      val = Number(val);
    }

    // Invalid ranges are not set to a default, so can range check early.
    if (start < 0 || this.length < start || this.length < end) {
      throw new RangeError("Out of range index");
    }
    if (end <= start) {
      return this;
    }
    start = start >>> 0;
    end = end === undefined ? this.length : end >>> 0;
    if (!val) val = 0;
    var i;
    if (typeof val === "number") {
      for (i = start; i < end; ++i) {
        this[i] = val;
      }
    } else {
      var bytes = Buffer.isBuffer(val) ? val : Buffer.from(val, encoding);
      var len = bytes.length;
      if (len === 0) {
        throw new TypeError('The value "' + val + '" is invalid for argument "value"');
      }
      for (i = 0; i < end - start; ++i) {
        this[i + start] = bytes[i % len];
      }
    }
    return this;
  };

  // CUSTOM ERRORS
  // =============

  // Simplified versions from Node, changed for Buffer-only usage
  var errors = {};
  function E(sym, getMessage, Base) {
    errors[sym] = /*#__PURE__*/function (_Base) {
      _inherits$1(NodeError, _Base);
      var _super = _createSuper$1(NodeError);
      function NodeError() {
        var _this;
        _classCallCheck$1(this, NodeError);
        _this = _super.call(this);
        Object.defineProperty(_assertThisInitialized$1(_this), "message", {
          value: getMessage.apply(_assertThisInitialized$1(_this), arguments),
          writable: true,
          configurable: true
        });

        // Add the error code to the name to include it in the stack trace.
        _this.name = "".concat(_this.name, " [").concat(sym, "]");
        // Access the stack to generate the error message including the error code
        // from the name.
        _this.stack; // eslint-disable-line no-unused-expressions
        // Reset the name to the actual name.
        delete _this.name;
        return _this;
      }
      _createClass$1(NodeError, [{
        key: "code",
        get: function get() {
          return sym;
        },
        set: function set(value) {
          Object.defineProperty(this, "code", {
            configurable: true,
            enumerable: true,
            value: value,
            writable: true
          });
        }
      }, {
        key: "toString",
        value: function toString() {
          return "".concat(this.name, " [").concat(sym, "]: ").concat(this.message);
        }
      }]);
      return NodeError;
    }(Base);
  }
  E("ERR_BUFFER_OUT_OF_BOUNDS", function (name) {
    if (name) {
      return "".concat(name, " is outside of buffer bounds");
    }
    return "Attempt to access memory outside buffer bounds";
  }, RangeError);
  E("ERR_INVALID_ARG_TYPE", function (name, actual) {
    return "The \"".concat(name, "\" argument must be of type number. Received type ").concat(_typeof$2(actual));
  }, TypeError);
  E("ERR_OUT_OF_RANGE", function (str, range, input) {
    var msg = "The value of \"".concat(str, "\" is out of range.");
    var received = input;
    if (Number.isInteger(input) && Math.abs(input) > Math.pow(2, 32)) {
      received = addNumericalSeparator(String(input));
    } else if (typeof input === "bigint") {
      received = String(input);
      if (input > Math.pow(BigInt(2), BigInt(32)) || input < -Math.pow(BigInt(2), BigInt(32))) {
        received = addNumericalSeparator(received);
      }
      received += "n";
    }
    msg += " It must be ".concat(range, ". Received ").concat(received);
    return msg;
  }, RangeError);
  function addNumericalSeparator(val) {
    var res = "";
    var i = val.length;
    var start = val[0] === "-" ? 1 : 0;
    for (; i >= start + 4; i -= 3) {
      res = "_".concat(val.slice(i - 3, i)).concat(res);
    }
    return "".concat(val.slice(0, i)).concat(res);
  }

  // CHECK FUNCTIONS
  // ===============

  function checkBounds(buf, offset, byteLength) {
    validateNumber(offset, "offset");
    if (buf[offset] === undefined || buf[offset + byteLength] === undefined) {
      boundsError(offset, buf.length - (byteLength + 1));
    }
  }
  function checkIntBI(value, min, max, buf, offset, byteLength) {
    if (value > max || value < min) {
      var n = typeof min === "bigint" ? "n" : "";
      var range;
      if (byteLength > 3) {
        if (min === 0 || min === BigInt(0)) {
          range = ">= 0".concat(n, " and < 2").concat(n, " ** ").concat((byteLength + 1) * 8).concat(n);
        } else {
          range = ">= -(2".concat(n, " ** ").concat((byteLength + 1) * 8 - 1).concat(n, ") and < 2 ** ") + "".concat((byteLength + 1) * 8 - 1).concat(n);
        }
      } else {
        range = ">= ".concat(min).concat(n, " and <= ").concat(max).concat(n);
      }
      throw new errors.ERR_OUT_OF_RANGE("value", range, value);
    }
    checkBounds(buf, offset, byteLength);
  }
  function validateNumber(value, name) {
    if (typeof value !== "number") {
      throw new errors.ERR_INVALID_ARG_TYPE(name, "number", value);
    }
  }
  function boundsError(value, length, type) {
    if (Math.floor(value) !== value) {
      validateNumber(value, type);
      throw new errors.ERR_OUT_OF_RANGE(type || "offset", "an integer", value);
    }
    if (length < 0) {
      throw new errors.ERR_BUFFER_OUT_OF_BOUNDS();
    }
    throw new errors.ERR_OUT_OF_RANGE(type || "offset", ">= ".concat(type ? 1 : 0, " and <= ").concat(length), value);
  }

  // HELPER FUNCTIONS
  // ================

  var INVALID_BASE64_RE = /[^+/0-9A-Za-z-_]/g;
  function base64clean(str) {
    // Node takes equal signs as end of the Base64 encoding
    str = str.split("=")[0];
    // Node strips out invalid characters like \n and \t from the string, base64-js does not
    str = str.trim().replace(INVALID_BASE64_RE, "");
    // Node converts strings with length < 2 to ''
    if (str.length < 2) return "";
    // Node allows for non-padded base64 strings (missing trailing ===), base64-js does not
    while (str.length % 4 !== 0) {
      str = str + "=";
    }
    return str;
  }
  function utf8ToBytes(string, units) {
    units = units || Infinity;
    var codePoint;
    var length = string.length;
    var leadSurrogate = null;
    var bytes = [];
    for (var i = 0; i < length; ++i) {
      codePoint = string.charCodeAt(i);

      // is surrogate component
      if (codePoint > 0xd7ff && codePoint < 0xe000) {
        // last char was a lead
        if (!leadSurrogate) {
          // no lead yet
          if (codePoint > 0xdbff) {
            // unexpected trail
            if ((units -= 3) > -1) bytes.push(0xef, 0xbf, 0xbd);
            continue;
          } else if (i + 1 === length) {
            // unpaired lead
            if ((units -= 3) > -1) bytes.push(0xef, 0xbf, 0xbd);
            continue;
          }

          // valid lead
          leadSurrogate = codePoint;
          continue;
        }

        // 2 leads in a row
        if (codePoint < 0xdc00) {
          if ((units -= 3) > -1) bytes.push(0xef, 0xbf, 0xbd);
          leadSurrogate = codePoint;
          continue;
        }

        // valid surrogate pair
        codePoint = (leadSurrogate - 0xd800 << 10 | codePoint - 0xdc00) + 0x10000;
      } else if (leadSurrogate) {
        // valid bmp char, but last char was a lead
        if ((units -= 3) > -1) bytes.push(0xef, 0xbf, 0xbd);
      }
      leadSurrogate = null;

      // encode utf8
      if (codePoint < 0x80) {
        if ((units -= 1) < 0) break;
        bytes.push(codePoint);
      } else if (codePoint < 0x800) {
        if ((units -= 2) < 0) break;
        bytes.push(codePoint >> 0x6 | 0xc0, codePoint & 0x3f | 0x80);
      } else if (codePoint < 0x10000) {
        if ((units -= 3) < 0) break;
        bytes.push(codePoint >> 0xc | 0xe0, codePoint >> 0x6 & 0x3f | 0x80, codePoint & 0x3f | 0x80);
      } else if (codePoint < 0x110000) {
        if ((units -= 4) < 0) break;
        bytes.push(codePoint >> 0x12 | 0xf0, codePoint >> 0xc & 0x3f | 0x80, codePoint >> 0x6 & 0x3f | 0x80, codePoint & 0x3f | 0x80);
      } else {
        throw new Error("Invalid code point");
      }
    }
    return bytes;
  }
  function asciiToBytes(str) {
    var byteArray = [];
    for (var i = 0; i < str.length; ++i) {
      // Node's code seems to be doing this and not & 0x7F..
      byteArray.push(str.charCodeAt(i) & 0xff);
    }
    return byteArray;
  }
  function utf16leToBytes(str, units) {
    var c, hi, lo;
    var byteArray = [];
    for (var i = 0; i < str.length; ++i) {
      if ((units -= 2) < 0) break;
      c = str.charCodeAt(i);
      hi = c >> 8;
      lo = c % 256;
      byteArray.push(lo);
      byteArray.push(hi);
    }
    return byteArray;
  }
  function base64ToBytes(str) {
    return base64.toByteArray(base64clean(str));
  }
  function blitBuffer(src, dst, offset, length) {
    var i;
    for (i = 0; i < length; ++i) {
      if (i + offset >= dst.length || i >= src.length) break;
      dst[i + offset] = src[i];
    }
    return i;
  }

  // ArrayBuffer or Uint8Array objects from other contexts (i.e. iframes) do not pass
  // the `instanceof` check but they should be treated as of that type.
  // See: https://github.com/feross/buffer/issues/166
  function isInstance(obj, type) {
    return obj instanceof type || obj != null && obj.constructor != null && obj.constructor.name != null && obj.constructor.name === type.name;
  }
  function numberIsNaN(obj) {
    // For IE11 support
    return obj !== obj; // eslint-disable-line no-self-compare
  }

  // Create lookup table for `toString('hex')`
  // See: https://github.com/feross/buffer/issues/219
  var hexSliceLookupTable = function () {
    var alphabet = "0123456789abcdef";
    var table = new Array(256);
    for (var i = 0; i < 16; ++i) {
      var i16 = i * 16;
      for (var j = 0; j < 16; ++j) {
        table[i16 + j] = alphabet[i] + alphabet[j];
      }
    }
    return table;
  }();

  // Return not function with Error if BigInt not supported
  function defineBigIntMethod(fn) {
    return typeof BigInt === "undefined" ? BufferBigIntNotDefined : fn;
  }
  function BufferBigIntNotDefined() {
    throw new Error("BigInt not supported");
  }
})(buffer);
var index$2 = /*@__PURE__*/getDefaultExportFromCjs$2(buffer);

var e = /*#__PURE__*/Object.freeze({
    __proto__: null,
    default: index$2
});

var n$1 = {
    2737: (t, e, r) => {
      var o = r(8750),
        n = r(4573),
        i = n(o("String.prototype.indexOf"));
      t.exports = function (t, e) {
        var r = o(t, !!e);
        return "function" == typeof r && i(t, ".prototype.") > -1 ? n(r) : r;
      };
    },
    4573: (t, e, r) => {
      var o = r(132),
        n = r(8750),
        i = n("%Function.prototype.apply%"),
        a = n("%Function.prototype.call%"),
        u = n("%Reflect.apply%", !0) || o.call(a, i),
        p = n("%Object.getOwnPropertyDescriptor%", !0),
        c = n("%Object.defineProperty%", !0),
        l = n("%Math.max%");
      if (c)
        try {
          c({}, "a", { value: 1 });
        } catch (t) {
          c = null;
        }
      t.exports = function (t) {
        var e = u(o, a, arguments);
        return (
          p &&
            c &&
            p(e, "length").configurable &&
            c(e, "length", {
              value: 1 + l(0, t.length - (arguments.length - 1)),
            }),
          e
        );
      };
      var f = function () {
        return u(o, i, arguments);
      };
      c ? c(t.exports, "apply", { value: f }) : (t.exports.apply = f);
    },
    7392: (t, e, r) => {
      var o = r(4733),
        n = "function" == typeof Symbol && "symbol" == typeof Symbol("foo"),
        i = Object.prototype.toString,
        a = Array.prototype.concat,
        u = Object.defineProperty,
        p = r(1365)(),
        c = u && p,
        l = function (t, e, r, o) {
          if (e in t)
            if (!0 === o) {
              if (t[e] === r) return;
            } else if (
              "function" != typeof (n = o) ||
              "[object Function]" !== i.call(n) ||
              !o()
            )
              return;
          var n;
          c
            ? u(t, e, {
                configurable: !0,
                enumerable: !1,
                value: r,
                writable: !0,
              })
            : (t[e] = r);
        },
        f = function (t, e) {
          var r = arguments.length > 2 ? arguments[2] : {},
            i = o(e);
          n && (i = a.call(i, Object.getOwnPropertySymbols(e)));
          for (var u = 0; u < i.length; u += 1) l(t, i[u], e[i[u]], r[i[u]]);
        };
      (f.supportsDescriptors = !!c), (t.exports = f);
    },
    1919: (t, e, r) => {
      var o = r(6597),
        n = r(3512),
        i = r(2435),
        a = r(1819),
        u = r(7057),
        p = r(8429),
        c = r(5627),
        l = r(9517),
        f = r(8750),
        y = r(4602),
        s = r(1365)(),
        b = f("%Error%");
      function g(t, e) {
        var r = new b(e);
        c(r, h), delete r.constructor;
        var i = p(
          t,
          y({ AdvanceStringIndex: o, GetMethod: a, IsArray: u, Type: l }, t)
        );
        return n(r, "errors", i), r;
      }
      s && Object.defineProperty(g, "prototype", { writable: !1 });
      var h = g.prototype;
      if (
        !i(h, "constructor", g) ||
        !i(h, "message", "") ||
        !i(h, "name", "AggregateError")
      )
        throw new b(
          "unable to install AggregateError.prototype properties; please report this!"
        );
      c(g.prototype, Error.prototype), (t.exports = g);
    },
    216: (t, e, r) => {
      var o = r(132),
        n = r(7392),
        i = r(222).functionsHaveConfigurableNames(),
        a = r(1919),
        u = r(6810),
        p = r(116),
        c = u(),
        l = o.call(c);
      Object.defineProperty &&
        (i && Object.defineProperty(l, "name", { value: c.name }),
        Object.defineProperty(l, "prototype", { value: c.prototype })),
        n(l, { getPolyfill: u, implementation: a, shim: p }),
        (t.exports = l);
    },
    6810: (t, e, r) => {
      var o = r(1919);
      t.exports = function () {
        return "function" == typeof AggregateError ? AggregateError : o;
      };
    },
    116: (t, e, r) => {
      var o = r(7392),
        n = r(1913)(),
        i = r(6810);
      t.exports = function () {
        var t = i();
        return (
          o(
            n,
            { AggregateError: t },
            {
              AggregateError: function () {
                return n.AggregateError !== t;
              },
            }
          ),
          t
        );
      };
    },
    8458: (t) => {
      var e = Array.prototype.slice,
        r = Object.prototype.toString;
      t.exports = function (t) {
        var o = this;
        if ("function" != typeof o || "[object Function]" !== r.call(o))
          throw new TypeError(
            "Function.prototype.bind called on incompatible " + o
          );
        for (
          var n,
            i = e.call(arguments, 1),
            a = Math.max(0, o.length - i.length),
            u = [],
            p = 0;
          p < a;
          p++
        )
          u.push("$" + p);
        if (
          ((n = Function(
            "binder",
            "return function (" +
              u.join(",") +
              "){ return binder.apply(this,arguments); }"
          )(function () {
            if (this instanceof n) {
              var r = o.apply(this, i.concat(e.call(arguments)));
              return Object(r) === r ? r : this;
            }
            return o.apply(t, i.concat(e.call(arguments)));
          })),
          o.prototype)
        ) {
          var c = function () {};
          (c.prototype = o.prototype),
            (n.prototype = new c()),
            (c.prototype = null);
        }
        return n;
      };
    },
    132: (t, e, r) => {
      var o = r(8458);
      t.exports = Function.prototype.bind || o;
    },
    222: (t) => {
      var e = function () {
          return "string" == typeof function () {}.name;
        },
        r = Object.getOwnPropertyDescriptor;
      if (r)
        try {
          r([], "length");
        } catch (t) {
          r = null;
        }
      e.functionsHaveConfigurableNames = function () {
        if (!e() || !r) return !1;
        var t = r(function () {}, "name");
        return !!t && !!t.configurable;
      };
      var o = Function.prototype.bind;
      (e.boundFunctionsHaveNames = function () {
        return (
          e() && "function" == typeof o && "" !== function () {}.bind().name
        );
      }),
        (t.exports = e);
    },
    8750: (t, e, r) => {
      var o,
        n = SyntaxError,
        i = Function,
        a = TypeError,
        u = function (t) {
          try {
            return i('"use strict"; return (' + t + ").constructor;")();
          } catch (t) {}
        },
        p = Object.getOwnPropertyDescriptor;
      if (p)
        try {
          p({}, "");
        } catch (t) {
          p = null;
        }
      var c = function () {
          throw new a();
        },
        l = p
          ? (function () {
              try {
                return c;
              } catch (t) {
                try {
                  return p(arguments, "callee").get;
                } catch (t) {
                  return c;
                }
              }
            })()
          : c,
        f = r(679)(),
        y = r(2574)(),
        s =
          Object.getPrototypeOf ||
          (y
            ? function (t) {
                return t.__proto__;
              }
            : null),
        b = {},
        g = "undefined" != typeof Uint8Array && s ? s(Uint8Array) : o,
        h = {
          "%AggregateError%":
            "undefined" == typeof AggregateError ? o : AggregateError,
          "%Array%": Array,
          "%ArrayBuffer%": "undefined" == typeof ArrayBuffer ? o : ArrayBuffer,
          "%ArrayIteratorPrototype%": f && s ? s([][Symbol.iterator]()) : o,
          "%AsyncFromSyncIteratorPrototype%": o,
          "%AsyncFunction%": b,
          "%AsyncGenerator%": b,
          "%AsyncGeneratorFunction%": b,
          "%AsyncIteratorPrototype%": b,
          "%Atomics%": "undefined" == typeof Atomics ? o : Atomics,
          "%BigInt%": "undefined" == typeof BigInt ? o : BigInt,
          "%BigInt64Array%":
            "undefined" == typeof BigInt64Array ? o : BigInt64Array,
          "%BigUint64Array%":
            "undefined" == typeof BigUint64Array ? o : BigUint64Array,
          "%Boolean%": Boolean,
          "%DataView%": "undefined" == typeof DataView ? o : DataView,
          "%Date%": Date,
          "%decodeURI%": decodeURI,
          "%decodeURIComponent%": decodeURIComponent,
          "%encodeURI%": encodeURI,
          "%encodeURIComponent%": encodeURIComponent,
          "%Error%": Error,
          "%eval%": eval,
          "%EvalError%": EvalError,
          "%Float32Array%":
            "undefined" == typeof Float32Array ? o : Float32Array,
          "%Float64Array%":
            "undefined" == typeof Float64Array ? o : Float64Array,
          "%FinalizationRegistry%":
            "undefined" == typeof FinalizationRegistry
              ? o
              : FinalizationRegistry,
          "%Function%": i,
          "%GeneratorFunction%": b,
          "%Int8Array%": "undefined" == typeof Int8Array ? o : Int8Array,
          "%Int16Array%": "undefined" == typeof Int16Array ? o : Int16Array,
          "%Int32Array%": "undefined" == typeof Int32Array ? o : Int32Array,
          "%isFinite%": isFinite,
          "%isNaN%": isNaN,
          "%IteratorPrototype%": f && s ? s(s([][Symbol.iterator]())) : o,
          "%JSON%": "object" == typeof JSON ? JSON : o,
          "%Map%": "undefined" == typeof Map ? o : Map,
          "%MapIteratorPrototype%":
            "undefined" != typeof Map && f && s
              ? s(new Map()[Symbol.iterator]())
              : o,
          "%Math%": Math,
          "%Number%": Number,
          "%Object%": Object,
          "%parseFloat%": parseFloat,
          "%parseInt%": parseInt,
          "%Promise%": "undefined" == typeof Promise ? o : Promise,
          "%Proxy%": "undefined" == typeof Proxy ? o : Proxy,
          "%RangeError%": RangeError,
          "%ReferenceError%": ReferenceError,
          "%Reflect%": "undefined" == typeof Reflect ? o : Reflect,
          "%RegExp%": RegExp,
          "%Set%": "undefined" == typeof Set ? o : Set,
          "%SetIteratorPrototype%":
            "undefined" != typeof Set && f && s
              ? s(new Set()[Symbol.iterator]())
              : o,
          "%SharedArrayBuffer%":
            "undefined" == typeof SharedArrayBuffer ? o : SharedArrayBuffer,
          "%String%": String,
          "%StringIteratorPrototype%": f && s ? s(""[Symbol.iterator]()) : o,
          "%Symbol%": f ? Symbol : o,
          "%SyntaxError%": n,
          "%ThrowTypeError%": l,
          "%TypedArray%": g,
          "%TypeError%": a,
          "%Uint8Array%": "undefined" == typeof Uint8Array ? o : Uint8Array,
          "%Uint8ClampedArray%":
            "undefined" == typeof Uint8ClampedArray ? o : Uint8ClampedArray,
          "%Uint16Array%": "undefined" == typeof Uint16Array ? o : Uint16Array,
          "%Uint32Array%": "undefined" == typeof Uint32Array ? o : Uint32Array,
          "%URIError%": URIError,
          "%WeakMap%": "undefined" == typeof WeakMap ? o : WeakMap,
          "%WeakRef%": "undefined" == typeof WeakRef ? o : WeakRef,
          "%WeakSet%": "undefined" == typeof WeakSet ? o : WeakSet,
        };
      if (s)
        try {
          null.error;
        } catch (t) {
          var d = s(s(t));
          h["%Error.prototype%"] = d;
        }
      var m = function t(e) {
          var r;
          if ("%AsyncFunction%" === e) r = u("async function () {}");
          else if ("%GeneratorFunction%" === e) r = u("function* () {}");
          else if ("%AsyncGeneratorFunction%" === e)
            r = u("async function* () {}");
          else if ("%AsyncGenerator%" === e) {
            var o = t("%AsyncGeneratorFunction%");
            o && (r = o.prototype);
          } else if ("%AsyncIteratorPrototype%" === e) {
            var n = t("%AsyncGenerator%");
            n && s && (r = s(n.prototype));
          }
          return (h[e] = r), r;
        },
        v = {
          "%ArrayBufferPrototype%": ["ArrayBuffer", "prototype"],
          "%ArrayPrototype%": ["Array", "prototype"],
          "%ArrayProto_entries%": ["Array", "prototype", "entries"],
          "%ArrayProto_forEach%": ["Array", "prototype", "forEach"],
          "%ArrayProto_keys%": ["Array", "prototype", "keys"],
          "%ArrayProto_values%": ["Array", "prototype", "values"],
          "%AsyncFunctionPrototype%": ["AsyncFunction", "prototype"],
          "%AsyncGenerator%": ["AsyncGeneratorFunction", "prototype"],
          "%AsyncGeneratorPrototype%": [
            "AsyncGeneratorFunction",
            "prototype",
            "prototype",
          ],
          "%BooleanPrototype%": ["Boolean", "prototype"],
          "%DataViewPrototype%": ["DataView", "prototype"],
          "%DatePrototype%": ["Date", "prototype"],
          "%ErrorPrototype%": ["Error", "prototype"],
          "%EvalErrorPrototype%": ["EvalError", "prototype"],
          "%Float32ArrayPrototype%": ["Float32Array", "prototype"],
          "%Float64ArrayPrototype%": ["Float64Array", "prototype"],
          "%FunctionPrototype%": ["Function", "prototype"],
          "%Generator%": ["GeneratorFunction", "prototype"],
          "%GeneratorPrototype%": [
            "GeneratorFunction",
            "prototype",
            "prototype",
          ],
          "%Int8ArrayPrototype%": ["Int8Array", "prototype"],
          "%Int16ArrayPrototype%": ["Int16Array", "prototype"],
          "%Int32ArrayPrototype%": ["Int32Array", "prototype"],
          "%JSONParse%": ["JSON", "parse"],
          "%JSONStringify%": ["JSON", "stringify"],
          "%MapPrototype%": ["Map", "prototype"],
          "%NumberPrototype%": ["Number", "prototype"],
          "%ObjectPrototype%": ["Object", "prototype"],
          "%ObjProto_toString%": ["Object", "prototype", "toString"],
          "%ObjProto_valueOf%": ["Object", "prototype", "valueOf"],
          "%PromisePrototype%": ["Promise", "prototype"],
          "%PromiseProto_then%": ["Promise", "prototype", "then"],
          "%Promise_all%": ["Promise", "all"],
          "%Promise_reject%": ["Promise", "reject"],
          "%Promise_resolve%": ["Promise", "resolve"],
          "%RangeErrorPrototype%": ["RangeError", "prototype"],
          "%ReferenceErrorPrototype%": ["ReferenceError", "prototype"],
          "%RegExpPrototype%": ["RegExp", "prototype"],
          "%SetPrototype%": ["Set", "prototype"],
          "%SharedArrayBufferPrototype%": ["SharedArrayBuffer", "prototype"],
          "%StringPrototype%": ["String", "prototype"],
          "%SymbolPrototype%": ["Symbol", "prototype"],
          "%SyntaxErrorPrototype%": ["SyntaxError", "prototype"],
          "%TypedArrayPrototype%": ["TypedArray", "prototype"],
          "%TypeErrorPrototype%": ["TypeError", "prototype"],
          "%Uint8ArrayPrototype%": ["Uint8Array", "prototype"],
          "%Uint8ClampedArrayPrototype%": ["Uint8ClampedArray", "prototype"],
          "%Uint16ArrayPrototype%": ["Uint16Array", "prototype"],
          "%Uint32ArrayPrototype%": ["Uint32Array", "prototype"],
          "%URIErrorPrototype%": ["URIError", "prototype"],
          "%WeakMapPrototype%": ["WeakMap", "prototype"],
          "%WeakSetPrototype%": ["WeakSet", "prototype"],
        },
        S = r(132),
        A = r(7492),
        w = S.call(Function.call, Array.prototype.concat),
        P = S.call(Function.apply, Array.prototype.splice),
        j = S.call(Function.call, String.prototype.replace),
        O = S.call(Function.call, String.prototype.slice),
        E = S.call(Function.call, RegExp.prototype.exec),
        x =
          /[^%.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|%$))/g,
        I = /\\(\\)?/g,
        M = function (t, e) {
          var r,
            o = t;
          if ((A(v, o) && (o = "%" + (r = v[o])[0] + "%"), A(h, o))) {
            var i = h[o];
            if ((i === b && (i = m(o)), void 0 === i && !e))
              throw new a(
                "intrinsic " +
                  t +
                  " exists, but is not available. Please file an issue!"
              );
            return { alias: r, name: o, value: i };
          }
          throw new n("intrinsic " + t + " does not exist!");
        };
      t.exports = function (t, e) {
        if ("string" != typeof t || 0 === t.length)
          throw new a("intrinsic name must be a non-empty string");
        if (arguments.length > 1 && "boolean" != typeof e)
          throw new a('"allowMissing" argument must be a boolean');
        if (null === E(/^%?[^%]*%?$/, t))
          throw new n(
            "`%` may not be present anywhere but at the beginning and end of the intrinsic name"
          );
        var r = (function (t) {
            var e = O(t, 0, 1),
              r = O(t, -1);
            if ("%" === e && "%" !== r)
              throw new n("invalid intrinsic syntax, expected closing `%`");
            if ("%" === r && "%" !== e)
              throw new n("invalid intrinsic syntax, expected opening `%`");
            var o = [];
            return (
              j(t, x, function (t, e, r, n) {
                o[o.length] = r ? j(n, I, "$1") : e || t;
              }),
              o
            );
          })(t),
          o = r.length > 0 ? r[0] : "",
          i = M("%" + o + "%", e),
          u = i.name,
          c = i.value,
          l = !1,
          f = i.alias;
        f && ((o = f[0]), P(r, w([0, 1], f)));
        for (var y = 1, s = !0; y < r.length; y += 1) {
          var b = r[y],
            g = O(b, 0, 1),
            d = O(b, -1);
          if (
            ('"' === g ||
              "'" === g ||
              "`" === g ||
              '"' === d ||
              "'" === d ||
              "`" === d) &&
            g !== d
          )
            throw new n("property names with quotes must have matching quotes");
          if (
            (("constructor" !== b && s) || (l = !0),
            A(h, (u = "%" + (o += "." + b) + "%")))
          )
            c = h[u];
          else if (null != c) {
            if (!(b in c)) {
              if (!e)
                throw new a(
                  "base intrinsic for " +
                    t +
                    " exists, but the property is not available."
                );
              return;
            }
            if (p && y + 1 >= r.length) {
              var m = p(c, b);
              c =
                (s = !!m) && "get" in m && !("originalValue" in m.get)
                  ? m.get
                  : c[b];
            } else (s = A(c, b)), (c = c[b]);
            s && !l && (h[u] = c);
          }
        }
        return c;
      };
    },
    1403: (t) => {
      "undefined" != typeof self
        ? (t.exports = self)
        : "undefined" != typeof window
        ? (t.exports = window)
        : (t.exports = Function("return this")());
    },
    1913: (t, e, r) => {
      var o = r(7392),
        n = r(1403),
        i = r(9958),
        a = r(7493),
        u = i(),
        p = function () {
          return u;
        };
      o(p, { getPolyfill: i, implementation: n, shim: a }), (t.exports = p);
    },
    9958: (t, e, r) => {
      var o = r(1403);
      t.exports = function () {
        return "object" == typeof r.g &&
          r.g &&
          r.g.Math === Math &&
          r.g.Array === Array
          ? r.g
          : o;
      };
    },
    7493: (t, e, r) => {
      var o = r(7392),
        n = r(9958);
      t.exports = function () {
        var t = n();
        if (o.supportsDescriptors) {
          var e = Object.getOwnPropertyDescriptor(t, "globalThis");
          (e &&
            (!e.configurable ||
              (!e.enumerable && e.writable && globalThis === t))) ||
            Object.defineProperty(t, "globalThis", {
              configurable: !0,
              enumerable: !1,
              value: t,
              writable: !0,
            });
        } else
          ("object" == typeof globalThis && globalThis === t) ||
            (t.globalThis = t);
        return t;
      };
    },
    7502: (t, e, r) => {
      var o = r(8750)("%Object.getOwnPropertyDescriptor%", !0);
      if (o)
        try {
          o([], "length");
        } catch (t) {
          o = null;
        }
      t.exports = o;
    },
    1365: (t, e, r) => {
      var o = r(8750)("%Object.defineProperty%", !0),
        n = function () {
          if (o)
            try {
              return o({}, "a", { value: 1 }), !0;
            } catch (t) {
              return !1;
            }
          return !1;
        };
      (n.hasArrayLengthDefineBug = function () {
        if (!n()) return null;
        try {
          return 1 !== o([], "length", { value: 1 }).length;
        } catch (t) {
          return !0;
        }
      }),
        (t.exports = n);
    },
    2574: (t) => {
      var e = { foo: {} },
        r = Object;
      t.exports = function () {
        return (
          { __proto__: e }.foo === e.foo && !({ __proto__: null } instanceof r)
        );
      };
    },
    679: (t, e, r) => {
      var o = "undefined" != typeof Symbol && Symbol,
        n = r(8186);
      t.exports = function () {
        return (
          "function" == typeof o &&
          "function" == typeof Symbol &&
          "symbol" == typeof o("foo") &&
          "symbol" == typeof Symbol("bar") &&
          n()
        );
      };
    },
    8186: (t) => {
      t.exports = function () {
        if (
          "function" != typeof Symbol ||
          "function" != typeof Object.getOwnPropertySymbols
        )
          return !1;
        if ("symbol" == typeof Symbol.iterator) return !0;
        var t = {},
          e = Symbol("test"),
          r = Object(e);
        if ("string" == typeof e) return !1;
        if ("[object Symbol]" !== Object.prototype.toString.call(e)) return !1;
        if ("[object Symbol]" !== Object.prototype.toString.call(r)) return !1;
        for (e in ((t[e] = 42), t)) return !1;
        if ("function" == typeof Object.keys && 0 !== Object.keys(t).length)
          return !1;
        if (
          "function" == typeof Object.getOwnPropertyNames &&
          0 !== Object.getOwnPropertyNames(t).length
        )
          return !1;
        var o = Object.getOwnPropertySymbols(t);
        if (1 !== o.length || o[0] !== e) return !1;
        if (!Object.prototype.propertyIsEnumerable.call(t, e)) return !1;
        if ("function" == typeof Object.getOwnPropertyDescriptor) {
          var n = Object.getOwnPropertyDescriptor(t, e);
          if (42 !== n.value || !0 !== n.enumerable) return !1;
        }
        return !0;
      };
    },
    698: (t, e, r) => {
      var o = r(8186);
      t.exports = function () {
        return o() && !!Symbol.toStringTag;
      };
    },
    7492: (t, e, r) => {
      var o = r(132);
      t.exports = o.call(Function.call, Object.prototype.hasOwnProperty);
    },
    2922: (t) => {
      var e,
        r,
        o = Function.prototype.toString,
        n = "object" == typeof Reflect && null !== Reflect && Reflect.apply;
      if ("function" == typeof n && "function" == typeof Object.defineProperty)
        try {
          (e = Object.defineProperty({}, "length", {
            get: function () {
              throw r;
            },
          })),
            (r = {}),
            n(
              function () {
                throw 42;
              },
              null,
              e
            );
        } catch (t) {
          t !== r && (n = null);
        }
      else n = null;
      var i = /^\s*class\b/,
        a = function (t) {
          try {
            var e = o.call(t);
            return i.test(e);
          } catch (t) {
            return !1;
          }
        },
        u = function (t) {
          try {
            return !a(t) && (o.call(t), !0);
          } catch (t) {
            return !1;
          }
        },
        p = Object.prototype.toString,
        c = "function" == typeof Symbol && !!Symbol.toStringTag,
        l = !(0 in [,]),
        f = function () {
          return !1;
        };
      if ("object" == typeof document) {
        var y = document.all;
        p.call(y) === p.call(document.all) &&
          (f = function (t) {
            if ((l || !t) && (void 0 === t || "object" == typeof t))
              try {
                var e = p.call(t);
                return (
                  ("[object HTMLAllCollection]" === e ||
                    "[object HTML document.all class]" === e ||
                    "[object HTMLCollection]" === e ||
                    "[object Object]" === e) &&
                  null == t("")
                );
              } catch (t) {}
            return !1;
          });
      }
      t.exports = n
        ? function (t) {
            if (f(t)) return !0;
            if (!t) return !1;
            if ("function" != typeof t && "object" != typeof t) return !1;
            try {
              n(t, null, e);
            } catch (t) {
              if (t !== r) return !1;
            }
            return !a(t) && u(t);
          }
        : function (t) {
            if (f(t)) return !0;
            if (!t) return !1;
            if ("function" != typeof t && "object" != typeof t) return !1;
            if (c) return u(t);
            if (a(t)) return !1;
            var e = p.call(t);
            return (
              !(
                "[object Function]" !== e &&
                "[object GeneratorFunction]" !== e &&
                !/^\[object HTML/.test(e)
              ) && u(t)
            );
          };
    },
    8559: (t, e, r) => {
      var o = String.prototype.valueOf,
        n = Object.prototype.toString,
        i = r(698)();
      t.exports = function (t) {
        return (
          "string" == typeof t ||
          ("object" == typeof t &&
            (i
              ? (function (t) {
                  try {
                    return o.call(t), !0;
                  } catch (t) {
                    return !1;
                  }
                })(t)
              : "[object String]" === n.call(t)))
        );
      };
    },
    6524: (t, e, r) => {
      var o = "function" == typeof Map && Map.prototype,
        n =
          Object.getOwnPropertyDescriptor && o
            ? Object.getOwnPropertyDescriptor(Map.prototype, "size")
            : null,
        i = o && n && "function" == typeof n.get ? n.get : null,
        a = o && Map.prototype.forEach,
        u = "function" == typeof Set && Set.prototype,
        p =
          Object.getOwnPropertyDescriptor && u
            ? Object.getOwnPropertyDescriptor(Set.prototype, "size")
            : null,
        c = u && p && "function" == typeof p.get ? p.get : null,
        l = u && Set.prototype.forEach,
        f =
          "function" == typeof WeakMap && WeakMap.prototype
            ? WeakMap.prototype.has
            : null,
        y =
          "function" == typeof WeakSet && WeakSet.prototype
            ? WeakSet.prototype.has
            : null,
        s =
          "function" == typeof WeakRef && WeakRef.prototype
            ? WeakRef.prototype.deref
            : null,
        b = Boolean.prototype.valueOf,
        g = Object.prototype.toString,
        h = Function.prototype.toString,
        d = String.prototype.match,
        m = String.prototype.slice,
        v = String.prototype.replace,
        S = String.prototype.toUpperCase,
        A = String.prototype.toLowerCase,
        w = RegExp.prototype.test,
        P = Array.prototype.concat,
        j = Array.prototype.join,
        O = Array.prototype.slice,
        E = Math.floor,
        x = "function" == typeof BigInt ? BigInt.prototype.valueOf : null,
        I = Object.getOwnPropertySymbols,
        M =
          "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
            ? Symbol.prototype.toString
            : null,
        R = "function" == typeof Symbol && "object" == typeof Symbol.iterator,
        T =
          "function" == typeof Symbol &&
          Symbol.toStringTag &&
          (1)
            ? Symbol.toStringTag
            : null,
        D = Object.prototype.propertyIsEnumerable,
        _ =
          ("function" == typeof Reflect
            ? Reflect.getPrototypeOf
            : Object.getPrototypeOf) ||
          ([].__proto__ === Array.prototype
            ? function (t) {
                return t.__proto__;
              }
            : null);
      function N(t, e) {
        if (
          t === 1 / 0 ||
          t === -1 / 0 ||
          t != t ||
          (t && t > -1e3 && t < 1e3) ||
          w.call(/e/, e)
        )
          return e;
        var r = /[0-9](?=(?:[0-9]{3})+(?![0-9]))/g;
        if ("number" == typeof t) {
          var o = t < 0 ? -E(-t) : E(t);
          if (o !== t) {
            var n = String(o),
              i = m.call(e, n.length + 1);
            return (
              v.call(n, r, "$&_") +
              "." +
              v.call(v.call(i, /([0-9]{3})/g, "$&_"), /_$/, "")
            );
          }
        }
        return v.call(e, r, "$&_");
      }
      var C = r(6553),
        F = C.custom,
        G = B(F) ? F : null;
      function U(t, e, r) {
        var o = "double" === (r.quoteStyle || e) ? '"' : "'";
        return o + t + o;
      }
      function k(t) {
        return v.call(String(t), /"/g, "&quot;");
      }
      function W(t) {
        return !(
          "[object Array]" !== H(t) ||
          (T && "object" == typeof t && T in t)
        );
      }
      function V(t) {
        return !(
          "[object RegExp]" !== H(t) ||
          (T && "object" == typeof t && T in t)
        );
      }
      function B(t) {
        if (R) return t && "object" == typeof t && t instanceof Symbol;
        if ("symbol" == typeof t) return !0;
        if (!t || "object" != typeof t || !M) return !1;
        try {
          return M.call(t), !0;
        } catch (t) {}
        return !1;
      }
      t.exports = function t(e, r, o, n) {
        var u = r || {};
        if (
          $(u, "quoteStyle") &&
          "single" !== u.quoteStyle &&
          "double" !== u.quoteStyle
        )
          throw new TypeError(
            'option "quoteStyle" must be "single" or "double"'
          );
        if (
          $(u, "maxStringLength") &&
          ("number" == typeof u.maxStringLength
            ? u.maxStringLength < 0 && u.maxStringLength !== 1 / 0
            : null !== u.maxStringLength)
        )
          throw new TypeError(
            'option "maxStringLength", if provided, must be a positive integer, Infinity, or `null`'
          );
        var p = !$(u, "customInspect") || u.customInspect;
        if ("boolean" != typeof p && "symbol" !== p)
          throw new TypeError(
            "option \"customInspect\", if provided, must be `true`, `false`, or `'symbol'`"
          );
        if (
          $(u, "indent") &&
          null !== u.indent &&
          "\t" !== u.indent &&
          !(parseInt(u.indent, 10) === u.indent && u.indent > 0)
        )
          throw new TypeError(
            'option "indent" must be "\\t", an integer > 0, or `null`'
          );
        if ($(u, "numericSeparator") && "boolean" != typeof u.numericSeparator)
          throw new TypeError(
            'option "numericSeparator", if provided, must be `true` or `false`'
          );
        var g = u.numericSeparator;
        if (void 0 === e) return "undefined";
        if (null === e) return "null";
        if ("boolean" == typeof e) return e ? "true" : "false";
        if ("string" == typeof e) return J(e, u);
        if ("number" == typeof e) {
          if (0 === e) return 1 / 0 / e > 0 ? "0" : "-0";
          var S = String(e);
          return g ? N(e, S) : S;
        }
        if ("bigint" == typeof e) {
          var w = String(e) + "n";
          return g ? N(e, w) : w;
        }
        var E = void 0 === u.depth ? 5 : u.depth;
        if ((void 0 === o && (o = 0), o >= E && E > 0 && "object" == typeof e))
          return W(e) ? "[Array]" : "[Object]";
        var I,
          F = (function (t, e) {
            var r;
            if ("\t" === t.indent) r = "\t";
            else {
              if (!("number" == typeof t.indent && t.indent > 0)) return null;
              r = j.call(Array(t.indent + 1), " ");
            }
            return { base: r, prev: j.call(Array(e + 1), r) };
          })(u, o);
        if (void 0 === n) n = [];
        else if (q(n, e) >= 0) return "[Circular]";
        function L(e, r, i) {
          if ((r && (n = O.call(n)).push(r), i)) {
            var a = { depth: u.depth };
            return (
              $(u, "quoteStyle") && (a.quoteStyle = u.quoteStyle),
              t(e, a, o + 1, n)
            );
          }
          return t(e, u, o + 1, n);
        }
        if ("function" == typeof e && !V(e)) {
          var Y = (function (t) {
              if (t.name) return t.name;
              var e = d.call(h.call(t), /^function\s*([\w$]+)/);
              return e ? e[1] : null;
            })(e),
            tt = Q(e, L);
          return (
            "[Function" +
            (Y ? ": " + Y : " (anonymous)") +
            "]" +
            (tt.length > 0 ? " { " + j.call(tt, ", ") + " }" : "")
          );
        }
        if (B(e)) {
          var et = R
            ? v.call(String(e), /^(Symbol\(.*\))_[^)]*$/, "$1")
            : M.call(e);
          return "object" != typeof e || R ? et : K(et);
        }
        if (
          (I = e) &&
          "object" == typeof I &&
          (("undefined" != typeof HTMLElement && I instanceof HTMLElement) ||
            ("string" == typeof I.nodeName &&
              "function" == typeof I.getAttribute))
        ) {
          for (
            var rt = "<" + A.call(String(e.nodeName)),
              ot = e.attributes || [],
              nt = 0;
            nt < ot.length;
            nt++
          )
            rt += " " + ot[nt].name + "=" + U(k(ot[nt].value), "double", u);
          return (
            (rt += ">"),
            e.childNodes && e.childNodes.length && (rt += "..."),
            rt + "</" + A.call(String(e.nodeName)) + ">"
          );
        }
        if (W(e)) {
          if (0 === e.length) return "[]";
          var it = Q(e, L);
          return F &&
            !(function (t) {
              for (var e = 0; e < t.length; e++)
                if (q(t[e], "\n") >= 0) return !1;
              return !0;
            })(it)
            ? "[" + X(it, F) + "]"
            : "[ " + j.call(it, ", ") + " ]";
        }
        if (
          (function (t) {
            return !(
              "[object Error]" !== H(t) ||
              (T && "object" == typeof t && T in t)
            );
          })(e)
        ) {
          var at = Q(e, L);
          return "cause" in Error.prototype ||
            !("cause" in e) ||
            D.call(e, "cause")
            ? 0 === at.length
              ? "[" + String(e) + "]"
              : "{ [" + String(e) + "] " + j.call(at, ", ") + " }"
            : "{ [" +
                String(e) +
                "] " +
                j.call(P.call("[cause]: " + L(e.cause), at), ", ") +
                " }";
        }
        if ("object" == typeof e && p) {
          if (G && "function" == typeof e[G] && C)
            return C(e, { depth: E - o });
          if ("symbol" !== p && "function" == typeof e.inspect)
            return e.inspect();
        }
        if (
          (function (t) {
            if (!i || !t || "object" != typeof t) return !1;
            try {
              i.call(t);
              try {
                c.call(t);
              } catch (t) {
                return !0;
              }
              return t instanceof Map;
            } catch (t) {}
            return !1;
          })(e)
        ) {
          var ut = [];
          return (
            a &&
              a.call(e, function (t, r) {
                ut.push(L(r, e, !0) + " => " + L(t, e));
              }),
            Z("Map", i.call(e), ut, F)
          );
        }
        if (
          (function (t) {
            if (!c || !t || "object" != typeof t) return !1;
            try {
              c.call(t);
              try {
                i.call(t);
              } catch (t) {
                return !0;
              }
              return t instanceof Set;
            } catch (t) {}
            return !1;
          })(e)
        ) {
          var pt = [];
          return (
            l &&
              l.call(e, function (t) {
                pt.push(L(t, e));
              }),
            Z("Set", c.call(e), pt, F)
          );
        }
        if (
          (function (t) {
            if (!f || !t || "object" != typeof t) return !1;
            try {
              f.call(t, f);
              try {
                y.call(t, y);
              } catch (t) {
                return !0;
              }
              return t instanceof WeakMap;
            } catch (t) {}
            return !1;
          })(e)
        )
          return z("WeakMap");
        if (
          (function (t) {
            if (!y || !t || "object" != typeof t) return !1;
            try {
              y.call(t, y);
              try {
                f.call(t, f);
              } catch (t) {
                return !0;
              }
              return t instanceof WeakSet;
            } catch (t) {}
            return !1;
          })(e)
        )
          return z("WeakSet");
        if (
          (function (t) {
            if (!s || !t || "object" != typeof t) return !1;
            try {
              return s.call(t), !0;
            } catch (t) {}
            return !1;
          })(e)
        )
          return z("WeakRef");
        if (
          (function (t) {
            return !(
              "[object Number]" !== H(t) ||
              (T && "object" == typeof t && T in t)
            );
          })(e)
        )
          return K(L(Number(e)));
        if (
          (function (t) {
            if (!t || "object" != typeof t || !x) return !1;
            try {
              return x.call(t), !0;
            } catch (t) {}
            return !1;
          })(e)
        )
          return K(L(x.call(e)));
        if (
          (function (t) {
            return !(
              "[object Boolean]" !== H(t) ||
              (T && "object" == typeof t && T in t)
            );
          })(e)
        )
          return K(b.call(e));
        if (
          (function (t) {
            return !(
              "[object String]" !== H(t) ||
              (T && "object" == typeof t && T in t)
            );
          })(e)
        )
          return K(L(String(e)));
        if (
          !(function (t) {
            return !(
              "[object Date]" !== H(t) ||
              (T && "object" == typeof t && T in t)
            );
          })(e) &&
          !V(e)
        ) {
          var ct = Q(e, L),
            lt = _
              ? _(e) === Object.prototype
              : e instanceof Object || e.constructor === Object,
            ft = e instanceof Object ? "" : "null prototype",
            yt =
              !lt && T && Object(e) === e && T in e
                ? m.call(H(e), 8, -1)
                : ft
                ? "Object"
                : "",
            st =
              (lt || "function" != typeof e.constructor
                ? ""
                : e.constructor.name
                ? e.constructor.name + " "
                : "") +
              (yt || ft
                ? "[" + j.call(P.call([], yt || [], ft || []), ": ") + "] "
                : "");
          return 0 === ct.length
            ? st + "{}"
            : F
            ? st + "{" + X(ct, F) + "}"
            : st + "{ " + j.call(ct, ", ") + " }";
        }
        return String(e);
      };
      var L =
        Object.prototype.hasOwnProperty ||
        function (t) {
          return t in this;
        };
      function $(t, e) {
        return L.call(t, e);
      }
      function H(t) {
        return g.call(t);
      }
      function q(t, e) {
        if (t.indexOf) return t.indexOf(e);
        for (var r = 0, o = t.length; r < o; r++) if (t[r] === e) return r;
        return -1;
      }
      function J(t, e) {
        if (t.length > e.maxStringLength) {
          var r = t.length - e.maxStringLength,
            o = "... " + r + " more character" + (r > 1 ? "s" : "");
          return J(m.call(t, 0, e.maxStringLength), e) + o;
        }
        return U(
          v.call(v.call(t, /(['\\])/g, "\\$1"), /[\x00-\x1f]/g, Y),
          "single",
          e
        );
      }
      function Y(t) {
        var e = t.charCodeAt(0),
          r = { 8: "b", 9: "t", 10: "n", 12: "f", 13: "r" }[e];
        return r
          ? "\\" + r
          : "\\x" + (e < 16 ? "0" : "") + S.call(e.toString(16));
      }
      function K(t) {
        return "Object(" + t + ")";
      }
      function z(t) {
        return t + " { ? }";
      }
      function Z(t, e, r, o) {
        return t + " (" + e + ") {" + (o ? X(r, o) : j.call(r, ", ")) + "}";
      }
      function X(t, e) {
        if (0 === t.length) return "";
        var r = "\n" + e.prev + e.base;
        return r + j.call(t, "," + r) + "\n" + e.prev;
      }
      function Q(t, e) {
        var r = W(t),
          o = [];
        if (r) {
          o.length = t.length;
          for (var n = 0; n < t.length; n++) o[n] = $(t, n) ? e(t[n], t) : "";
        }
        var i,
          a = "function" == typeof I ? I(t) : [];
        if (R) {
          i = {};
          for (var u = 0; u < a.length; u++) i["$" + a[u]] = a[u];
        }
        for (var p in t)
          $(t, p) &&
            ((r && String(Number(p)) === p && p < t.length) ||
              (R && i["$" + p] instanceof Symbol) ||
              (w.call(/[^\w$]/, p)
                ? o.push(e(p, t) + ": " + e(t[p], t))
                : o.push(p + ": " + e(t[p], t))));
        if ("function" == typeof I)
          for (var c = 0; c < a.length; c++)
            D.call(t, a[c]) && o.push("[" + e(a[c]) + "]: " + e(t[a[c]], t));
        return o;
      }
    },
    9538: (t, e, r) => {
      var o;
      if (!Object.keys) {
        var n = Object.prototype.hasOwnProperty,
          i = Object.prototype.toString,
          a = r(1030),
          u = Object.prototype.propertyIsEnumerable,
          p = !u.call({ toString: null }, "toString"),
          c = u.call(function () {}, "prototype"),
          l = [
            "toString",
            "toLocaleString",
            "valueOf",
            "hasOwnProperty",
            "isPrototypeOf",
            "propertyIsEnumerable",
            "constructor",
          ],
          f = function (t) {
            var e = t.constructor;
            return e && e.prototype === t;
          },
          y = {
            $applicationCache: !0,
            $console: !0,
            $external: !0,
            $frame: !0,
            $frameElement: !0,
            $frames: !0,
            $innerHeight: !0,
            $innerWidth: !0,
            $onmozfullscreenchange: !0,
            $onmozfullscreenerror: !0,
            $outerHeight: !0,
            $outerWidth: !0,
            $pageXOffset: !0,
            $pageYOffset: !0,
            $parent: !0,
            $scrollLeft: !0,
            $scrollTop: !0,
            $scrollX: !0,
            $scrollY: !0,
            $self: !0,
            $webkitIndexedDB: !0,
            $webkitStorageInfo: !0,
            $window: !0,
          },
          s = (function () {
            if ("undefined" == typeof window) return !1;
            for (var t in window)
              try {
                if (
                  !y["$" + t] &&
                  n.call(window, t) &&
                  null !== window[t] &&
                  "object" == typeof window[t]
                )
                  try {
                    f(window[t]);
                  } catch (t) {
                    return !0;
                  }
              } catch (t) {
                return !0;
              }
            return !1;
          })();
        o = function (t) {
          var e = null !== t && "object" == typeof t,
            r = "[object Function]" === i.call(t),
            o = a(t),
            u = e && "[object String]" === i.call(t),
            y = [];
          if (!e && !r && !o)
            throw new TypeError("Object.keys called on a non-object");
          var b = c && r;
          if (u && t.length > 0 && !n.call(t, 0))
            for (var g = 0; g < t.length; ++g) y.push(String(g));
          if (o && t.length > 0)
            for (var h = 0; h < t.length; ++h) y.push(String(h));
          else
            for (var d in t)
              (b && "prototype" === d) || !n.call(t, d) || y.push(String(d));
          if (p)
            for (
              var m = (function (t) {
                  if ("undefined" == typeof window || !s) return f(t);
                  try {
                    return f(t);
                  } catch (t) {
                    return !1;
                  }
                })(t),
                v = 0;
              v < l.length;
              ++v
            )
              (m && "constructor" === l[v]) || !n.call(t, l[v]) || y.push(l[v]);
          return y;
        };
      }
      t.exports = o;
    },
    4733: (t, e, r) => {
      var o = Array.prototype.slice,
        n = r(1030),
        i = Object.keys,
        a = i
          ? function (t) {
              return i(t);
            }
          : r(9538),
        u = Object.keys;
      (a.shim = function () {
        if (Object.keys) {
          var t = (function () {
            var t = Object.keys(arguments);
            return t && t.length === arguments.length;
          })(1, 2);
          t ||
            (Object.keys = function (t) {
              return n(t) ? u(o.call(t)) : u(t);
            });
        } else Object.keys = a;
        return Object.keys || a;
      }),
        (t.exports = a);
    },
    1030: (t) => {
      var e = Object.prototype.toString;
      t.exports = function (t) {
        var r = e.call(t),
          o = "[object Arguments]" === r;
        return (
          o ||
            (o =
              "[object Array]" !== r &&
              null !== t &&
              "object" == typeof t &&
              "number" == typeof t.length &&
              t.length >= 0 &&
              "[object Function]" === e.call(t.callee)),
          o
        );
      };
    },
    3849: (t, e, r) => {
      var o = r(216);
      t.exports = o;
    },
    3018: (t, e, r) => {
      t.exports = {
        uncurryThis: (function () {
          const { apply: t, bind: e, call: r } = Function.prototype;
          return e.bind(r);
        })(),
        JSONParse: (t) => JSON.parse(t),
        MathAbs: (t) => Math.abs(t),
        MathAcos: (t) => Math.acos(t),
        MathAcosh: (t) => Math.acosh(t),
        MathAsin: (t) => Math.asin(t),
        MathAsinh: (t) => Math.asinh(t),
        MathAtan: (t) => Math.atan(t),
        MathAtanh: (t) => Math.atanh(t),
        MathAtan2: (t) => Math.atan2(t),
        MathCeil: (t) => Math.ceil(t),
        MathCbrt: (t) => Math.cbrt(t),
        MathExpm1: (t) => Math.expm1(t),
        MathClz32: (t) => Math.clz32(t),
        MathCos: (t) => Math.cos(t),
        MathCosh: (t) => Math.cosh(t),
        MathExp: (t) => Math.exp(t),
        MathFround: (t) => Math.fround(t),
        MathHypot: (t) => Math.hypot(t),
        MathImul: (t) => Math.imul(t),
        MathLog: (t) => Math.log(t),
        MathLog1p: (t) => Math.log(t),
        MathLog2: (t) => Math.log2(t),
        MathLog10: (t) => Math.log10(t),
        MathMax: (...t) => Math.max(...t),
        MathMaxApply: (t) => Math.max.apply(null, t),
        MathMin: (t) => Math.min(t),
        MathPow: (t) => Math.pow(t),
        MathRandom: () => Math.random(),
        MathRound: (t) => Math.round(t),
        MathSign: (t) => Math.sign(t),
        MathSin: (t) => Math.sin(t),
        MathSinh: (t) => Math.sinh(t),
        MathSqrt: (t) => Math.sqrt(t),
        MathTan: (t) => Math.tan(t),
        MathTanh: (t) => Math.tanh(t),
        MathTrunc: (t) => Math.trunc(t),
        MathE: () => Math.E,
        MathLN10: () => Math.LN10,
        MathLN2: () => Math.LN2,
        MathLOG10E: () => Math.LOG10E,
        MathLOG2E: () => Math.LOG2E,
        MathPI: () => Math.PI,
        MathSQRT1_2: () => Math.SQRT1_2,
        MathSQRT2: () => Math.SQRT2,
        ReflectDefineProperty: Reflect.defineProperty,
        ReflectDeleteProperty: Reflect.deleteProperty,
        ReflectApply: Reflect.apply,
        ReflectConstruct: Reflect.construct,
        ReflectGet: Reflect.get,
        ReflectGetOwnPropertyDescriptor: Reflect.getOwnPropertyDescriptor,
        ReflectGetPrototypeOf: Reflect.getPrototypeOf,
        ReflectHas: Reflect.has,
        ReflectIsExtensible: Reflect.isExtensible,
        ReflectOwnKeys: Reflect.ownKeys,
        ReflectPreventExtensions: Reflect.preventExtensions,
        ReflectSet: Reflect.set,
        ReflectSetPrototypeOf: Reflect.setPrototypeOf,
        AggregateError: r(3849),
        ArrayFrom: (t, e) => Array.from(t, e),
        ArrayIsArray: (t) => Array.isArray(t),
        ArrayPrototypeIncludes: (t, e) => t.includes(e),
        ArrayPrototypeFilter: (t, e) => t.filter(e),
        ArrayPrototypeIndexOf: (t, e) => t.indexOf(e),
        ArrayPrototypeJoin: (t, e) => t.join(e),
        ArrayPrototypeMap: (t, e) => t.map(e),
        ArrayPrototypePop: (t, e) => t.pop(e),
        ArrayPrototypePush: (t, e) => t.push(e),
        ArrayPrototypeSlice: (t, e, r) => t.slice(e, r),
        ArrayPrototypeSplice: (t, e, r, ...o) => t.splice(e, r, ...o),
        ArrayPrototypeUnshift: (t, e) => t.unshift(e),
        MapPrototypeGet: Map.prototype.get,
        Error,
        ErrorCaptureStackTrace: Error.captureStackTrace,
        ErrorPrototypeToString: Error.prototype.toString,
        RangeError,
        JSONStringify: JSON.stringify,
        FunctionPrototypeCall: (t, e, ...r) => t.call(e, ...r),
        FunctionPrototypeBind: (t, e, ...r) => t.bind(e, ...r),
        FunctionPrototypeSymbolHasInstance: (t, e) =>
          Function.prototype[Symbol.hasInstance].call(t, e),
        MathFloor: Math.floor,
        Number,
        NumberIsInteger: Number.isInteger,
        NumberIsNaN: Number.isNaN,
        NumberMAX_SAFE_INTEGER: Number.MAX_SAFE_INTEGER,
        NumberMIN_SAFE_INTEGER: Number.MIN_SAFE_INTEGER,
        NumberParseInt: Number.parseInt,
        NumberIsFinite: Number.isFinite,
        NumberPrototypeToString: (t, e) => t.toString(e),
        ObjectPrototypeHasOwnProperty: (t, e) =>
          Object.prototype.hasOwnProperty.call(t, e),
        ObjectAssign: Object.assign,
        ObjectDefineProperties: (t, e) => Object.defineProperties(t, e),
        ObjectDefineProperty: (t, e, r) => Object.defineProperty(t, e, r),
        ObjectGetOwnPropertyDescriptor: (t, e) =>
          Object.getOwnPropertyDescriptor(t, e),
        ObjectKeys: (t) => Object.keys(t),
        ObjectCreate: (t) => Object.create(t),
        ObjectFreeze: (t) => Object.freeze(t),
        ObjectEntries: (t) => Object.entries(t),
        ObjectSetPrototypeOf: (t, e) => Object.setPrototypeOf(t, e),
        ObjectPrototypeToString: (t) => t.toString(),
        ObjectPrototypePropertyIsEnumerable: (t, e) =>
          t.propertyIsEnumerable(e),
        ObjectIsExtensible: Object.isExtensible,
        Promise,
        PromisePrototypeCatch: (t, e) => t.catch(e),
        PromisePrototypeThen: (t, e, r) => t.then(e, r),
        PromiseReject: (t) => Promise.reject(t),
        RegExpPrototypeTest: (t, e) => t.test(e),
        SafeSet: Set,
        String,
        StringPrototypeSlice: (t, e, r) => t.slice(e, r),
        StringPrototypeToLowerCase: (t) => t.toLowerCase(),
        StringPrototypeToUpperCase: (t) => t.toUpperCase(),
        StringPrototypeTrim: (t) => t.trim(),
        StringPrototypeCharCodeAt: (t, e) => t.charCodeAt(e),
        StringPrototypeLastIndexOf: (t, e) => t.lastIndexOf(e),
        StringPrototypeCharAt: (t, e) => t.charAt(e),
        StringPrototypeIndexOf: (t, e) => t.indexOf(e),
        StringPrototypeStartsWith: (t, e) => t.startsWith(e),
        StringPrototypeIncludes: (t, e, r) => t.includes(e, r),
        StringPrototypePadStart: (t, e, r) => t.padStart(e, r),
        StringPrototypeReplace: (t, e, r) => t.replace(e, r),
        DatePrototypeGetDate: (t) => t.getDate(),
        DatePrototypeGetHours: (t) => t.getHours(),
        DatePrototypeGetMinutes: (t) => t.getMinutes(),
        DatePrototypeGetMonth: (t) => t.getMonth(),
        DatePrototypeGetSeconds: (t) => t.getSeconds(),
        Symbol,
        SymbolAsyncIterator: Symbol.asyncIterator,
        SymbolHasInstance: Symbol.hasInstance,
        SymbolIterator: Symbol.iterator,
        TypedArrayPrototypeSet: (t, e, r) => t.set(e, r),
        decodeURIComponent,
        Uint8Array,
        Int8Array,
        Array,
        Date,
      };
    },
    3966: (t) => {
      var e = Array.prototype.slice,
        r =
          "function" == typeof Object.keys
            ? Object.keys
            : function (t) {
                var e = [];
                for (var r in t) e.push(r);
                return e;
              };
      function o(t, e) {
        if (0 === t && 0 === e) return 1 / t == 1 / e;
        if (t === e) return !0;
        if (!(t instanceof Date && e instanceof Date))
          return y(t)
            ? y(e)
            : "object" != typeof t && "object" != typeof e
            ? t === e
            : f(t) || f(e)
            ? ((o = e),
              !(!f((r = t)) || !f(o)) &&
                (a(r)
                  ? a(o) &&
                    ((n = r.valueOf()) === (i = o.valueOf())
                      ? 0 !== n || 1 / n == 1 / i
                      : n != n && i != i)
                  : u(r)
                  ? u(o) && r.valueOf() === o.valueOf()
                  : p(r)
                  ? p(o) && r.valueOf() === o.valueOf()
                  : c(r)
                  ? c(o) && r.valueOf() === o.valueOf()
                  : l(r)
                  ? l(o) && Object(r).toString() === Object(o).toString()
                  : s(r, o)))
            : s(t, e);
        var r, o, n, i;
        try {
          return t.getTime() === e.getTime();
        } catch (t) {
          return !1;
        }
      }
      function n(t) {
        return null == t;
      }
      function i(t) {
        return "[object Arguments]" == Object.prototype.toString.call(t);
      }
      function a(t) {
        return "[object Number]" == Object.prototype.toString.call(t);
      }
      function u(t) {
        return "[object String]" == Object.prototype.toString.call(t);
      }
      function p(t) {
        return "[object Boolean]" == Object.prototype.toString.call(t);
      }
      function c(t) {
        return "[object BigInt]" == Object.prototype.toString.call(t);
      }
      function l(t) {
        return "[object Symbol]" == Object.prototype.toString.call(t);
      }
      function f(t) {
        return (
          null === t ||
          "boolean" == typeof t ||
          "number" == typeof t ||
          "string" == typeof t ||
          "symbol" == typeof t ||
          void 0 === t
        );
      }
      function y(t) {
        return "number" == typeof t && t != t;
      }
      function s(t, a) {
        if (n(t) || n(a)) return !1;
        if (t.prototype !== a.prototype) return !1;
        if (i(t)) return !!i(a) && o((t = e.call(t)), (a = e.call(a)));
        try {
          var u,
            p,
            c = r(t),
            l = r(a);
        } catch (t) {
          return !1;
        }
        if (c.length != l.length) return !1;
        for (c.sort(), l.sort(), p = c.length - 1; p >= 0; p--)
          if (c[p] != l[p]) return !1;
        for (p = c.length - 1; p >= 0; p--)
          if (!o(t[(u = c[p])], a[u])) return !1;
        return !0;
      }
      t.exports = { isDeepStrictEqual: o };
    },
    4338: (t, e, r) => {
      const o = r(1350),
        n = r(2727),
        i = r(4597),
        {
          ObjectCreate: a,
          ObjectDefineProperty: u,
          StringPrototypeToUpperCase: p,
          ArrayPrototypeSlice: c,
        } = r(3018);
      let l = a(null);
      const f = () => {};
      t.exports = {
        debuglog: function (t, e) {
          function r() {
            (t = p(t)), (a = !0);
          }
          let a,
            y = (...u) => {
              switch (
                (r(),
                (y = (function (t, e) {
                  if (void 0 === l[e])
                    if (t) {
                      const t = o.pid;
                      l[e] = function (r, ...o) {
                        var a = i.printf(r, ...o);
                        n.debug(
                          t,
                          e,
                          "%{public}s %{public}s: %{public}s",
                          e,
                          t,
                          a
                        );
                      };
                    } else l[e] = f;
                  return l[e];
                })(a, t)),
                "function" == typeof e && e(y),
                u.length)
              ) {
                case 1:
                  return y(u[0]);
                case 2:
                  return y(u[0], u[1]);
                default:
                  return y(u[0], ...c(u, 1));
              }
            },
            s = () => (r(), (s = () => a), a);
          const b = (...t) => {
            switch (t.length) {
              case 1:
                return y(t[0]);
              case 2:
                return y(t[0], t[1]);
              default:
                return y(t[0], ...c(t, 1));
            }
          };
          return (
            u(b, "enabled", {
              __proto__: null,
              get: () => s(),
              configurable: !0,
              enumerable: !0,
            }),
            b
          );
        },
      };
    },
    3823: (t, e, r) => {
      const {
          ObjectDefineProperty: o,
          ArrayIsArray: n,
          ArrayPrototypeIncludes: i,
          NumberIsNaN: a,
        } = r(3018),
        u = (t) => {
          const e = "__node_internal_" + t.name;
          return o(t, "name", { __proto__: null, value: e }), t;
        };
      (e.validateString = u((t, e) => {
        if ("string" != typeof t)
          throw new Error("ERR_INVALID_ARG_TYPE value:" + t + " name:" + e);
      })),
        (e.validateFunction = u((t, e) => {
          if ("function" != typeof t)
            throw new Error("ERR_INVALID_ARG_TYPE value:" + t + " name:" + e);
        })),
        (e.validateAbortSignal = u((t, e) => {
          if (
            void 0 !== t &&
            (null === t || "object" != typeof t || !("aborted" in t))
          )
            throw new Error(
              "ERR_INVALID_ARG_TYPE value:" + value + " name:" + e
            );
        })),
        (e.validateObject = u((t, e, r) => {
          const o = null == r,
            i = !o && r.allowArray,
            a = !o && r.allowFunction;
          if (
            ((o || !r.nullable) && null === t) ||
            (!i && n(t)) ||
            ("object" != typeof t && (!a || "function" != typeof t))
          )
            throw new Error("ERR_INVALID_ARG_TYPE value:" + t + " name:" + e);
        })),
        (e.validateNumber = function (t, e, r = void 0, o) {
          if ("number" != typeof t)
            throw new Error("ERR_INVALID_ARG_TYPE value:" + t + " name:" + e);
          if (
            (null != r && t < r) ||
            (null != o && t > o) ||
            ((null != r || null != o) && a(t))
          )
            throw new Error(
              "ERR_OUT_OF_RANGE, name:" +
                e +
                ", " +
                `${null != r ? `>= ${r}` : ""}${
                  null != r && null != o ? " && " : ""
                }${null != o ? `<= ${o}` : ""}` +
                t
            );
        }),
        (e.validateBoolean = function (t, e) {
          if ("boolean" != typeof t)
            throw new Error("ERR_INVALID_ARG_TYPE value:" + t + " name:" + e);
        }),
        (e.validateArray = u((t, e, r = 0) => {
          if (!Array.isArray(t)) throw new Error("Array:" + e);
          if (t.length < r)
            throw new Error(
              "ERR_INVALID_ARG_VALUE name:" +
                e +
                ",value:" +
                t +
                ",reason:" +
                `must be longer than ${r}`
            );
        })),
        (e.validateUnion = function (t, e, r) {
          if (!i(r, t))
            throw new Error(
              "ERR_INVALID_ARG_TYPE, name:" + e + ",union:" + r + ",value:" + t
            );
        });
    },
    2727: (e) => {
      e.exports = t$1;
    },
    1350: (t) => {
      t.exports = e$1;
    },
    4597: (t) => {
      t.exports = r$1;
    },
    3368: (t, e, r) => {
      var n, i;
      t.exports = ((n = { Buffer: () => index$2.Buffer }), (i = {}), r.d(i, n), i);
    },
    6553: () => {},
    6597: (t, e, r) => {
      var o = r(8750),
        n = r(1883),
        i = r(3771),
        a = r(9517),
        u = r(1554),
        p = o("%TypeError%");
      t.exports = function (t, e, r) {
        if ("String" !== a(t))
          throw new p("Assertion failed: `S` must be a String");
        if (!i(e) || e < 0 || e > u)
          throw new p(
            "Assertion failed: `length` must be an integer >= 0 and <= 2**53"
          );
        if ("Boolean" !== a(r))
          throw new p("Assertion failed: `unicode` must be a Boolean");
        return r
          ? e + 1 >= t.length
            ? e + 1
            : e + n(t, e)["[[CodeUnitCount]]"]
          : e + 1;
      };
    },
    4101: (t, e, r) => {
      var o = r(8750),
        n = r(2737),
        i = o("%TypeError%"),
        a = r(7057),
        u = o("%Reflect.apply%", !0) || n("Function.prototype.apply");
      t.exports = function (t, e) {
        var r = arguments.length > 2 ? arguments[2] : [];
        if (!a(r))
          throw new i(
            "Assertion failed: optional `argumentsList`, if provided, must be a List"
          );
        return u(t, e, r);
      };
    },
    1883: (t, e, r) => {
      var o = r(8750)("%TypeError%"),
        n = r(2737),
        i = r(8714),
        a = r(5722),
        u = r(9517),
        p = r(2955),
        c = n("String.prototype.charAt"),
        l = n("String.prototype.charCodeAt");
      t.exports = function (t, e) {
        if ("String" !== u(t))
          throw new o("Assertion failed: `string` must be a String");
        var r = t.length;
        if (e < 0 || e >= r)
          throw new o(
            "Assertion failed: `position` must be >= 0, and < the length of `string`"
          );
        var n = l(t, e),
          f = c(t, e),
          y = i(n),
          s = a(n);
        if (!y && !s)
          return {
            "[[CodePoint]]": f,
            "[[CodeUnitCount]]": 1,
            "[[IsUnpairedSurrogate]]": !1,
          };
        if (s || e + 1 === r)
          return {
            "[[CodePoint]]": f,
            "[[CodeUnitCount]]": 1,
            "[[IsUnpairedSurrogate]]": !0,
          };
        var b = l(t, e + 1);
        return a(b)
          ? {
              "[[CodePoint]]": p(n, b),
              "[[CodeUnitCount]]": 2,
              "[[IsUnpairedSurrogate]]": !1,
            }
          : {
              "[[CodePoint]]": f,
              "[[CodeUnitCount]]": 1,
              "[[IsUnpairedSurrogate]]": !0,
            };
      };
    },
    7232: (t, e, r) => {
      var o = r(8750)("%TypeError%"),
        n = r(8831),
        i = r(323),
        a = r(9517);
      t.exports = function (t, e, r) {
        if ("Object" !== a(t))
          throw new o("Assertion failed: Type(O) is not Object");
        if (!n(e))
          throw new o("Assertion failed: IsPropertyKey(P) is not true");
        return i(t, e, {
          "[[Configurable]]": !0,
          "[[Enumerable]]": !0,
          "[[Value]]": r,
          "[[Writable]]": !0,
        });
      };
    },
    3512: (t, e, r) => {
      var o = r(8750)("%TypeError%"),
        n = r(7232),
        i = r(8831),
        a = r(9517);
      t.exports = function (t, e, r) {
        if ("Object" !== a(t))
          throw new o("Assertion failed: Type(O) is not Object");
        if (!i(e))
          throw new o("Assertion failed: IsPropertyKey(P) is not true");
        var u = n(t, e, r);
        if (!u) throw new o("unable to create data property");
        return u;
      };
    },
    2435: (t, e, r) => {
      var o = r(8750)("%TypeError%"),
        n = r(5628),
        i = r(9623),
        a = r(9121),
        u = r(8831),
        p = r(5403),
        c = r(9517);
      t.exports = function (t, e, r) {
        if ("Object" !== c(t))
          throw new o("Assertion failed: Type(O) is not Object");
        if (!u(e))
          throw new o("Assertion failed: IsPropertyKey(P) is not true");
        return n(a, p, i, t, e, {
          "[[Configurable]]": !0,
          "[[Enumerable]]": !1,
          "[[Value]]": r,
          "[[Writable]]": !0,
        });
      };
    },
    9623: (t, e, r) => {
      var o = r(6688),
        n = r(7226),
        i = r(9517);
      t.exports = function (t) {
        return void 0 !== t && o(i, "Property Descriptor", "Desc", t), n(t);
      };
    },
    2010: (t, e, r) => {
      var o = r(8750)("%TypeError%"),
        n = r(6524),
        i = r(8831),
        a = r(9517);
      t.exports = function (t, e) {
        if ("Object" !== a(t))
          throw new o("Assertion failed: Type(O) is not Object");
        if (!i(e))
          throw new o(
            "Assertion failed: IsPropertyKey(P) is not true, got " + n(e)
          );
        return t[e];
      };
    },
    1414: (t, e, r) => {
      var o = r(8750),
        n = o("%TypeError%"),
        i = o("%SyntaxError%"),
        a = o("%Symbol.asyncIterator%", !0),
        u = r(6524),
        p = r(679)(),
        c = r(4602),
        l = r(6597),
        f = r(4101),
        y = r(1819),
        s = r(7057),
        b = r(9517);
      t.exports = function (t, e, r) {
        var o = e;
        if (
          (arguments.length < 2 && (o = "sync"), "sync" !== o && "async" !== o)
        )
          throw new n(
            "Assertion failed: `hint` must be one of 'sync' or 'async', got " +
              u(e)
          );
        var g = r;
        if (arguments.length < 3)
          if ("async" === o) {
            if ((p && a && (g = y(t, a)), void 0 === g))
              throw new i(
                "async from sync iterators aren't currently supported"
              );
          } else g = c({ AdvanceStringIndex: l, GetMethod: y, IsArray: s }, t);
        var h = f(g, t);
        if ("Object" !== b(h)) throw new n("iterator must return an object");
        return h;
      };
    },
    1819: (t, e, r) => {
      var o = r(8750)("%TypeError%"),
        n = r(267),
        i = r(212),
        a = r(8831),
        u = r(6524);
      t.exports = function (t, e) {
        if (!a(e))
          throw new o("Assertion failed: IsPropertyKey(P) is not true");
        var r = n(t, e);
        if (null != r) {
          if (!i(r)) throw new o(u(e) + " is not a function: " + u(r));
          return r;
        }
      };
    },
    267: (t, e, r) => {
      var o = r(8750)("%TypeError%"),
        n = r(8831),
        i = r(9424);
      t.exports = function (t, e) {
        if (!n(e))
          throw new o("Assertion failed: IsPropertyKey(P) is not true");
        return i(t)[e];
      };
    },
    5522: (t, e, r) => {
      var o = r(8750)("%TypeError%"),
        n = r(4101),
        i = r(7057),
        a = r(267),
        u = r(8831);
      t.exports = function (t, e) {
        if (!u(e)) throw new o("Assertion failed: P must be a Property Key");
        var r = arguments.length > 2 ? arguments[2] : [];
        if (!i(r))
          throw new o(
            "Assertion failed: optional `argumentsList`, if provided, must be a List"
          );
        var p = a(t, e);
        return n(p, t, r);
      };
    },
    6445: (t, e, r) => {
      var o = r(7492),
        n = r(9517),
        i = r(6688);
      t.exports = function (t) {
        return (
          void 0 !== t &&
          (i(n, "Property Descriptor", "Desc", t),
          !(!o(t, "[[Get]]") && !o(t, "[[Set]]")))
        );
      };
    },
    7057: (t, e, r) => {
      t.exports = r(2924);
    },
    212: (t, e, r) => {
      t.exports = r(2922);
    },
    9121: (t, e, r) => {
      var o = r(7492),
        n = r(9517),
        i = r(6688);
      t.exports = function (t) {
        return (
          void 0 !== t &&
          (i(n, "Property Descriptor", "Desc", t),
          !(!o(t, "[[Value]]") && !o(t, "[[Writable]]")))
        );
      };
    },
    4996: (t, e, r) => {
      var o = r(8750),
        n = o("%Object.preventExtensions%", !0),
        i = o("%Object.isExtensible%", !0),
        a = r(2410);
      t.exports = n
        ? function (t) {
            return !a(t) && i(t);
          }
        : function (t) {
            return !a(t);
          };
    },
    4635: (t, e, r) => {
      var o = r(6688),
        n = r(6445),
        i = r(9121),
        a = r(9517);
      t.exports = function (t) {
        return (
          void 0 !== t &&
          (o(a, "Property Descriptor", "Desc", t), !n(t) && !i(t))
        );
      };
    },
    3771: (t, e, r) => {
      var o = r(4642),
        n = r(1068),
        i = r(9517),
        a = r(7152),
        u = r(8426);
      t.exports = function (t) {
        if ("Number" !== i(t) || a(t) || !u(t)) return !1;
        var e = o(t);
        return n(e) === e;
      };
    },
    8831: (t) => {
      t.exports = function (t) {
        return "string" == typeof t || "symbol" == typeof t;
      };
    },
    8429: (t, e, r) => {
      var o = r(2737)("Array.prototype.push"),
        n = r(1414),
        i = r(806),
        a = r(564);
      t.exports = function (t) {
        var e;
        e = arguments.length > 1 ? n(t, "sync", arguments[1]) : n(t, "sync");
        for (var r = [], u = !0; u; )
          if ((u = i(e))) {
            var p = a(u);
            o(r, p);
          }
        return r;
      };
    },
    5389: (t, e, r) => {
      var o = r(8750)("%TypeError%"),
        n = r(2010),
        i = r(2975),
        a = r(9517);
      t.exports = function (t) {
        if ("Object" !== a(t))
          throw new o("Assertion failed: Type(iterResult) is not Object");
        return i(n(t, "done"));
      };
    },
    1673: (t, e, r) => {
      var o = r(8750)("%TypeError%"),
        n = r(5522),
        i = r(9517);
      t.exports = function (t, e) {
        var r = n(t, "next", arguments.length < 2 ? [] : [e]);
        if ("Object" !== i(r))
          throw new o("iterator next must return an object");
        return r;
      };
    },
    806: (t, e, r) => {
      var o = r(5389),
        n = r(1673);
      t.exports = function (t) {
        var e = n(t);
        return !0 !== o(e) && e;
      };
    },
    564: (t, e, r) => {
      var o = r(8750)("%TypeError%"),
        n = r(2010),
        i = r(9517);
      t.exports = function (t) {
        if ("Object" !== i(t))
          throw new o("Assertion failed: Type(iterResult) is not Object");
        return n(t, "value");
      };
    },
    323: (t, e, r) => {
      var o = r(8750),
        n = r(7502),
        i = o("%SyntaxError%"),
        a = o("%TypeError%"),
        u = r(5505),
        p = r(6445),
        c = r(9121),
        l = r(4996),
        f = r(8831),
        y = r(6212),
        s = r(5403),
        b = r(9517),
        g = r(7299);
      t.exports = function (t, e, r) {
        if ("Object" !== b(t))
          throw new a("Assertion failed: O must be an Object");
        if (!f(e)) throw new a("Assertion failed: P must be a Property Key");
        if (!u({ Type: b, IsDataDescriptor: c, IsAccessorDescriptor: p }, r))
          throw new a("Assertion failed: Desc must be a Property Descriptor");
        if (!n) {
          if (p(r))
            throw new i(
              "This environment does not support accessor property descriptors."
            );
          var o =
              !(e in t) &&
              r["[[Writable]]"] &&
              r["[[Enumerable]]"] &&
              r["[[Configurable]]"] &&
              "[[Value]]" in r,
            h =
              e in t &&
              (!("[[Configurable]]" in r) || r["[[Configurable]]"]) &&
              (!("[[Enumerable]]" in r) || r["[[Enumerable]]"]) &&
              (!("[[Writable]]" in r) || r["[[Writable]]"]) &&
              "[[Value]]" in r;
          if (o || h) return (t[e] = r["[[Value]]"]), s(t[e], r["[[Value]]"]);
          throw new i(
            "This environment does not support defining non-writable, non-enumerable, or non-configurable properties"
          );
        }
        var d = n(t, e),
          m = d && y(d),
          v = l(t);
        return g(t, e, v, r, m);
      };
    },
    7685: (t, e, r) => {
      var o = r(8750)("%TypeError%"),
        n = r(3330),
        i = r(9517);
      t.exports = function (t) {
        if ("Object" !== i(t))
          throw new o("Assertion failed: O must be an Object");
        if (!n)
          throw new o("This environment does not support fetching prototypes.");
        return n(t);
      };
    },
    5627: (t, e, r) => {
      var o = r(8750)("%TypeError%"),
        n = r(8780),
        i = r(7685),
        a = r(9517);
      t.exports = function (t, e) {
        if ("Object" !== a(e) && "Null" !== a(e))
          throw new o("Assertion failed: V must be Object or Null");
        try {
          n(t, e);
        } catch (t) {
          return !1;
        }
        return i(t) === e;
      };
    },
    2355: (t, e, r) => {
      t.exports = r(3802);
    },
    5403: (t, e, r) => {
      var o = r(7152);
      t.exports = function (t, e) {
        return t === e ? 0 !== t || 1 / t == 1 / e : o(t) && o(e);
      };
    },
    2975: (t) => {
      t.exports = function (t) {
        return !!t;
      };
    },
    9424: (t, e, r) => {
      var o = r(8750)("%Object%"),
        n = r(2355);
      t.exports = function (t) {
        return n(t), o(t);
      };
    },
    6212: (t, e, r) => {
      var o = r(7492),
        n = r(8750)("%TypeError%"),
        i = r(9517),
        a = r(2975),
        u = r(212);
      t.exports = function (t) {
        if ("Object" !== i(t))
          throw new n("ToPropertyDescriptor requires an object");
        var e = {};
        if (
          (o(t, "enumerable") && (e["[[Enumerable]]"] = a(t.enumerable)),
          o(t, "configurable") && (e["[[Configurable]]"] = a(t.configurable)),
          o(t, "value") && (e["[[Value]]"] = t.value),
          o(t, "writable") && (e["[[Writable]]"] = a(t.writable)),
          o(t, "get"))
        ) {
          var r = t.get;
          if (void 0 !== r && !u(r)) throw new n("getter must be a function");
          e["[[Get]]"] = r;
        }
        if (o(t, "set")) {
          var p = t.set;
          if (void 0 !== p && !u(p)) throw new n("setter must be a function");
          e["[[Set]]"] = p;
        }
        if (
          (o(e, "[[Get]]") || o(e, "[[Set]]")) &&
          (o(e, "[[Value]]") || o(e, "[[Writable]]"))
        )
          throw new n(
            "Invalid property descriptor. Cannot both specify accessors and a value or writable attribute"
          );
        return e;
      };
    },
    9517: (t, e, r) => {
      var o = r(224);
      t.exports = function (t) {
        return "symbol" == typeof t
          ? "Symbol"
          : "bigint" == typeof t
          ? "BigInt"
          : o(t);
      };
    },
    2955: (t, e, r) => {
      var o = r(8750),
        n = o("%TypeError%"),
        i = o("%String.fromCharCode%"),
        a = r(8714),
        u = r(5722);
      t.exports = function (t, e) {
        if (!a(t) || !u(e))
          throw new n(
            "Assertion failed: `lead` must be a leading surrogate char code, and `trail` must be a trailing surrogate char code"
          );
        return i(t) + i(e);
      };
    },
    7299: (t, e, r) => {
      var o = r(8750)("%TypeError%"),
        n = r(5628),
        i = r(9283),
        a = r(5505),
        u = r(9623),
        p = r(6445),
        c = r(9121),
        l = r(4635),
        f = r(8831),
        y = r(5403),
        s = r(9517);
      t.exports = function (t, e, r, b, g) {
        var h,
          d,
          m = s(t);
        if ("Undefined" !== m && "Object" !== m)
          throw new o("Assertion failed: O must be undefined or an Object");
        if (!f(e)) throw new o("Assertion failed: P must be a Property Key");
        if ("Boolean" !== s(r))
          throw new o("Assertion failed: extensible must be a Boolean");
        if (!a({ Type: s, IsDataDescriptor: c, IsAccessorDescriptor: p }, b))
          throw new o("Assertion failed: Desc must be a Property Descriptor");
        if (
          "Undefined" !== s(g) &&
          !a({ Type: s, IsDataDescriptor: c, IsAccessorDescriptor: p }, g)
        )
          throw new o(
            "Assertion failed: current must be a Property Descriptor, or undefined"
          );
        if ("Undefined" === s(g))
          return (
            !!r &&
            ("Undefined" === m ||
              (p(b)
                ? n(c, y, u, t, e, b)
                : n(c, y, u, t, e, {
                    "[[Configurable]]": !!b["[[Configurable]]"],
                    "[[Enumerable]]": !!b["[[Enumerable]]"],
                    "[[Value]]": b["[[Value]]"],
                    "[[Writable]]": !!b["[[Writable]]"],
                  })))
          );
        if (!i({ IsAccessorDescriptor: p, IsDataDescriptor: c }, g))
          throw new o(
            "`current`, when present, must be a fully populated and valid Property Descriptor"
          );
        if (!g["[[Configurable]]"]) {
          if ("[[Configurable]]" in b && b["[[Configurable]]"]) return !1;
          if (
            "[[Enumerable]]" in b &&
            !y(b["[[Enumerable]]"], g["[[Enumerable]]"])
          )
            return !1;
          if (!l(b) && !y(p(b), p(g))) return !1;
          if (p(g)) {
            if ("[[Get]]" in b && !y(b["[[Get]]"], g["[[Get]]"])) return !1;
            if ("[[Set]]" in b && !y(b["[[Set]]"], g["[[Set]]"])) return !1;
          } else if (!g["[[Writable]]"]) {
            if ("[[Writable]]" in b && b["[[Writable]]"]) return !1;
            if ("[[Value]]" in b && !y(b["[[Value]]"], g["[[Value]]"]))
              return !1;
          }
        }
        return (
          "Undefined" === m ||
          (c(g) && p(b)
            ? ((h = ("[[Configurable]]" in b ? b : g)["[[Configurable]]"]),
              (d = ("[[Enumerable]]" in b ? b : g)["[[Enumerable]]"]),
              n(c, y, u, t, e, {
                "[[Configurable]]": !!h,
                "[[Enumerable]]": !!d,
                "[[Get]]": ("[[Get]]" in b ? b : g)["[[Get]]"],
                "[[Set]]": ("[[Set]]" in b ? b : g)["[[Set]]"],
              }))
            : p(g) && c(b)
            ? ((h = ("[[Configurable]]" in b ? b : g)["[[Configurable]]"]),
              (d = ("[[Enumerable]]" in b ? b : g)["[[Enumerable]]"]),
              n(c, y, u, t, e, {
                "[[Configurable]]": !!h,
                "[[Enumerable]]": !!d,
                "[[Value]]": ("[[Value]]" in b ? b : g)["[[Value]]"],
                "[[Writable]]": !!("[[Writable]]" in b ? b : g)["[[Writable]]"],
              }))
            : n(c, y, u, t, e, b))
        );
      };
    },
    4642: (t, e, r) => {
      var o = r(8750)("%Math.abs%");
      t.exports = function (t) {
        return o(t);
      };
    },
    1068: (t, e, r) => {
      var o = r(9517),
        n = Math.floor;
      t.exports = function (t) {
        return "BigInt" === o(t) ? t : n(t);
      };
    },
    3802: (t, e, r) => {
      var o = r(8750)("%TypeError%");
      t.exports = function (t, e) {
        if (null == t) throw new o(e || "Cannot call method on " + t);
        return t;
      };
    },
    224: (t) => {
      t.exports = function (t) {
        return null === t
          ? "Null"
          : void 0 === t
          ? "Undefined"
          : "function" == typeof t || "object" == typeof t
          ? "Object"
          : "number" == typeof t
          ? "Number"
          : "boolean" == typeof t
          ? "Boolean"
          : "string" == typeof t
          ? "String"
          : void 0;
      };
    },
    5628: (t, e, r) => {
      var o = r(1365),
        n = r(8750),
        i = o() && n("%Object.defineProperty%", !0),
        a = o.hasArrayLengthDefineBug(),
        u = a && r(2924),
        p = r(2737)("Object.prototype.propertyIsEnumerable");
      t.exports = function (t, e, r, o, n, c) {
        if (!i) {
          if (!t(c)) return !1;
          if (!c["[[Configurable]]"] || !c["[[Writable]]"]) return !1;
          if (n in o && p(o, n) !== !!c["[[Enumerable]]"]) return !1;
          var l = c["[[Value]]"];
          return (o[n] = l), e(o[n], l);
        }
        return a &&
          "length" === n &&
          "[[Value]]" in c &&
          u(o) &&
          o.length !== c["[[Value]]"]
          ? ((o.length = c["[[Value]]"]), o.length === c["[[Value]]"])
          : (i(o, n, r(c)), !0);
      };
    },
    2924: (t, e, r) => {
      var o = r(8750)("%Array%"),
        n = !o.isArray && r(2737)("Object.prototype.toString");
      t.exports =
        o.isArray ||
        function (t) {
          return "[object Array]" === n(t);
        };
    },
    6688: (t, e, r) => {
      var o = r(8750),
        n = o("%TypeError%"),
        i = o("%SyntaxError%"),
        a = r(7492),
        u = {
          "Property Descriptor": function (t) {
            var e = {
              "[[Configurable]]": !0,
              "[[Enumerable]]": !0,
              "[[Get]]": !0,
              "[[Set]]": !0,
              "[[Value]]": !0,
              "[[Writable]]": !0,
            };
            if (!t) return !1;
            for (var r in t) if (a(t, r) && !e[r]) return !1;
            var o = a(t, "[[Value]]"),
              i = a(t, "[[Get]]") || a(t, "[[Set]]");
            if (o && i)
              throw new n(
                "Property Descriptors may not be both accessor and data descriptors"
              );
            return !0;
          },
          "Match Record": r(1271),
          "Iterator Record": function (t) {
            return (
              a(t, "[[Iterator]]") && a(t, "[[NextMethod]]") && a(t, "[[Done]]")
            );
          },
          "PromiseCapability Record": function (t) {
            return (
              !!t &&
              a(t, "[[Resolve]]") &&
              "function" == typeof t["[[Resolve]]"] &&
              a(t, "[[Reject]]") &&
              "function" == typeof t["[[Reject]]"] &&
              a(t, "[[Promise]]") &&
              t["[[Promise]]"] &&
              "function" == typeof t["[[Promise]]"].then
            );
          },
          "AsyncGeneratorRequest Record": function (t) {
            return (
              !!t &&
              a(t, "[[Completion]]") &&
              a(t, "[[Capability]]") &&
              u["PromiseCapability Record"](t["[[Capability]]"])
            );
          },
        };
      t.exports = function (t, e, r, o) {
        var a = u[e];
        if ("function" != typeof a) throw new i("unknown record type: " + e);
        if ("Object" !== t(o) || !a(o)) throw new n(r + " must be a " + e);
      };
    },
    7226: (t) => {
      t.exports = function (t) {
        if (void 0 === t) return t;
        var e = {};
        return (
          "[[Value]]" in t && (e.value = t["[[Value]]"]),
          "[[Writable]]" in t && (e.writable = !!t["[[Writable]]"]),
          "[[Get]]" in t && (e.get = t["[[Get]]"]),
          "[[Set]]" in t && (e.set = t["[[Set]]"]),
          "[[Enumerable]]" in t && (e.enumerable = !!t["[[Enumerable]]"]),
          "[[Configurable]]" in t && (e.configurable = !!t["[[Configurable]]"]),
          e
        );
      };
    },
    4602: (t, e, r) => {
      var o = r(679)(),
        n = r(8750),
        i = r(2737),
        a = r(8559),
        u = n("%Symbol.iterator%", !0),
        p = i("String.prototype.slice"),
        c = n("%String%");
      t.exports = function (t, e) {
        var r;
        return (
          o
            ? (r = t.GetMethod(e, u))
            : t.IsArray(e)
            ? (r = function () {
                var t = -1,
                  e = this;
                return {
                  next: function () {
                    return { done: (t += 1) >= e.length, value: e[t] };
                  },
                };
              })
            : a(e) &&
              (r = function () {
                var r = 0;
                return {
                  next: function () {
                    var o = t.AdvanceStringIndex(c(e), r, !0),
                      n = p(e, r, o);
                    return (r = o), { done: o > e.length, value: n };
                  },
                };
              }),
          r
        );
      };
    },
    3330: (t, e, r) => {
      var o = r(8750)("%Object.getPrototypeOf%", !0),
        n = r(2574)();
      t.exports =
        o ||
        (n
          ? function (t) {
              return t.__proto__;
            }
          : null);
    },
    8426: (t, e, r) => {
      var o = r(7152);
      t.exports = function (t) {
        return (
          ("number" == typeof t || "bigint" == typeof t) &&
          !o(t) &&
          t !== 1 / 0 &&
          t !== -1 / 0
        );
      };
    },
    9283: (t) => {
      t.exports = function (t, e) {
        return (
          !!e &&
          "object" == typeof e &&
          "[[Enumerable]]" in e &&
          "[[Configurable]]" in e &&
          (t.IsAccessorDescriptor(e) || t.IsDataDescriptor(e))
        );
      };
    },
    8714: (t) => {
      t.exports = function (t) {
        return "number" == typeof t && t >= 55296 && t <= 56319;
      };
    },
    1271: (t, e, r) => {
      var o = r(7492);
      t.exports = function (t) {
        return (
          o(t, "[[StartIndex]]") &&
          o(t, "[[EndIndex]]") &&
          t["[[StartIndex]]"] >= 0 &&
          t["[[EndIndex]]"] >= t["[[StartIndex]]"] &&
          String(parseInt(t["[[StartIndex]]"], 10)) ===
            String(t["[[StartIndex]]"]) &&
          String(parseInt(t["[[EndIndex]]"], 10)) === String(t["[[EndIndex]]"])
        );
      };
    },
    7152: (t) => {
      t.exports =
        Number.isNaN ||
        function (t) {
          return t != t;
        };
    },
    2410: (t) => {
      t.exports = function (t) {
        return null === t || ("function" != typeof t && "object" != typeof t);
      };
    },
    5505: (t, e, r) => {
      var o = r(8750),
        n = r(7492),
        i = o("%TypeError%");
      t.exports = function (t, e) {
        if ("Object" !== t.Type(e)) return !1;
        var r = {
          "[[Configurable]]": !0,
          "[[Enumerable]]": !0,
          "[[Get]]": !0,
          "[[Set]]": !0,
          "[[Value]]": !0,
          "[[Writable]]": !0,
        };
        for (var o in e) if (n(e, o) && !r[o]) return !1;
        if (t.IsDataDescriptor(e) && t.IsAccessorDescriptor(e))
          throw new i(
            "Property Descriptors may not be both accessor and data descriptors"
          );
        return !0;
      };
    },
    5722: (t) => {
      t.exports = function (t) {
        return "number" == typeof t && t >= 56320 && t <= 57343;
      };
    },
    1554: (t, e, r) => {
      var o = r(8750),
        n = o("%Math%"),
        i = o("%Number%");
      t.exports = i.MAX_SAFE_INTEGER || n.pow(2, 53) - 1;
    },
    8780: (t, e, r) => {
      var o = r(8750)("%Object.setPrototypeOf%", !0),
        n = r(2574)();
      t.exports =
        o ||
        (n
          ? function (t, e) {
              return (t.__proto__ = e), t;
            }
          : null);
    },
  },
  i$2 = {};
function a$1(t) {
  var e = i$2[t];
  if (void 0 !== e) return e.exports;
  var r = (i$2[t] = { exports: {} });
  return n$1[t](r, r.exports, a$1), r.exports;
}
(a$1.d = (t, e) => {
  for (var r in e)
    a$1.o(e, r) &&
      !a$1.o(t, r) &&
      Object.defineProperty(t, r, { enumerable: !0, get: e[r] });
}),
  (a$1.g = (function () {
    if ("object" == typeof globalThis) return globalThis;
    try {
      return this || new Function("return this")();
    } catch (t) {
      if ("object" == typeof window) return window;
    }
  })()),
  (a$1.o = (t, e) => Object.prototype.hasOwnProperty.call(t, e));
var u = {};
(() => {
  var t = u;
  const e = a$1(4597),
    {
      ArrayIsArray: r,
      ArrayPrototypeJoin: o,
      Date: n,
      DatePrototypeGetDate: i,
      DatePrototypeGetHours: p,
      DatePrototypeGetMinutes: c,
      DatePrototypeGetMonth: l,
      DatePrototypeGetSeconds: f,
      Error: y,
      ObjectDefineProperty: s,
      ObjectKeys: b,
      ObjectPrototypeToString: g,
      ObjectSetPrototypeOf: h,
      StringPrototypePadStart: d,
    } = a$1(3018),
    { validateString: m } = a$1(3823),
    { debuglog: v } = a$1(4338),
    { isDeepStrictEqual: S } = a$1(3966),
    { isBuffer: A } = a$1(3368).Buffer;
  function w(t) {
    return "boolean" == typeof t;
  }
  function P(t) {
    return null === t;
  }
  function j(t) {
    return null == t;
  }
  function O(t) {
    return "number" == typeof t;
  }
  function E(t) {
    return "string" == typeof t;
  }
  function x(t) {
    return "symbol" == typeof t;
  }
  function I(t) {
    return void 0 === t;
  }
  function M(t) {
    return null !== t && "object" == typeof t;
  }
  function R(t) {
    return "[object Error]" === g(t) || t instanceof y;
  }
  function T(t) {
    return "function" == typeof t;
  }
  function D(t) {
    return null === t || ("object" != typeof t && "function" != typeof t);
  }
  function _(t) {
    return d(t.toString(), 2, "0");
  }
  const N = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  function C(...t) {
    console.log(
      "%s - %s",
      (function () {
        const t = new n(),
          e = o([_(p(t)), _(c(t)), _(f(t))], ":");
        return `${i(t)} ${N[l(t)]} ${e}`;
      })(),
      ...t
    );
  }
  function F(t, e) {
    if (null == t) throw new y("ERR_INVALID_ARG_TYPE, ctor:" + t);
    if (null == e) throw new y("ERR_INVALID_ARG_TYPE, superCtor:" + e);
    if (void 0 === e.prototype)
      throw new y("ERR_INVALID_ARG_TYPE, superCtor.prototype:" + e.prototype);
    s(t, "super_", {
      __proto__: null,
      value: e,
      writable: !0,
      configurable: !0,
    }),
      h(t.prototype, e.prototype);
  }
  function G(t, e) {
    if (null === e || "object" != typeof e) return t;
    const r = b(e);
    let o = r.length;
    for (; o--; ) t[r[o]] = e[r[o]];
    return t;
  }
  function U(t) {
    return new e.types().isRegExp(t);
  }
  function k(t) {
    return new e.types().isDate(t);
  }
  function W() {
    return new e.types();
  }
  const V = new RegExp(
    "[\\u001B\\u009B][[\\]()#;?]*(?:(?:(?:(?:;[-a-zA-Z\\d\\/#&.:=?%@~_]+)*|[a-zA-Z\\d]+(?:;[-a-zA-Z\\d\\/#&.:=?%@~_]*)*)?\\u0007)|(?:(?:\\d{1,4}(?:;\\d{0,4})*)?[\\dA-PR-TZcf-ntqry=><~]))",
    "g"
  );
  function B(t) {
    return m(t, "str"), t.replace(V, "");
  }
  const {
    printf: L,
    getErrorString: $,
    callbackWrapper: H,
    promiseWrapper: q,
    TextDecoder: J,
    TextEncoder: Y,
  } = e;
  (t.ZP = {
    _extend: G,
    callbackify: H,
    debug: v,
    debuglog: v,
    format: L,
    getSystemErrorName: $,
    inherits: F,
    isArray: r,
    isBoolean: w,
    isBuffer: A,
    isDeepStrictEqual: S,
    isNull: P,
    isNullOrUndefined: j,
    isNumber: O,
    isString: E,
    isSymbol: x,
    isUndefined: I,
    isRegExp: U,
    isObject: M,
    isDate: k,
    isError: R,
    isFunction: T,
    isPrimitive: D,
    log: C,
    promisify: q,
    stripVTControlCharacters: B,
    TextDecoder: J,
    TextEncoder: Y,
    types: W(),
    inspect: function () {},
  }),
    (t.uk = G),
    (t.wI = H),
    (t.fF = v),
    (t.ZR = v),
    (t.WU = L),
    (t.sR = $),
    (t.XW = F),
    (t.kJ = r),
    (t.jn = w),
    (t.zH = A),
    (t.QY = S),
    (t.Ft = P),
    (t.le = j),
    (t.hj = O),
    (t.HD = E),
    (t.yk = x),
    (t.o8 = I),
    (t.Kj = U),
    (t.Kn = M),
    (t.J_ = k),
    (t.VZ = R),
    (t.mf = T),
    (t.pt = D),
    (t.cM = C),
    (t.Fr = q),
    (t.S9 = B),
    (t.kY = J),
    (t.po = Y),
    (t.V5 = W()),
    (t.XY = function () {});
})();
var b = u.ZP;

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);
  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    enumerableOnly && (symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    })), keys.push.apply(keys, symbols);
  }
  return keys;
}
function _objectSpread2(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = null != arguments[i] ? arguments[i] : {};
    i % 2 ? ownKeys(Object(source), !0).forEach(function (key) {
      _defineProperty(target, key, source[key]);
    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) {
      Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
    });
  }
  return target;
}
function _typeof$1(obj) {
  "@babel/helpers - typeof";

  return _typeof$1 = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) {
    return typeof obj;
  } : function (obj) {
    return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
  }, _typeof$1(obj);
}
function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}
function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor);
  }
}
function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  Object.defineProperty(Constructor, "prototype", {
    writable: false
  });
  return Constructor;
}
function _defineProperty(obj, key, value) {
  key = _toPropertyKey(key);
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }
  return obj;
}
function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }
  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  Object.defineProperty(subClass, "prototype", {
    writable: false
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}
function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}
function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };
  return _setPrototypeOf(o, p);
}
function _isNativeReflectConstruct() {
  if (typeof Reflect === "undefined" || !Reflect.construct) return false;
  if (Reflect.construct.sham) return false;
  if (typeof Proxy === "function") return true;
  try {
    Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
    return true;
  } catch (e) {
    return false;
  }
}
function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }
  return self;
}
function _possibleConstructorReturn(self, call) {
  if (call && (typeof call === "object" || typeof call === "function")) {
    return call;
  } else if (call !== void 0) {
    throw new TypeError("Derived constructors may only return object or undefined");
  }
  return _assertThisInitialized(self);
}
function _createSuper(Derived) {
  var hasNativeReflectConstruct = _isNativeReflectConstruct();
  return function _createSuperInternal() {
    var Super = _getPrototypeOf(Derived),
      result;
    if (hasNativeReflectConstruct) {
      var NewTarget = _getPrototypeOf(this).constructor;
      result = Reflect.construct(Super, arguments, NewTarget);
    } else {
      result = Super.apply(this, arguments);
    }
    return _possibleConstructorReturn(this, result);
  };
}
function _toPrimitive(input, hint) {
  if (typeof input !== "object" || input === null) return input;
  var prim = input[Symbol.toPrimitive];
  if (prim !== undefined) {
    var res = prim.call(input, hint || "default");
    if (typeof res !== "object") return res;
    throw new TypeError("@@toPrimitive must return a primitive value.");
  }
  return (hint === "string" ? String : Number)(input);
}
function _toPropertyKey(arg) {
  var key = _toPrimitive(arg, "string");
  return typeof key === "symbol" ? key : String(key);
}

var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

/* eslint no-invalid-this: 1 */

var ERROR_MESSAGE = 'Function.prototype.bind called on incompatible ';
var slice$1 = Array.prototype.slice;
var toStr$4 = Object.prototype.toString;
var funcType = '[object Function]';
var implementation$8 = function bind(that) {
  var target = this;
  if (typeof target !== 'function' || toStr$4.call(target) !== funcType) {
    throw new TypeError(ERROR_MESSAGE + target);
  }
  var args = slice$1.call(arguments, 1);
  var bound;
  var binder = function binder() {
    if (this instanceof bound) {
      var result = target.apply(this, args.concat(slice$1.call(arguments)));
      if (Object(result) === result) {
        return result;
      }
      return this;
    } else {
      return target.apply(that, args.concat(slice$1.call(arguments)));
    }
  };
  var boundLength = Math.max(0, target.length - args.length);
  var boundArgs = [];
  for (var i = 0; i < boundLength; i++) {
    boundArgs.push('$' + i);
  }
  bound = Function('binder', 'return function (' + boundArgs.join(',') + '){ return binder.apply(this,arguments); }')(binder);
  if (target.prototype) {
    var Empty = function Empty() {};
    Empty.prototype = target.prototype;
    bound.prototype = new Empty();
    Empty.prototype = null;
  }
  return bound;
};

var implementation$7 = implementation$8;
var functionBind = Function.prototype.bind || implementation$7;

var toStr$3 = Object.prototype.toString;
var isArguments = function isArguments(value) {
  var str = toStr$3.call(value);
  var isArgs = str === '[object Arguments]';
  if (!isArgs) {
    isArgs = str !== '[object Array]' && value !== null && _typeof$1(value) === 'object' && typeof value.length === 'number' && value.length >= 0 && toStr$3.call(value.callee) === '[object Function]';
  }
  return isArgs;
};

var implementation$6;
var hasRequiredImplementation;
function requireImplementation() {
  if (hasRequiredImplementation) return implementation$6;
  hasRequiredImplementation = 1;
  var keysShim;
  if (!Object.keys) {
    // modified from https://github.com/es-shims/es5-shim
    var has = Object.prototype.hasOwnProperty;
    var toStr = Object.prototype.toString;
    var isArgs = isArguments; // eslint-disable-line global-require
    var isEnumerable = Object.prototype.propertyIsEnumerable;
    var hasDontEnumBug = !isEnumerable.call({
      toString: null
    }, 'toString');
    var hasProtoEnumBug = isEnumerable.call(function () {}, 'prototype');
    var dontEnums = ['toString', 'toLocaleString', 'valueOf', 'hasOwnProperty', 'isPrototypeOf', 'propertyIsEnumerable', 'constructor'];
    var equalsConstructorPrototype = function equalsConstructorPrototype(o) {
      var ctor = o.constructor;
      return ctor && ctor.prototype === o;
    };
    var excludedKeys = {
      $applicationCache: true,
      $console: true,
      $external: true,
      $frame: true,
      $frameElement: true,
      $frames: true,
      $innerHeight: true,
      $innerWidth: true,
      $onmozfullscreenchange: true,
      $onmozfullscreenerror: true,
      $outerHeight: true,
      $outerWidth: true,
      $pageXOffset: true,
      $pageYOffset: true,
      $parent: true,
      $scrollLeft: true,
      $scrollTop: true,
      $scrollX: true,
      $scrollY: true,
      $self: true,
      $webkitIndexedDB: true,
      $webkitStorageInfo: true,
      $window: true
    };
    var hasAutomationEqualityBug = function () {
      /* global window */
      if (typeof window === 'undefined') {
        return false;
      }
      for (var k in window) {
        try {
          if (!excludedKeys['$' + k] && has.call(window, k) && window[k] !== null && _typeof$1(window[k]) === 'object') {
            try {
              equalsConstructorPrototype(window[k]);
            } catch (e) {
              return true;
            }
          }
        } catch (e) {
          return true;
        }
      }
      return false;
    }();
    var equalsConstructorPrototypeIfNotBuggy = function equalsConstructorPrototypeIfNotBuggy(o) {
      /* global window */
      if (typeof window === 'undefined' || !hasAutomationEqualityBug) {
        return equalsConstructorPrototype(o);
      }
      try {
        return equalsConstructorPrototype(o);
      } catch (e) {
        return false;
      }
    };
    keysShim = function keys(object) {
      var isObject = object !== null && _typeof$1(object) === 'object';
      var isFunction = toStr.call(object) === '[object Function]';
      var isArguments = isArgs(object);
      var isString = isObject && toStr.call(object) === '[object String]';
      var theKeys = [];
      if (!isObject && !isFunction && !isArguments) {
        throw new TypeError('Object.keys called on a non-object');
      }
      var skipProto = hasProtoEnumBug && isFunction;
      if (isString && object.length > 0 && !has.call(object, 0)) {
        for (var i = 0; i < object.length; ++i) {
          theKeys.push(String(i));
        }
      }
      if (isArguments && object.length > 0) {
        for (var j = 0; j < object.length; ++j) {
          theKeys.push(String(j));
        }
      } else {
        for (var name in object) {
          if (!(skipProto && name === 'prototype') && has.call(object, name)) {
            theKeys.push(String(name));
          }
        }
      }
      if (hasDontEnumBug) {
        var skipConstructor = equalsConstructorPrototypeIfNotBuggy(object);
        for (var k = 0; k < dontEnums.length; ++k) {
          if (!(skipConstructor && dontEnums[k] === 'constructor') && has.call(object, dontEnums[k])) {
            theKeys.push(dontEnums[k]);
          }
        }
      }
      return theKeys;
    };
  }
  implementation$6 = keysShim;
  return implementation$6;
}

var slice = Array.prototype.slice;
var isArgs = isArguments;
var origKeys = Object.keys;
var keysShim = origKeys ? function keys(o) {
  return origKeys(o);
} : requireImplementation();
var originalKeys = Object.keys;
keysShim.shim = function shimObjectKeys() {
  if (Object.keys) {
    var keysWorksWithArguments = function () {
      // Safari 5.0 bug
      var args = Object.keys(arguments);
      return args && args.length === arguments.length;
    }(1, 2);
    if (!keysWorksWithArguments) {
      Object.keys = function keys(object) {
        // eslint-disable-line func-name-matching
        if (isArgs(object)) {
          return originalKeys(slice.call(object));
        }
        return originalKeys(object);
      };
    }
  } else {
    Object.keys = keysShim;
  }
  return Object.keys || keysShim;
};
var objectKeys = keysShim;

/* eslint complexity: [2, 18], max-statements: [2, 33] */
var shams$1 = function hasSymbols() {
  if (typeof Symbol !== 'function' || typeof Object.getOwnPropertySymbols !== 'function') {
    return false;
  }
  if (_typeof$1(Symbol.iterator) === 'symbol') {
    return true;
  }
  var obj = {};
  var sym = Symbol('test');
  var symObj = Object(sym);
  if (typeof sym === 'string') {
    return false;
  }
  if (Object.prototype.toString.call(sym) !== '[object Symbol]') {
    return false;
  }
  if (Object.prototype.toString.call(symObj) !== '[object Symbol]') {
    return false;
  }

  // temp disabled per https://github.com/ljharb/object.assign/issues/17
  // if (sym instanceof Symbol) { return false; }
  // temp disabled per https://github.com/WebReflection/get-own-property-symbols/issues/4
  // if (!(symObj instanceof Symbol)) { return false; }

  // if (typeof Symbol.prototype.toString !== 'function') { return false; }
  // if (String(sym) !== Symbol.prototype.toString.call(sym)) { return false; }

  var symVal = 42;
  obj[sym] = symVal;
  for (sym in obj) {
    return false;
  } // eslint-disable-line no-restricted-syntax, no-unreachable-loop
  if (typeof Object.keys === 'function' && Object.keys(obj).length !== 0) {
    return false;
  }
  if (typeof Object.getOwnPropertyNames === 'function' && Object.getOwnPropertyNames(obj).length !== 0) {
    return false;
  }
  var syms = Object.getOwnPropertySymbols(obj);
  if (syms.length !== 1 || syms[0] !== sym) {
    return false;
  }
  if (!Object.prototype.propertyIsEnumerable.call(obj, sym)) {
    return false;
  }
  if (typeof Object.getOwnPropertyDescriptor === 'function') {
    var descriptor = Object.getOwnPropertyDescriptor(obj, sym);
    if (descriptor.value !== symVal || descriptor.enumerable !== true) {
      return false;
    }
  }
  return true;
};

var origSymbol = typeof Symbol !== 'undefined' && Symbol;
var hasSymbolSham = shams$1;
var hasSymbols$4 = function hasNativeSymbols() {
  if (typeof origSymbol !== 'function') {
    return false;
  }
  if (typeof Symbol !== 'function') {
    return false;
  }
  if (_typeof$1(origSymbol('foo')) !== 'symbol') {
    return false;
  }
  if (_typeof$1(Symbol('bar')) !== 'symbol') {
    return false;
  }
  return hasSymbolSham();
};

var test = {
  foo: {}
};
var $Object = Object;
var hasProto$1 = function hasProto() {
  return {
    __proto__: test
  }.foo === test.foo && !({
    __proto__: null
  } instanceof $Object);
};

var bind$2 = functionBind;
var src = bind$2.call(Function.call, Object.prototype.hasOwnProperty);

var undefined$1;
var $SyntaxError = SyntaxError;
var $Function = Function;
var $TypeError$6 = TypeError;

// eslint-disable-next-line consistent-return
var getEvalledConstructor = function getEvalledConstructor(expressionSyntax) {
  try {
    return $Function('"use strict"; return (' + expressionSyntax + ').constructor;')();
  } catch (e) {}
};
var $gOPD = Object.getOwnPropertyDescriptor;
if ($gOPD) {
  try {
    $gOPD({}, '');
  } catch (e) {
    $gOPD = null; // this is IE 8, which has a broken gOPD
  }
}

var throwTypeError = function throwTypeError() {
  throw new $TypeError$6();
};
var ThrowTypeError = $gOPD ? function () {
  try {
    // eslint-disable-next-line no-unused-expressions, no-caller, no-restricted-properties
    arguments.callee; // IE 8 does not throw here
    return throwTypeError;
  } catch (calleeThrows) {
    try {
      // IE 8 throws on Object.getOwnPropertyDescriptor(arguments, '')
      return $gOPD(arguments, 'callee').get;
    } catch (gOPDthrows) {
      return throwTypeError;
    }
  }
}() : throwTypeError;
var hasSymbols$3 = hasSymbols$4();
var hasProto = hasProto$1();
var getProto$1 = Object.getPrototypeOf || (hasProto ? function (x) {
  return x.__proto__;
} // eslint-disable-line no-proto
: null);
var needsEval = {};
var TypedArray = typeof Uint8Array === 'undefined' || !getProto$1 ? undefined$1 : getProto$1(Uint8Array);
var INTRINSICS = {
  '%AggregateError%': typeof AggregateError === 'undefined' ? undefined$1 : AggregateError,
  '%Array%': Array,
  '%ArrayBuffer%': typeof ArrayBuffer === 'undefined' ? undefined$1 : ArrayBuffer,
  '%ArrayIteratorPrototype%': hasSymbols$3 && getProto$1 ? getProto$1([][Symbol.iterator]()) : undefined$1,
  '%AsyncFromSyncIteratorPrototype%': undefined$1,
  '%AsyncFunction%': needsEval,
  '%AsyncGenerator%': needsEval,
  '%AsyncGeneratorFunction%': needsEval,
  '%AsyncIteratorPrototype%': needsEval,
  '%Atomics%': typeof Atomics === 'undefined' ? undefined$1 : Atomics,
  '%BigInt%': typeof BigInt === 'undefined' ? undefined$1 : BigInt,
  '%BigInt64Array%': typeof BigInt64Array === 'undefined' ? undefined$1 : BigInt64Array,
  '%BigUint64Array%': typeof BigUint64Array === 'undefined' ? undefined$1 : BigUint64Array,
  '%Boolean%': Boolean,
  '%DataView%': typeof DataView === 'undefined' ? undefined$1 : DataView,
  '%Date%': Date,
  '%decodeURI%': decodeURI,
  '%decodeURIComponent%': decodeURIComponent,
  '%encodeURI%': encodeURI,
  '%encodeURIComponent%': encodeURIComponent,
  '%Error%': Error,
  '%eval%': eval,
  // eslint-disable-line no-eval
  '%EvalError%': EvalError,
  '%Float32Array%': typeof Float32Array === 'undefined' ? undefined$1 : Float32Array,
  '%Float64Array%': typeof Float64Array === 'undefined' ? undefined$1 : Float64Array,
  '%FinalizationRegistry%': typeof FinalizationRegistry === 'undefined' ? undefined$1 : FinalizationRegistry,
  '%Function%': $Function,
  '%GeneratorFunction%': needsEval,
  '%Int8Array%': typeof Int8Array === 'undefined' ? undefined$1 : Int8Array,
  '%Int16Array%': typeof Int16Array === 'undefined' ? undefined$1 : Int16Array,
  '%Int32Array%': typeof Int32Array === 'undefined' ? undefined$1 : Int32Array,
  '%isFinite%': isFinite,
  '%isNaN%': isNaN,
  '%IteratorPrototype%': hasSymbols$3 && getProto$1 ? getProto$1(getProto$1([][Symbol.iterator]())) : undefined$1,
  '%JSON%': (typeof JSON === "undefined" ? "undefined" : _typeof$1(JSON)) === 'object' ? JSON : undefined$1,
  '%Map%': typeof Map === 'undefined' ? undefined$1 : Map,
  '%MapIteratorPrototype%': typeof Map === 'undefined' || !hasSymbols$3 || !getProto$1 ? undefined$1 : getProto$1(new Map()[Symbol.iterator]()),
  '%Math%': Math,
  '%Number%': Number,
  '%Object%': Object,
  '%parseFloat%': parseFloat,
  '%parseInt%': parseInt,
  '%Promise%': typeof Promise === 'undefined' ? undefined$1 : Promise,
  '%Proxy%': typeof Proxy === 'undefined' ? undefined$1 : Proxy,
  '%RangeError%': RangeError,
  '%ReferenceError%': ReferenceError,
  '%Reflect%': typeof Reflect === 'undefined' ? undefined$1 : Reflect,
  '%RegExp%': RegExp,
  '%Set%': typeof Set === 'undefined' ? undefined$1 : Set,
  '%SetIteratorPrototype%': typeof Set === 'undefined' || !hasSymbols$3 || !getProto$1 ? undefined$1 : getProto$1(new Set()[Symbol.iterator]()),
  '%SharedArrayBuffer%': typeof SharedArrayBuffer === 'undefined' ? undefined$1 : SharedArrayBuffer,
  '%String%': String,
  '%StringIteratorPrototype%': hasSymbols$3 && getProto$1 ? getProto$1(''[Symbol.iterator]()) : undefined$1,
  '%Symbol%': hasSymbols$3 ? Symbol : undefined$1,
  '%SyntaxError%': $SyntaxError,
  '%ThrowTypeError%': ThrowTypeError,
  '%TypedArray%': TypedArray,
  '%TypeError%': $TypeError$6,
  '%Uint8Array%': typeof Uint8Array === 'undefined' ? undefined$1 : Uint8Array,
  '%Uint8ClampedArray%': typeof Uint8ClampedArray === 'undefined' ? undefined$1 : Uint8ClampedArray,
  '%Uint16Array%': typeof Uint16Array === 'undefined' ? undefined$1 : Uint16Array,
  '%Uint32Array%': typeof Uint32Array === 'undefined' ? undefined$1 : Uint32Array,
  '%URIError%': URIError,
  '%WeakMap%': typeof WeakMap === 'undefined' ? undefined$1 : WeakMap,
  '%WeakRef%': typeof WeakRef === 'undefined' ? undefined$1 : WeakRef,
  '%WeakSet%': typeof WeakSet === 'undefined' ? undefined$1 : WeakSet
};
if (getProto$1) {
  try {
    null.error; // eslint-disable-line no-unused-expressions
  } catch (e) {
    // https://github.com/tc39/proposal-shadowrealm/pull/384#issuecomment-1364264229
    var errorProto = getProto$1(getProto$1(e));
    INTRINSICS['%Error.prototype%'] = errorProto;
  }
}
var doEval = function doEval(name) {
  var value;
  if (name === '%AsyncFunction%') {
    value = getEvalledConstructor('async function () {}');
  } else if (name === '%GeneratorFunction%') {
    value = getEvalledConstructor('function* () {}');
  } else if (name === '%AsyncGeneratorFunction%') {
    value = getEvalledConstructor('async function* () {}');
  } else if (name === '%AsyncGenerator%') {
    var fn = doEval('%AsyncGeneratorFunction%');
    if (fn) {
      value = fn.prototype;
    }
  } else if (name === '%AsyncIteratorPrototype%') {
    var gen = doEval('%AsyncGenerator%');
    if (gen && getProto$1) {
      value = getProto$1(gen.prototype);
    }
  }
  INTRINSICS[name] = value;
  return value;
};
var LEGACY_ALIASES = {
  '%ArrayBufferPrototype%': ['ArrayBuffer', 'prototype'],
  '%ArrayPrototype%': ['Array', 'prototype'],
  '%ArrayProto_entries%': ['Array', 'prototype', 'entries'],
  '%ArrayProto_forEach%': ['Array', 'prototype', 'forEach'],
  '%ArrayProto_keys%': ['Array', 'prototype', 'keys'],
  '%ArrayProto_values%': ['Array', 'prototype', 'values'],
  '%AsyncFunctionPrototype%': ['AsyncFunction', 'prototype'],
  '%AsyncGenerator%': ['AsyncGeneratorFunction', 'prototype'],
  '%AsyncGeneratorPrototype%': ['AsyncGeneratorFunction', 'prototype', 'prototype'],
  '%BooleanPrototype%': ['Boolean', 'prototype'],
  '%DataViewPrototype%': ['DataView', 'prototype'],
  '%DatePrototype%': ['Date', 'prototype'],
  '%ErrorPrototype%': ['Error', 'prototype'],
  '%EvalErrorPrototype%': ['EvalError', 'prototype'],
  '%Float32ArrayPrototype%': ['Float32Array', 'prototype'],
  '%Float64ArrayPrototype%': ['Float64Array', 'prototype'],
  '%FunctionPrototype%': ['Function', 'prototype'],
  '%Generator%': ['GeneratorFunction', 'prototype'],
  '%GeneratorPrototype%': ['GeneratorFunction', 'prototype', 'prototype'],
  '%Int8ArrayPrototype%': ['Int8Array', 'prototype'],
  '%Int16ArrayPrototype%': ['Int16Array', 'prototype'],
  '%Int32ArrayPrototype%': ['Int32Array', 'prototype'],
  '%JSONParse%': ['JSON', 'parse'],
  '%JSONStringify%': ['JSON', 'stringify'],
  '%MapPrototype%': ['Map', 'prototype'],
  '%NumberPrototype%': ['Number', 'prototype'],
  '%ObjectPrototype%': ['Object', 'prototype'],
  '%ObjProto_toString%': ['Object', 'prototype', 'toString'],
  '%ObjProto_valueOf%': ['Object', 'prototype', 'valueOf'],
  '%PromisePrototype%': ['Promise', 'prototype'],
  '%PromiseProto_then%': ['Promise', 'prototype', 'then'],
  '%Promise_all%': ['Promise', 'all'],
  '%Promise_reject%': ['Promise', 'reject'],
  '%Promise_resolve%': ['Promise', 'resolve'],
  '%RangeErrorPrototype%': ['RangeError', 'prototype'],
  '%ReferenceErrorPrototype%': ['ReferenceError', 'prototype'],
  '%RegExpPrototype%': ['RegExp', 'prototype'],
  '%SetPrototype%': ['Set', 'prototype'],
  '%SharedArrayBufferPrototype%': ['SharedArrayBuffer', 'prototype'],
  '%StringPrototype%': ['String', 'prototype'],
  '%SymbolPrototype%': ['Symbol', 'prototype'],
  '%SyntaxErrorPrototype%': ['SyntaxError', 'prototype'],
  '%TypedArrayPrototype%': ['TypedArray', 'prototype'],
  '%TypeErrorPrototype%': ['TypeError', 'prototype'],
  '%Uint8ArrayPrototype%': ['Uint8Array', 'prototype'],
  '%Uint8ClampedArrayPrototype%': ['Uint8ClampedArray', 'prototype'],
  '%Uint16ArrayPrototype%': ['Uint16Array', 'prototype'],
  '%Uint32ArrayPrototype%': ['Uint32Array', 'prototype'],
  '%URIErrorPrototype%': ['URIError', 'prototype'],
  '%WeakMapPrototype%': ['WeakMap', 'prototype'],
  '%WeakSetPrototype%': ['WeakSet', 'prototype']
};
var bind$1 = functionBind;
var hasOwn = src;
var $concat = bind$1.call(Function.call, Array.prototype.concat);
var $spliceApply = bind$1.call(Function.apply, Array.prototype.splice);
var $replace = bind$1.call(Function.call, String.prototype.replace);
var $strSlice = bind$1.call(Function.call, String.prototype.slice);
var $exec = bind$1.call(Function.call, RegExp.prototype.exec);

/* adapted from https://github.com/lodash/lodash/blob/4.17.15/dist/lodash.js#L6735-L6744 */
var rePropName = /[^%.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|%$))/g;
var reEscapeChar = /\\(\\)?/g; /** Used to match backslashes in property paths. */
var stringToPath = function stringToPath(string) {
  var first = $strSlice(string, 0, 1);
  var last = $strSlice(string, -1);
  if (first === '%' && last !== '%') {
    throw new $SyntaxError('invalid intrinsic syntax, expected closing `%`');
  } else if (last === '%' && first !== '%') {
    throw new $SyntaxError('invalid intrinsic syntax, expected opening `%`');
  }
  var result = [];
  $replace(string, rePropName, function (match, number, quote, subString) {
    result[result.length] = quote ? $replace(subString, reEscapeChar, '$1') : number || match;
  });
  return result;
};
/* end adaptation */

var getBaseIntrinsic = function getBaseIntrinsic(name, allowMissing) {
  var intrinsicName = name;
  var alias;
  if (hasOwn(LEGACY_ALIASES, intrinsicName)) {
    alias = LEGACY_ALIASES[intrinsicName];
    intrinsicName = '%' + alias[0] + '%';
  }
  if (hasOwn(INTRINSICS, intrinsicName)) {
    var value = INTRINSICS[intrinsicName];
    if (value === needsEval) {
      value = doEval(intrinsicName);
    }
    if (typeof value === 'undefined' && !allowMissing) {
      throw new $TypeError$6('intrinsic ' + name + ' exists, but is not available. Please file an issue!');
    }
    return {
      alias: alias,
      name: intrinsicName,
      value: value
    };
  }
  throw new $SyntaxError('intrinsic ' + name + ' does not exist!');
};
var getIntrinsic = function GetIntrinsic(name, allowMissing) {
  if (typeof name !== 'string' || name.length === 0) {
    throw new $TypeError$6('intrinsic name must be a non-empty string');
  }
  if (arguments.length > 1 && typeof allowMissing !== 'boolean') {
    throw new $TypeError$6('"allowMissing" argument must be a boolean');
  }
  if ($exec(/^%?[^%]*%?$/, name) === null) {
    throw new $SyntaxError('`%` may not be present anywhere but at the beginning and end of the intrinsic name');
  }
  var parts = stringToPath(name);
  var intrinsicBaseName = parts.length > 0 ? parts[0] : '';
  var intrinsic = getBaseIntrinsic('%' + intrinsicBaseName + '%', allowMissing);
  var intrinsicRealName = intrinsic.name;
  var value = intrinsic.value;
  var skipFurtherCaching = false;
  var alias = intrinsic.alias;
  if (alias) {
    intrinsicBaseName = alias[0];
    $spliceApply(parts, $concat([0, 1], alias));
  }
  for (var i = 1, isOwn = true; i < parts.length; i += 1) {
    var part = parts[i];
    var first = $strSlice(part, 0, 1);
    var last = $strSlice(part, -1);
    if ((first === '"' || first === "'" || first === '`' || last === '"' || last === "'" || last === '`') && first !== last) {
      throw new $SyntaxError('property names with quotes must have matching quotes');
    }
    if (part === 'constructor' || !isOwn) {
      skipFurtherCaching = true;
    }
    intrinsicBaseName += '.' + part;
    intrinsicRealName = '%' + intrinsicBaseName + '%';
    if (hasOwn(INTRINSICS, intrinsicRealName)) {
      value = INTRINSICS[intrinsicRealName];
    } else if (value != null) {
      if (!(part in value)) {
        if (!allowMissing) {
          throw new $TypeError$6('base intrinsic for ' + name + ' exists, but the property is not available.');
        }
        return void undefined$1;
      }
      if ($gOPD && i + 1 >= parts.length) {
        var desc = $gOPD(value, part);
        isOwn = !!desc;

        // By convention, when a data property is converted to an accessor
        // property to emulate a data property that does not suffer from
        // the override mistake, that accessor's getter is marked with
        // an `originalValue` property. Here, when we detect this, we
        // uphold the illusion by pretending to see that original data
        // property, i.e., returning the value rather than the getter
        // itself.
        if (isOwn && 'get' in desc && !('originalValue' in desc.get)) {
          value = desc.get;
        } else {
          value = value[part];
        }
      } else {
        isOwn = hasOwn(value, part);
        value = value[part];
      }
      if (isOwn && !skipFurtherCaching) {
        INTRINSICS[intrinsicRealName] = value;
      }
    }
  }
  return value;
};

var GetIntrinsic$c = getIntrinsic;
var $defineProperty = GetIntrinsic$c('%Object.defineProperty%', true);
var hasPropertyDescriptors$2 = function hasPropertyDescriptors() {
  if ($defineProperty) {
    try {
      $defineProperty({}, 'a', {
        value: 1
      });
      return true;
    } catch (e) {
      // IE 8 has a broken defineProperty
      return false;
    }
  }
  return false;
};
hasPropertyDescriptors$2.hasArrayLengthDefineBug = function hasArrayLengthDefineBug() {
  // node v0.6 has a bug where array lengths can be Set but not Defined
  if (!hasPropertyDescriptors$2()) {
    return null;
  }
  try {
    return $defineProperty([], 'length', {
      value: 1
    }).length !== 1;
  } catch (e) {
    // In Firefox 4-22, defining length on an array throws an exception.
    return true;
  }
};
var hasPropertyDescriptors_1 = hasPropertyDescriptors$2;

var keys = objectKeys;
var hasSymbols$2 = typeof Symbol === 'function' && _typeof$1(Symbol('foo')) === 'symbol';
var toStr$2 = Object.prototype.toString;
var concat = Array.prototype.concat;
var origDefineProperty = Object.defineProperty;
var isFunction = function isFunction(fn) {
  return typeof fn === 'function' && toStr$2.call(fn) === '[object Function]';
};
var hasPropertyDescriptors$1 = hasPropertyDescriptors_1();
var supportsDescriptors = origDefineProperty && hasPropertyDescriptors$1;
var defineProperty = function defineProperty(object, name, value, predicate) {
  if (name in object) {
    if (predicate === true) {
      if (object[name] === value) {
        return;
      }
    } else if (!isFunction(predicate) || !predicate()) {
      return;
    }
  }
  if (supportsDescriptors) {
    origDefineProperty(object, name, {
      configurable: true,
      enumerable: false,
      value: value,
      writable: true
    });
  } else {
    object[name] = value; // eslint-disable-line no-param-reassign
  }
};

var defineProperties$1 = function defineProperties(object, map) {
  var predicates = arguments.length > 2 ? arguments[2] : {};
  var props = keys(map);
  if (hasSymbols$2) {
    props = concat.call(props, Object.getOwnPropertySymbols(map));
  }
  for (var i = 0; i < props.length; i += 1) {
    defineProperty(object, props[i], map[props[i]], predicates[props[i]]);
  }
};
defineProperties$1.supportsDescriptors = !!supportsDescriptors;
var defineProperties_1 = defineProperties$1;

var functionsHaveNames = function functionsHaveNames() {
  return typeof function f() {}.name === 'string';
};
var gOPD = Object.getOwnPropertyDescriptor;
if (gOPD) {
  try {
    gOPD([], 'length');
  } catch (e) {
    // IE 8 has a broken gOPD
    gOPD = null;
  }
}
functionsHaveNames.functionsHaveConfigurableNames = function functionsHaveConfigurableNames() {
  if (!functionsHaveNames() || !gOPD) {
    return false;
  }
  var desc = gOPD(function () {}, 'name');
  return !!desc && !!desc.configurable;
};
var $bind = Function.prototype.bind;
functionsHaveNames.boundFunctionsHaveNames = function boundFunctionsHaveNames() {
  return functionsHaveNames() && typeof $bind === 'function' && function f() {}.bind().name !== '';
};
var functionsHaveNames_1 = functionsHaveNames;

var callBind$1 = {exports: {}};

(function (module) {

  var bind = functionBind;
  var GetIntrinsic = getIntrinsic;
  var $apply = GetIntrinsic('%Function.prototype.apply%');
  var $call = GetIntrinsic('%Function.prototype.call%');
  var $reflectApply = GetIntrinsic('%Reflect.apply%', true) || bind.call($call, $apply);
  var $gOPD = GetIntrinsic('%Object.getOwnPropertyDescriptor%', true);
  var $defineProperty = GetIntrinsic('%Object.defineProperty%', true);
  var $max = GetIntrinsic('%Math.max%');
  if ($defineProperty) {
    try {
      $defineProperty({}, 'a', {
        value: 1
      });
    } catch (e) {
      // IE 8 has a broken defineProperty
      $defineProperty = null;
    }
  }
  module.exports = function callBind(originalFunction) {
    var func = $reflectApply(bind, $call, arguments);
    if ($gOPD && $defineProperty) {
      var desc = $gOPD(func, 'length');
      if (desc.configurable) {
        // original length, plus the receiver, minus any additional arguments (after the receiver)
        $defineProperty(func, 'length', {
          value: 1 + $max(0, originalFunction.length - (arguments.length - 1))
        });
      }
    }
    return func;
  };
  var applyBind = function applyBind() {
    return $reflectApply(bind, $apply, arguments);
  };
  if ($defineProperty) {
    $defineProperty(module.exports, 'apply', {
      value: applyBind
    });
  } else {
    module.exports.apply = applyBind;
  }
})(callBind$1);
var callBindExports = callBind$1.exports;

var GetIntrinsic$b = getIntrinsic;
var callBind = callBindExports;
var $indexOf = callBind(GetIntrinsic$b('String.prototype.indexOf'));
var callBound$3 = function callBoundIntrinsic(name, allowMissing) {
  var intrinsic = GetIntrinsic$b(name, !!allowMissing);
  if (typeof intrinsic === 'function' && $indexOf(name, '.prototype.') > -1) {
    return callBind(intrinsic);
  }
  return intrinsic;
};

var isLeadingSurrogate$1;
var hasRequiredIsLeadingSurrogate;
function requireIsLeadingSurrogate() {
  if (hasRequiredIsLeadingSurrogate) return isLeadingSurrogate$1;
  hasRequiredIsLeadingSurrogate = 1;
  isLeadingSurrogate$1 = function isLeadingSurrogate(charCode) {
    return typeof charCode === 'number' && charCode >= 0xD800 && charCode <= 0xDBFF;
  };
  return isLeadingSurrogate$1;
}

var isTrailingSurrogate$1;
var hasRequiredIsTrailingSurrogate;
function requireIsTrailingSurrogate() {
  if (hasRequiredIsTrailingSurrogate) return isTrailingSurrogate$1;
  hasRequiredIsTrailingSurrogate = 1;
  isTrailingSurrogate$1 = function isTrailingSurrogate(charCode) {
    return typeof charCode === 'number' && charCode >= 0xDC00 && charCode <= 0xDFFF;
  };
  return isTrailingSurrogate$1;
}

// https://262.ecma-international.org/5.1/#sec-8

var Type$9 = function Type(x) {
  if (x === null) {
    return 'Null';
  }
  if (typeof x === 'undefined') {
    return 'Undefined';
  }
  if (typeof x === 'function' || _typeof$1(x) === 'object') {
    return 'Object';
  }
  if (typeof x === 'number') {
    return 'Number';
  }
  if (typeof x === 'boolean') {
    return 'Boolean';
  }
  if (typeof x === 'string') {
    return 'String';
  }
};

var ES5Type = Type$9;

// https://262.ecma-international.org/11.0/#sec-ecmascript-data-types-and-values

var Type$8 = function Type(x) {
  if (_typeof$1(x) === 'symbol') {
    return 'Symbol';
  }
  if (typeof x === 'bigint') {
    return 'BigInt';
  }
  return ES5Type(x);
};

var UTF16SurrogatePairToCodePoint$1;
var hasRequiredUTF16SurrogatePairToCodePoint;
function requireUTF16SurrogatePairToCodePoint() {
  if (hasRequiredUTF16SurrogatePairToCodePoint) return UTF16SurrogatePairToCodePoint$1;
  hasRequiredUTF16SurrogatePairToCodePoint = 1;
  var GetIntrinsic = getIntrinsic;
  var $TypeError = GetIntrinsic('%TypeError%');
  var $fromCharCode = GetIntrinsic('%String.fromCharCode%');
  var isLeadingSurrogate = requireIsLeadingSurrogate();
  var isTrailingSurrogate = requireIsTrailingSurrogate();

  // https://tc39.es/ecma262/2020/#sec-utf16decodesurrogatepair

  UTF16SurrogatePairToCodePoint$1 = function UTF16SurrogatePairToCodePoint(lead, trail) {
    if (!isLeadingSurrogate(lead) || !isTrailingSurrogate(trail)) {
      throw new $TypeError('Assertion failed: `lead` must be a leading surrogate char code, and `trail` must be a trailing surrogate char code');
    }
    // var cp = (lead - 0xD800) * 0x400 + (trail - 0xDC00) + 0x10000;
    return $fromCharCode(lead) + $fromCharCode(trail);
  };
  return UTF16SurrogatePairToCodePoint$1;
}

var GetIntrinsic$a = getIntrinsic;
var $TypeError$5 = GetIntrinsic$a('%TypeError%');
var callBound$2 = callBound$3;
var isLeadingSurrogate = requireIsLeadingSurrogate();
var isTrailingSurrogate = requireIsTrailingSurrogate();
var Type$7 = Type$8;
var UTF16SurrogatePairToCodePoint = requireUTF16SurrogatePairToCodePoint();
var $charAt = callBound$2('String.prototype.charAt');
var $charCodeAt = callBound$2('String.prototype.charCodeAt');

// https://262.ecma-international.org/12.0/#sec-codepointat

var CodePointAt$1 = function CodePointAt(string, position) {
  if (Type$7(string) !== 'String') {
    throw new $TypeError$5('Assertion failed: `string` must be a String');
  }
  var size = string.length;
  if (position < 0 || position >= size) {
    throw new $TypeError$5('Assertion failed: `position` must be >= 0, and < the length of `string`');
  }
  var first = $charCodeAt(string, position);
  var cp = $charAt(string, position);
  var firstIsLeading = isLeadingSurrogate(first);
  var firstIsTrailing = isTrailingSurrogate(first);
  if (!firstIsLeading && !firstIsTrailing) {
    return {
      '[[CodePoint]]': cp,
      '[[CodeUnitCount]]': 1,
      '[[IsUnpairedSurrogate]]': false
    };
  }
  if (firstIsTrailing || position + 1 === size) {
    return {
      '[[CodePoint]]': cp,
      '[[CodeUnitCount]]': 1,
      '[[IsUnpairedSurrogate]]': true
    };
  }
  var second = $charCodeAt(string, position + 1);
  if (!isTrailingSurrogate(second)) {
    return {
      '[[CodePoint]]': cp,
      '[[CodeUnitCount]]': 1,
      '[[IsUnpairedSurrogate]]': true
    };
  }
  return {
    '[[CodePoint]]': UTF16SurrogatePairToCodePoint(first, second),
    '[[CodeUnitCount]]': 2,
    '[[IsUnpairedSurrogate]]': false
  };
};

var GetIntrinsic$9 = getIntrinsic;
var $abs = GetIntrinsic$9('%Math.abs%');

// http://262.ecma-international.org/5.1/#sec-5.2

var abs$1 = function abs(x) {
  return $abs(x);
};

var Type$6 = Type$8;

// var modulo = require('./modulo');
var $floor = Math.floor;

// http://262.ecma-international.org/11.0/#eqn-floor

var floor$1 = function floor(x) {
  // return x - modulo(x, 1);
  if (Type$6(x) === 'BigInt') {
    return x;
  }
  return $floor(x);
};

var _isNaN = Number.isNaN || function isNaN(a) {
  return a !== a;
};

var $isNaN$1 = _isNaN;
var _isFinite = function _isFinite(x) {
  return (typeof x === 'number' || typeof x === 'bigint') && !$isNaN$1(x) && x !== Infinity && x !== -Infinity;
};

var abs = abs$1;
var floor = floor$1;
var Type$5 = Type$8;
var $isNaN = _isNaN;
var $isFinite = _isFinite;

// https://tc39.es/ecma262/#sec-isintegralnumber

var IsIntegralNumber$1 = function IsIntegralNumber(argument) {
  if (Type$5(argument) !== 'Number' || $isNaN(argument) || !$isFinite(argument)) {
    return false;
  }
  var absValue = abs(argument);
  return floor(absValue) === absValue;
};

var GetIntrinsic$8 = getIntrinsic;
var $Math = GetIntrinsic$8('%Math%');
var $Number = GetIntrinsic$8('%Number%');
var maxSafeInteger = $Number.MAX_SAFE_INTEGER || $Math.pow(2, 53) - 1;

var GetIntrinsic$7 = getIntrinsic;
var CodePointAt = CodePointAt$1;
var IsIntegralNumber = IsIntegralNumber$1;
var Type$4 = Type$8;
var MAX_SAFE_INTEGER = maxSafeInteger;
var $TypeError$4 = GetIntrinsic$7('%TypeError%');

// https://262.ecma-international.org/12.0/#sec-advancestringindex

var AdvanceStringIndex$1 = function AdvanceStringIndex(S, index, unicode) {
  if (Type$4(S) !== 'String') {
    throw new $TypeError$4('Assertion failed: `S` must be a String');
  }
  if (!IsIntegralNumber(index) || index < 0 || index > MAX_SAFE_INTEGER) {
    throw new $TypeError$4('Assertion failed: `length` must be an integer >= 0 and <= 2**53');
  }
  if (Type$4(unicode) !== 'Boolean') {
    throw new $TypeError$4('Assertion failed: `unicode` must be a Boolean');
  }
  if (!unicode) {
    return index + 1;
  }
  var length = S.length;
  if (index + 1 >= length) {
    return index + 1;
  }
  var cp = CodePointAt(S, index);
  return index + cp['[[CodeUnitCount]]'];
};

var IsPropertyKey$3;
var hasRequiredIsPropertyKey;
function requireIsPropertyKey() {
  if (hasRequiredIsPropertyKey) return IsPropertyKey$3;
  hasRequiredIsPropertyKey = 1;

  // https://262.ecma-international.org/6.0/#sec-ispropertykey

  IsPropertyKey$3 = function IsPropertyKey(argument) {
    return typeof argument === 'string' || _typeof$1(argument) === 'symbol';
  };
  return IsPropertyKey$3;
}

var gopd;
var hasRequiredGopd;
function requireGopd() {
  if (hasRequiredGopd) return gopd;
  hasRequiredGopd = 1;
  var GetIntrinsic = getIntrinsic;
  var $gOPD = GetIntrinsic('%Object.getOwnPropertyDescriptor%', true);
  if ($gOPD) {
    try {
      $gOPD([], 'length');
    } catch (e) {
      // IE 8 has a broken gOPD
      $gOPD = null;
    }
  }
  gopd = $gOPD;
  return gopd;
}

var isPropertyDescriptor;
var hasRequiredIsPropertyDescriptor;
function requireIsPropertyDescriptor() {
  if (hasRequiredIsPropertyDescriptor) return isPropertyDescriptor;
  hasRequiredIsPropertyDescriptor = 1;
  var GetIntrinsic = getIntrinsic;
  var has = src;
  var $TypeError = GetIntrinsic('%TypeError%');
  isPropertyDescriptor = function IsPropertyDescriptor(ES, Desc) {
    if (ES.Type(Desc) !== 'Object') {
      return false;
    }
    var allowed = {
      '[[Configurable]]': true,
      '[[Enumerable]]': true,
      '[[Get]]': true,
      '[[Set]]': true,
      '[[Value]]': true,
      '[[Writable]]': true
    };
    for (var key in Desc) {
      // eslint-disable-line no-restricted-syntax
      if (has(Desc, key) && !allowed[key]) {
        return false;
      }
    }
    if (ES.IsDataDescriptor(Desc) && ES.IsAccessorDescriptor(Desc)) {
      throw new $TypeError('Property Descriptors may not be both accessor and data descriptors');
    }
    return true;
  };
  return isPropertyDescriptor;
}

var isMatchRecord;
var hasRequiredIsMatchRecord;
function requireIsMatchRecord() {
  if (hasRequiredIsMatchRecord) return isMatchRecord;
  hasRequiredIsMatchRecord = 1;
  var has = src;

  // https://262.ecma-international.org/13.0/#sec-match-records

  isMatchRecord = function isMatchRecord(record) {
    return has(record, '[[StartIndex]]') && has(record, '[[EndIndex]]') && record['[[StartIndex]]'] >= 0 && record['[[EndIndex]]'] >= record['[[StartIndex]]'] && String(parseInt(record['[[StartIndex]]'], 10)) === String(record['[[StartIndex]]']) && String(parseInt(record['[[EndIndex]]'], 10)) === String(record['[[EndIndex]]']);
  };
  return isMatchRecord;
}

var assertRecord;
var hasRequiredAssertRecord;
function requireAssertRecord() {
  if (hasRequiredAssertRecord) return assertRecord;
  hasRequiredAssertRecord = 1;
  var GetIntrinsic = getIntrinsic;
  var $TypeError = GetIntrinsic('%TypeError%');
  var $SyntaxError = GetIntrinsic('%SyntaxError%');
  var has = src;
  var isMatchRecord = requireIsMatchRecord();
  var predicates = {
    // https://262.ecma-international.org/6.0/#sec-property-descriptor-specification-type
    'Property Descriptor': function isPropertyDescriptor(Desc) {
      var allowed = {
        '[[Configurable]]': true,
        '[[Enumerable]]': true,
        '[[Get]]': true,
        '[[Set]]': true,
        '[[Value]]': true,
        '[[Writable]]': true
      };
      if (!Desc) {
        return false;
      }
      for (var key in Desc) {
        // eslint-disable-line
        if (has(Desc, key) && !allowed[key]) {
          return false;
        }
      }
      var isData = has(Desc, '[[Value]]');
      var IsAccessor = has(Desc, '[[Get]]') || has(Desc, '[[Set]]');
      if (isData && IsAccessor) {
        throw new $TypeError('Property Descriptors may not be both accessor and data descriptors');
      }
      return true;
    },
    // https://262.ecma-international.org/13.0/#sec-match-records
    'Match Record': isMatchRecord,
    'Iterator Record': function isIteratorRecord(value) {
      return has(value, '[[Iterator]]') && has(value, '[[NextMethod]]') && has(value, '[[Done]]');
    },
    'PromiseCapability Record': function isPromiseCapabilityRecord(value) {
      return !!value && has(value, '[[Resolve]]') && typeof value['[[Resolve]]'] === 'function' && has(value, '[[Reject]]') && typeof value['[[Reject]]'] === 'function' && has(value, '[[Promise]]') && value['[[Promise]]'] && typeof value['[[Promise]]'].then === 'function';
    },
    'AsyncGeneratorRequest Record': function isAsyncGeneratorRequestRecord(value) {
      return !!value && has(value, '[[Completion]]') // TODO: confirm is a completion record
      && has(value, '[[Capability]]') && predicates['PromiseCapability Record'](value['[[Capability]]']);
    }
  };
  assertRecord = function assertRecord(Type, recordType, argumentName, value) {
    var predicate = predicates[recordType];
    if (typeof predicate !== 'function') {
      throw new $SyntaxError('unknown record type: ' + recordType);
    }
    if (Type(value) !== 'Object' || !predicate(value)) {
      throw new $TypeError(argumentName + ' must be a ' + recordType);
    }
  };
  return assertRecord;
}

var IsAccessorDescriptor;
var hasRequiredIsAccessorDescriptor;
function requireIsAccessorDescriptor() {
  if (hasRequiredIsAccessorDescriptor) return IsAccessorDescriptor;
  hasRequiredIsAccessorDescriptor = 1;
  var has = src;
  var Type = Type$8;
  var assertRecord = requireAssertRecord();

  // https://262.ecma-international.org/5.1/#sec-8.10.1

  IsAccessorDescriptor = function IsAccessorDescriptor(Desc) {
    if (typeof Desc === 'undefined') {
      return false;
    }
    assertRecord(Type, 'Property Descriptor', 'Desc', Desc);
    if (!has(Desc, '[[Get]]') && !has(Desc, '[[Set]]')) {
      return false;
    }
    return true;
  };
  return IsAccessorDescriptor;
}

var IsDataDescriptor$1;
var hasRequiredIsDataDescriptor;
function requireIsDataDescriptor() {
  if (hasRequiredIsDataDescriptor) return IsDataDescriptor$1;
  hasRequiredIsDataDescriptor = 1;
  var has = src;
  var Type = Type$8;
  var assertRecord = requireAssertRecord();

  // https://262.ecma-international.org/5.1/#sec-8.10.2

  IsDataDescriptor$1 = function IsDataDescriptor(Desc) {
    if (typeof Desc === 'undefined') {
      return false;
    }
    assertRecord(Type, 'Property Descriptor', 'Desc', Desc);
    if (!has(Desc, '[[Value]]') && !has(Desc, '[[Writable]]')) {
      return false;
    }
    return true;
  };
  return IsDataDescriptor$1;
}

var isPrimitive;
var hasRequiredIsPrimitive;
function requireIsPrimitive() {
  if (hasRequiredIsPrimitive) return isPrimitive;
  hasRequiredIsPrimitive = 1;
  isPrimitive = function isPrimitive(value) {
    return value === null || typeof value !== 'function' && _typeof$1(value) !== 'object';
  };
  return isPrimitive;
}

var IsExtensible;
var hasRequiredIsExtensible;
function requireIsExtensible() {
  if (hasRequiredIsExtensible) return IsExtensible;
  hasRequiredIsExtensible = 1;
  var GetIntrinsic = getIntrinsic;
  var $preventExtensions = GetIntrinsic('%Object.preventExtensions%', true);
  var $isExtensible = GetIntrinsic('%Object.isExtensible%', true);
  var isPrimitive = requireIsPrimitive();

  // https://262.ecma-international.org/6.0/#sec-isextensible-o

  IsExtensible = $preventExtensions ? function IsExtensible(obj) {
    return !isPrimitive(obj) && $isExtensible(obj);
  } : function IsExtensible(obj) {
    return !isPrimitive(obj);
  };
  return IsExtensible;
}

var ToBoolean;
var hasRequiredToBoolean;
function requireToBoolean() {
  if (hasRequiredToBoolean) return ToBoolean;
  hasRequiredToBoolean = 1;

  // http://262.ecma-international.org/5.1/#sec-9.2

  ToBoolean = function ToBoolean(value) {
    return !!value;
  };
  return ToBoolean;
}

var isCallable;
var hasRequiredIsCallable$1;
function requireIsCallable$1() {
  if (hasRequiredIsCallable$1) return isCallable;
  hasRequiredIsCallable$1 = 1;
  var fnToStr = Function.prototype.toString;
  var reflectApply = (typeof Reflect === "undefined" ? "undefined" : _typeof$1(Reflect)) === 'object' && Reflect !== null && Reflect.apply;
  var badArrayLike;
  var isCallableMarker;
  if (typeof reflectApply === 'function' && typeof Object.defineProperty === 'function') {
    try {
      badArrayLike = Object.defineProperty({}, 'length', {
        get: function get() {
          throw isCallableMarker;
        }
      });
      isCallableMarker = {};
      // eslint-disable-next-line no-throw-literal
      reflectApply(function () {
        throw 42;
      }, null, badArrayLike);
    } catch (_) {
      if (_ !== isCallableMarker) {
        reflectApply = null;
      }
    }
  } else {
    reflectApply = null;
  }
  var constructorRegex = /^\s*class\b/;
  var isES6ClassFn = function isES6ClassFunction(value) {
    try {
      var fnStr = fnToStr.call(value);
      return constructorRegex.test(fnStr);
    } catch (e) {
      return false; // not a function
    }
  };

  var tryFunctionObject = function tryFunctionToStr(value) {
    try {
      if (isES6ClassFn(value)) {
        return false;
      }
      fnToStr.call(value);
      return true;
    } catch (e) {
      return false;
    }
  };
  var toStr = Object.prototype.toString;
  var objectClass = '[object Object]';
  var fnClass = '[object Function]';
  var genClass = '[object GeneratorFunction]';
  var ddaClass = '[object HTMLAllCollection]'; // IE 11
  var ddaClass2 = '[object HTML document.all class]';
  var ddaClass3 = '[object HTMLCollection]'; // IE 9-10
  var hasToStringTag = typeof Symbol === 'function' && !!Symbol.toStringTag; // better: use `has-tostringtag`

  var isIE68 = !(0 in [,]); // eslint-disable-line no-sparse-arrays, comma-spacing

  var isDDA = function isDocumentDotAll() {
    return false;
  };
  if ((typeof document === "undefined" ? "undefined" : _typeof$1(document)) === 'object') {
    // Firefox 3 canonicalizes DDA to undefined when it's not accessed directly
    var all = document.all;
    if (toStr.call(all) === toStr.call(document.all)) {
      isDDA = function isDocumentDotAll(value) {
        /* globals document: false */
        // in IE 6-8, typeof document.all is "object" and it's truthy
        if ((isIE68 || !value) && (typeof value === 'undefined' || _typeof$1(value) === 'object')) {
          try {
            var str = toStr.call(value);
            return (str === ddaClass || str === ddaClass2 || str === ddaClass3 // opera 12.16
            || str === objectClass // IE 6-8
            ) && value('') == null; // eslint-disable-line eqeqeq
          } catch (e) {/**/}
        }
        return false;
      };
    }
  }
  isCallable = reflectApply ? function isCallable(value) {
    if (isDDA(value)) {
      return true;
    }
    if (!value) {
      return false;
    }
    if (typeof value !== 'function' && _typeof$1(value) !== 'object') {
      return false;
    }
    try {
      reflectApply(value, null, badArrayLike);
    } catch (e) {
      if (e !== isCallableMarker) {
        return false;
      }
    }
    return !isES6ClassFn(value) && tryFunctionObject(value);
  } : function isCallable(value) {
    if (isDDA(value)) {
      return true;
    }
    if (!value) {
      return false;
    }
    if (typeof value !== 'function' && _typeof$1(value) !== 'object') {
      return false;
    }
    if (hasToStringTag) {
      return tryFunctionObject(value);
    }
    if (isES6ClassFn(value)) {
      return false;
    }
    var strClass = toStr.call(value);
    if (strClass !== fnClass && strClass !== genClass && !/^\[object HTML/.test(strClass)) {
      return false;
    }
    return tryFunctionObject(value);
  };
  return isCallable;
}

var IsCallable$1;
var hasRequiredIsCallable;
function requireIsCallable() {
  if (hasRequiredIsCallable) return IsCallable$1;
  hasRequiredIsCallable = 1;

  // http://262.ecma-international.org/5.1/#sec-9.11

  IsCallable$1 = requireIsCallable$1();
  return IsCallable$1;
}

var ToPropertyDescriptor;
var hasRequiredToPropertyDescriptor;
function requireToPropertyDescriptor() {
  if (hasRequiredToPropertyDescriptor) return ToPropertyDescriptor;
  hasRequiredToPropertyDescriptor = 1;
  var has = src;
  var GetIntrinsic = getIntrinsic;
  var $TypeError = GetIntrinsic('%TypeError%');
  var Type = Type$8;
  var ToBoolean = requireToBoolean();
  var IsCallable = requireIsCallable();

  // https://262.ecma-international.org/5.1/#sec-8.10.5

  ToPropertyDescriptor = function ToPropertyDescriptor(Obj) {
    if (Type(Obj) !== 'Object') {
      throw new $TypeError('ToPropertyDescriptor requires an object');
    }
    var desc = {};
    if (has(Obj, 'enumerable')) {
      desc['[[Enumerable]]'] = ToBoolean(Obj.enumerable);
    }
    if (has(Obj, 'configurable')) {
      desc['[[Configurable]]'] = ToBoolean(Obj.configurable);
    }
    if (has(Obj, 'value')) {
      desc['[[Value]]'] = Obj.value;
    }
    if (has(Obj, 'writable')) {
      desc['[[Writable]]'] = ToBoolean(Obj.writable);
    }
    if (has(Obj, 'get')) {
      var getter = Obj.get;
      if (typeof getter !== 'undefined' && !IsCallable(getter)) {
        throw new $TypeError('getter must be a function');
      }
      desc['[[Get]]'] = getter;
    }
    if (has(Obj, 'set')) {
      var setter = Obj.set;
      if (typeof setter !== 'undefined' && !IsCallable(setter)) {
        throw new $TypeError('setter must be a function');
      }
      desc['[[Set]]'] = setter;
    }
    if ((has(desc, '[[Get]]') || has(desc, '[[Set]]')) && (has(desc, '[[Value]]') || has(desc, '[[Writable]]'))) {
      throw new $TypeError('Invalid property descriptor. Cannot both specify accessors and a value or writable attribute');
    }
    return desc;
  };
  return ToPropertyDescriptor;
}

var SameValue$1;
var hasRequiredSameValue;
function requireSameValue() {
  if (hasRequiredSameValue) return SameValue$1;
  hasRequiredSameValue = 1;
  var $isNaN = _isNaN;

  // http://262.ecma-international.org/5.1/#sec-9.12

  SameValue$1 = function SameValue(x, y) {
    if (x === y) {
      // 0 === -0, but they are not identical.
      if (x === 0) {
        return 1 / x === 1 / y;
      }
      return true;
    }
    return $isNaN(x) && $isNaN(y);
  };
  return SameValue$1;
}

var GetIntrinsic$6 = getIntrinsic;
var $Array = GetIntrinsic$6('%Array%');

// eslint-disable-next-line global-require
var toStr$1 = !$Array.isArray && callBound$3('Object.prototype.toString');
var IsArray$2 = $Array.isArray || function IsArray(argument) {
  return toStr$1(argument) === '[object Array]';
};

var DefineOwnProperty$1;
var hasRequiredDefineOwnProperty;
function requireDefineOwnProperty() {
  if (hasRequiredDefineOwnProperty) return DefineOwnProperty$1;
  hasRequiredDefineOwnProperty = 1;
  var hasPropertyDescriptors = hasPropertyDescriptors_1;
  var GetIntrinsic = getIntrinsic;
  var $defineProperty = hasPropertyDescriptors() && GetIntrinsic('%Object.defineProperty%', true);
  var hasArrayLengthDefineBug = hasPropertyDescriptors.hasArrayLengthDefineBug();

  // eslint-disable-next-line global-require
  var isArray = hasArrayLengthDefineBug && IsArray$2;
  var callBound = callBound$3;
  var $isEnumerable = callBound('Object.prototype.propertyIsEnumerable');

  // eslint-disable-next-line max-params
  DefineOwnProperty$1 = function DefineOwnProperty(IsDataDescriptor, SameValue, FromPropertyDescriptor, O, P, desc) {
    if (!$defineProperty) {
      if (!IsDataDescriptor(desc)) {
        // ES3 does not support getters/setters
        return false;
      }
      if (!desc['[[Configurable]]'] || !desc['[[Writable]]']) {
        return false;
      }

      // fallback for ES3
      if (P in O && $isEnumerable(O, P) !== !!desc['[[Enumerable]]']) {
        // a non-enumerable existing property
        return false;
      }

      // property does not exist at all, or exists but is enumerable
      var V = desc['[[Value]]'];
      // eslint-disable-next-line no-param-reassign
      O[P] = V; // will use [[Define]]
      return SameValue(O[P], V);
    }
    if (hasArrayLengthDefineBug && P === 'length' && '[[Value]]' in desc && isArray(O) && O.length !== desc['[[Value]]']) {
      // eslint-disable-next-line no-param-reassign
      O.length = desc['[[Value]]'];
      return O.length === desc['[[Value]]'];
    }
    $defineProperty(O, P, FromPropertyDescriptor(desc));
    return true;
  };
  return DefineOwnProperty$1;
}

var isFullyPopulatedPropertyDescriptor;
var hasRequiredIsFullyPopulatedPropertyDescriptor;
function requireIsFullyPopulatedPropertyDescriptor() {
  if (hasRequiredIsFullyPopulatedPropertyDescriptor) return isFullyPopulatedPropertyDescriptor;
  hasRequiredIsFullyPopulatedPropertyDescriptor = 1;
  isFullyPopulatedPropertyDescriptor = function isFullyPopulatedPropertyDescriptor(ES, Desc) {
    return !!Desc && _typeof$1(Desc) === 'object' && '[[Enumerable]]' in Desc && '[[Configurable]]' in Desc && (ES.IsAccessorDescriptor(Desc) || ES.IsDataDescriptor(Desc));
  };
  return isFullyPopulatedPropertyDescriptor;
}

var fromPropertyDescriptor;
var hasRequiredFromPropertyDescriptor$1;
function requireFromPropertyDescriptor$1() {
  if (hasRequiredFromPropertyDescriptor$1) return fromPropertyDescriptor;
  hasRequiredFromPropertyDescriptor$1 = 1;
  fromPropertyDescriptor = function fromPropertyDescriptor(Desc) {
    if (typeof Desc === 'undefined') {
      return Desc;
    }
    var obj = {};
    if ('[[Value]]' in Desc) {
      obj.value = Desc['[[Value]]'];
    }
    if ('[[Writable]]' in Desc) {
      obj.writable = !!Desc['[[Writable]]'];
    }
    if ('[[Get]]' in Desc) {
      obj.get = Desc['[[Get]]'];
    }
    if ('[[Set]]' in Desc) {
      obj.set = Desc['[[Set]]'];
    }
    if ('[[Enumerable]]' in Desc) {
      obj.enumerable = !!Desc['[[Enumerable]]'];
    }
    if ('[[Configurable]]' in Desc) {
      obj.configurable = !!Desc['[[Configurable]]'];
    }
    return obj;
  };
  return fromPropertyDescriptor;
}

var FromPropertyDescriptor$1;
var hasRequiredFromPropertyDescriptor;
function requireFromPropertyDescriptor() {
  if (hasRequiredFromPropertyDescriptor) return FromPropertyDescriptor$1;
  hasRequiredFromPropertyDescriptor = 1;
  var assertRecord = requireAssertRecord();
  var fromPropertyDescriptor = requireFromPropertyDescriptor$1();
  var Type = Type$8;

  // https://262.ecma-international.org/6.0/#sec-frompropertydescriptor

  FromPropertyDescriptor$1 = function FromPropertyDescriptor(Desc) {
    if (typeof Desc !== 'undefined') {
      assertRecord(Type, 'Property Descriptor', 'Desc', Desc);
    }
    return fromPropertyDescriptor(Desc);
  };
  return FromPropertyDescriptor$1;
}

var IsGenericDescriptor;
var hasRequiredIsGenericDescriptor;
function requireIsGenericDescriptor() {
  if (hasRequiredIsGenericDescriptor) return IsGenericDescriptor;
  hasRequiredIsGenericDescriptor = 1;
  var assertRecord = requireAssertRecord();
  var IsAccessorDescriptor = requireIsAccessorDescriptor();
  var IsDataDescriptor = requireIsDataDescriptor();
  var Type = Type$8;

  // https://262.ecma-international.org/6.0/#sec-isgenericdescriptor

  IsGenericDescriptor = function IsGenericDescriptor(Desc) {
    if (typeof Desc === 'undefined') {
      return false;
    }
    assertRecord(Type, 'Property Descriptor', 'Desc', Desc);
    if (!IsAccessorDescriptor(Desc) && !IsDataDescriptor(Desc)) {
      return true;
    }
    return false;
  };
  return IsGenericDescriptor;
}

var ValidateAndApplyPropertyDescriptor;
var hasRequiredValidateAndApplyPropertyDescriptor;
function requireValidateAndApplyPropertyDescriptor() {
  if (hasRequiredValidateAndApplyPropertyDescriptor) return ValidateAndApplyPropertyDescriptor;
  hasRequiredValidateAndApplyPropertyDescriptor = 1;
  var GetIntrinsic = getIntrinsic;
  var $TypeError = GetIntrinsic('%TypeError%');
  var DefineOwnProperty = requireDefineOwnProperty();
  var isFullyPopulatedPropertyDescriptor = requireIsFullyPopulatedPropertyDescriptor();
  var isPropertyDescriptor = requireIsPropertyDescriptor();
  var FromPropertyDescriptor = requireFromPropertyDescriptor();
  var IsAccessorDescriptor = requireIsAccessorDescriptor();
  var IsDataDescriptor = requireIsDataDescriptor();
  var IsGenericDescriptor = requireIsGenericDescriptor();
  var IsPropertyKey = requireIsPropertyKey();
  var SameValue = requireSameValue();
  var Type = Type$8;

  // https://262.ecma-international.org/13.0/#sec-validateandapplypropertydescriptor

  // see https://github.com/tc39/ecma262/pull/2468 for ES2022 changes

  // eslint-disable-next-line max-lines-per-function, max-statements, max-params
  ValidateAndApplyPropertyDescriptor = function ValidateAndApplyPropertyDescriptor(O, P, extensible, Desc, current) {
    var oType = Type(O);
    if (oType !== 'Undefined' && oType !== 'Object') {
      throw new $TypeError('Assertion failed: O must be undefined or an Object');
    }
    if (!IsPropertyKey(P)) {
      throw new $TypeError('Assertion failed: P must be a Property Key');
    }
    if (Type(extensible) !== 'Boolean') {
      throw new $TypeError('Assertion failed: extensible must be a Boolean');
    }
    if (!isPropertyDescriptor({
      Type: Type,
      IsDataDescriptor: IsDataDescriptor,
      IsAccessorDescriptor: IsAccessorDescriptor
    }, Desc)) {
      throw new $TypeError('Assertion failed: Desc must be a Property Descriptor');
    }
    if (Type(current) !== 'Undefined' && !isPropertyDescriptor({
      Type: Type,
      IsDataDescriptor: IsDataDescriptor,
      IsAccessorDescriptor: IsAccessorDescriptor
    }, current)) {
      throw new $TypeError('Assertion failed: current must be a Property Descriptor, or undefined');
    }
    if (Type(current) === 'Undefined') {
      // step 2
      if (!extensible) {
        return false; // step 2.a
      }

      if (oType === 'Undefined') {
        return true; // step 2.b
      }

      if (IsAccessorDescriptor(Desc)) {
        // step 2.c
        return DefineOwnProperty(IsDataDescriptor, SameValue, FromPropertyDescriptor, O, P, Desc);
      }
      // step 2.d
      return DefineOwnProperty(IsDataDescriptor, SameValue, FromPropertyDescriptor, O, P, {
        '[[Configurable]]': !!Desc['[[Configurable]]'],
        '[[Enumerable]]': !!Desc['[[Enumerable]]'],
        '[[Value]]': Desc['[[Value]]'],
        '[[Writable]]': !!Desc['[[Writable]]']
      });
    }

    // 3. Assert: current is a fully populated Property Descriptor.
    if (!isFullyPopulatedPropertyDescriptor({
      IsAccessorDescriptor: IsAccessorDescriptor,
      IsDataDescriptor: IsDataDescriptor
    }, current)) {
      throw new $TypeError('`current`, when present, must be a fully populated and valid Property Descriptor');
    }

    // 4. If every field in Desc is absent, return true.
    // this can't really match the assertion that it's a Property Descriptor in our JS implementation

    // 5. If current.[[Configurable]] is false, then
    if (!current['[[Configurable]]']) {
      if ('[[Configurable]]' in Desc && Desc['[[Configurable]]']) {
        // step 5.a
        return false;
      }
      if ('[[Enumerable]]' in Desc && !SameValue(Desc['[[Enumerable]]'], current['[[Enumerable]]'])) {
        // step 5.b
        return false;
      }
      if (!IsGenericDescriptor(Desc) && !SameValue(IsAccessorDescriptor(Desc), IsAccessorDescriptor(current))) {
        // step 5.c
        return false;
      }
      if (IsAccessorDescriptor(current)) {
        // step 5.d
        if ('[[Get]]' in Desc && !SameValue(Desc['[[Get]]'], current['[[Get]]'])) {
          return false;
        }
        if ('[[Set]]' in Desc && !SameValue(Desc['[[Set]]'], current['[[Set]]'])) {
          return false;
        }
      } else if (!current['[[Writable]]']) {
        // step 5.e
        if ('[[Writable]]' in Desc && Desc['[[Writable]]']) {
          return false;
        }
        if ('[[Value]]' in Desc && !SameValue(Desc['[[Value]]'], current['[[Value]]'])) {
          return false;
        }
      }
    }

    // 6. If O is not undefined, then
    if (oType !== 'Undefined') {
      var configurable;
      var enumerable;
      if (IsDataDescriptor(current) && IsAccessorDescriptor(Desc)) {
        // step 6.a
        configurable = ('[[Configurable]]' in Desc ? Desc : current)['[[Configurable]]'];
        enumerable = ('[[Enumerable]]' in Desc ? Desc : current)['[[Enumerable]]'];
        // Replace the property named P of object O with an accessor property having [[Configurable]] and [[Enumerable]] attributes as described by current and each other attribute set to its default value.
        return DefineOwnProperty(IsDataDescriptor, SameValue, FromPropertyDescriptor, O, P, {
          '[[Configurable]]': !!configurable,
          '[[Enumerable]]': !!enumerable,
          '[[Get]]': ('[[Get]]' in Desc ? Desc : current)['[[Get]]'],
          '[[Set]]': ('[[Set]]' in Desc ? Desc : current)['[[Set]]']
        });
      } else if (IsAccessorDescriptor(current) && IsDataDescriptor(Desc)) {
        configurable = ('[[Configurable]]' in Desc ? Desc : current)['[[Configurable]]'];
        enumerable = ('[[Enumerable]]' in Desc ? Desc : current)['[[Enumerable]]'];
        // i. Replace the property named P of object O with a data property having [[Configurable]] and [[Enumerable]] attributes as described by current and each other attribute set to its default value.
        return DefineOwnProperty(IsDataDescriptor, SameValue, FromPropertyDescriptor, O, P, {
          '[[Configurable]]': !!configurable,
          '[[Enumerable]]': !!enumerable,
          '[[Value]]': ('[[Value]]' in Desc ? Desc : current)['[[Value]]'],
          '[[Writable]]': !!('[[Writable]]' in Desc ? Desc : current)['[[Writable]]']
        });
      }

      // For each field of Desc that is present, set the corresponding attribute of the property named P of object O to the value of the field.
      return DefineOwnProperty(IsDataDescriptor, SameValue, FromPropertyDescriptor, O, P, Desc);
    }
    return true; // step 7
  };

  return ValidateAndApplyPropertyDescriptor;
}

var OrdinaryDefineOwnProperty;
var hasRequiredOrdinaryDefineOwnProperty;
function requireOrdinaryDefineOwnProperty() {
  if (hasRequiredOrdinaryDefineOwnProperty) return OrdinaryDefineOwnProperty;
  hasRequiredOrdinaryDefineOwnProperty = 1;
  var GetIntrinsic = getIntrinsic;
  var $gOPD = requireGopd();
  var $SyntaxError = GetIntrinsic('%SyntaxError%');
  var $TypeError = GetIntrinsic('%TypeError%');
  var isPropertyDescriptor = requireIsPropertyDescriptor();
  var IsAccessorDescriptor = requireIsAccessorDescriptor();
  var IsDataDescriptor = requireIsDataDescriptor();
  var IsExtensible = requireIsExtensible();
  var IsPropertyKey = requireIsPropertyKey();
  var ToPropertyDescriptor = requireToPropertyDescriptor();
  var SameValue = requireSameValue();
  var Type = Type$8;
  var ValidateAndApplyPropertyDescriptor = requireValidateAndApplyPropertyDescriptor();

  // https://262.ecma-international.org/6.0/#sec-ordinarydefineownproperty

  OrdinaryDefineOwnProperty = function OrdinaryDefineOwnProperty(O, P, Desc) {
    if (Type(O) !== 'Object') {
      throw new $TypeError('Assertion failed: O must be an Object');
    }
    if (!IsPropertyKey(P)) {
      throw new $TypeError('Assertion failed: P must be a Property Key');
    }
    if (!isPropertyDescriptor({
      Type: Type,
      IsDataDescriptor: IsDataDescriptor,
      IsAccessorDescriptor: IsAccessorDescriptor
    }, Desc)) {
      throw new $TypeError('Assertion failed: Desc must be a Property Descriptor');
    }
    if (!$gOPD) {
      // ES3/IE 8 fallback
      if (IsAccessorDescriptor(Desc)) {
        throw new $SyntaxError('This environment does not support accessor property descriptors.');
      }
      var creatingNormalDataProperty = !(P in O) && Desc['[[Writable]]'] && Desc['[[Enumerable]]'] && Desc['[[Configurable]]'] && '[[Value]]' in Desc;
      var settingExistingDataProperty = P in O && (!('[[Configurable]]' in Desc) || Desc['[[Configurable]]']) && (!('[[Enumerable]]' in Desc) || Desc['[[Enumerable]]']) && (!('[[Writable]]' in Desc) || Desc['[[Writable]]']) && '[[Value]]' in Desc;
      if (creatingNormalDataProperty || settingExistingDataProperty) {
        O[P] = Desc['[[Value]]']; // eslint-disable-line no-param-reassign
        return SameValue(O[P], Desc['[[Value]]']);
      }
      throw new $SyntaxError('This environment does not support defining non-writable, non-enumerable, or non-configurable properties');
    }
    var desc = $gOPD(O, P);
    var current = desc && ToPropertyDescriptor(desc);
    var extensible = IsExtensible(O);
    return ValidateAndApplyPropertyDescriptor(O, P, extensible, Desc, current);
  };
  return OrdinaryDefineOwnProperty;
}

var CreateDataProperty$1;
var hasRequiredCreateDataProperty;
function requireCreateDataProperty() {
  if (hasRequiredCreateDataProperty) return CreateDataProperty$1;
  hasRequiredCreateDataProperty = 1;
  var GetIntrinsic = getIntrinsic;
  var $TypeError = GetIntrinsic('%TypeError%');
  var IsPropertyKey = requireIsPropertyKey();
  var OrdinaryDefineOwnProperty = requireOrdinaryDefineOwnProperty();
  var Type = Type$8;

  // https://262.ecma-international.org/6.0/#sec-createdataproperty

  CreateDataProperty$1 = function CreateDataProperty(O, P, V) {
    if (Type(O) !== 'Object') {
      throw new $TypeError('Assertion failed: Type(O) is not Object');
    }
    if (!IsPropertyKey(P)) {
      throw new $TypeError('Assertion failed: IsPropertyKey(P) is not true');
    }
    var newDesc = {
      '[[Configurable]]': true,
      '[[Enumerable]]': true,
      '[[Value]]': V,
      '[[Writable]]': true
    };
    return OrdinaryDefineOwnProperty(O, P, newDesc);
  };
  return CreateDataProperty$1;
}

var GetIntrinsic$5 = getIntrinsic;
var $TypeError$3 = GetIntrinsic$5('%TypeError%');
var CreateDataProperty = requireCreateDataProperty();
var IsPropertyKey$2 = requireIsPropertyKey();
var Type$3 = Type$8;

// // https://262.ecma-international.org/6.0/#sec-createdatapropertyorthrow

var CreateDataPropertyOrThrow$1 = function CreateDataPropertyOrThrow(O, P, V) {
  if (Type$3(O) !== 'Object') {
    throw new $TypeError$3('Assertion failed: Type(O) is not Object');
  }
  if (!IsPropertyKey$2(P)) {
    throw new $TypeError$3('Assertion failed: IsPropertyKey(P) is not true');
  }
  var success = CreateDataProperty(O, P, V);
  if (!success) {
    throw new $TypeError$3('unable to create data property');
  }
  return success;
};

var GetIntrinsic$4 = getIntrinsic;
var $TypeError$2 = GetIntrinsic$4('%TypeError%');
var DefineOwnProperty = requireDefineOwnProperty();
var FromPropertyDescriptor = requireFromPropertyDescriptor();
var IsDataDescriptor = requireIsDataDescriptor();
var IsPropertyKey$1 = requireIsPropertyKey();
var SameValue = requireSameValue();
var Type$2 = Type$8;

// https://262.ecma-international.org/6.0/#sec-createmethodproperty

var CreateMethodProperty$1 = function CreateMethodProperty(O, P, V) {
  if (Type$2(O) !== 'Object') {
    throw new $TypeError$2('Assertion failed: Type(O) is not Object');
  }
  if (!IsPropertyKey$1(P)) {
    throw new $TypeError$2('Assertion failed: IsPropertyKey(P) is not true');
  }
  var newDesc = {
    '[[Configurable]]': true,
    '[[Enumerable]]': false,
    '[[Value]]': V,
    '[[Writable]]': true
  };
  return DefineOwnProperty(IsDataDescriptor, SameValue, FromPropertyDescriptor, O, P, newDesc);
};

var CheckObjectCoercible;
var hasRequiredCheckObjectCoercible;
function requireCheckObjectCoercible() {
  if (hasRequiredCheckObjectCoercible) return CheckObjectCoercible;
  hasRequiredCheckObjectCoercible = 1;
  var GetIntrinsic = getIntrinsic;
  var $TypeError = GetIntrinsic('%TypeError%');

  // http://262.ecma-international.org/5.1/#sec-9.10

  CheckObjectCoercible = function CheckObjectCoercible(value, optMessage) {
    if (value == null) {
      throw new $TypeError(optMessage || 'Cannot call method on ' + value);
    }
    return value;
  };
  return CheckObjectCoercible;
}

var RequireObjectCoercible;
var hasRequiredRequireObjectCoercible;
function requireRequireObjectCoercible() {
  if (hasRequiredRequireObjectCoercible) return RequireObjectCoercible;
  hasRequiredRequireObjectCoercible = 1;
  RequireObjectCoercible = requireCheckObjectCoercible();
  return RequireObjectCoercible;
}

var ToObject;
var hasRequiredToObject;
function requireToObject() {
  if (hasRequiredToObject) return ToObject;
  hasRequiredToObject = 1;
  var GetIntrinsic = getIntrinsic;
  var $Object = GetIntrinsic('%Object%');
  var RequireObjectCoercible = requireRequireObjectCoercible();

  // https://262.ecma-international.org/6.0/#sec-toobject

  ToObject = function ToObject(value) {
    RequireObjectCoercible(value);
    return $Object(value);
  };
  return ToObject;
}

var GetV$1;
var hasRequiredGetV;
function requireGetV() {
  if (hasRequiredGetV) return GetV$1;
  hasRequiredGetV = 1;
  var GetIntrinsic = getIntrinsic;
  var $TypeError = GetIntrinsic('%TypeError%');
  var IsPropertyKey = requireIsPropertyKey();
  var ToObject = requireToObject();

  // https://262.ecma-international.org/6.0/#sec-getv

  GetV$1 = function GetV(V, P) {
    // 7.3.2.1
    if (!IsPropertyKey(P)) {
      throw new $TypeError('Assertion failed: IsPropertyKey(P) is not true');
    }

    // 7.3.2.2-3
    var O = ToObject(V);

    // 7.3.2.4
    return O[P];
  };
  return GetV$1;
}

var util_inspect;
var hasRequiredUtil_inspect;
function requireUtil_inspect() {
  if (hasRequiredUtil_inspect) return util_inspect;
  hasRequiredUtil_inspect = 1;
  util_inspect = b.inspect;
  return util_inspect;
}

var objectInspect;
var hasRequiredObjectInspect;
function requireObjectInspect() {
  if (hasRequiredObjectInspect) return objectInspect;
  hasRequiredObjectInspect = 1;
  var hasMap = typeof Map === 'function' && Map.prototype;
  var mapSizeDescriptor = Object.getOwnPropertyDescriptor && hasMap ? Object.getOwnPropertyDescriptor(Map.prototype, 'size') : null;
  var mapSize = hasMap && mapSizeDescriptor && typeof mapSizeDescriptor.get === 'function' ? mapSizeDescriptor.get : null;
  var mapForEach = hasMap && Map.prototype.forEach;
  var hasSet = typeof Set === 'function' && Set.prototype;
  var setSizeDescriptor = Object.getOwnPropertyDescriptor && hasSet ? Object.getOwnPropertyDescriptor(Set.prototype, 'size') : null;
  var setSize = hasSet && setSizeDescriptor && typeof setSizeDescriptor.get === 'function' ? setSizeDescriptor.get : null;
  var setForEach = hasSet && Set.prototype.forEach;
  var hasWeakMap = typeof WeakMap === 'function' && WeakMap.prototype;
  var weakMapHas = hasWeakMap ? WeakMap.prototype.has : null;
  var hasWeakSet = typeof WeakSet === 'function' && WeakSet.prototype;
  var weakSetHas = hasWeakSet ? WeakSet.prototype.has : null;
  var hasWeakRef = typeof WeakRef === 'function' && WeakRef.prototype;
  var weakRefDeref = hasWeakRef ? WeakRef.prototype.deref : null;
  var booleanValueOf = Boolean.prototype.valueOf;
  var objectToString = Object.prototype.toString;
  var functionToString = Function.prototype.toString;
  var $match = String.prototype.match;
  var $slice = String.prototype.slice;
  var $replace = String.prototype.replace;
  var $toUpperCase = String.prototype.toUpperCase;
  var $toLowerCase = String.prototype.toLowerCase;
  var $test = RegExp.prototype.test;
  var $concat = Array.prototype.concat;
  var $join = Array.prototype.join;
  var $arrSlice = Array.prototype.slice;
  var $floor = Math.floor;
  var bigIntValueOf = typeof BigInt === 'function' ? BigInt.prototype.valueOf : null;
  var gOPS = Object.getOwnPropertySymbols;
  var symToString = typeof Symbol === 'function' && _typeof$1(Symbol.iterator) === 'symbol' ? Symbol.prototype.toString : null;
  var hasShammedSymbols = typeof Symbol === 'function' && _typeof$1(Symbol.iterator) === 'object';
  // ie, `has-tostringtag/shams
  var toStringTag = typeof Symbol === 'function' && Symbol.toStringTag && (_typeof$1(Symbol.toStringTag) === hasShammedSymbols ? 'object' : 'symbol') ? Symbol.toStringTag : null;
  var isEnumerable = Object.prototype.propertyIsEnumerable;
  var gPO = (typeof Reflect === 'function' ? Reflect.getPrototypeOf : Object.getPrototypeOf) || ([].__proto__ === Array.prototype // eslint-disable-line no-proto
  ? function (O) {
    return O.__proto__; // eslint-disable-line no-proto
  } : null);
  function addNumericSeparator(num, str) {
    if (num === Infinity || num === -Infinity || num !== num || num && num > -1000 && num < 1000 || $test.call(/e/, str)) {
      return str;
    }
    var sepRegex = /[0-9](?=(?:[0-9]{3})+(?![0-9]))/g;
    if (typeof num === 'number') {
      var _int = num < 0 ? -$floor(-num) : $floor(num); // trunc(num)
      if (_int !== num) {
        var intStr = String(_int);
        var dec = $slice.call(str, intStr.length + 1);
        return $replace.call(intStr, sepRegex, '$&_') + '.' + $replace.call($replace.call(dec, /([0-9]{3})/g, '$&_'), /_$/, '');
      }
    }
    return $replace.call(str, sepRegex, '$&_');
  }
  var utilInspect = requireUtil_inspect();
  var inspectCustom = utilInspect.custom;
  var inspectSymbol = isSymbol(inspectCustom) ? inspectCustom : null;
  objectInspect = function inspect_(obj, options, depth, seen) {
    var opts = options || {};
    if (has(opts, 'quoteStyle') && opts.quoteStyle !== 'single' && opts.quoteStyle !== 'double') {
      throw new TypeError('option "quoteStyle" must be "single" or "double"');
    }
    if (has(opts, 'maxStringLength') && (typeof opts.maxStringLength === 'number' ? opts.maxStringLength < 0 && opts.maxStringLength !== Infinity : opts.maxStringLength !== null)) {
      throw new TypeError('option "maxStringLength", if provided, must be a positive integer, Infinity, or `null`');
    }
    var customInspect = has(opts, 'customInspect') ? opts.customInspect : true;
    if (typeof customInspect !== 'boolean' && customInspect !== 'symbol') {
      throw new TypeError('option "customInspect", if provided, must be `true`, `false`, or `\'symbol\'`');
    }
    if (has(opts, 'indent') && opts.indent !== null && opts.indent !== '\t' && !(parseInt(opts.indent, 10) === opts.indent && opts.indent > 0)) {
      throw new TypeError('option "indent" must be "\\t", an integer > 0, or `null`');
    }
    if (has(opts, 'numericSeparator') && typeof opts.numericSeparator !== 'boolean') {
      throw new TypeError('option "numericSeparator", if provided, must be `true` or `false`');
    }
    var numericSeparator = opts.numericSeparator;
    if (typeof obj === 'undefined') {
      return 'undefined';
    }
    if (obj === null) {
      return 'null';
    }
    if (typeof obj === 'boolean') {
      return obj ? 'true' : 'false';
    }
    if (typeof obj === 'string') {
      return inspectString(obj, opts);
    }
    if (typeof obj === 'number') {
      if (obj === 0) {
        return Infinity / obj > 0 ? '0' : '-0';
      }
      var str = String(obj);
      return numericSeparator ? addNumericSeparator(obj, str) : str;
    }
    if (typeof obj === 'bigint') {
      var bigIntStr = String(obj) + 'n';
      return numericSeparator ? addNumericSeparator(obj, bigIntStr) : bigIntStr;
    }
    var maxDepth = typeof opts.depth === 'undefined' ? 5 : opts.depth;
    if (typeof depth === 'undefined') {
      depth = 0;
    }
    if (depth >= maxDepth && maxDepth > 0 && _typeof$1(obj) === 'object') {
      return isArray(obj) ? '[Array]' : '[Object]';
    }
    var indent = getIndent(opts, depth);
    if (typeof seen === 'undefined') {
      seen = [];
    } else if (indexOf(seen, obj) >= 0) {
      return '[Circular]';
    }
    function inspect(value, from, noIndent) {
      if (from) {
        seen = $arrSlice.call(seen);
        seen.push(from);
      }
      if (noIndent) {
        var newOpts = {
          depth: opts.depth
        };
        if (has(opts, 'quoteStyle')) {
          newOpts.quoteStyle = opts.quoteStyle;
        }
        return inspect_(value, newOpts, depth + 1, seen);
      }
      return inspect_(value, opts, depth + 1, seen);
    }
    if (typeof obj === 'function' && !isRegExp(obj)) {
      // in older engines, regexes are callable
      var name = nameOf(obj);
      var keys = arrObjKeys(obj, inspect);
      return '[Function' + (name ? ': ' + name : ' (anonymous)') + ']' + (keys.length > 0 ? ' { ' + $join.call(keys, ', ') + ' }' : '');
    }
    if (isSymbol(obj)) {
      var symString = hasShammedSymbols ? $replace.call(String(obj), /^(Symbol\(.*\))_[^)]*$/, '$1') : symToString.call(obj);
      return _typeof$1(obj) === 'object' && !hasShammedSymbols ? markBoxed(symString) : symString;
    }
    if (isElement(obj)) {
      var s = '<' + $toLowerCase.call(String(obj.nodeName));
      var attrs = obj.attributes || [];
      for (var i = 0; i < attrs.length; i++) {
        s += ' ' + attrs[i].name + '=' + wrapQuotes(quote(attrs[i].value), 'double', opts);
      }
      s += '>';
      if (obj.childNodes && obj.childNodes.length) {
        s += '...';
      }
      s += '</' + $toLowerCase.call(String(obj.nodeName)) + '>';
      return s;
    }
    if (isArray(obj)) {
      if (obj.length === 0) {
        return '[]';
      }
      var xs = arrObjKeys(obj, inspect);
      if (indent && !singleLineValues(xs)) {
        return '[' + indentedJoin(xs, indent) + ']';
      }
      return '[ ' + $join.call(xs, ', ') + ' ]';
    }
    if (isError(obj)) {
      var parts = arrObjKeys(obj, inspect);
      if (!('cause' in Error.prototype) && 'cause' in obj && !isEnumerable.call(obj, 'cause')) {
        return '{ [' + String(obj) + '] ' + $join.call($concat.call('[cause]: ' + inspect(obj.cause), parts), ', ') + ' }';
      }
      if (parts.length === 0) {
        return '[' + String(obj) + ']';
      }
      return '{ [' + String(obj) + '] ' + $join.call(parts, ', ') + ' }';
    }
    if (_typeof$1(obj) === 'object' && customInspect) {
      if (inspectSymbol && typeof obj[inspectSymbol] === 'function' && utilInspect) {
        return utilInspect(obj, {
          depth: maxDepth - depth
        });
      } else if (customInspect !== 'symbol' && typeof obj.inspect === 'function') {
        return obj.inspect();
      }
    }
    if (isMap(obj)) {
      var mapParts = [];
      if (mapForEach) {
        mapForEach.call(obj, function (value, key) {
          mapParts.push(inspect(key, obj, true) + ' => ' + inspect(value, obj));
        });
      }
      return collectionOf('Map', mapSize.call(obj), mapParts, indent);
    }
    if (isSet(obj)) {
      var setParts = [];
      if (setForEach) {
        setForEach.call(obj, function (value) {
          setParts.push(inspect(value, obj));
        });
      }
      return collectionOf('Set', setSize.call(obj), setParts, indent);
    }
    if (isWeakMap(obj)) {
      return weakCollectionOf('WeakMap');
    }
    if (isWeakSet(obj)) {
      return weakCollectionOf('WeakSet');
    }
    if (isWeakRef(obj)) {
      return weakCollectionOf('WeakRef');
    }
    if (isNumber(obj)) {
      return markBoxed(inspect(Number(obj)));
    }
    if (isBigInt(obj)) {
      return markBoxed(inspect(bigIntValueOf.call(obj)));
    }
    if (isBoolean(obj)) {
      return markBoxed(booleanValueOf.call(obj));
    }
    if (isString(obj)) {
      return markBoxed(inspect(String(obj)));
    }
    if (!isDate(obj) && !isRegExp(obj)) {
      var ys = arrObjKeys(obj, inspect);
      var isPlainObject = gPO ? gPO(obj) === Object.prototype : obj instanceof Object || obj.constructor === Object;
      var protoTag = obj instanceof Object ? '' : 'null prototype';
      var stringTag = !isPlainObject && toStringTag && Object(obj) === obj && toStringTag in obj ? $slice.call(toStr(obj), 8, -1) : protoTag ? 'Object' : '';
      var constructorTag = isPlainObject || typeof obj.constructor !== 'function' ? '' : obj.constructor.name ? obj.constructor.name + ' ' : '';
      var tag = constructorTag + (stringTag || protoTag ? '[' + $join.call($concat.call([], stringTag || [], protoTag || []), ': ') + '] ' : '');
      if (ys.length === 0) {
        return tag + '{}';
      }
      if (indent) {
        return tag + '{' + indentedJoin(ys, indent) + '}';
      }
      return tag + '{ ' + $join.call(ys, ', ') + ' }';
    }
    return String(obj);
  };
  function wrapQuotes(s, defaultStyle, opts) {
    var quoteChar = (opts.quoteStyle || defaultStyle) === 'double' ? '"' : "'";
    return quoteChar + s + quoteChar;
  }
  function quote(s) {
    return $replace.call(String(s), /"/g, '&quot;');
  }
  function isArray(obj) {
    return toStr(obj) === '[object Array]' && (!toStringTag || !(_typeof$1(obj) === 'object' && toStringTag in obj));
  }
  function isDate(obj) {
    return toStr(obj) === '[object Date]' && (!toStringTag || !(_typeof$1(obj) === 'object' && toStringTag in obj));
  }
  function isRegExp(obj) {
    return toStr(obj) === '[object RegExp]' && (!toStringTag || !(_typeof$1(obj) === 'object' && toStringTag in obj));
  }
  function isError(obj) {
    return toStr(obj) === '[object Error]' && (!toStringTag || !(_typeof$1(obj) === 'object' && toStringTag in obj));
  }
  function isString(obj) {
    return toStr(obj) === '[object String]' && (!toStringTag || !(_typeof$1(obj) === 'object' && toStringTag in obj));
  }
  function isNumber(obj) {
    return toStr(obj) === '[object Number]' && (!toStringTag || !(_typeof$1(obj) === 'object' && toStringTag in obj));
  }
  function isBoolean(obj) {
    return toStr(obj) === '[object Boolean]' && (!toStringTag || !(_typeof$1(obj) === 'object' && toStringTag in obj));
  }

  // Symbol and BigInt do have Symbol.toStringTag by spec, so that can't be used to eliminate false positives
  function isSymbol(obj) {
    if (hasShammedSymbols) {
      return obj && _typeof$1(obj) === 'object' && obj instanceof Symbol;
    }
    if (_typeof$1(obj) === 'symbol') {
      return true;
    }
    if (!obj || _typeof$1(obj) !== 'object' || !symToString) {
      return false;
    }
    try {
      symToString.call(obj);
      return true;
    } catch (e) {}
    return false;
  }
  function isBigInt(obj) {
    if (!obj || _typeof$1(obj) !== 'object' || !bigIntValueOf) {
      return false;
    }
    try {
      bigIntValueOf.call(obj);
      return true;
    } catch (e) {}
    return false;
  }
  var hasOwn = Object.prototype.hasOwnProperty || function (key) {
    return key in this;
  };
  function has(obj, key) {
    return hasOwn.call(obj, key);
  }
  function toStr(obj) {
    return objectToString.call(obj);
  }
  function nameOf(f) {
    if (f.name) {
      return f.name;
    }
    var m = $match.call(functionToString.call(f), /^function\s*([\w$]+)/);
    if (m) {
      return m[1];
    }
    return null;
  }
  function indexOf(xs, x) {
    if (xs.indexOf) {
      return xs.indexOf(x);
    }
    for (var i = 0, l = xs.length; i < l; i++) {
      if (xs[i] === x) {
        return i;
      }
    }
    return -1;
  }
  function isMap(x) {
    if (!mapSize || !x || _typeof$1(x) !== 'object') {
      return false;
    }
    try {
      mapSize.call(x);
      try {
        setSize.call(x);
      } catch (s) {
        return true;
      }
      return x instanceof Map; // core-js workaround, pre-v2.5.0
    } catch (e) {}
    return false;
  }
  function isWeakMap(x) {
    if (!weakMapHas || !x || _typeof$1(x) !== 'object') {
      return false;
    }
    try {
      weakMapHas.call(x, weakMapHas);
      try {
        weakSetHas.call(x, weakSetHas);
      } catch (s) {
        return true;
      }
      return x instanceof WeakMap; // core-js workaround, pre-v2.5.0
    } catch (e) {}
    return false;
  }
  function isWeakRef(x) {
    if (!weakRefDeref || !x || _typeof$1(x) !== 'object') {
      return false;
    }
    try {
      weakRefDeref.call(x);
      return true;
    } catch (e) {}
    return false;
  }
  function isSet(x) {
    if (!setSize || !x || _typeof$1(x) !== 'object') {
      return false;
    }
    try {
      setSize.call(x);
      try {
        mapSize.call(x);
      } catch (m) {
        return true;
      }
      return x instanceof Set; // core-js workaround, pre-v2.5.0
    } catch (e) {}
    return false;
  }
  function isWeakSet(x) {
    if (!weakSetHas || !x || _typeof$1(x) !== 'object') {
      return false;
    }
    try {
      weakSetHas.call(x, weakSetHas);
      try {
        weakMapHas.call(x, weakMapHas);
      } catch (s) {
        return true;
      }
      return x instanceof WeakSet; // core-js workaround, pre-v2.5.0
    } catch (e) {}
    return false;
  }
  function isElement(x) {
    if (!x || _typeof$1(x) !== 'object') {
      return false;
    }
    if (typeof HTMLElement !== 'undefined' && x instanceof HTMLElement) {
      return true;
    }
    return typeof x.nodeName === 'string' && typeof x.getAttribute === 'function';
  }
  function inspectString(str, opts) {
    if (str.length > opts.maxStringLength) {
      var remaining = str.length - opts.maxStringLength;
      var trailer = '... ' + remaining + ' more character' + (remaining > 1 ? 's' : '');
      return inspectString($slice.call(str, 0, opts.maxStringLength), opts) + trailer;
    }
    // eslint-disable-next-line no-control-regex
    var s = $replace.call($replace.call(str, /(['\\])/g, '\\$1'), /[\x00-\x1f]/g, lowbyte);
    return wrapQuotes(s, 'single', opts);
  }
  function lowbyte(c) {
    var n = c.charCodeAt(0);
    var x = {
      8: 'b',
      9: 't',
      10: 'n',
      12: 'f',
      13: 'r'
    }[n];
    if (x) {
      return '\\' + x;
    }
    return '\\x' + (n < 0x10 ? '0' : '') + $toUpperCase.call(n.toString(16));
  }
  function markBoxed(str) {
    return 'Object(' + str + ')';
  }
  function weakCollectionOf(type) {
    return type + ' { ? }';
  }
  function collectionOf(type, size, entries, indent) {
    var joinedEntries = indent ? indentedJoin(entries, indent) : $join.call(entries, ', ');
    return type + ' (' + size + ') {' + joinedEntries + '}';
  }
  function singleLineValues(xs) {
    for (var i = 0; i < xs.length; i++) {
      if (indexOf(xs[i], '\n') >= 0) {
        return false;
      }
    }
    return true;
  }
  function getIndent(opts, depth) {
    var baseIndent;
    if (opts.indent === '\t') {
      baseIndent = '\t';
    } else if (typeof opts.indent === 'number' && opts.indent > 0) {
      baseIndent = $join.call(Array(opts.indent + 1), ' ');
    } else {
      return null;
    }
    return {
      base: baseIndent,
      prev: $join.call(Array(depth + 1), baseIndent)
    };
  }
  function indentedJoin(xs, indent) {
    if (xs.length === 0) {
      return '';
    }
    var lineJoiner = '\n' + indent.prev + indent.base;
    return lineJoiner + $join.call(xs, ',' + lineJoiner) + '\n' + indent.prev;
  }
  function arrObjKeys(obj, inspect) {
    var isArr = isArray(obj);
    var xs = [];
    if (isArr) {
      xs.length = obj.length;
      for (var i = 0; i < obj.length; i++) {
        xs[i] = has(obj, i) ? inspect(obj[i], obj) : '';
      }
    }
    var syms = typeof gOPS === 'function' ? gOPS(obj) : [];
    var symMap;
    if (hasShammedSymbols) {
      symMap = {};
      for (var k = 0; k < syms.length; k++) {
        symMap['$' + syms[k]] = syms[k];
      }
    }
    for (var key in obj) {
      // eslint-disable-line no-restricted-syntax
      if (!has(obj, key)) {
        continue;
      } // eslint-disable-line no-restricted-syntax, no-continue
      if (isArr && String(Number(key)) === key && key < obj.length) {
        continue;
      } // eslint-disable-line no-restricted-syntax, no-continue
      if (hasShammedSymbols && symMap['$' + key] instanceof Symbol) {
        // this is to prevent shammed Symbols, which are stored as strings, from being included in the string key section
        continue; // eslint-disable-line no-restricted-syntax, no-continue
      } else if ($test.call(/[^\w$]/, key)) {
        xs.push(inspect(key, obj) + ': ' + inspect(obj[key], obj));
      } else {
        xs.push(key + ': ' + inspect(obj[key], obj));
      }
    }
    if (typeof gOPS === 'function') {
      for (var j = 0; j < syms.length; j++) {
        if (isEnumerable.call(obj, syms[j])) {
          xs.push('[' + inspect(syms[j]) + ']: ' + inspect(obj[syms[j]], obj));
        }
      }
    }
    return xs;
  }
  return objectInspect;
}

var GetIntrinsic$3 = getIntrinsic;
var $TypeError$1 = GetIntrinsic$3('%TypeError%');
var GetV = requireGetV();
var IsCallable = requireIsCallable();
var IsPropertyKey = requireIsPropertyKey();
var inspect = requireObjectInspect();

// https://262.ecma-international.org/6.0/#sec-getmethod

var GetMethod$1 = function GetMethod(O, P) {
  // 7.3.9.1
  if (!IsPropertyKey(P)) {
    throw new $TypeError$1('Assertion failed: IsPropertyKey(P) is not true');
  }

  // 7.3.9.2
  var func = GetV(O, P);

  // 7.3.9.4
  if (func == null) {
    return void 0;
  }

  // 7.3.9.5
  if (!IsCallable(func)) {
    throw new $TypeError$1(inspect(P) + ' is not a function: ' + inspect(func));
  }

  // 7.3.9.6
  return func;
};

// https://262.ecma-international.org/6.0/#sec-isarray
var IsArray$1 = IsArray$2;

var hasSymbols$1 = shams$1;
var shams = function hasToStringTagShams() {
  return hasSymbols$1() && !!Symbol.toStringTag;
};

var strValue = String.prototype.valueOf;
var tryStringObject = function tryStringObject(value) {
  try {
    strValue.call(value);
    return true;
  } catch (e) {
    return false;
  }
};
var toStr = Object.prototype.toString;
var strClass = '[object String]';
var hasToStringTag = shams();
var isString$1 = function isString(value) {
  if (typeof value === 'string') {
    return true;
  }
  if (_typeof$1(value) !== 'object') {
    return false;
  }
  return hasToStringTag ? tryStringObject(value) : toStr.call(value) === strClass;
};

var hasSymbols = hasSymbols$4();
var GetIntrinsic$2 = getIntrinsic;
var callBound$1 = callBound$3;
var isString = isString$1;
var $iterator = GetIntrinsic$2('%Symbol.iterator%', true);
var $stringSlice = callBound$1('String.prototype.slice');
var $String = GetIntrinsic$2('%String%');
var getIteratorMethod$1 = function getIteratorMethod(ES, iterable) {
  var usingIterator;
  if (hasSymbols) {
    usingIterator = ES.GetMethod(iterable, $iterator);
  } else if (ES.IsArray(iterable)) {
    usingIterator = function usingIterator() {
      var i = -1;
      var arr = this; // eslint-disable-line no-invalid-this
      return {
        next: function next() {
          i += 1;
          return {
            done: i >= arr.length,
            value: arr[i]
          };
        }
      };
    };
  } else if (isString(iterable)) {
    usingIterator = function usingIterator() {
      var i = 0;
      return {
        next: function next() {
          var nextIndex = ES.AdvanceStringIndex($String(iterable), i, true);
          var value = $stringSlice(iterable, i, nextIndex);
          i = nextIndex;
          return {
            done: nextIndex > iterable.length,
            value: value
          };
        }
      };
    };
  }
  return usingIterator;
};

var Call;
var hasRequiredCall;
function requireCall() {
  if (hasRequiredCall) return Call;
  hasRequiredCall = 1;
  var GetIntrinsic = getIntrinsic;
  var callBound = callBound$3;
  var $TypeError = GetIntrinsic('%TypeError%');
  var IsArray = IsArray$1;
  var $apply = GetIntrinsic('%Reflect.apply%', true) || callBound('Function.prototype.apply');

  // https://262.ecma-international.org/6.0/#sec-call

  Call = function Call(F, V) {
    var argumentsList = arguments.length > 2 ? arguments[2] : [];
    if (!IsArray(argumentsList)) {
      throw new $TypeError('Assertion failed: optional `argumentsList`, if provided, must be a List');
    }
    return $apply(F, V, argumentsList);
  };
  return Call;
}

var GetIterator$1;
var hasRequiredGetIterator;
function requireGetIterator() {
  if (hasRequiredGetIterator) return GetIterator$1;
  hasRequiredGetIterator = 1;
  var GetIntrinsic = getIntrinsic;
  var $TypeError = GetIntrinsic('%TypeError%');
  var $SyntaxError = GetIntrinsic('%SyntaxError%');
  var $asyncIterator = GetIntrinsic('%Symbol.asyncIterator%', true);
  var inspect = requireObjectInspect();
  var hasSymbols = hasSymbols$4();
  var getIteratorMethod = getIteratorMethod$1;
  var AdvanceStringIndex = AdvanceStringIndex$1;
  var Call = requireCall();
  var GetMethod = GetMethod$1;
  var IsArray = IsArray$1;
  var Type = Type$8;

  // https://262.ecma-international.org/11.0/#sec-getiterator

  GetIterator$1 = function GetIterator(obj, hint, method) {
    var actualHint = hint;
    if (arguments.length < 2) {
      actualHint = 'sync';
    }
    if (actualHint !== 'sync' && actualHint !== 'async') {
      throw new $TypeError("Assertion failed: `hint` must be one of 'sync' or 'async', got " + inspect(hint));
    }
    var actualMethod = method;
    if (arguments.length < 3) {
      if (actualHint === 'async') {
        if (hasSymbols && $asyncIterator) {
          actualMethod = GetMethod(obj, $asyncIterator);
        }
        if (actualMethod === undefined) {
          throw new $SyntaxError("async from sync iterators aren't currently supported");
        }
      } else {
        actualMethod = getIteratorMethod({
          AdvanceStringIndex: AdvanceStringIndex,
          GetMethod: GetMethod,
          IsArray: IsArray
        }, obj);
      }
    }
    var iterator = Call(actualMethod, obj);
    if (Type(iterator) !== 'Object') {
      throw new $TypeError('iterator must return an object');
    }
    return iterator;

    // TODO: This should return an IteratorRecord
    /*
    var nextMethod = GetV(iterator, 'next');
    return {
    	'[[Iterator]]': iterator,
    	'[[NextMethod]]': nextMethod,
    	'[[Done]]': false
    };
    */
  };

  return GetIterator$1;
}

var Get;
var hasRequiredGet;
function requireGet() {
  if (hasRequiredGet) return Get;
  hasRequiredGet = 1;
  var GetIntrinsic = getIntrinsic;
  var $TypeError = GetIntrinsic('%TypeError%');
  var inspect = requireObjectInspect();
  var IsPropertyKey = requireIsPropertyKey();
  var Type = Type$8;

  // https://262.ecma-international.org/6.0/#sec-get-o-p

  Get = function Get(O, P) {
    // 7.3.1.1
    if (Type(O) !== 'Object') {
      throw new $TypeError('Assertion failed: Type(O) is not Object');
    }
    // 7.3.1.2
    if (!IsPropertyKey(P)) {
      throw new $TypeError('Assertion failed: IsPropertyKey(P) is not true, got ' + inspect(P));
    }
    // 7.3.1.3
    return O[P];
  };
  return Get;
}

var IteratorComplete;
var hasRequiredIteratorComplete;
function requireIteratorComplete() {
  if (hasRequiredIteratorComplete) return IteratorComplete;
  hasRequiredIteratorComplete = 1;
  var GetIntrinsic = getIntrinsic;
  var $TypeError = GetIntrinsic('%TypeError%');
  var Get = requireGet();
  var ToBoolean = requireToBoolean();
  var Type = Type$8;

  // https://262.ecma-international.org/6.0/#sec-iteratorcomplete

  IteratorComplete = function IteratorComplete(iterResult) {
    if (Type(iterResult) !== 'Object') {
      throw new $TypeError('Assertion failed: Type(iterResult) is not Object');
    }
    return ToBoolean(Get(iterResult, 'done'));
  };
  return IteratorComplete;
}

var Invoke;
var hasRequiredInvoke;
function requireInvoke() {
  if (hasRequiredInvoke) return Invoke;
  hasRequiredInvoke = 1;
  var GetIntrinsic = getIntrinsic;
  var $TypeError = GetIntrinsic('%TypeError%');
  var Call = requireCall();
  var IsArray = IsArray$1;
  var GetV = requireGetV();
  var IsPropertyKey = requireIsPropertyKey();

  // https://262.ecma-international.org/6.0/#sec-invoke

  Invoke = function Invoke(O, P) {
    if (!IsPropertyKey(P)) {
      throw new $TypeError('Assertion failed: P must be a Property Key');
    }
    var argumentsList = arguments.length > 2 ? arguments[2] : [];
    if (!IsArray(argumentsList)) {
      throw new $TypeError('Assertion failed: optional `argumentsList`, if provided, must be a List');
    }
    var func = GetV(O, P);
    return Call(func, O, argumentsList);
  };
  return Invoke;
}

var IteratorNext;
var hasRequiredIteratorNext;
function requireIteratorNext() {
  if (hasRequiredIteratorNext) return IteratorNext;
  hasRequiredIteratorNext = 1;
  var GetIntrinsic = getIntrinsic;
  var $TypeError = GetIntrinsic('%TypeError%');
  var Invoke = requireInvoke();
  var Type = Type$8;

  // https://262.ecma-international.org/6.0/#sec-iteratornext

  IteratorNext = function IteratorNext(iterator, value) {
    var result = Invoke(iterator, 'next', arguments.length < 2 ? [] : [value]);
    if (Type(result) !== 'Object') {
      throw new $TypeError('iterator next must return an object');
    }
    return result;
  };
  return IteratorNext;
}

var IteratorStep$1;
var hasRequiredIteratorStep;
function requireIteratorStep() {
  if (hasRequiredIteratorStep) return IteratorStep$1;
  hasRequiredIteratorStep = 1;
  var IteratorComplete = requireIteratorComplete();
  var IteratorNext = requireIteratorNext();

  // https://262.ecma-international.org/6.0/#sec-iteratorstep

  IteratorStep$1 = function IteratorStep(iterator) {
    var result = IteratorNext(iterator);
    var done = IteratorComplete(result);
    return done === true ? false : result;
  };
  return IteratorStep$1;
}

var IteratorValue$1;
var hasRequiredIteratorValue;
function requireIteratorValue() {
  if (hasRequiredIteratorValue) return IteratorValue$1;
  hasRequiredIteratorValue = 1;
  var GetIntrinsic = getIntrinsic;
  var $TypeError = GetIntrinsic('%TypeError%');
  var Get = requireGet();
  var Type = Type$8;

  // https://262.ecma-international.org/6.0/#sec-iteratorvalue

  IteratorValue$1 = function IteratorValue(iterResult) {
    if (Type(iterResult) !== 'Object') {
      throw new $TypeError('Assertion failed: Type(iterResult) is not Object');
    }
    return Get(iterResult, 'value');
  };
  return IteratorValue$1;
}

var callBound = callBound$3;
var $arrayPush = callBound('Array.prototype.push');
var GetIterator = requireGetIterator();
var IteratorStep = requireIteratorStep();
var IteratorValue = requireIteratorValue();

// https://262.ecma-international.org/12.0/#sec-iterabletolist

var IterableToList$1 = function IterableToList(items) {
  var iterator;
  if (arguments.length > 1) {
    iterator = GetIterator(items, 'sync', arguments[1]);
  } else {
    iterator = GetIterator(items, 'sync');
  }
  var values = [];
  var next = true;
  while (next) {
    next = IteratorStep(iterator);
    if (next) {
      var nextValue = IteratorValue(next);
      $arrayPush(values, nextValue);
    }
  }
  return values;
};

var setProto;
var hasRequiredSetProto;
function requireSetProto() {
  if (hasRequiredSetProto) return setProto;
  hasRequiredSetProto = 1;
  var GetIntrinsic = getIntrinsic;
  var originalSetProto = GetIntrinsic('%Object.setPrototypeOf%', true);
  var hasProto = hasProto$1();
  setProto = originalSetProto || (hasProto ? function (O, proto) {
    O.__proto__ = proto; // eslint-disable-line no-proto, no-param-reassign
    return O;
  } : null);
  return setProto;
}

var getProto;
var hasRequiredGetProto;
function requireGetProto() {
  if (hasRequiredGetProto) return getProto;
  hasRequiredGetProto = 1;
  var GetIntrinsic = getIntrinsic;
  var originalGetProto = GetIntrinsic('%Object.getPrototypeOf%', true);
  var hasProto = hasProto$1();
  getProto = originalGetProto || (hasProto ? function (O) {
    return O.__proto__; // eslint-disable-line no-proto
  } : null);
  return getProto;
}

var OrdinaryGetPrototypeOf$1;
var hasRequiredOrdinaryGetPrototypeOf;
function requireOrdinaryGetPrototypeOf() {
  if (hasRequiredOrdinaryGetPrototypeOf) return OrdinaryGetPrototypeOf$1;
  hasRequiredOrdinaryGetPrototypeOf = 1;
  var GetIntrinsic = getIntrinsic;
  var $TypeError = GetIntrinsic('%TypeError%');
  var $getProto = requireGetProto();
  var Type = Type$8;

  // https://262.ecma-international.org/7.0/#sec-ordinarygetprototypeof

  OrdinaryGetPrototypeOf$1 = function OrdinaryGetPrototypeOf(O) {
    if (Type(O) !== 'Object') {
      throw new $TypeError('Assertion failed: O must be an Object');
    }
    if (!$getProto) {
      throw new $TypeError('This environment does not support fetching prototypes.');
    }
    return $getProto(O);
  };
  return OrdinaryGetPrototypeOf$1;
}

var GetIntrinsic$1 = getIntrinsic;
var $TypeError = GetIntrinsic$1('%TypeError%');
var $setProto = requireSetProto();
var OrdinaryGetPrototypeOf = requireOrdinaryGetPrototypeOf();
var Type$1 = Type$8;

// https://262.ecma-international.org/7.0/#sec-ordinarysetprototypeof

var OrdinarySetPrototypeOf$1 = function OrdinarySetPrototypeOf(O, V) {
  if (Type$1(V) !== 'Object' && Type$1(V) !== 'Null') {
    throw new $TypeError('Assertion failed: V must be Object or Null');
  }
  /*
  var extensible = IsExtensible(O);
  var current = OrdinaryGetPrototypeOf(O);
  if (SameValue(V, current)) {
  	return true;
  }
  if (!extensible) {
  	return false;
  }
  */
  try {
    $setProto(O, V);
  } catch (e) {
    return false;
  }
  return OrdinaryGetPrototypeOf(O) === V;
  /*
  var p = V;
  var done = false;
  while (!done) {
  	if (p === null) {
  		done = true;
  	} else if (SameValue(p, O)) {
  		return false;
  	} else {
  		if (wat) {
  			done = true;
  		} else {
  			p = p.[[Prototype]];
  		}
  	}
  }
  O.[[Prototype]] = V;
  return true;
  */
};

var AdvanceStringIndex = AdvanceStringIndex$1;
var CreateDataPropertyOrThrow = CreateDataPropertyOrThrow$1;
var CreateMethodProperty = CreateMethodProperty$1;
var GetMethod = GetMethod$1;
var IsArray = IsArray$1;
var IterableToList = IterableToList$1;
var OrdinarySetPrototypeOf = OrdinarySetPrototypeOf$1;
var Type = Type$8;
var GetIntrinsic = getIntrinsic;
var getIteratorMethod = getIteratorMethod$1;
var hasPropertyDescriptors = hasPropertyDescriptors_1();
var $Error = GetIntrinsic('%Error%');

// eslint-disable-next-line func-style
function AggregateError$2(errors, message) {
  var error = new $Error(message);
  OrdinarySetPrototypeOf(error, proto); // eslint-disable-line no-use-before-define
  delete error.constructor;
  var errorsList = IterableToList(errors, getIteratorMethod({
    AdvanceStringIndex: AdvanceStringIndex,
    GetMethod: GetMethod,
    IsArray: IsArray,
    Type: Type
  }, errors));
  CreateDataPropertyOrThrow(error, 'errors', errorsList);
  return error;
}
if (hasPropertyDescriptors) {
  Object.defineProperty(AggregateError$2, 'prototype', {
    writable: false
  });
}
var proto = AggregateError$2.prototype;
if (!CreateMethodProperty(proto, 'constructor', AggregateError$2) || !CreateMethodProperty(proto, 'message', '') || !CreateMethodProperty(proto, 'name', 'AggregateError')) {
  throw new $Error('unable to install AggregateError.prototype properties; please report this!');
}
OrdinarySetPrototypeOf(AggregateError$2.prototype, Error.prototype);
var implementation$5 = AggregateError$2;

var implementation$4 = implementation$5;
var polyfill$3 = function getPolyfill() {
  return typeof AggregateError === 'function' ? AggregateError : implementation$4;
};

var implementation$3 = commonjsGlobal;

var implementation$2 = implementation$3;
var polyfill$2 = function getPolyfill() {
  if (_typeof$1(commonjsGlobal) !== 'object' || !commonjsGlobal || commonjsGlobal.Math !== Math || commonjsGlobal.Array !== Array) {
    return implementation$2;
  }
  return commonjsGlobal;
};

var define$2 = defineProperties_1;
var getPolyfill$3 = polyfill$2;
var shim$3 = function shimGlobal() {
  var polyfill = getPolyfill$3();
  if (define$2.supportsDescriptors) {
    var descriptor = Object.getOwnPropertyDescriptor(polyfill, 'globalThis');
    if (!descriptor || descriptor.configurable && (descriptor.enumerable || !descriptor.writable || globalThis !== polyfill)) {
      // eslint-disable-line max-len
      Object.defineProperty(polyfill, 'globalThis', {
        configurable: true,
        enumerable: false,
        value: polyfill,
        writable: true
      });
    }
  } else if ((typeof globalThis === "undefined" ? "undefined" : _typeof$1(globalThis)) !== 'object' || globalThis !== polyfill) {
    polyfill.globalThis = polyfill;
  }
  return polyfill;
};

var defineProperties = defineProperties_1;
var implementation$1 = implementation$3;
var getPolyfill$2 = polyfill$2;
var shim$2 = shim$3;
var polyfill$1 = getPolyfill$2();
var getGlobal = function getGlobal() {
  return polyfill$1;
};
defineProperties(getGlobal, {
  getPolyfill: getPolyfill$2,
  implementation: implementation$1,
  shim: shim$2
});
var globalthis = getGlobal;

var define$1 = defineProperties_1;
var globalThis$1 = globalthis();
var getPolyfill$1 = polyfill$3;
var shim$1 = function shimAggregateError() {
  var polyfill = getPolyfill$1();
  define$1(globalThis$1, {
    AggregateError: polyfill
  }, {
    AggregateError: function testAggregateError() {
      return globalThis$1.AggregateError !== polyfill;
    }
  });
  return polyfill;
};

var bind = functionBind;
var define = defineProperties_1;
var functionsHaveConfigurableNames = functionsHaveNames_1.functionsHaveConfigurableNames();
var implementation = implementation$5;
var getPolyfill = polyfill$3;
var shim = shim$1;
var polyfill = getPolyfill();
var bound = bind.call(polyfill);
if (Object.defineProperty) {
  if (functionsHaveConfigurableNames) {
    Object.defineProperty(bound, 'name', {
      value: polyfill.name
    });
  }
  Object.defineProperty(bound, 'prototype', {
    value: polyfill.prototype
  });
}
define(bound, {
  getPolyfill: getPolyfill,
  implementation: implementation,
  shim: shim
});
var esAggregateError = bound;

/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var AggregateError$1 = esAggregateError;
var AggregateError_1 = AggregateError$1;

/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var primordials = {
  uncurryThis: function () {
    var _Function$prototype = Function.prototype,
      bind = _Function$prototype.bind,
      call = _Function$prototype.call;
    return bind.bind(call);
  }(),
  JSONParse: function JSONParse(self) {
    return JSON.parse(self);
  },
  /**
   * Math start
   */
  MathAbs: function MathAbs(self) {
    return Math.abs(self);
  },
  //typeof Math.abs
  MathAcos: function MathAcos(self) {
    return Math.acos(self);
  },
  // typeof Math.acos
  MathAcosh: function MathAcosh(self) {
    return Math.acosh(self);
  },
  //typeof Math.acosh
  MathAsin: function MathAsin(self) {
    return Math.asin(self);
  },
  //typeof Math.asin
  MathAsinh: function MathAsinh(self) {
    return Math.asinh(self);
  },
  //typeof Math.asinh
  MathAtan: function MathAtan(self) {
    return Math.atan(self);
  },
  //typeof Math.atan
  MathAtanh: function MathAtanh(self) {
    return Math.atanh(self);
  },
  //typeof Math.atanh
  MathAtan2: function MathAtan2(self) {
    return Math.atan2(self);
  },
  //typeof Math.atan2
  MathCeil: function MathCeil(self) {
    return Math.ceil(self);
  },
  //typeof Math.ceil
  MathCbrt: function MathCbrt(self) {
    return Math.cbrt(self);
  },
  //typeof Math.cbrt
  MathExpm1: function MathExpm1(self) {
    return Math.expm1(self);
  },
  //typeof Math.expm1
  MathClz32: function MathClz32(self) {
    return Math.clz32(self);
  },
  //typeof Math.clz32
  MathCos: function MathCos(self) {
    return Math.cos(self);
  },
  //typeof Math.cos
  MathCosh: function MathCosh(self) {
    return Math.cosh(self);
  },
  //typeof Math.cosh
  MathExp: function MathExp(self) {
    return Math.exp(self);
  },
  //typeof Math.exp
  MathFround: function MathFround(self) {
    return Math.fround(self);
  },
  //typeof Math.fround
  MathHypot: function MathHypot(self) {
    return Math.hypot(self);
  },
  //typeof Math.hypot
  MathImul: function MathImul(self) {
    return Math.imul(self);
  },
  //typeof Math.imul
  MathLog: function MathLog(self) {
    return Math.log(self);
  },
  //typeof Math.log
  MathLog1p: function MathLog1p(self) {
    return Math.log(self);
  },
  //typeof Math.log1p
  MathLog2: function MathLog2(self) {
    return Math.log2(self);
  },
  //typeof Math.log2
  MathLog10: function MathLog10(self) {
    return Math.log10(self);
  },
  //typeof Math.log10
  MathMax: function MathMax() {
    return Math.max.apply(Math, arguments);
  },
  //typeof Math.max
  MathMaxApply: function MathMaxApply(self) {
    return Math.max.apply(null, self);
  },
  //StaticApply<typeof Math.max>
  MathMin: function MathMin(self) {
    return Math.min(self);
  },
  //typeof Math.min
  MathPow: function MathPow(self) {
    return Math.pow(self);
  },
  //typeof Math.pow
  MathRandom: function MathRandom() {
    return Math.random();
  },
  //typeof Math.random
  MathRound: function MathRound(self) {
    return Math.round(self);
  },
  //typeof Math.round
  MathSign: function MathSign(self) {
    return Math.sign(self);
  },
  //typeof Math.sign
  MathSin: function MathSin(self) {
    return Math.sin(self);
  },
  //typeof Math.sin
  MathSinh: function MathSinh(self) {
    return Math.sinh(self);
  },
  //typeof Math.sinh
  MathSqrt: function MathSqrt(self) {
    return Math.sqrt(self);
  },
  //typeof Math.sqrt
  MathTan: function MathTan(self) {
    return Math.tan(self);
  },
  //typeof Math.tan
  MathTanh: function MathTanh(self) {
    return Math.tanh(self);
  },
  //typeof Math.tanh
  MathTrunc: function MathTrunc(self) {
    return Math.trunc(self);
  },
  //typeof Math.trunc
  MathE: function MathE() {
    return Math.E;
  },
  //typeof Math.E
  MathLN10: function MathLN10() {
    return Math.LN10;
  },
  //typeof Math.LN10
  MathLN2: function MathLN2() {
    return Math.LN2;
  },
  //typeof Math.LN2
  MathLOG10E: function MathLOG10E() {
    return Math.LOG10E;
  },
  //typeof Math.LOG10E
  MathLOG2E: function MathLOG2E() {
    return Math.LOG2E;
  },
  //typeof Math.LOG2E
  MathPI: function MathPI() {
    return Math.PI;
  },
  //typeof Math.PI
  MathSQRT1_2: function MathSQRT1_2() {
    return Math.SQRT1_2;
  },
  //typeof Math.SQRT1_2
  MathSQRT2: function MathSQRT2() {
    return Math.SQRT2;
  },
  //typeof Math.SQRT2

  /**
   * Math end
   */

  /**
   * Reflect start
   */
  ReflectDefineProperty: Reflect.defineProperty,
  //typeof Reflect.defineProperty
  ReflectDeleteProperty: Reflect.deleteProperty,
  // typeof Reflect.deleteProperty
  ReflectApply: Reflect.apply,
  ReflectConstruct: Reflect.construct,
  // typeof Reflect.construct
  ReflectGet: Reflect.get,
  // typeof Reflect.get
  ReflectGetOwnPropertyDescriptor: Reflect.getOwnPropertyDescriptor,
  // typeof Reflect.getOwnPropertyDescriptor
  ReflectGetPrototypeOf: Reflect.getPrototypeOf,
  // typeof Reflect.getPrototypeOf
  ReflectHas: Reflect.has,
  // typeof Reflect.has
  ReflectIsExtensible: Reflect.isExtensible,
  // typeof Reflect.isExtensible
  ReflectOwnKeys: Reflect.ownKeys,
  // typeof Reflect.ownKeys
  ReflectPreventExtensions: Reflect.preventExtensions,
  // typeof Reflect.preventExtensions
  ReflectSet: Reflect.set,
  //typeof Reflect.set
  ReflectSetPrototypeOf: Reflect.setPrototypeOf,
  // typeof Reflect.setPrototypeOf
  /**
   * Reflect end
   */

  AggregateError: AggregateError_1,
  /**
   * Array start
   */
  ArrayFrom: function ArrayFrom(self, fn) {
    return Array.from(self, fn);
  },
  ArrayIsArray: function ArrayIsArray(self) {
    return Array.isArray(self);
  },
  ArrayPrototypeIncludes: function ArrayPrototypeIncludes(self, el) {
    return self.includes(el);
  },
  ArrayPrototypeFilter: function ArrayPrototypeFilter(self, fn) {
    return self.filter(fn);
  },
  ArrayPrototypeIndexOf: function ArrayPrototypeIndexOf(self, el) {
    return self.indexOf(el);
  },
  ArrayPrototypeJoin: function ArrayPrototypeJoin(self, sep) {
    return self.join(sep);
  },
  ArrayPrototypeMap: function ArrayPrototypeMap(self, fn) {
    return self.map(fn);
  },
  ArrayPrototypePop: function ArrayPrototypePop(self, el) {
    return self.pop(el);
  },
  ArrayPrototypePush: function ArrayPrototypePush(self, el) {
    return self.push(el);
  },
  ArrayPrototypeSlice: function ArrayPrototypeSlice(self, start, end) {
    return self.slice(start, end);
  },
  ArrayPrototypeSplice: function ArrayPrototypeSplice(self, start, end) {
    for (var _len = arguments.length, args = new Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {
      args[_key - 3] = arguments[_key];
    }
    return self.splice.apply(self, [start, end].concat(args));
  },
  ArrayPrototypeUnshift: function ArrayPrototypeUnshift(self, value) {
    return self.unshift(value);
  },
  /**
   * Array end
   */

  /**
   * Map start
   */

  MapPrototypeGet: Map.prototype.get,
  /**
   * Map end
   */
  /**
   * Error start
   */
  Error: Error,
  ErrorCaptureStackTrace: Error.captureStackTrace,
  ErrorPrototypeToString: Error.prototype.toString,
  RangeError: RangeError,
  /**
   * Error end
   */

  /**
   * JSON start
   */
  JSONStringify: JSON.stringify,
  /**
   * JSON end
   */
  FunctionPrototypeCall: function FunctionPrototypeCall(fn, thisArgs) {
    for (var _len2 = arguments.length, args = new Array(_len2 > 2 ? _len2 - 2 : 0), _key2 = 2; _key2 < _len2; _key2++) {
      args[_key2 - 2] = arguments[_key2];
    }
    return fn.call.apply(fn, [thisArgs].concat(args));
  },
  FunctionPrototypeBind: function FunctionPrototypeBind(fn, thisArgs) {
    for (var _len3 = arguments.length, args = new Array(_len3 > 2 ? _len3 - 2 : 0), _key3 = 2; _key3 < _len3; _key3++) {
      args[_key3 - 2] = arguments[_key3];
    }
    return fn.bind.apply(fn, [thisArgs].concat(args));
  },
  FunctionPrototypeSymbolHasInstance: function FunctionPrototypeSymbolHasInstance(self, instance) {
    return Function.prototype[Symbol.hasInstance].call(self, instance);
  },
  MathFloor: Math.floor,
  Number: Number,
  NumberIsInteger: Number.isInteger,
  NumberIsNaN: Number.isNaN,
  NumberMAX_SAFE_INTEGER: Number.MAX_SAFE_INTEGER,
  NumberMIN_SAFE_INTEGER: Number.MIN_SAFE_INTEGER,
  NumberParseInt: Number.parseInt,
  NumberIsFinite: Number.isFinite,
  NumberPrototypeToString: function NumberPrototypeToString(value, radix) {
    return value.toString(radix);
  },
  /**
   * Object start
   */
  ObjectPrototypeHasOwnProperty: function ObjectPrototypeHasOwnProperty(self, name) {
    return Object.prototype.hasOwnProperty.call(self, name);
  },
  ObjectAssign: Object.assign,
  ObjectDefineProperties: function ObjectDefineProperties(self, props) {
    return Object.defineProperties(self, props);
  },
  ObjectDefineProperty: function ObjectDefineProperty(self, name, prop) {
    return Object.defineProperty(self, name, prop);
  },
  ObjectGetOwnPropertyDescriptor: function ObjectGetOwnPropertyDescriptor(self, name) {
    return Object.getOwnPropertyDescriptor(self, name);
  },
  ObjectKeys: function ObjectKeys(obj) {
    return Object.keys(obj);
  },
  ObjectCreate: function ObjectCreate(obj) {
    return Object.create(obj);
  },
  ObjectFreeze: function ObjectFreeze(obj) {
    return Object.freeze(obj);
  },
  ObjectEntries: function ObjectEntries(obj) {
    return Object.entries(obj);
  },
  ObjectSetPrototypeOf: function ObjectSetPrototypeOf(target, proto) {
    return Object.setPrototypeOf(target, proto);
  },
  ObjectPrototypeToString: function ObjectPrototypeToString(obj) {
    return obj.toString();
  },
  ObjectPrototypePropertyIsEnumerable: function ObjectPrototypePropertyIsEnumerable(self, val) {
    return self.propertyIsEnumerable(val);
  },
  ObjectIsExtensible: Object.isExtensible,
  /**
   * Object end
   */
  Promise: Promise,
  PromisePrototypeCatch: function PromisePrototypeCatch(self, fn) {
    return self["catch"](fn);
  },
  PromisePrototypeThen: function PromisePrototypeThen(self, thenFn, catchFn) {
    return self.then(thenFn, catchFn);
  },
  PromiseReject: function PromiseReject(err) {
    return Promise.reject(err);
  },
  RegExpPrototypeTest: function RegExpPrototypeTest(self, value) {
    return self.test(value);
  },
  SafeSet: Set,
  String: String,
  StringPrototypeSlice: function StringPrototypeSlice(self, start, end) {
    return self.slice(start, end);
  },
  StringPrototypeToLowerCase: function StringPrototypeToLowerCase(self) {
    return self.toLowerCase();
  },
  StringPrototypeToUpperCase: function StringPrototypeToUpperCase(self) {
    return self.toUpperCase();
  },
  StringPrototypeTrim: function StringPrototypeTrim(self) {
    return self.trim();
  },
  StringPrototypeCharCodeAt: function StringPrototypeCharCodeAt(value, index) {
    return value.charCodeAt(index);
  },
  StringPrototypeLastIndexOf: function StringPrototypeLastIndexOf(value, separator) {
    return value.lastIndexOf(separator);
  },
  StringPrototypeCharAt: function StringPrototypeCharAt(value, index) {
    return value.charAt(index);
  },
  StringPrototypeIndexOf: function StringPrototypeIndexOf(value, index) {
    return value.indexOf(index);
  },
  StringPrototypeStartsWith: function StringPrototypeStartsWith(value, index) {
    return value.startsWith(index);
  },
  StringPrototypeIncludes: function StringPrototypeIncludes(self, value, start) {
    return self.includes(value, start);
  },
  StringPrototypePadStart: function StringPrototypePadStart(self, targetLength, padString) {
    return self.padStart(targetLength, padString);
  },
  StringPrototypeReplace: function StringPrototypeReplace(self, searchValue, replaceValue) {
    return self.replace(searchValue, replaceValue);
  },
  DatePrototypeGetDate: function DatePrototypeGetDate(date) {
    return date.getDate();
  },
  DatePrototypeGetHours: function DatePrototypeGetHours(date) {
    return date.getHours();
  },
  DatePrototypeGetMinutes: function DatePrototypeGetMinutes(date) {
    return date.getMinutes();
  },
  DatePrototypeGetMonth: function DatePrototypeGetMonth(date) {
    return date.getMonth();
  },
  DatePrototypeGetSeconds: function DatePrototypeGetSeconds(date) {
    return date.getSeconds();
  },
  Symbol: Symbol,
  SymbolAsyncIterator: Symbol.asyncIterator,
  SymbolHasInstance: Symbol.hasInstance,
  SymbolIterator: Symbol.iterator,
  TypedArrayPrototypeSet: function TypedArrayPrototypeSet(self, buf, len) {
    return self.set(buf, len);
  },
  decodeURIComponent: decodeURIComponent,
  Uint8Array: Uint8Array,
  Int8Array: Int8Array,
  Array: Array,
  Date: Date
};

var Array$2 = primordials.Array,
  Int8Array$3 = primordials.Int8Array,
  NumberPrototypeToString = primordials.NumberPrototypeToString,
  StringPrototypeCharCodeAt$4 = primordials.StringPrototypeCharCodeAt,
  StringPrototypeSlice$3 = primordials.StringPrototypeSlice,
  StringPrototypeToUpperCase = primordials.StringPrototypeToUpperCase;

//const { ERR_INVALID_URI } = require('internal/errors').codes;

var hexTable$2 = new Array$2(256);
for (var i$1 = 0; i$1 < 256; ++i$1) hexTable$2[i$1] = "%" + StringPrototypeToUpperCase((i$1 < 16 ? "0" : "") + NumberPrototypeToString(i$1, 16));
var isHexTable$1 = new Int8Array$3([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
// 0 - 15
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
// 16 - 31
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
// 32 - 47
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
// 48 - 63
0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
// 64 - 79
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
// 80 - 95
0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
// 96 - 111
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
// 112 - 127
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
// 128 ...
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 // ... 256
]);

/**
 * @param {string} str
 * @param {Int8Array} noEscapeTable
 * @param {string[]} hexTable
 * @returns {string}
 */
function encodeStr$3(str, noEscapeTable, hexTable) {
  var len = str.length;
  if (len === 0) return "";
  var out = "";
  var lastPos = 0;
  var i = 0;
  outer: for (; i < len; i++) {
    var c = StringPrototypeCharCodeAt$4(str, i);

    // ASCII
    while (c < 0x80) {
      if (noEscapeTable[c] !== 1) {
        if (lastPos < i) out += StringPrototypeSlice$3(str, lastPos, i);
        lastPos = i + 1;
        out += hexTable[c];
      }
      if (++i === len) break outer;
      c = StringPrototypeCharCodeAt$4(str, i);
    }
    if (lastPos < i) out += StringPrototypeSlice$3(str, lastPos, i);

    // Multi-byte characters ...
    if (c < 0x800) {
      lastPos = i + 1;
      out += hexTable[0xc0 | c >> 6] + hexTable[0x80 | c & 0x3f];
      continue;
    }
    if (c < 0xd800 || c >= 0xe000) {
      lastPos = i + 1;
      out += hexTable[0xe0 | c >> 12] + hexTable[0x80 | c >> 6 & 0x3f] + hexTable[0x80 | c & 0x3f];
      continue;
    }
    // Surrogate pair
    ++i;

    // This branch should never happen because all URLSearchParams entries
    // should already be converted to USVString. But, included for
    // completion's sake anyway.
    if (i >= len)
      //throw new ERR_INVALID_URI();
      throw new Error("ERR_INVALID_URI");
    var c2 = StringPrototypeCharCodeAt$4(str, i) & 0x3ff;
    lastPos = i + 1;
    c = 0x10000 + ((c & 0x3ff) << 10 | c2);
    out += hexTable[0xf0 | c >> 18] + hexTable[0x80 | c >> 12 & 0x3f] + hexTable[0x80 | c >> 6 & 0x3f] + hexTable[0x80 | c & 0x3f];
  }
  if (lastPos === 0) return str;
  if (lastPos < len) return out + StringPrototypeSlice$3(str, lastPos);
  return out;
}
var querystring$2 = {
  encodeStr: encodeStr$3,
  hexTable: hexTable$2,
  isHexTable: isHexTable$1
};
index$2.Buffer;

var validator = {};

var nodeInternalPrefix = "__node_internal_";
var ObjectDefineProperty = primordials.ObjectDefineProperty,
  ArrayIsArray = primordials.ArrayIsArray,
  ArrayPrototypeIncludes = primordials.ArrayPrototypeIncludes,
  NumberIsNaN$1 = primordials.NumberIsNaN;
var hideStackFrames = function hideStackFrames(fn) {
  // We rename the functions that will be hidden to cut off the stacktrace
  // at the outermost one
  var hidden = nodeInternalPrefix + fn.name;
  ObjectDefineProperty(fn, "name", {
    __proto__: null,
    value: hidden
  });
  return fn;
};
validator.validateString = hideStackFrames(function (value, name) {
  if (typeof value !== "string") {
    throw new Error("ERR_INVALID_ARG_TYPE value:" + value + " name:" + name);
  }
});
validator.validateFunction = hideStackFrames(function (value, name) {
  if (typeof value !== "function") throw new Error("ERR_INVALID_ARG_TYPE value:" + value + " name:" + name);
});
validator.validateAbortSignal = hideStackFrames(function (signal, name) {
  if (signal !== undefined && (signal === null || _typeof$1(signal) !== "object" || !("aborted" in signal))) {
    throw new Error("ERR_INVALID_ARG_TYPE value:" + value + " name:" + name);
  }
});
validator.validateObject = hideStackFrames(function (value, name, options) {
  var useDefaultOptions = options == null;
  var allowArray = useDefaultOptions ? false : options.allowArray;
  var allowFunction = useDefaultOptions ? false : options.allowFunction;
  var nullable = useDefaultOptions ? false : options.nullable;
  if (!nullable && value === null || !allowArray && ArrayIsArray(value) || _typeof$1(value) !== "object" && (!allowFunction || typeof value !== "function")) {
    throw new Error("ERR_INVALID_ARG_TYPE value:" + value + " name:" + name);
  }
});
validator.validateNumber = function validateNumber(value, name) {
  var min = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : undefined;
  var max = arguments.length > 3 ? arguments[3] : undefined;
  if (typeof value !== "number") throw new Error("ERR_INVALID_ARG_TYPE value:" + value + " name:" + name);
  if (min != null && value < min || max != null && value > max || (min != null || max != null) && NumberIsNaN$1(value)) {
    throw new Error("ERR_OUT_OF_RANGE, name:" + name + ", " + "".concat(min != null ? ">= ".concat(min) : "").concat(min != null && max != null ? " && " : "").concat(max != null ? "<= ".concat(max) : "") + value);
  }
};
validator.validateBoolean = function validateBoolean(value, name) {
  if (typeof value !== "boolean") throw new Error("ERR_INVALID_ARG_TYPE value:" + value + " name:" + name);
};
validator.validateArray = hideStackFrames(function (value, name) {
  var minLength = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
  if (!Array.isArray(value)) {
    throw new Error("Array:" + name);
  }
  if (value.length < minLength) {
    var reason = "must be longer than ".concat(minLength);
    throw new Error("ERR_INVALID_ARG_VALUE name:" + name + ",value:" + value + ",reason:" + reason);
  }
});
validator.validateUnion = function validateUnion(value, name, union) {
  if (!ArrayPrototypeIncludes(union, value)) {
    throw new Error("ERR_INVALID_ARG_TYPE, name:" + name + ",union:" + union + ",value:" + value);
  }
};

var constants$1 = {
  // Alphabet chars.
  CHAR_UPPERCASE_A: 65 /* A */,
  CHAR_LOWERCASE_A: 97 /* a */,
  CHAR_UPPERCASE_Z: 90 /* Z */,
  CHAR_LOWERCASE_Z: 122 /* z */,
  CHAR_UPPERCASE_C: 67 /* C */,
  CHAR_LOWERCASE_B: 98 /* b */,
  CHAR_LOWERCASE_E: 101 /* e */,
  CHAR_LOWERCASE_N: 110 /* n */,

  // Non-alphabetic chars.
  CHAR_DOT: 46 /* . */,
  CHAR_FORWARD_SLASH: 47 /* / */,
  CHAR_BACKWARD_SLASH: 92 /* \ */,
  CHAR_VERTICAL_LINE: 124 /* | */,
  CHAR_COLON: 58 /* : */,
  CHAR_QUESTION_MARK: 63 /* ? */,
  CHAR_UNDERSCORE: 95 /* _ */,
  CHAR_LINE_FEED: 10 /* \n */,
  CHAR_CARRIAGE_RETURN: 13 /* \r */,
  CHAR_TAB: 9 /* \t */,
  CHAR_FORM_FEED: 12 /* \f */,
  CHAR_EXCLAMATION_MARK: 33 /* ! */,
  CHAR_HASH: 35 /* # */,
  CHAR_SPACE: 32 /*   */,
  CHAR_NO_BREAK_SPACE: 160 /* \u00A0 */,
  CHAR_ZERO_WIDTH_NOBREAK_SPACE: 65279 /* \uFEFF */,
  CHAR_LEFT_SQUARE_BRACKET: 91,
  //* [ */
  CHAR_RIGHT_SQUARE_BRACKET: 93 /* ] */,
  CHAR_LEFT_ANGLE_BRACKET: 60 /* < */,
  CHAR_RIGHT_ANGLE_BRACKET: 62 /* > */,
  CHAR_LEFT_CURLY_BRACKET: 123 /* { */,
  CHAR_RIGHT_CURLY_BRACKET: 125 /* } */,
  CHAR_HYPHEN_MINUS: 45 /* - */,
  CHAR_PLUS: 43 /* + */,
  CHAR_DOUBLE_QUOTE: 34 /* " */,
  CHAR_SINGLE_QUOTE: 39 /* ' */,
  CHAR_PERCENT: 37 /* % */,
  CHAR_SEMICOLON: 59 /* ; */,
  CHAR_CIRCUMFLEX_ACCENT: 94 /* ^ */,
  CHAR_GRAVE_ACCENT: 96 /* ` */,
  CHAR_AT: 64 /* @ */,
  CHAR_AMPERSAND: 38 /* & */,
  CHAR_EQUAL: 61 /* = */,

  // Digits
  CHAR_0: 48 /* 0 */,
  CHAR_9: 57 /* 9 */,

  EOL: "\n"
};

// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
var constants = constants$1;
var StringPrototypeCharCodeAt$2 = primordials.StringPrototypeCharCodeAt,
  StringPrototypeSlice$1 = primordials.StringPrototypeSlice,
  StringPrototypeLastIndexOf = primordials.StringPrototypeLastIndexOf,
  FunctionPrototypeBind = primordials.FunctionPrototypeBind;
var validateString$1 = validator.validateString,
  validateObject$1 = validator.validateObject;
var sep = "/";
function isPathSeparator(code) {
  return code === constants.CHAR_FORWARD_SLASH;
}
function _format(sep, pathObject) {
  validateObject$1(pathObject, "pathObject");
  var dir = pathObject.dir || pathObject.root;
  var base = pathObject.base || "".concat(pathObject.name || "").concat(pathObject.ext || "");
  if (!dir) {
    return base;
  }
  return dir === pathObject.root ? "".concat(dir).concat(base) : "".concat(dir).concat(sep).concat(base);
}
function basename(path, ext) {
  if (ext !== undefined) validateString$1(ext, "ext");
  validateString$1(path, "path");
  var start = 0;
  var end = -1;
  var matchedSlash = true;
  if (ext !== undefined && ext.length > 0 && ext.length <= path.length) {
    if (ext === path) return "";
    var extIdx = ext.length - 1;
    var firstNonSlashEnd = -1;
    for (var i = path.length - 1; i >= start; --i) {
      var code = StringPrototypeCharCodeAt$2(path, i);
      if (isPathSeparator(code)) {
        // If we reached a path separator that was not part of a set of path
        // separators at the end of the string, stop now
        if (!matchedSlash) {
          start = i + 1;
          break;
        }
      } else {
        if (firstNonSlashEnd === -1) {
          // We saw the first non-path separator, remember this index in case
          // we need it if the extension ends up not matching
          matchedSlash = false;
          firstNonSlashEnd = i + 1;
        }
        if (extIdx >= 0) {
          // Try to match the explicit extension
          if (code === StringPrototypeCharCodeAt$2(ext, extIdx)) {
            if (--extIdx === -1) {
              // We matched the extension, so mark this as the end of our path
              // component
              end = i;
            }
          } else {
            // Extension does not match, so our result is the entire path
            // component
            extIdx = -1;
            end = firstNonSlashEnd;
          }
        }
      }
    }
    if (start === end) end = firstNonSlashEnd;else if (end === -1) end = path.length;
    return StringPrototypeSlice$1(path, start, end);
  }
  for (var _i = path.length - 1; _i >= start; --_i) {
    if (isPathSeparator(StringPrototypeCharCodeAt$2(path, _i))) {
      // If we reached a path separator that was not part of a set of path
      // separators at the end of the string, stop now
      if (!matchedSlash) {
        start = _i + 1;
        break;
      }
    } else if (end === -1) {
      // We saw the first non-path separator, mark this as the end of our
      // path component
      matchedSlash = false;
      end = _i + 1;
    }
  }
  if (end === -1) return "";
  return StringPrototypeSlice$1(path, start, end);
}
function dirname(path) {
  validateString$1(path, "path");
  var len = path.length;
  if (len === 0) return ".";
  var rootEnd = -1;
  var offset = 0;
  var code = StringPrototypeCharCodeAt$2(path, 0);
  if (len === 1) {
    // `path` contains just a path separator, exit early to avoid
    // unnecessary work or a dot.
    return isPathSeparator(code) ? path : ".";
  }

  // Try to match a root
  if (isPathSeparator(code)) {
    // Possible UNC root

    rootEnd = offset = 1;
    if (isPathSeparator(StringPrototypeCharCodeAt$2(path, 1))) {
      // Matched double path separator at beginning
      var j = 2;
      var last = j;
      // Match 1 or more non-path separators
      while (j < len && !isPathSeparator(StringPrototypeCharCodeAt$2(path, j))) {
        j++;
      }
      if (j < len && j !== last) {
        // Matched!
        last = j;
        // Match 1 or more path separators
        while (j < len && isPathSeparator(StringPrototypeCharCodeAt$2(path, j))) {
          j++;
        }
        if (j < len && j !== last) {
          // Matched!
          last = j;
          // Match 1 or more non-path separators
          while (j < len && !isPathSeparator(StringPrototypeCharCodeAt$2(path, j))) {
            j++;
          }
          if (j === len) {
            // We matched a UNC root only
            return path;
          }
          if (j !== last) {
            // We matched a UNC root with leftovers

            // Offset by 1 to include the separator after the UNC root to
            // treat it as a "normal root" on top of a (UNC) root
            rootEnd = offset = j + 1;
          }
        }
      }
    }
    // Possible device root
  }

  var end = -1;
  var matchedSlash = true;
  for (var i = len - 1; i >= offset; --i) {
    if (isPathSeparator(StringPrototypeCharCodeAt$2(path, i))) {
      if (!matchedSlash) {
        end = i;
        break;
      }
    } else {
      // We saw the first non-path separator
      matchedSlash = false;
    }
  }
  if (end === -1) {
    if (rootEnd === -1) return ".";
    end = rootEnd;
  }
  return StringPrototypeSlice$1(path, 0, end);
}
function join() {
  if (arguments.length === 0) return ".";
  var joined;
  for (var i = 0; i < arguments.length; ++i) {
    var arg = i < 0 || arguments.length <= i ? undefined : arguments[i];
    validateString$1(arg, "path");
    if (arg.length > 0) {
      if (joined === undefined) joined = arg;else joined += "/".concat(arg);
    }
  }
  if (joined === undefined) return ".";
  return normalize(joined);
}
function normalize(path) {
  validateString$1(path, "path");
  if (path.length === 0) return ".";
  var isAbsolute = StringPrototypeCharCodeAt$2(path, 0) === constants.CHAR_FORWARD_SLASH;
  var trailingSeparator = StringPrototypeCharCodeAt$2(path, path.length - 1) === constants.CHAR_FORWARD_SLASH;

  // Normalize the path
  path = normalizeString(path, !isAbsolute, "/");
  if (path.length === 0) {
    if (isAbsolute) return "/";
    return trailingSeparator ? "./" : ".";
  }
  if (trailingSeparator) path += "/";
  return isAbsolute ? "/".concat(path) : path;
}
function normalizeString(path, allowAboveRoot, separator) {
  var res = "";
  var lastSegmentLength = 0;
  var lastSlash = -1;
  var dots = 0;
  var code = 0;
  for (var i = 0; i <= path.length; ++i) {
    if (i < path.length) code = StringPrototypeCharCodeAt$2(path, i);else if (isPathSeparator(code)) break;else code = constants.CHAR_FORWARD_SLASH;
    if (isPathSeparator(code)) {
      if (lastSlash === i - 1 || dots === 1) ; else if (dots === 2) {
        if (res.length < 2 || lastSegmentLength !== 2 || StringPrototypeCharCodeAt$2(res, res.length - 1) !== constants.CHAR_DOT || StringPrototypeCharCodeAt$2(res, res.length - 2) !== constants.CHAR_DOT) {
          if (res.length > 2) {
            var lastSlashIndex = StringPrototypeLastIndexOf(res, separator);
            if (lastSlashIndex === -1) {
              res = "";
              lastSegmentLength = 0;
            } else {
              res = StringPrototypeSlice$1(res, 0, lastSlashIndex);
              lastSegmentLength = res.length - 1 - StringPrototypeLastIndexOf(res, separator);
            }
            lastSlash = i;
            dots = 0;
            continue;
          } else if (res.length !== 0) {
            res = "";
            lastSegmentLength = 0;
            lastSlash = i;
            dots = 0;
            continue;
          }
        }
        if (allowAboveRoot) {
          res += res.length > 0 ? "".concat(separator, "..") : "..";
          lastSegmentLength = 2;
        }
      } else {
        if (res.length > 0) res += "".concat(separator).concat(StringPrototypeSlice$1(path, lastSlash + 1, i));else res = StringPrototypeSlice$1(path, lastSlash + 1, i);
        lastSegmentLength = i - lastSlash - 1;
      }
      lastSlash = i;
      dots = 0;
    } else if (code === constants.CHAR_DOT && dots !== -1) {
      ++dots;
    } else {
      dots = -1;
    }
  }
  return res;
}
function resolve$1() {
  var resolvedPath = "";
  var resolvedAbsolute = false;
  for (var i = arguments.length - 1; i >= -1 && !resolvedAbsolute; i--) {
    var _path = i >= 0 ? i < 0 || arguments.length <= i ? undefined : arguments[i] : sep;
    validateString$1(_path, "path");

    // Skip empty entries
    if (_path.length === 0) {
      continue;
    }
    resolvedPath = "".concat(_path, "/").concat(resolvedPath);
    resolvedAbsolute = StringPrototypeCharCodeAt$2(_path, 0) === constants.CHAR_FORWARD_SLASH;
  }

  // At this point the path should be resolved to a full absolute path, but
  // handle relative paths to be safe (might happen when process.cwd() fails)

  // Normalize the path
  resolvedPath = normalizeString(resolvedPath, !resolvedAbsolute, "/");
  if (resolvedAbsolute) {
    return "/".concat(resolvedPath);
  }
  return resolvedPath.length > 0 ? resolvedPath : ".";
}
function isAbsolute(path) {
  validateString$1(path, "path");
  return path.length > 0 && StringPrototypeCharCodeAt$2(path, 0) === constants.CHAR_FORWARD_SLASH;
}
function relative(from, to) {
  validateString$1(from, "from");
  validateString$1(to, "to");
  if (from === to) return "";

  // Trim leading forward slashes.
  from = resolve$1(from);
  to = resolve$1(to);
  if (from === to) return "";
  var fromStart = 1;
  var fromEnd = from.length;
  var fromLen = fromEnd - fromStart;
  var toStart = 1;
  var toLen = to.length - toStart;

  // Compare paths to find the longest common path from root
  var length = fromLen < toLen ? fromLen : toLen;
  var lastCommonSep = -1;
  var i = 0;
  for (; i < length; i++) {
    var fromCode = StringPrototypeCharCodeAt$2(from, fromStart + i);
    if (fromCode !== StringPrototypeCharCodeAt$2(to, toStart + i)) break;else if (fromCode === constants.CHAR_FORWARD_SLASH) lastCommonSep = i;
  }
  if (i === length) {
    if (toLen > length) {
      if (StringPrototypeCharCodeAt$2(to, toStart + i) === constants.CHAR_FORWARD_SLASH) {
        // We get here if `from` is the exact base path for `to`.
        // For example: from='/foo/bar'; to='/foo/bar/baz'
        return StringPrototypeSlice$1(to, toStart + i + 1);
      }
      if (i === 0) {
        // We get here if `from` is the root
        // For example: from='/'; to='/foo'
        return StringPrototypeSlice$1(to, toStart + i);
      }
    } else if (fromLen > length) {
      if (StringPrototypeCharCodeAt$2(from, fromStart + i) === constants.CHAR_FORWARD_SLASH) {
        // We get here if `to` is the exact base path for `from`.
        // For example: from='/foo/bar/baz'; to='/foo/bar'
        lastCommonSep = i;
      } else if (i === 0) {
        // We get here if `to` is the root.
        // For example: from='/foo/bar'; to='/'
        lastCommonSep = 0;
      }
    }
  }
  var out = "";
  // Generate the relative path based on the path difference between `to`
  // and `from`.
  for (i = fromStart + lastCommonSep + 1; i <= fromEnd; ++i) {
    if (i === fromEnd || StringPrototypeCharCodeAt$2(from, i) === constants.CHAR_FORWARD_SLASH) {
      out += out.length === 0 ? ".." : "/..";
    }
  }

  // Lastly, append the rest of the destination (`to`) path that comes after
  // the common path parts.
  return "".concat(out).concat(StringPrototypeSlice$1(to, toStart + lastCommonSep));
}
function extname(path) {
  validateString$1(path, "path");
  var startDot = -1;
  var startPart = 0;
  var end = -1;
  var matchedSlash = true;
  // Track the state of characters (if any) we see before our first dot and
  // after any path separator we find
  var preDotState = 0;
  for (var i = path.length - 1; i >= 0; --i) {
    path.charcode;
    var code = StringPrototypeCharCodeAt$2(path, i);
    if (code === constants.CHAR_FORWARD_SLASH) {
      // If we reached a path separator that was not part of a set of path
      // separators at the end of the string, stop now
      if (!matchedSlash) {
        startPart = i + 1;
        break;
      }
      continue;
    }
    if (end === -1) {
      // We saw the first non-path separator, mark this as the end of our
      // extension
      matchedSlash = false;
      end = i + 1;
    }
    if (code === constants.CHAR_DOT) {
      // If this is our first dot, mark it as the start of our extension
      if (startDot === -1) startDot = i;else if (preDotState !== 1) preDotState = 1;
    } else if (startDot !== -1) {
      // We saw a non-dot and non-path separator before our dot, so we should
      // have a good chance at having a non-empty extension
      preDotState = -1;
    }
  }
  if (startDot === -1 || end === -1 ||
  // We saw a non-dot character immediately before the dot
  preDotState === 0 ||
  // The (right-most) trimmed path component is exactly '..'
  preDotState === 1 && startDot === end - 1 && startDot === startPart + 1) {
    return "";
  }
  return StringPrototypeSlice$1(path, startDot, end);
}
var format$1 = FunctionPrototypeBind(_format, null, "/");
function parse$1(path) {
  validateString$1(path, "path");
  var ret = {
    root: "",
    dir: "",
    base: "",
    ext: "",
    name: ""
  };
  if (path.length === 0) return ret;
  var isAbsolute = StringPrototypeCharCodeAt$2(path, 0) === constants.CHAR_FORWARD_SLASH;
  var start;
  if (isAbsolute) {
    ret.root = "/";
    start = 1;
  } else {
    start = 0;
  }
  var startDot = -1;
  var startPart = 0;
  var end = -1;
  var matchedSlash = true;
  var i = path.length - 1;

  // Track the state of characters (if any) we see before our first dot and
  // after any path separator we find
  var preDotState = 0;

  // Get non-dir info
  for (; i >= start; --i) {
    var code = StringPrototypeCharCodeAt$2(path, i);
    if (code === constants.CHAR_FORWARD_SLASH) {
      // If we reached a path separator that was not part of a set of path
      // separators at the end of the string, stop now
      if (!matchedSlash) {
        startPart = i + 1;
        break;
      }
      continue;
    }
    if (end === -1) {
      // We saw the first non-path separator, mark this as the end of our
      // extension
      matchedSlash = false;
      end = i + 1;
    }
    if (code === constants.CHAR_DOT) {
      // If this is our first dot, mark it as the start of our extension
      if (startDot === -1) startDot = i;else if (preDotState !== 1) preDotState = 1;
    } else if (startDot !== -1) {
      // We saw a non-dot and non-path separator before our dot, so we should
      // have a good chance at having a non-empty extension
      preDotState = -1;
    }
  }
  if (end !== -1) {
    var _start = startPart === 0 && isAbsolute ? 1 : startPart;
    if (startDot === -1 ||
    // We saw a non-dot character immediately before the dot
    preDotState === 0 ||
    // The (right-most) trimmed path component is exactly '..'
    preDotState === 1 && startDot === end - 1 && startDot === startPart + 1) {
      ret.base = ret.name = StringPrototypeSlice$1(path, _start, end);
    } else {
      ret.name = StringPrototypeSlice$1(path, _start, startDot);
      ret.base = StringPrototypeSlice$1(path, _start, end);
      ret.ext = StringPrototypeSlice$1(path, startDot, end);
    }
  }
  if (startPart > 0) ret.dir = StringPrototypeSlice$1(path, 0, startPart - 1);else if (isAbsolute) ret.dir = "/";
  return ret;
}
var path$1 = {
  basename: basename,
  dirname: dirname,
  extname: extname,
  format: format$1,
  parse: parse$1,
  sep: sep,
  join: join,
  resolve: resolve$1,
  isAbsolute: isAbsolute,
  relative: relative,
  normalize: normalize
};

var utf16 = {
  decode: function decode(input) {
    var output = [],
      i = 0,
      len = input.length,
      value,
      extra;
    while (i < len) {
      value = input.charCodeAt(i++);
      if ((value & 0xF800) === 0xD800) {
        extra = input.charCodeAt(i++);
        if ((value & 0xFC00) !== 0xD800 || (extra & 0xFC00) !== 0xDC00) {
          throw new RangeError("UTF-16(decode): Illegal UTF-16 sequence");
        }
        value = ((value & 0x3FF) << 10) + (extra & 0x3FF) + 0x10000;
      }
      output.push(value);
    }
    return output;
  },
  encode: function encode(input) {
    var output = [],
      i = 0,
      len = input.length,
      value;
    while (i < len) {
      value = input[i++];
      if ((value & 0xF800) === 0xD800) {
        throw new RangeError("UTF-16(encode): Illegal UTF-16 value");
      }
      if (value > 0xFFFF) {
        value -= 0x10000;
        output.push(String.fromCharCode(value >>> 10 & 0x3FF | 0xD800));
        value = 0xDC00 | value & 0x3FF;
      }
      output.push(String.fromCharCode(value));
    }
    return output.join("");
  }
};
var initial_n = 0x80;
var initial_bias = 72;
var delimiter = "-";
var base = 36;
var damp = 700;
var tmin = 1;
var tmax = 26;
var skew = 38;
var maxint = 0x7FFFFFFF;
function decode_digit(cp) {
  return cp - 48 < 10 ? cp - 22 : cp - 65 < 26 ? cp - 65 : cp - 97 < 26 ? cp - 97 : base;
}
function encode_digit(d, flag) {
  return d + 22 + Number(75 * Number(d < 26)) - (Number(flag != 0) << 5);
}
function adapt(delta, numpoints, firsttime) {
  var k;
  delta = firsttime ? Math.floor(delta / damp) : delta >> 1;
  delta += Math.floor(delta / numpoints);
  for (k = 0; delta > (base - tmin) * tmax >> 1; k += base) {
    delta = Math.floor(delta / (base - tmin));
  }
  return Math.floor(k + (base - tmin + 1) * delta / (delta + skew));
}
function encode_basic(bcp, flag) {
  bcp -= Number(bcp - 97 < 26) << 5;
  return bcp + ((!flag && Number(bcp - 65 < 26)) << 5);
}
function decode$1(input, preserveCase) {
  var output = [];
  var case_flags = [];
  var input_length = input.length;
  var n, out, i, bias, basic, j, ic, oldi, w, k, digit, t, len;
  n = initial_n;
  i = 0;
  bias = initial_bias;
  basic = input.lastIndexOf(delimiter);
  if (basic < 0) basic = 0;
  for (j = 0; j < basic; ++j) {
    if (preserveCase) case_flags[output.length] = input.charCodeAt(j) - 65 < 26;
    if (input.charCodeAt(j) >= 0x80) {
      throw new RangeError("Illegal input >= 0x80");
    }
    output.push(input.charCodeAt(j));
  }
  for (ic = basic > 0 ? basic + 1 : 0; ic < input_length;) {
    for (oldi = i, w = 1, k = base;; k += base) {
      if (ic >= input_length) {
        throw RangeError("punycode_bad_input(1)");
      }
      digit = decode_digit(input.charCodeAt(ic++));
      if (digit >= base) {
        throw RangeError("punycode_bad_input(2)");
      }
      if (digit > Math.floor((maxint - i) / w)) {
        throw RangeError("punycode_overflow(1)");
      }
      i += digit * w;
      t = k <= bias ? tmin : k >= bias + tmax ? tmax : k - bias;
      if (digit < t) {
        break;
      }
      if (w > Math.floor(maxint / (base - t))) {
        throw RangeError("punycode_overflow(2)");
      }
      w *= base - t;
    }
    out = output.length + 1;
    bias = adapt(i - oldi, out, oldi === 0);
    if (Math.floor(i / out) > maxint - n) {
      throw RangeError("punycode_overflow(3)");
    }
    n += Math.floor(i / out);
    i %= out;
    if (preserveCase) {
      case_flags.splice(i, 0, input.charCodeAt(ic - 1) - 65 < 26);
    }
    output.splice(i, 0, n);
    i++;
  }
  if (preserveCase) {
    for (i = 0, len = output.length; i < len; i++) {
      if (case_flags[i]) {
        output[i] = String.fromCharCode(output[i]).toUpperCase().charCodeAt(0);
      }
    }
  }
  return utf16.encode(output);
}
function encode$1(input, preserveCase) {
  var n, delta, h, b, bias, j, m, q, k, t, ijv, case_flags;
  if (preserveCase) {
    case_flags = utf16.decode(input);
  }
  input = utf16.decode(input.toLowerCase());
  var input_length = input.length; // Cache the length

  if (preserveCase) {
    for (j = 0; j < input_length; j++) {
      case_flags[j] = input[j] != case_flags[j];
    }
  }
  var output = [];
  n = initial_n;
  delta = 0;
  bias = initial_bias;
  for (j = 0; j < input_length; ++j) {
    if (input[j] < 0x80) {
      output.push(String.fromCharCode(case_flags ? encode_basic(input[j], case_flags[j]) : input[j]));
    }
  }
  h = b = output.length;
  if (b > 0) output.push(delimiter);
  while (h < input_length) {
    for (m = maxint, j = 0; j < input_length; ++j) {
      ijv = input[j];
      if (ijv >= n && ijv < m) m = ijv;
    }
    if (m - n > Math.floor((maxint - delta) / (h + 1))) {
      throw RangeError("punycode_overflow (1)");
    }
    delta += (m - n) * (h + 1);
    n = m;
    for (j = 0; j < input_length; ++j) {
      ijv = input[j];
      if (ijv < n) {
        if (++delta > maxint) return Error("punycode_overflow(2)");
      }
      if (ijv == n) {
        for (q = delta, k = base;; k += base) {
          t = k <= bias ? tmin : k >= bias + tmax ? tmax : k - bias;
          if (q < t) break;
          output.push(String.fromCharCode(encode_digit(t + (q - t) % (base - t), 0)));
          q = Math.floor((q - t) / (base - t));
        }
        output.push(String.fromCharCode(encode_digit(q, preserveCase && case_flags[j] ? 1 : 0)));
        bias = adapt(delta, h + 1, h == b);
        delta = 0;
        ++h;
      }
    }
    ++delta, ++n;
  }
  return output.join("");
}
var punycode = {
  decode: decode$1,
  encode: encode$1
};

var url_oh = require$$0$1;
var Number$1 = primordials.Number,
  StringPrototypeCharCodeAt$1 = primordials.StringPrototypeCharCodeAt,
  StringPrototypeIncludes = primordials.StringPrototypeIncludes,
  StringPrototypeReplace = primordials.StringPrototypeReplace,
  StringPrototypeSlice = primordials.StringPrototypeSlice,
  StringPrototypeStartsWith = primordials.StringPrototypeStartsWith,
  decodeURIComponent$2 = primordials.decodeURIComponent;
var validateObject = validator.validateObject;
var encodeStr$1 = querystring$2.encodeStr;
var CHAR_BACKWARD_SLASH$1 = constants$1.CHAR_BACKWARD_SLASH,
  CHAR_FORWARD_SLASH$1 = constants$1.CHAR_FORWARD_SLASH;
var path = path$1;
var URLSearchParams$1 = url_oh.URLSearchParams;
var encode = punycode.encode,
  decode = punycode.decode;
var kFormat = Symbol("format");
var URL$1 = /*#__PURE__*/function (_url_oh$URL) {
  _inherits(URL, _url_oh$URL);
  var _super = _createSuper(URL);
  function URL(input) {
    var _this;
    var base = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;
    _classCallCheck(this, URL);
    if (base == undefined || base == null) {
      _this = _super.call(this, input);
    } else {
      _this = _super.call(this, input, base);
    }
    return _possibleConstructorReturn(_this);
  }
  _createClass(URL, [{
    key: kFormat,
    value: function value(options) {
      if (options) validateObject(options, "options");
      options = _objectSpread2({
        fragment: true,
        unicode: false,
        search: true,
        auth: true
      }, options);

      // https://url.spec.whatwg.org/#url-serializing
      var ret = this.protocol;
      if (this.host !== null) {
        this.host = domainToASCII$1(decodeURI(this.host));
        ret += "//";
        var has_username = this.username !== "";
        var has_password = this.password !== "";
        if (options.auth && (has_username || has_password)) {
          if (has_username) ret += this.username;
          if (has_password) ret += ":".concat(this.password);
          ret += "@";
        }
        ret += options.unicode ? domainToUnicode$1(this.host) : this.host;
        if (this.port !== null && this.port.length > 0) ret += ":".concat(this.port);
      }
      ret += this.pathname;
      if (options.search && this.search !== null && this.search.length > 0) ret += "".concat(this.search);
      if (options.fragment && this.hash !== null && this.hash.length > 0) ret += "".concat(this.hash);
      return ret;
    }
  }], [{
    key: "createObjectURL",
    value: function createObjectURL(obj) {
      //待实现
    }
  }, {
    key: "revokeObjectURL",
    value: function revokeObjectURL(url) {
      //待实现
    }
  }]);
  return URL;
}(url_oh.URL);
function domainToASCII$1(domain) {
  if (typeof domain !== "string") {
    return "";
  }
  if (domain.startsWith("xn--") || domain.includes(":") || domain.includes("//")) {
    return "";
  }
  try {
    var domainArray = domain.split(".");
    var out = [];
    for (var i = 0; i < domainArray.length; ++i) {
      var s = domainArray[i];
      out.push(s.match(/[^A-Za-z0-9-]/) ? "xn--" + encode(s) : s);
    }
    return out.join(".");
  } catch (err) {
    return "";
  }
}
function domainToUnicode$1(domain) {
  try {
    var domainArray = domain.split(".");
    var out = [];
    for (var i = 0; i < domainArray.length; ++i) {
      var s = domainArray[i];
      out.push(s.match(/^xn--/) ? decode(s.slice(4)) : s);
    }
    return out.join(".");
  } catch (err) {
    return "";
  }
}

// Utility function that converts a URL object into an ordinary
// options object as expected by the http.request and https.request
// APIs.
function urlToHttpOptions$1(url) {
  var options = {
    protocol: url.protocol,
    hostname: typeof url.hostname === "string" && StringPrototypeStartsWith(url.hostname, "[") ? StringPrototypeSlice(url.hostname, 1, -1) : url.hostname,
    hash: url.hash,
    search: url.search,
    pathname: url.pathname,
    path: "".concat(url.pathname || "").concat(url.search || ""),
    href: url.href
  };
  if (url.port !== "") {
    options.port = Number$1(url.port);
  }
  if (url.username || url.password) {
    options.auth = "".concat(decodeURIComponent$2(url.username), ":").concat(decodeURIComponent$2(url.password));
  }
  return options;
}
function getPathFromURLPosix(url) {
  if (url.hostname !== "") {
    throw new Error("");
  }
  var pathname = url.pathname;
  for (var n = 0; n < pathname.length; n++) {
    if (pathname[n] === "%") {
      var third = pathname.codePointAt(n + 2) | 0x20;
      if (pathname[n + 1] === "2" && third === 102) {
        throw new Error("must not include encoded / characters");
      }
    }
  }
  return decodeURIComponent$2(pathname);
}
function fileURLToPath$1(path) {
  if (typeof path === "string") path = new URL$1(path);else if (!isURLInstance(path)) throw new Error("ERR_INVALID_ARG_TYPE, path:" + path);
  if (path.protocol !== "file:") throw new Error("ERR_INVALID_URL_SCHEME, file");
  return getPathFromURLPosix(path);
}
var backslashRegEx = /\\/g;
var newlineRegEx = /\n/g;
var carriageReturnRegEx = /\r/g;
var tabRegEx = /\t/g;
var hashRegEx = /#/g;
function encodePathChars(filepath) {
  //    if (StringPrototypeIncludes(filepath, '%'))
  //    filepath = StringPrototypeReplace(filepath, percentRegEx, '%25');
  if (StringPrototypeIncludes(filepath, "#")) filepath = StringPrototypeReplace(filepath, hashRegEx, "%23");
  // In posix, backslash is a valid character in paths:
  if (StringPrototypeIncludes(filepath, "\\")) filepath = StringPrototypeReplace(filepath, backslashRegEx, "%5C");
  if (StringPrototypeIncludes(filepath, "\n")) filepath = StringPrototypeReplace(filepath, newlineRegEx, "%0A");
  if (StringPrototypeIncludes(filepath, "\r")) filepath = StringPrototypeReplace(filepath, carriageReturnRegEx, "%0D");
  if (StringPrototypeIncludes(filepath, "\t")) filepath = StringPrototypeReplace(filepath, tabRegEx, "%09");
  return filepath;
}
function pathToFileURL$1(filepath) {
  var outURL = new URL$1("file://");
  var resolved = path.resolve(filepath);
  var filePathLast = StringPrototypeCharCodeAt$1(filepath, filepath.length - 1);
  if ((filePathLast === CHAR_FORWARD_SLASH$1 || filePathLast === CHAR_BACKWARD_SLASH$1) && resolved[resolved.length - 1] !== path.sep) resolved += "/";
  outURL.pathname = encodePathChars(resolved);
  return outURL;
}
function isURLInstance(fileURLOrPath) {
  return fileURLOrPath != null && fileURLOrPath.href && fileURLOrPath.origin;
}
function toPathIfFileURL(fileURLOrPath) {
  if (!isURLInstance(fileURLOrPath)) return fileURLOrPath;
  return fileURLToPath$1(fileURLOrPath);
}
var url = {
  fileURLToPath: fileURLToPath$1,
  pathToFileURL: pathToFileURL$1,
  toPathIfFileURL: toPathIfFileURL,
  isURLInstance: isURLInstance,
  URL: URL$1,
  URLSearchParams: URLSearchParams$1,
  domainToASCII: domainToASCII$1,
  domainToUnicode: domainToUnicode$1,
  urlToHttpOptions: urlToHttpOptions$1,
  formatSymbol: kFormat,
  encodeStr: encodeStr$1
};
var URL = url.URL,
  URLSearchParams = url.URLSearchParams;
var URL_1 = URL;
var URLSearchParams_1 = URLSearchParams;

function querystringify(obj) {
    const params = new URLSearchParams_1();
    for (const [key, value] of Object.entries(obj)) {
        if (value === undefined)
            continue;
        if (!Array.isArray(value)) {
            params.append(key, value + ""); //TODO oh的URLSearchParams不支持除了字符之外的类型
        }
        else {
            for (const item of value) {
                params.append(key, item + "");
            }
        }
    }
    return String(params);
}

/**
 * 参数转换
 * @param options
 */
function transformOtherInputToOhos(input) {
    let { responseType, url, body, method, headers, timeout, expectBinary } = input;
    return {
        url,
        method: http.RequestMethod[method],
        header: typeof headers === "object" ? headers : null,
        extraData: body || {},
        expectDataType: expectBinary
            ? http.HttpDataType.ARRAY_BUFFER
            : http.HttpDataType.STRING,
    };
}
/**
 * 返回值转换
 * @param output
 */
function transformOhosOutputToOther(output) {
    return {
        statusCode: output.responseCode,
        headers: output.header,
        body: output.result,
    };
}
function NetHttp (options, cb) {
    let { url, method, header, extraData, expectDataType } = transformOtherInputToOhos(options);
    let httpRequest = http.createHttp();
    httpRequest.request(url, {
        method,
        header,
        extraData,
        expectDataType,
    }, (err, data) => {
        if (!err) {
            cb(null, transformOhosOutputToOther(data));
        }
        else {
            cb(err);
        }
    });
}

function createRequest(baseUrl, agentOptions, agent) {
    const base = new URL_1(baseUrl);
    const auth = base64Encode(`${base.username || "root"}:${base.password}`);
    base.username = "";
    base.password = "";
    const options = agentOptions; // TODO 是否使用omit剔除 omit(agentOptions,["maxSockets"])
    return function request({ method, url: reqUrl, headers, body, timeout, expectBinary }, cb) {
        const url = new URL_1(reqUrl.pathname, base);
        if (base.search || reqUrl.search) {
            url.search = reqUrl.search ? `${base.search}${base.search ? "&" : ""}${reqUrl.search.slice(1)}` : base.search; // TODO 注意“&”要严格按照标准格式
        }
        if (!headers["authorization"]) {
            headers["authorization"] = `Basic ${auth}}`;
        }
        let callback = (err, res) => {
            callback = () => undefined;
            cb(err, res);
        };
        const req = NetHttp({
            useXDR: true,
            withCredentials: true,
            ...options,
            responseType: expectBinary,
            url: String(url),
            body,
            method,
            headers,
            timeout,
        }, (err, res) => {
            if (!err) {
                const response = res;
                // @ts-ignore
                response.request = req;
                if (!response.body)
                    response.body = "";
                if (options.after) {
                    options.after(null, response);
                }
                callback(null, response);
            }
            else {
                const error = err;
                if (options.after) {
                    options.after(error);
                }
                callback(error);
            }
        });
    };
}

const MIME_JSON = /\/(json|javascript)(\W|$)/;
const LEADER_ENDPOINT_HEADER = "x-arango-endpoint";
function isBearerAuth(auth) {
    return auth.hasOwnProperty("token");
}
/**
 * @internal
 */
function generateStackTrace() {
    let err = new Error();
    if (!err.stack) {
        try {
            throw err;
        }
        catch (e) {
            err = e;
        }
    }
    return err;
}
/**
 * Represents a connection pool shared by one or more databases.
 *
 * @internal
 */
class Connection {
    /**
     * @internal
     *
     * Creates a new `Connection` instance.
     *
     * @param config - An object with configuration options.
     *
     */
    constructor(config = {}) {
        this._activeTasks = 0;
        this._arangoVersion = 30900;
        this._queue = new dist.LinkedList();
        this._databases = new Map();
        this._hosts = [];
        this._hostUrls = [];
        this._transactionId = null;
        this._queueTimes = new dist.LinkedList();
        const URLS = config.url
            ? Array.isArray(config.url)
                ? config.url
                : [config.url]
            : ["http://127.0.0.1:8529"];
        const MAX_SOCKETS = 3 * (config.loadBalancingStrategy === "ROUND_ROBIN" ? URLS.length : 1);
        if (config.arangoVersion !== undefined) {
            this._arangoVersion = config.arangoVersion;
        }
        this._agent = config.agent;
        this._agentOptions = { maxSockets: MAX_SOCKETS, ...config.agentOptions }
            ;
        this._maxTasks = this._agentOptions.maxSockets;
        this._headers = { ...config.headers };
        this._loadBalancingStrategy = config.loadBalancingStrategy ?? "NONE";
        this._precaptureStackTraces = Boolean(config.precaptureStackTraces);
        this._responseQueueTimeSamples = config.responseQueueTimeSamples ?? 10;
        this._retryOnConflict = config.retryOnConflict ?? 0;
        if (this._responseQueueTimeSamples < 0) {
            this._responseQueueTimeSamples = Infinity;
        }
        if (config.maxRetries === false) {
            this._maxRetries = false;
        }
        else {
            this._maxRetries = Number(config.maxRetries ?? 0);
        }
        this.addToHostList(URLS);
        if (config.auth) {
            if (isBearerAuth(config.auth)) {
                this.setBearerAuth(config.auth);
            }
            else {
                this.setBasicAuth(config.auth);
            }
        }
        if (this._loadBalancingStrategy === "ONE_RANDOM") {
            this._activeHostUrl =
                this._hostUrls[Math.floor(Math.random() * this._hostUrls.length)];
            this._activeDirtyHostUrl =
                this._hostUrls[Math.floor(Math.random() * this._hostUrls.length)];
        }
        else {
            this._activeHostUrl = this._hostUrls[0];
            this._activeDirtyHostUrl = this._hostUrls[0];
        }
    }
    /**
     * @internal
     *
     * Indicates that this object represents an ArangoDB connection.
     */
    get isArangoConnection() {
        return true;
    }
    get queueTime() {
        return {
            getLatest: () => this._queueTimes.last?.value[1],
            getValues: () => Array.from(this._queueTimes.values()),
            getAvg: () => {
                let avg = 0;
                for (const [, [, value]] of this._queueTimes) {
                    avg += value / this._queueTimes.length;
                }
                return avg;
            },
        };
    }
    _runQueue() {
        if (!this._queue.length || this._activeTasks >= this._maxTasks)
            return;
        const task = this._queue.shift();
        let hostUrl = this._activeHostUrl;
        if (task.hostUrl !== undefined) {
            hostUrl = task.hostUrl;
        }
        else if (task.allowDirtyRead) {
            hostUrl = this._activeDirtyHostUrl;
            this._activeDirtyHostUrl =
                this._hostUrls[(this._hostUrls.indexOf(this._activeDirtyHostUrl) + 1) %
                    this._hostUrls.length];
            task.options.headers["x-arango-allow-dirty-read"] = "true";
        }
        else if (this._loadBalancingStrategy === "ROUND_ROBIN") {
            this._activeHostUrl =
                this._hostUrls[(this._hostUrls.indexOf(this._activeHostUrl) + 1) %
                    this._hostUrls.length];
        }
        this._activeTasks += 1;
        const callback = (err, res) => {
            this._activeTasks -= 1;
            if (!err && res) {
                if (res.statusCode === 503 && res.headers[LEADER_ENDPOINT_HEADER]) {
                    const url = res.headers[LEADER_ENDPOINT_HEADER];
                    const [cleanUrl] = this.addToHostList(url);
                    task.hostUrl = cleanUrl;
                    if (this._activeHostUrl === hostUrl) {
                        this._activeHostUrl = cleanUrl;
                    }
                    this._queue.push(task);
                }
                else {
                    res.arangojsHostUrl = hostUrl;
                    const contentType = res.headers["content-type"];
                    const queueTime = res.headers["x-arango-queue-time-seconds"];
                    if (queueTime) {
                        this._queueTimes.push([Date.now(), Number(queueTime)]);
                        while (this._responseQueueTimeSamples < this._queueTimes.length) {
                            this._queueTimes.shift();
                        }
                    }
                    let parsedBody = undefined;
                    if (res.body.length && contentType && contentType.match(MIME_JSON)) {
                        try {
                            parsedBody = res.body;
                            parsedBody = JSON.parse(parsedBody);
                        }
                        catch (e) {
                            if (!task.options.expectBinary) {
                                if (typeof parsedBody !== "string") {
                                    parsedBody = res.body.toString("utf-8");
                                }
                                e.res = res;
                                if (task.stack) {
                                    e.stack += task.stack();
                                }
                                callback(e);
                                return;
                            }
                        }
                    }
                    else if (res.body && !task.options.expectBinary) {
                        parsedBody = res.body.toString("utf-8");
                    }
                    else {
                        parsedBody = res.body;
                    }
                    if (isArangoErrorResponse(parsedBody)) {
                        res.body = parsedBody;
                        err = new ArangoError(res);
                    }
                    else if (res.statusCode && res.statusCode >= 400) {
                        res.body = parsedBody;
                        err = new HttpError(res);
                    }
                    else {
                        if (!task.options.expectBinary)
                            res.body = parsedBody;
                        task.resolve(task.transform ? task.transform(res) : res);
                    }
                }
            }
            if (err) {
                if (!task.allowDirtyRead &&
                    this._hosts.length > 1 &&
                    this._activeHostUrl === hostUrl &&
                    this._loadBalancingStrategy !== "ROUND_ROBIN") {
                    this._activeHostUrl =
                        this._hostUrls[(this._hostUrls.indexOf(this._activeHostUrl) + 1) %
                            this._hostUrls.length];
                }
                if (isArangoError(err) &&
                    err.errorNum === ERROR_ARANGO_CONFLICT &&
                    task.retryOnConflict > 0) {
                    task.retryOnConflict -= 1;
                    this._queue.push(task);
                }
                else if (((isSystemError(err) &&
                    err.syscall === "connect" &&
                    err.code === "ECONNREFUSED") ||
                    (isArangoError(err) &&
                        err.errorNum === ERROR_ARANGO_MAINTENANCE_MODE)) &&
                    task.hostUrl === undefined &&
                    this._maxRetries !== false &&
                    task.retries < (this._maxRetries || this._hosts.length - 1)) {
                    task.retries += 1;
                    this._queue.push(task);
                }
                else {
                    if (task.stack) {
                        err.stack += task.stack();
                    }
                    task.reject(err);
                }
            }
            this._runQueue();
        };
        try {
            this._hosts[this._hostUrls.indexOf(hostUrl)](task.options, callback);
        }
        catch (e) {
            callback(e);
        }
    }
    _buildUrl({ basePath, path, qs }) {
        const pathname = `${basePath || ""}${path || ""}`;
        let search;
        if (qs) {
            if (typeof qs === "string")
                search = `?${qs}`;
            else
                search = `?${querystringify(qs)}`;
        }
        return search ? { pathname, search } : { pathname };
    }
    setBearerAuth(auth) {
        this.setHeader("authorization", `Bearer ${auth.token}`);
    }
    setBasicAuth(auth) {
        this.setHeader("authorization", `Basic ${base64Encode(`${auth.username}:${auth.password}`)}`);
    }
    setResponseQueueTimeSamples(responseQueueTimeSamples) {
        if (responseQueueTimeSamples < 0) {
            responseQueueTimeSamples = Infinity;
        }
        this._responseQueueTimeSamples = responseQueueTimeSamples;
        while (this._responseQueueTimeSamples < this._queueTimes.length) {
            this._queueTimes.shift();
        }
    }
    database(databaseName, database) {
        if (database === null) {
            this._databases.delete(databaseName);
            return undefined;
        }
        if (!database) {
            return this._databases.get(databaseName);
        }
        this._databases.set(databaseName, database);
        return database;
    }
    /**
     * @internal
     *
     * Replaces the host list with the given URLs.
     *
     * See {@link Connection#acquireHostList}.
     *
     * @param urls - URLs to use as host list.
     */
    setHostList(urls) {
        const cleanUrls = urls.map((url) => normalizeUrl(url));
        this._hosts.splice(0, this._hosts.length, ...cleanUrls.map((url) => {
            const i = this._hostUrls.indexOf(url);
            if (i !== -1)
                return this._hosts[i];
            return createRequest(url, this._agentOptions, this._agent);
        }));
        this._hostUrls.splice(0, this._hostUrls.length, ...cleanUrls);
    }
    /**
     * @internal
     *
     * Adds the given URL or URLs to the host list.
     *
     * See {@link Connection#acquireHostList}.
     *
     * @param urls - URL or URLs to add.
     */
    addToHostList(urls) {
        const cleanUrls = (Array.isArray(urls) ? urls : [urls]).map((url) => normalizeUrl(url));
        const newUrls = cleanUrls.filter((url) => this._hostUrls.indexOf(url) === -1);
        this._hostUrls.push(...newUrls);
        this._hosts.push(...newUrls.map((url) => createRequest(url, this._agentOptions, this._agent)));
        return cleanUrls;
    }
    /**
     * @internal
     *
     * Sets the connection's active `transactionId`.
     *
     * While set, all requests will use this ID, ensuring the requests are executed
     * within the transaction if possible. Setting the ID manually may cause
     * unexpected behavior.
     *
     * See also {@link Connection#clearTransactionId}.
     *
     * @param transactionId - ID of the active transaction.
     */
    setTransactionId(transactionId) {
        this._transactionId = transactionId;
    }
    /**
     * @internal
     *
     * Clears the connection's active `transactionId`.
     */
    clearTransactionId() {
        this._transactionId = null;
    }
    /**
     * @internal
     *
     * Sets the header `headerName` with the given `value` or clears the header if
     * `value` is `null`.
     *
     * @param headerName - Name of the header to set.
     * @param value - Value of the header.
     */
    setHeader(headerName, value) {
        if (value === null) {
            delete this._headers[headerName];
        }
        else {
            this._headers[headerName] = value;
        }
    }
    /**
     * @internal
     *
     * Closes all open connections.
     *
     * See {@link database.Database#close}.
     */
    close() {
        for (const host of this._hosts) {
            if (host.close)
                host.close();
        }
    }
    /**
     * @internal
     *
     * Waits for propagation.
     *
     * See {@link database.Database#waitForPropagation}.
     *
     * @param request - Request to perform against each coordinator.
     * @param timeout - Maximum number of milliseconds to wait for propagation.
     */
    async waitForPropagation(request, timeout = Infinity) {
        const numHosts = this._hosts.length;
        const propagated = [];
        const started = Date.now();
        let index = 0;
        while (true) {
            if (propagated.length === numHosts) {
                return;
            }
            while (propagated.includes(this._hostUrls[index])) {
                index = (index + 1) % numHosts;
            }
            const hostUrl = this._hostUrls[index];
            try {
                await this.request({ ...request, hostUrl });
            }
            catch (e) {
                if (started + timeout < Date.now()) {
                    throw e;
                }
                await new Promise((resolve) => setTimeout(resolve, 1000));
                continue;
            }
            if (!propagated.includes(hostUrl)) {
                propagated.push(hostUrl);
            }
        }
    }
    /**
     * @internal
     *
     * Performs a request using the arangojs connection pool.
     */
    request({ hostUrl, method = "GET", body, expectBinary = false, isBinary = false, allowDirtyRead = false, retryOnConflict = this._retryOnConflict, timeout = 0, headers, ...urlInfo }, transform) {
        return new Promise((resolve, reject) => {
            let contentType = "text/plain";
            if (isBinary) {
                contentType = "application/octet-stream";
            }
            else if (body) {
                if (typeof body === "object") {
                    body = JSON.stringify(body);
                    contentType = "application/json";
                }
                else {
                    body = String(body);
                }
            }
            const extraHeaders = {
                ...this._headers,
                "content-type": contentType,
                "x-arango-version": String(this._arangoVersion),
            };
            if (this._transactionId) {
                extraHeaders["x-arango-trx-id"] = this._transactionId;
            }
            const task = {
                retries: 0,
                hostUrl,
                allowDirtyRead,
                retryOnConflict,
                options: {
                    url: this._buildUrl(urlInfo),
                    headers: { ...extraHeaders, ...headers },
                    timeout,
                    method,
                    expectBinary,
                    body,
                },
                reject,
                resolve,
                transform,
            };
            if (this._precaptureStackTraces) {
                if (typeof Error.captureStackTrace === "function") {
                    const capture = {};
                    Error.captureStackTrace(capture);
                    task.stack = () => `\n${capture.stack.split("\n").slice(3).join("\n")}`;
                }
                else {
                    const capture = generateStackTrace();
                    if (Object.prototype.hasOwnProperty.call(capture, "stack")) {
                        task.stack = () => `\n${capture.stack.split("\n").slice(4).join("\n")}`;
                    }
                }
            }
            this._queue.push(task);
            this._runQueue();
        });
    }
}

function _typeof(obj) {
  "@babel/helpers - typeof";

  return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) {
    return typeof obj;
  } : function (obj) {
    return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
  }, _typeof(obj);
}

function getDefaultExportFromCjs$1 (x) {
	return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x['default'] : x;
}

var events = {exports: {}};

// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
var R = (typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" ? Reflect : null;
var ReflectApply = R && typeof R.apply === "function" ? R.apply : function ReflectApply(target, receiver, args) {
  return Function.prototype.apply.call(target, receiver, args);
};
var ReflectOwnKeys;
if (R && typeof R.ownKeys === "function") {
  ReflectOwnKeys = R.ownKeys;
} else if (Object.getOwnPropertySymbols) {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target).concat(Object.getOwnPropertySymbols(target));
  };
} else {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target);
  };
}
function ProcessEmitWarning(warning) {
  if (console && console.warn) console.warn(warning);
}
var NumberIsNaN = Number.isNaN || function NumberIsNaN(value) {
  return value !== value;
};
function EventEmitter() {
  EventEmitter.init.call(this);
}
var EventEmitter_1 = events.exports.EventEmitter = EventEmitter;
events.exports = EventEmitter;
var once_1 = events.exports.once = once;

// Backwards-compat with node 0.10.x
EventEmitter.EventEmitter = EventEmitter;
EventEmitter.prototype._events = undefined;
EventEmitter.prototype._eventsCount = 0;
EventEmitter.prototype._maxListeners = undefined;

// By default EventEmitters will print a warning if more than 10 listeners are
// added to it. This is a useful default which helps finding memory leaks.
var defaultMaxListeners = 10;
function checkListener(listener) {
  if (typeof listener !== "function") {
    throw new TypeError('The "listener" argument must be of type Function. Received type ' + _typeof(listener));
  }
}
Object.defineProperty(EventEmitter, "defaultMaxListeners", {
  enumerable: true,
  get: function get() {
    return defaultMaxListeners;
  },
  set: function set(arg) {
    if (typeof arg !== "number" || arg < 0 || NumberIsNaN(arg)) {
      throw new RangeError('The value of "defaultMaxListeners" is out of range. It must be a non-negative number. Received ' + arg + ".");
    }
    defaultMaxListeners = arg;
  }
});
EventEmitter.init = function () {
  if (this._events === undefined || this._events === Object.getPrototypeOf(this)._events) {
    this._events = Object.create(null);
    this._eventsCount = 0;
  }
  this._maxListeners = this._maxListeners || undefined;
};

// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
EventEmitter.prototype.setMaxListeners = function setMaxListeners(n) {
  if (typeof n !== "number" || n < 0 || NumberIsNaN(n)) {
    throw new RangeError('The value of "n" is out of range. It must be a non-negative number. Received ' + n + ".");
  }
  this._maxListeners = n;
  return this;
};
function _getMaxListeners(that) {
  if (that._maxListeners === undefined) return EventEmitter.defaultMaxListeners;
  return that._maxListeners;
}
EventEmitter.prototype.getMaxListeners = function getMaxListeners() {
  return _getMaxListeners(this);
};
EventEmitter.prototype.emit = function emit(type) {
  var args = [];
  for (var i = 1; i < arguments.length; i++) args.push(arguments[i]);
  var doError = type === "error";
  var events = this._events;
  if (events !== undefined) doError = doError && events.error === undefined;else if (!doError) return false;

  // If there is no 'error' event listener then throw.
  if (doError) {
    var er;
    if (args.length > 0) er = args[0];
    if (er instanceof Error) {
      // Note: The comments on the `throw` lines are intentional, they show
      // up in Node's output if this results in an unhandled exception.
      throw er; // Unhandled 'error' event
    }
    // At least give some kind of context to the user
    var err = new Error("Unhandled error." + (er ? " (" + er.message + ")" : ""));
    err.context = er;
    throw err; // Unhandled 'error' event
  }

  var handler = events[type];
  if (handler === undefined) return false;
  if (typeof handler === "function") {
    ReflectApply(handler, this, args);
  } else {
    var len = handler.length;
    var listeners = arrayClone(handler, len);
    for (var i = 0; i < len; ++i) ReflectApply(listeners[i], this, args);
  }
  return true;
};
function _addListener(target, type, listener, prepend) {
  var m;
  var events;
  var existing;
  checkListener(listener);
  events = target._events;
  if (events === undefined) {
    events = target._events = Object.create(null);
    target._eventsCount = 0;
  } else {
    // To avoid recursion in the case that type === "newListener"! Before
    // adding it to the listeners, first emit "newListener".
    if (events.newListener !== undefined) {
      target.emit("newListener", type, listener.listener ? listener.listener : listener);

      // Re-assign `events` because a newListener handler could have caused the
      // this._events to be assigned to a new object
      events = target._events;
    }
    existing = events[type];
  }
  if (existing === undefined) {
    // Optimize the case of one listener. Don't need the extra array object.
    existing = events[type] = listener;
    ++target._eventsCount;
  } else {
    if (typeof existing === "function") {
      // Adding the second element, need to change to array.
      existing = events[type] = prepend ? [listener, existing] : [existing, listener];
      // If we've already got an array, just append.
    } else if (prepend) {
      existing.unshift(listener);
    } else {
      existing.push(listener);
    }

    // Check for listener leak
    m = _getMaxListeners(target);
    if (m > 0 && existing.length > m && !existing.warned) {
      existing.warned = true;
      // No error code for this since it is a Warning
      // eslint-disable-next-line no-restricted-syntax
      var w = new Error("Possible EventEmitter memory leak detected. " + existing.length + " " + String(type) + " listeners " + "added. Use emitter.setMaxListeners() to " + "increase limit");
      w.name = "MaxListenersExceededWarning";
      w.emitter = target;
      w.type = type;
      w.count = existing.length;
      ProcessEmitWarning(w);
    }
  }
  return target;
}
EventEmitter.prototype.addListener = function addListener(type, listener) {
  return _addListener(this, type, listener, false);
};
EventEmitter.prototype.on = EventEmitter.prototype.addListener;
EventEmitter.prototype.prependListener = function prependListener(type, listener) {
  return _addListener(this, type, listener, true);
};
function onceWrapper() {
  if (!this.fired) {
    this.target.removeListener(this.type, this.wrapFn);
    this.fired = true;
    if (arguments.length === 0) return this.listener.call(this.target);
    return this.listener.apply(this.target, arguments);
  }
}
function _onceWrap(target, type, listener) {
  var state = {
    fired: false,
    wrapFn: undefined,
    target: target,
    type: type,
    listener: listener
  };
  var wrapped = onceWrapper.bind(state);
  wrapped.listener = listener;
  state.wrapFn = wrapped;
  return wrapped;
}
EventEmitter.prototype.once = function once(type, listener) {
  checkListener(listener);
  this.on(type, _onceWrap(this, type, listener));
  return this;
};
EventEmitter.prototype.prependOnceListener = function prependOnceListener(type, listener) {
  checkListener(listener);
  this.prependListener(type, _onceWrap(this, type, listener));
  return this;
};

// Emits a 'removeListener' event if and only if the listener was removed.
EventEmitter.prototype.removeListener = function removeListener(type, listener) {
  var list, events, position, i, originalListener;
  checkListener(listener);
  events = this._events;
  if (events === undefined) return this;
  list = events[type];
  if (list === undefined) return this;
  if (list === listener || list.listener === listener) {
    if (--this._eventsCount === 0) this._events = Object.create(null);else {
      delete events[type];
      if (events.removeListener) this.emit("removeListener", type, list.listener || listener);
    }
  } else if (typeof list !== "function") {
    position = -1;
    for (i = list.length - 1; i >= 0; i--) {
      if (list[i] === listener || list[i].listener === listener) {
        originalListener = list[i].listener;
        position = i;
        break;
      }
    }
    if (position < 0) return this;
    if (position === 0) list.shift();else {
      spliceOne(list, position);
    }
    if (list.length === 1) events[type] = list[0];
    if (events.removeListener !== undefined) this.emit("removeListener", type, originalListener || listener);
  }
  return this;
};
EventEmitter.prototype.off = EventEmitter.prototype.removeListener;
EventEmitter.prototype.removeAllListeners = function removeAllListeners(type) {
  var listeners, events, i;
  events = this._events;
  if (events === undefined) return this;

  // not listening for removeListener, no need to emit
  if (events.removeListener === undefined) {
    if (arguments.length === 0) {
      this._events = Object.create(null);
      this._eventsCount = 0;
    } else if (events[type] !== undefined) {
      if (--this._eventsCount === 0) this._events = Object.create(null);else delete events[type];
    }
    return this;
  }

  // emit removeListener for all listeners on all events
  if (arguments.length === 0) {
    var keys = Object.keys(events);
    var key;
    for (i = 0; i < keys.length; ++i) {
      key = keys[i];
      if (key === "removeListener") continue;
      this.removeAllListeners(key);
    }
    this.removeAllListeners("removeListener");
    this._events = Object.create(null);
    this._eventsCount = 0;
    return this;
  }
  listeners = events[type];
  if (typeof listeners === "function") {
    this.removeListener(type, listeners);
  } else if (listeners !== undefined) {
    // LIFO order
    for (i = listeners.length - 1; i >= 0; i--) {
      this.removeListener(type, listeners[i]);
    }
  }
  return this;
};
function _listeners(target, type, unwrap) {
  var events = target._events;
  if (events === undefined) return [];
  var evlistener = events[type];
  if (evlistener === undefined) return [];
  if (typeof evlistener === "function") return unwrap ? [evlistener.listener || evlistener] : [evlistener];
  return unwrap ? unwrapListeners(evlistener) : arrayClone(evlistener, evlistener.length);
}
EventEmitter.prototype.listeners = function listeners(type) {
  return _listeners(this, type, true);
};
EventEmitter.prototype.rawListeners = function rawListeners(type) {
  return _listeners(this, type, false);
};
EventEmitter.listenerCount = function (emitter, type) {
  if (typeof emitter.listenerCount === "function") {
    return emitter.listenerCount(type);
  } else {
    return listenerCount.call(emitter, type);
  }
};
EventEmitter.prototype.listenerCount = listenerCount;
function listenerCount(type) {
  var events = this._events;
  if (events !== undefined) {
    var evlistener = events[type];
    if (typeof evlistener === "function") {
      return 1;
    } else if (evlistener !== undefined) {
      return evlistener.length;
    }
  }
  return 0;
}
EventEmitter.prototype.eventNames = function eventNames() {
  return this._eventsCount > 0 ? ReflectOwnKeys(this._events) : [];
};
function arrayClone(arr, n) {
  var copy = new Array(n);
  for (var i = 0; i < n; ++i) copy[i] = arr[i];
  return copy;
}
function spliceOne(list, index) {
  for (; index + 1 < list.length; index++) list[index] = list[index + 1];
  list.pop();
}
function unwrapListeners(arr) {
  var ret = new Array(arr.length);
  for (var i = 0; i < ret.length; ++i) {
    ret[i] = arr[i].listener || arr[i];
  }
  return ret;
}
function once(emitter, name) {
  return new Promise(function (resolve, reject) {
    function errorListener(err) {
      emitter.removeListener(name, resolver);
      reject(err);
    }
    function resolver() {
      if (typeof emitter.removeListener === "function") {
        emitter.removeListener("error", errorListener);
      }
      resolve([].slice.call(arguments));
    }
    eventTargetAgnosticAddListener(emitter, name, resolver, {
      once: true
    });
    if (name !== "error") {
      addErrorHandlerIfEventEmitter(emitter, errorListener, {
        once: true
      });
    }
  });
}
function addErrorHandlerIfEventEmitter(emitter, handler, flags) {
  if (typeof emitter.on === "function") {
    eventTargetAgnosticAddListener(emitter, "error", handler, flags);
  }
}
function eventTargetAgnosticAddListener(emitter, name, listener, flags) {
  if (typeof emitter.on === "function") {
    if (flags.once) {
      emitter.once(name, listener);
    } else {
      emitter.on(name, listener);
    }
  } else if (typeof emitter.addEventListener === "function") {
    // EventTarget does not have `error` event semantics like Node
    // EventEmitters, we do not listen for `error` events here.
    emitter.addEventListener(name, function wrapListener(arg) {
      // IE does not have builtin `{ once: true }` support so we
      // have to do it manually.
      if (flags.once) {
        emitter.removeEventListener(name, wrapListener);
      }
      listener(arg);
    });
  } else {
    throw new TypeError('The "emitter" argument must be of type EventEmitter. Received type ' + _typeof(emitter));
  }
}
var eventsExports = events.exports;
var index$1 = /*@__PURE__*/getDefaultExportFromCjs$1(eventsExports);

var t = /*#__PURE__*/Object.freeze({
    __proto__: null,
    EventEmitter: EventEmitter_1,
    default: index$1,
    once: once_1
});

function getDefaultExportFromCjs (x) {
	return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x['default'] : x;
}

var process$1 = {exports: {}};

// shim for using process in browser
var process = process$1.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;
function defaultSetTimout() {
  throw new Error("setTimeout has not been defined");
}
function defaultClearTimeout() {
  throw new Error("clearTimeout has not been defined");
}
(function () {
  try {
    if (typeof setTimeout === "function") {
      cachedSetTimeout = setTimeout;
    } else {
      cachedSetTimeout = defaultSetTimout;
    }
  } catch (e) {
    cachedSetTimeout = defaultSetTimout;
  }
  try {
    if (typeof clearTimeout === "function") {
      cachedClearTimeout = clearTimeout;
    } else {
      cachedClearTimeout = defaultClearTimeout;
    }
  } catch (e) {
    cachedClearTimeout = defaultClearTimeout;
  }
})();
function runTimeout(fun) {
  if (cachedSetTimeout === setTimeout) {
    //normal enviroments in sane situations
    return setTimeout(fun, 0);
  }
  // if setTimeout wasn't available but was latter defined
  if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
    cachedSetTimeout = setTimeout;
    return setTimeout(fun, 0);
  }
  try {
    // when when somebody has screwed with setTimeout but no I.E. maddness
    return cachedSetTimeout(fun, 0);
  } catch (e) {
    try {
      // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
      return cachedSetTimeout.call(null, fun, 0);
    } catch (e) {
      // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
      return cachedSetTimeout.call(this, fun, 0);
    }
  }
}
function runClearTimeout(marker) {
  if (cachedClearTimeout === clearTimeout) {
    //normal enviroments in sane situations
    return clearTimeout(marker);
  }
  // if clearTimeout wasn't available but was latter defined
  if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
    cachedClearTimeout = clearTimeout;
    return clearTimeout(marker);
  }
  try {
    // when when somebody has screwed with setTimeout but no I.E. maddness
    return cachedClearTimeout(marker);
  } catch (e) {
    try {
      // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
      return cachedClearTimeout.call(null, marker);
    } catch (e) {
      // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
      // Some versions of I.E. have different rules for clearTimeout vs setTimeout
      return cachedClearTimeout.call(this, marker);
    }
  }
}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;
function cleanUpNextTick() {
  if (!draining || !currentQueue) {
    return;
  }
  draining = false;
  if (currentQueue.length) {
    queue = currentQueue.concat(queue);
  } else {
    queueIndex = -1;
  }
  if (queue.length) {
    drainQueue();
  }
}
function drainQueue() {
  if (draining) {
    return;
  }
  var timeout = runTimeout(cleanUpNextTick);
  draining = true;
  var len = queue.length;
  while (len) {
    currentQueue = queue;
    queue = [];
    while (++queueIndex < len) {
      if (currentQueue) {
        currentQueue[queueIndex].run();
      }
    }
    queueIndex = -1;
    len = queue.length;
  }
  currentQueue = null;
  draining = false;
  runClearTimeout(timeout);
}
process.nextTick = function (fun) {
  var args = new Array(arguments.length - 1);
  if (arguments.length > 1) {
    for (var i = 1; i < arguments.length; i++) {
      args[i - 1] = arguments[i];
    }
  }
  queue.push(new Item(fun, args));
  if (queue.length === 1 && !draining) {
    runTimeout(drainQueue);
  }
};

// v8 likes predictible objects
function Item(fun, array) {
  this.fun = fun;
  this.array = array;
}
Item.prototype.run = function () {
  this.fun.apply(null, this.array);
};

// other interfaces
process.env = {};
var processExports = process$1.exports;
var index = /*@__PURE__*/getDefaultExportFromCjs(processExports);

var n = /*#__PURE__*/Object.freeze({
    __proto__: null,
    default: index
});

var string_decoder = {};

/*<replacement>*/

// var Buffer = require("safe-buffer").Buffer;
var Buffer = index$2.Buffer;
/*</replacement>*/

var isEncoding = Buffer.isEncoding || function (encoding) {
  encoding = "" + encoding;
  switch (encoding && encoding.toLowerCase()) {
    case "hex":
    case "utf8":
    case "utf-8":
    case "ascii":
    case "binary":
    case "base64":
    case "ucs2":
    case "ucs-2":
    case "utf16le":
    case "utf-16le":
    case "raw":
      return true;
    default:
      return false;
  }
};
function _normalizeEncoding(enc) {
  if (!enc) return "utf8";
  var retried;
  while (true) {
    switch (enc) {
      case "utf8":
      case "utf-8":
        return "utf8";
      case "ucs2":
      case "ucs-2":
      case "utf16le":
      case "utf-16le":
        return "utf16le";
      case "latin1":
      case "binary":
        return "latin1";
      case "base64":
      case "ascii":
      case "hex":
        return enc;
      default:
        if (retried) return; // undefined
        enc = ("" + enc).toLowerCase();
        retried = true;
    }
  }
}

// Do not cache `Buffer.isEncoding` when checking encoding names as some
// modules monkey-patch it to support additional encodings
function normalizeEncoding(enc) {
  var nenc = _normalizeEncoding(enc);
  if (typeof nenc !== "string" && (Buffer.isEncoding === isEncoding || !isEncoding(enc))) throw new Error("Unknown encoding: " + enc);
  return nenc || enc;
}

// StringDecoder provides an interface for efficiently splitting a series of
// buffers into a series of JS strings without breaking apart multi-byte
// characters.
var StringDecoder_1 = string_decoder.StringDecoder = StringDecoder;
function StringDecoder(encoding) {
  this.encoding = normalizeEncoding(encoding);
  var nb;
  switch (this.encoding) {
    case "utf16le":
      this.text = utf16Text;
      this.end = utf16End;
      nb = 4;
      break;
    case "utf8":
      this.fillLast = utf8FillLast;
      nb = 4;
      break;
    case "base64":
      this.text = base64Text;
      this.end = base64End;
      nb = 3;
      break;
    default:
      this.write = simpleWrite;
      this.end = simpleEnd;
      return;
  }
  this.lastNeed = 0;
  this.lastTotal = 0;
  this.lastChar = Buffer.allocUnsafe(nb);
}
StringDecoder.prototype.write = function (buf) {
  if (buf.length === 0) return "";
  var r;
  var i;
  if (this.lastNeed) {
    r = this.fillLast(buf);
    if (r === undefined) return "";
    i = this.lastNeed;
    this.lastNeed = 0;
  } else {
    i = 0;
  }
  if (i < buf.length) return r ? r + this.text(buf, i) : this.text(buf, i);
  return r || "";
};
StringDecoder.prototype.end = utf8End;

// Returns only complete characters in a Buffer
StringDecoder.prototype.text = utf8Text;

// Attempts to complete a partial non-UTF-8 character using bytes from a Buffer
StringDecoder.prototype.fillLast = function (buf) {
  if (this.lastNeed <= buf.length) {
    buf.copy(this.lastChar, this.lastTotal - this.lastNeed, 0, this.lastNeed);
    return this.lastChar.toString(this.encoding, 0, this.lastTotal);
  }
  buf.copy(this.lastChar, this.lastTotal - this.lastNeed, 0, buf.length);
  this.lastNeed -= buf.length;
};

// Checks the type of a UTF-8 byte, whether it's ASCII, a leading byte, or a
// continuation byte. If an invalid byte is detected, -2 is returned.
function utf8CheckByte(_byte) {
  if (_byte <= 0x7f) return 0;else if (_byte >> 5 === 0x06) return 2;else if (_byte >> 4 === 0x0e) return 3;else if (_byte >> 3 === 0x1e) return 4;
  return _byte >> 6 === 0x02 ? -1 : -2;
}

// Checks at most 3 bytes at the end of a Buffer in order to detect an
// incomplete multi-byte UTF-8 character. The total number of bytes (2, 3, or 4)
// needed to complete the UTF-8 character (if applicable) are returned.
function utf8CheckIncomplete(self, buf, i) {
  var j = buf.length - 1;
  if (j < i) return 0;
  var nb = utf8CheckByte(buf[j]);
  if (nb >= 0) {
    if (nb > 0) self.lastNeed = nb - 1;
    return nb;
  }
  if (--j < i || nb === -2) return 0;
  nb = utf8CheckByte(buf[j]);
  if (nb >= 0) {
    if (nb > 0) self.lastNeed = nb - 2;
    return nb;
  }
  if (--j < i || nb === -2) return 0;
  nb = utf8CheckByte(buf[j]);
  if (nb >= 0) {
    if (nb > 0) {
      if (nb === 2) nb = 0;else self.lastNeed = nb - 3;
    }
    return nb;
  }
  return 0;
}

// Validates as many continuation bytes for a multi-byte UTF-8 character as
// needed or are available. If we see a non-continuation byte where we expect
// one, we "replace" the validated continuation bytes we've seen so far with
// a single UTF-8 replacement character ('\ufffd'), to match v8's UTF-8 decoding
// behavior. The continuation byte check is included three times in the case
// where all of the continuation bytes for a character exist in the same buffer.
// It is also done this way as a slight performance increase instead of using a
// loop.
function utf8CheckExtraBytes(self, buf, p) {
  if ((buf[0] & 0xc0) !== 0x80) {
    self.lastNeed = 0;
    return "\uFFFD";
  }
  if (self.lastNeed > 1 && buf.length > 1) {
    if ((buf[1] & 0xc0) !== 0x80) {
      self.lastNeed = 1;
      return "\uFFFD";
    }
    if (self.lastNeed > 2 && buf.length > 2) {
      if ((buf[2] & 0xc0) !== 0x80) {
        self.lastNeed = 2;
        return "\uFFFD";
      }
    }
  }
}

// Attempts to complete a multi-byte UTF-8 character using bytes from a Buffer.
function utf8FillLast(buf) {
  var p = this.lastTotal - this.lastNeed;
  var r = utf8CheckExtraBytes(this, buf);
  if (r !== undefined) return r;
  if (this.lastNeed <= buf.length) {
    buf.copy(this.lastChar, p, 0, this.lastNeed);
    return this.lastChar.toString(this.encoding, 0, this.lastTotal);
  }
  buf.copy(this.lastChar, p, 0, buf.length);
  this.lastNeed -= buf.length;
}

// Returns all complete UTF-8 characters in a Buffer. If the Buffer ended on a
// partial character, the character's bytes are buffered until the required
// number of bytes are available.
function utf8Text(buf, i) {
  var total = utf8CheckIncomplete(this, buf, i);
  if (!this.lastNeed) return buf.toString("utf8", i);
  this.lastTotal = total;
  var end = buf.length - (total - this.lastNeed);
  buf.copy(this.lastChar, 0, end);
  return buf.toString("utf8", i, end);
}

// For UTF-8, a replacement character is added when ending on a partial
// character.
function utf8End(buf) {
  var r = buf && buf.length ? this.write(buf) : "";
  if (this.lastNeed) return r + "\uFFFD";
  return r;
}

// UTF-16LE typically needs two bytes per character, but even if we have an even
// number of bytes available, we need to check if we end on a leading/high
// surrogate. In that case, we need to wait for the next two bytes in order to
// decode the last character properly.
function utf16Text(buf, i) {
  if ((buf.length - i) % 2 === 0) {
    var r = buf.toString("utf16le", i);
    if (r) {
      var c = r.charCodeAt(r.length - 1);
      if (c >= 0xd800 && c <= 0xdbff) {
        this.lastNeed = 2;
        this.lastTotal = 4;
        this.lastChar[0] = buf[buf.length - 2];
        this.lastChar[1] = buf[buf.length - 1];
        return r.slice(0, -1);
      }
    }
    return r;
  }
  this.lastNeed = 1;
  this.lastTotal = 2;
  this.lastChar[0] = buf[buf.length - 1];
  return buf.toString("utf16le", i, buf.length - 1);
}

// For UTF-16LE we do not explicitly append special replacement characters if we
// end on a partial character, we simply let v8 handle that.
function utf16End(buf) {
  var r = buf && buf.length ? this.write(buf) : "";
  if (this.lastNeed) {
    var end = this.lastTotal - this.lastNeed;
    return r + this.lastChar.toString("utf16le", 0, end);
  }
  return r;
}
function base64Text(buf, i) {
  var n = (buf.length - i) % 3;
  if (n === 0) return buf.toString("base64", i);
  this.lastNeed = 3 - n;
  this.lastTotal = 3;
  if (n === 1) {
    this.lastChar[0] = buf[buf.length - 1];
  } else {
    this.lastChar[0] = buf[buf.length - 2];
    this.lastChar[1] = buf[buf.length - 1];
  }
  return buf.toString("base64", i, buf.length - n);
}
function base64End(buf) {
  var r = buf && buf.length ? this.write(buf) : "";
  if (this.lastNeed) return r + this.lastChar.toString("base64", 0, 3 - this.lastNeed);
  return r;
}

// Pass bytes on through for single-byte encodings (e.g. ascii, latin1, hex)
function simpleWrite(buf) {
  return buf.toString(this.encoding);
}
function simpleEnd(buf) {
  return buf && buf.length ? this.write(buf) : "";
}

var r = /*#__PURE__*/Object.freeze({
    __proto__: null,
    StringDecoder: StringDecoder_1,
    default: string_decoder
});

var o = {
    920: (e, t, n) => {
      const { AbortError: r, codes: o } = n(994),
        i = n(355),
        { ERR_INVALID_ARG_TYPE: a } = o;
      (e.exports.addAbortSignal = function (t, n) {
        if (
          (((e, t) => {
            if ("object" != typeof e || !("aborted" in e))
              throw new a("signal", "AbortSignal", e);
          })(t),
          !(r = n) || "function" != typeof r.pipe)
        )
          throw new a("stream", "stream.Stream", n);
        var r;
        return e.exports.addAbortSignalNoValidate(t, n);
      }),
        (e.exports.addAbortSignalNoValidate = function (e, t) {
          if ("object" != typeof e || !("aborted" in e)) return t;
          const n = () => {
            t.destroy(new r(void 0, { cause: e.reason }));
          };
          return (
            e.aborted
              ? n()
              : (e.addEventListener("abort", n),
                i(t, () => e.removeEventListener("abort", n))),
            t
          );
        });
    },
    340: (e, t, n) => {
      const {
          StringPrototypeSlice: r,
          SymbolIterator: o,
          TypedArrayPrototypeSet: i,
          Uint8Array: a,
        } = n(557),
        { Buffer: l } = n(368),
        { inspect: s } = n(501);
      e.exports = class {
        constructor() {
          (this.head = null), (this.tail = null), (this.length = 0);
        }
        push(e) {
          const t = { data: e, next: null };
          this.length > 0 ? (this.tail.next = t) : (this.head = t),
            (this.tail = t),
            ++this.length;
        }
        unshift(e) {
          const t = { data: e, next: this.head };
          0 === this.length && (this.tail = t), (this.head = t), ++this.length;
        }
        shift() {
          if (0 === this.length) return;
          const e = this.head.data;
          return (
            1 === this.length
              ? (this.head = this.tail = null)
              : (this.head = this.head.next),
            --this.length,
            e
          );
        }
        clear() {
          (this.head = this.tail = null), (this.length = 0);
        }
        join(e) {
          if (0 === this.length) return "";
          let t = this.head,
            n = "" + t.data;
          for (; null !== (t = t.next); ) n += e + t.data;
          return n;
        }
        concat(e) {
          if (0 === this.length) return l.alloc(0);
          const t = l.allocUnsafe(e >>> 0);
          let n = this.head,
            r = 0;
          for (; n; ) i(t, n.data, r), (r += n.data.length), (n = n.next);
          return t;
        }
        consume(e, t) {
          const n = this.head.data;
          if (e < n.length) {
            const t = n.slice(0, e);
            return (this.head.data = n.slice(e)), t;
          }
          return e === n.length
            ? this.shift()
            : t
            ? this._getString(e)
            : this._getBuffer(e);
        }
        first() {
          return this.head.data;
        }
        *[o]() {
          for (let e = this.head; e; e = e.next) yield e.data;
        }
        _getString(e) {
          let t = "",
            n = this.head,
            o = 0;
          do {
            const i = n.data;
            if (!(e > i.length)) {
              e === i.length
                ? ((t += i),
                  ++o,
                  n.next
                    ? (this.head = n.next)
                    : (this.head = this.tail = null))
                : ((t += r(i, 0, e)), (this.head = n), (n.data = r(i, e)));
              break;
            }
            (t += i), (e -= i.length), ++o;
          } while (null !== (n = n.next));
          return (this.length -= o), t;
        }
        _getBuffer(e) {
          const t = l.allocUnsafe(e),
            n = e;
          let r = this.head,
            o = 0;
          do {
            const l = r.data;
            if (!(e > l.length)) {
              e === l.length
                ? (i(t, l, n - e),
                  ++o,
                  r.next
                    ? (this.head = r.next)
                    : (this.head = this.tail = null))
                : (i(t, new a(l.buffer, l.byteOffset, e), n - e),
                  (this.head = r),
                  (r.data = l.slice(e)));
              break;
            }
            i(t, l, n - e), (e -= l.length), ++o;
          } while (null !== (r = r.next));
          return (this.length -= o), t;
        }
        [Symbol.for("nodejs.util.inspect.custom")](e, t) {
          return s(this, { ...t, depth: 0, customInspect: !1 });
        }
      };
    },
    262: (e, t, n) => {
      const { pipeline: r } = n(792),
        o = n(438),
        { destroyer: i } = n(701),
        { isNodeStream: a, isReadable: l, isWritable: s } = n(433),
        {
          AbortError: u,
          codes: { ERR_INVALID_ARG_VALUE: d, ERR_MISSING_ARGS: c },
        } = n(994);
      e.exports = function (...e) {
        if (0 === e.length) throw new c("streams");
        if (1 === e.length) return o.from(e[0]);
        const t = [...e];
        if (
          ("function" == typeof e[0] && (e[0] = o.from(e[0])),
          "function" == typeof e[e.length - 1])
        ) {
          const t = e.length - 1;
          e[t] = o.from(e[t]);
        }
        for (let n = 0; n < e.length; ++n)
          if (a(e[n])) {
            if (n < e.length - 1 && !l(e[n]))
              throw new d(`streams[${n}]`, t[n], "must be readable");
            if (n > 0 && !s(e[n]))
              throw new d(`streams[${n}]`, t[n], "must be writable");
          }
        let n, f, b, h, p;
        const y = e[0],
          g = r(e, function (e) {
            const t = h;
            (h = null), t ? t(e) : e ? p.destroy(e) : w || _ || p.destroy();
          }),
          _ = !!s(y),
          w = !!l(g);
        return (
          (p = new o({
            writableObjectMode: !(null == y || !y.writableObjectMode),
            readableObjectMode: !(null == g || !g.writableObjectMode),
            writable: _,
            readable: w,
          })),
          _ &&
            ((p._write = function (e, t, r) {
              y.write(e, t) ? r() : (n = r);
            }),
            (p._final = function (e) {
              y.end(), (f = e);
            }),
            y.on("drain", function () {
              if (n) {
                const e = n;
                (n = null), e();
              }
            }),
            g.on("finish", function () {
              if (f) {
                const e = f;
                (f = null), e();
              }
            })),
          w &&
            (g.on("readable", function () {
              if (b) {
                const e = b;
                (b = null), e();
              }
            }),
            g.on("end", function () {
              p.push(null);
            }),
            (p._read = function () {
              for (;;) {
                const e = g.read();
                if (null === e) return void (b = p._read);
                if (!p.push(e)) return;
              }
            })),
          (p._destroy = function (e, t) {
            e || null === h || (e = new u()),
              (b = null),
              (n = null),
              (f = null),
              null === h ? t(e) : ((h = t), i(g, e));
          }),
          p
        );
      };
    },
    701: (e, t, n) => {
      const r = n(394),
        {
          aggregateTwoErrors: o,
          codes: { ERR_MULTIPLE_CALLBACK: i },
          AbortError: a,
        } = n(994),
        { Symbol: l } = n(557),
        {
          kDestroyed: s,
          isDestroyed: u,
          isFinished: d,
          isServerRequest: c,
        } = n(433),
        f = l("kDestroy"),
        b = l("kConstruct");
      function h(e, t, n) {
        e &&
          (e.stack,
          t && !t.errored && (t.errored = e),
          n && !n.errored && (n.errored = e));
      }
      function p(e, t, n) {
        let o = !1;
        function i(t) {
          if (o) return;
          o = !0;
          const i = e._readableState,
            a = e._writableState;
          h(t, a, i),
            a && (a.closed = !0),
            i && (i.closed = !0),
            "function" == typeof n && n(t),
            t ? r.nextTick(y, e, t) : r.nextTick(g, e);
        }
        try {
          e._destroy(t || null, i);
        } catch (t) {
          i(t);
        }
      }
      function y(e, t) {
        _(e, t), g(e);
      }
      function g(e) {
        const t = e._readableState,
          n = e._writableState;
        n && (n.closeEmitted = !0),
          t && (t.closeEmitted = !0),
          ((n && n.emitClose) || (t && t.emitClose)) && e.emit("close");
      }
      function _(e, t) {
        const n = e._readableState,
          r = e._writableState;
        (r && r.errorEmitted) ||
          (n && n.errorEmitted) ||
          (r && (r.errorEmitted = !0),
          n && (n.errorEmitted = !0),
          e.emit("error", t));
      }
      function w(e, t, n) {
        const o = e._readableState,
          i = e._writableState;
        if ((i && i.destroyed) || (o && o.destroyed)) return this;
        (o && o.autoDestroy) || (i && i.autoDestroy)
          ? e.destroy(t)
          : t &&
            (t.stack,
            i && !i.errored && (i.errored = t),
            o && !o.errored && (o.errored = t),
            n ? r.nextTick(_, e, t) : _(e, t));
      }
      function m(e) {
        let t = !1;
        function n(n) {
          if (t) return void w(e, null != n ? n : new i());
          t = !0;
          const o = e._readableState,
            a = e._writableState,
            l = a || o;
          o && (o.constructed = !0),
            a && (a.constructed = !0),
            l.destroyed ? e.emit(f, n) : n ? w(e, n, !0) : r.nextTick(E, e);
        }
        try {
          e._construct(n);
        } catch (e) {
          n(e);
        }
      }
      function E(e) {
        e.emit(b);
      }
      function S(e) {
        return e && e.setHeader && "function" == typeof e.abort;
      }
      function v(e) {
        e.emit("close");
      }
      function R(e, t) {
        e.emit("error", t), r.nextTick(v, e);
      }
      e.exports = {
        construct: function (e, t) {
          if ("function" != typeof e._construct) return;
          const n = e._readableState,
            o = e._writableState;
          n && (n.constructed = !1),
            o && (o.constructed = !1),
            e.once(b, t),
            e.listenerCount(b) > 1 || r.nextTick(m, e);
        },
        destroyer: function (e, t) {
          e &&
            !u(e) &&
            (t || d(e) || (t = new a()),
            c(e)
              ? ((e.socket = null), e.destroy(t))
              : S(e)
              ? e.abort()
              : S(e.req)
              ? e.req.abort()
              : "function" == typeof e.destroy
              ? e.destroy(t)
              : "function" == typeof e.close
              ? e.close()
              : t
              ? r.nextTick(R, e, t)
              : r.nextTick(v, e),
            e.destroyed || (e[s] = !0));
        },
        destroy: function (e, t) {
          const n = this._readableState,
            r = this._writableState,
            i = r || n;
          return (r && r.destroyed) || (n && n.destroyed)
            ? ("function" == typeof t && t(), this)
            : (h(e, r, n),
              r && (r.destroyed = !0),
              n && (n.destroyed = !0),
              i.constructed
                ? p(this, e, t)
                : this.once(f, function (n) {
                    p(this, o(n, e), t);
                  }),
              this);
        },
        undestroy: function () {
          const e = this._readableState,
            t = this._writableState;
          e &&
            ((e.constructed = !0),
            (e.closed = !1),
            (e.closeEmitted = !1),
            (e.destroyed = !1),
            (e.errored = null),
            (e.errorEmitted = !1),
            (e.reading = !1),
            (e.ended = !1 === e.readable),
            (e.endEmitted = !1 === e.readable)),
            t &&
              ((t.constructed = !0),
              (t.destroyed = !1),
              (t.closed = !1),
              (t.closeEmitted = !1),
              (t.errored = null),
              (t.errorEmitted = !1),
              (t.finalCalled = !1),
              (t.prefinished = !1),
              (t.ended = !1 === t.writable),
              (t.ending = !1 === t.writable),
              (t.finished = !1 === t.writable));
        },
        errorOrDestroy: w,
      };
    },
    438: (e, t, n) => {
      const {
        ObjectDefineProperties: r,
        ObjectGetOwnPropertyDescriptor: o,
        ObjectKeys: i,
        ObjectSetPrototypeOf: a,
      } = n(557);
      e.exports = u;
      const l = n(392),
        s = n(772);
      a(u.prototype, l.prototype), a(u, l);
      {
        const e = i(s.prototype);
        for (let t = 0; t < e.length; t++) {
          const n = e[t];
          u.prototype[n] || (u.prototype[n] = s.prototype[n]);
        }
      }
      function u(e) {
        if (!(this instanceof u)) return new u(e);
        l.call(this, e),
          s.call(this, e),
          e
            ? ((this.allowHalfOpen = !1 !== e.allowHalfOpen),
              !1 === e.readable &&
                ((this._readableState.readable = !1),
                (this._readableState.ended = !0),
                (this._readableState.endEmitted = !0)),
              !1 === e.writable &&
                ((this._writableState.writable = !1),
                (this._writableState.ending = !0),
                (this._writableState.ended = !0),
                (this._writableState.finished = !0)))
            : (this.allowHalfOpen = !0);
      }
      let d, c;
      function f() {
        return void 0 === d && (d = {}), d;
      }
      r(u.prototype, {
        writable: { __proto__: null, ...o(s.prototype, "writable") },
        writableHighWaterMark: {
          __proto__: null,
          ...o(s.prototype, "writableHighWaterMark"),
        },
        writableObjectMode: {
          __proto__: null,
          ...o(s.prototype, "writableObjectMode"),
        },
        writableBuffer: {
          __proto__: null,
          ...o(s.prototype, "writableBuffer"),
        },
        writableLength: {
          __proto__: null,
          ...o(s.prototype, "writableLength"),
        },
        writableFinished: {
          __proto__: null,
          ...o(s.prototype, "writableFinished"),
        },
        writableCorked: {
          __proto__: null,
          ...o(s.prototype, "writableCorked"),
        },
        writableEnded: { __proto__: null, ...o(s.prototype, "writableEnded") },
        writableNeedDrain: {
          __proto__: null,
          ...o(s.prototype, "writableNeedDrain"),
        },
        destroyed: {
          __proto__: null,
          get() {
            return (
              void 0 !== this._readableState &&
              void 0 !== this._writableState &&
              this._readableState.destroyed &&
              this._writableState.destroyed
            );
          },
          set(e) {
            this._readableState &&
              this._writableState &&
              ((this._readableState.destroyed = e),
              (this._writableState.destroyed = e));
          },
        },
      }),
        (u.fromWeb = function (e, t) {
          return f().newStreamDuplexFromReadableWritablePair(e, t);
        }),
        (u.toWeb = function (e) {
          return f().newReadableWritablePairFromDuplex(e);
        }),
        (u.from = function (e) {
          return c || (c = n(873)), c(e, "body");
        });
    },
    873: (e, t, n) => {
      const r = n(394),
        o = n(368),
        {
          isReadable: i,
          isWritable: a,
          isIterable: l,
          isNodeStream: s,
          isReadableNodeStream: u,
          isWritableNodeStream: d,
          isDuplexNodeStream: c,
        } = n(433),
        f = n(355),
        {
          AbortError: b,
          codes: { ERR_INVALID_ARG_TYPE: h, ERR_INVALID_RETURN_VALUE: p },
        } = n(994),
        { destroyer: y } = n(701),
        g = n(438),
        _ = n(392),
        { createDeferredPromise: w } = n(501),
        m = n(533),
        E = globalThis.Blob || o.Blob,
        S =
          void 0 !== E
            ? function (e) {
                return e instanceof E;
              }
            : function (e) {
                return !1;
              },
        v = globalThis.AbortController || n(499).AbortController,
        { FunctionPrototypeCall: R } = n(557);
      class A extends g {
        constructor(e) {
          super(e),
            !1 === (null == e ? void 0 : e.readable) &&
              ((this._readableState.readable = !1),
              (this._readableState.ended = !0),
              (this._readableState.endEmitted = !0)),
            !1 === (null == e ? void 0 : e.writable) &&
              ((this._writableState.writable = !1),
              (this._writableState.ending = !0),
              (this._writableState.ended = !0),
              (this._writableState.finished = !0));
        }
      }
      function T(e) {
        const t =
            e.readable && "function" != typeof e.readable.read
              ? _.wrap(e.readable)
              : e.readable,
          n = e.writable;
        let r,
          o,
          l,
          s,
          u,
          d = !!i(t),
          c = !!a(n);
        function h(e) {
          const t = s;
          (s = null), t ? t(e) : e ? u.destroy(e) : d || c || u.destroy();
        }
        return (
          (u = new A({
            readableObjectMode: !(null == t || !t.readableObjectMode),
            writableObjectMode: !(null == n || !n.writableObjectMode),
            readable: d,
            writable: c,
          })),
          c &&
            (f(n, (e) => {
              (c = !1), e && y(t, e), h(e);
            }),
            (u._write = function (e, t, o) {
              n.write(e, t) ? o() : (r = o);
            }),
            (u._final = function (e) {
              n.end(), (o = e);
            }),
            n.on("drain", function () {
              if (r) {
                const e = r;
                (r = null), e();
              }
            }),
            n.on("finish", function () {
              if (o) {
                const e = o;
                (o = null), e();
              }
            })),
          d &&
            (f(t, (e) => {
              (d = !1), e && y(t, e), h(e);
            }),
            t.on("readable", function () {
              if (l) {
                const e = l;
                (l = null), e();
              }
            }),
            t.on("end", function () {
              u.push(null);
            }),
            (u._read = function () {
              for (;;) {
                const e = t.read();
                if (null === e) return void (l = u._read);
                if (!u.push(e)) return;
              }
            })),
          (u._destroy = function (e, i) {
            e || null === s || (e = new b()),
              (l = null),
              (r = null),
              (o = null),
              null === s ? i(e) : ((s = i), y(n, e), y(t, e));
          }),
          u
        );
      }
      e.exports = function e(t, n) {
        if (c(t)) return t;
        if (u(t)) return T({ readable: t });
        if (d(t)) return T({ writable: t });
        if (s(t)) return T({ writable: !1, readable: !1 });
        if ("function" == typeof t) {
          const {
            value: e,
            write: o,
            final: i,
            destroy: a,
          } = (function (e) {
            let { promise: t, resolve: n } = w();
            const o = new v(),
              i = o.signal;
            return {
              value: e(
                (async function* () {
                  for (;;) {
                    const e = t;
                    t = null;
                    const { chunk: o, done: a, cb: l } = await e;
                    if ((r.nextTick(l), a)) return;
                    if (i.aborted) throw new b(void 0, { cause: i.reason });
                    (({ promise: t, resolve: n } = w())), yield o;
                  }
                })(),
                { signal: i }
              ),
              write(e, t, r) {
                const o = n;
                (n = null), o({ chunk: e, done: !1, cb: r });
              },
              final(e) {
                const t = n;
                (n = null), t({ done: !0, cb: e });
              },
              destroy(e, t) {
                o.abort(), t(e);
              },
            };
          })(t);
          if (l(e))
            return m(A, e, { objectMode: !0, write: o, final: i, destroy: a });
          const s = null == e ? void 0 : e.then;
          if ("function" == typeof s) {
            let t;
            const n = R(
              s,
              e,
              (e) => {
                if (null != e) throw new p("nully", "body", e);
              },
              (e) => {
                y(t, e);
              }
            );
            return (t = new A({
              objectMode: !0,
              readable: !1,
              write: o,
              final(e) {
                i(async () => {
                  try {
                    await n, r.nextTick(e, null);
                  } catch (t) {
                    r.nextTick(e, t);
                  }
                });
              },
              destroy: a,
            }));
          }
          throw new p("Iterable, AsyncIterable or AsyncFunction", n, e);
        }
        if (S(t)) return e(t.arrayBuffer());
        if (l(t)) return m(A, t, { objectMode: !0, writable: !1 });
        if (
          "object" == typeof (null == t ? void 0 : t.writable) ||
          "object" == typeof (null == t ? void 0 : t.readable)
        )
          return T({
            readable:
              null != t && t.readable
                ? u(null == t ? void 0 : t.readable)
                  ? null == t
                    ? void 0
                    : t.readable
                  : e(t.readable)
                : void 0,
            writable:
              null != t && t.writable
                ? d(null == t ? void 0 : t.writable)
                  ? null == t
                    ? void 0
                    : t.writable
                  : e(t.writable)
                : void 0,
          });
        const o = null == t ? void 0 : t.then;
        if ("function" == typeof o) {
          let e;
          return (
            R(
              o,
              t,
              (t) => {
                null != t && e.push(t), e.push(null);
              },
              (t) => {
                y(e, t);
              }
            ),
            (e = new A({ objectMode: !0, writable: !1, read() {} }))
          );
        }
        throw new h(
          n,
          [
            "Blob",
            "ReadableStream",
            "WritableStream",
            "Stream",
            "Iterable",
            "AsyncIterable",
            "Function",
            "{ readable, writable } pair",
            "Promise",
          ],
          t
        );
      };
    },
    355: (e, t, n) => {
      const r = n(394),
        { AbortError: o, codes: i } = n(994),
        { ERR_INVALID_ARG_TYPE: a, ERR_STREAM_PREMATURE_CLOSE: l } = i,
        { kEmptyObject: s, once: u } = n(501),
        {
          validateAbortSignal: d,
          validateFunction: c,
          validateObject: f,
        } = n(215),
        { Promise: b } = n(557),
        {
          isClosed: h,
          isReadable: p,
          isReadableNodeStream: y,
          isReadableFinished: g,
          isReadableErrored: _,
          isWritable: w,
          isWritableNodeStream: m,
          isWritableFinished: E,
          isWritableErrored: S,
          isNodeStream: v,
          willEmitClose: R,
        } = n(433),
        A = () => {};
      function T(e, t, n) {
        var i, b;
        2 === arguments.length
          ? ((n = t), (t = s))
          : null == t
          ? (t = s)
          : f(t, "options"),
          c(n, "callback"),
          d(t.signal, "options.signal"),
          (n = u(n));
        const T = null !== (i = t.readable) && void 0 !== i ? i : y(e),
          I = null !== (b = t.writable) && void 0 !== b ? b : m(e);
        if (!v(e)) throw new a("stream", "Stream", e);
        const k = e._writableState,
          O = e._readableState,
          P = () => {
            e.writable || N();
          };
        let M = R(e) && y(e) === T && m(e) === I,
          x = E(e, !1);
        const N = () => {
          (x = !0),
            e.destroyed && (M = !1),
            (!M || (e.readable && !T)) && ((T && !j) || n.call(e));
        };
        let j = g(e, !1);
        const D = () => {
            (j = !0),
              e.destroyed && (M = !1),
              (!M || (e.writable && !I)) && ((I && !x) || n.call(e));
          },
          L = (t) => {
            n.call(e, t);
          };
        let W = h(e);
        const $ = () => {
            W = !0;
            const t = S(e) || _(e);
            return t && "boolean" != typeof t
              ? n.call(e, t)
              : T && !j && y(e, !0) && !g(e, !1)
              ? n.call(e, new l())
              : !I || x || E(e, !1)
              ? void n.call(e)
              : n.call(e, new l());
          },
          C = () => {
            e.req.on("finish", N);
          };
        !(function (e) {
          return e.setHeader && "function" == typeof e.abort;
        })(e)
          ? I && !k && (e.on("end", P), e.on("close", P))
          : (e.on("complete", N),
            M || e.on("abort", $),
            e.req ? C() : e.on("request", C)),
          M || "boolean" != typeof e.aborted || e.on("aborted", $),
          e.on("end", D),
          e.on("finish", N),
          !1 !== t.error && e.on("error", L),
          e.on("close", $),
          W
            ? r.nextTick($)
            : (null != k && k.errorEmitted) || (null != O && O.errorEmitted)
            ? M || r.nextTick($)
            : (T || (M && !p(e)) || (!x && !1 !== w(e))) &&
              (I || (M && !w(e)) || (!j && !1 !== p(e)))
            ? O && e.req && e.aborted && r.nextTick($)
            : r.nextTick($);
        const F = () => {
          (n = A),
            e.removeListener("aborted", $),
            e.removeListener("complete", N),
            e.removeListener("abort", $),
            e.removeListener("request", C),
            e.req && e.req.removeListener("finish", N),
            e.removeListener("end", P),
            e.removeListener("close", P),
            e.removeListener("finish", N),
            e.removeListener("end", D),
            e.removeListener("error", L),
            e.removeListener("close", $);
        };
        if (t.signal && !W) {
          const i = () => {
            const r = n;
            F(), r.call(e, new o(void 0, { cause: t.signal.reason }));
          };
          if (t.signal.aborted) r.nextTick(i);
          else {
            const r = n;
            (n = u((...n) => {
              t.signal.removeEventListener("abort", i), r.apply(e, n);
            })),
              t.signal.addEventListener("abort", i);
          }
        }
        return F;
      }
      (e.exports = T),
        (e.exports.finished = function (e, t) {
          return new b((n, r) => {
            T(e, t, (e) => {
              e ? r(e) : n();
            });
          });
        });
    },
    533: (e, t, n) => {
      const r = n(394),
        {
          PromisePrototypeThen: o,
          SymbolAsyncIterator: i,
          SymbolIterator: a,
        } = n(557),
        { Buffer: l } = n(368),
        { ERR_INVALID_ARG_TYPE: s, ERR_STREAM_NULL_VALUES: u } = n(994).codes;
      e.exports = function (e, t, n) {
        let d, c;
        if ("string" == typeof t || t instanceof l)
          return new e({
            objectMode: !0,
            ...n,
            read() {
              this.push(t), this.push(null);
            },
          });
        if (t && t[i]) (c = !0), (d = t[i]());
        else {
          if (!t || !t[a]) throw new s("iterable", ["Iterable"], t);
          (c = !1), (d = t[a]());
        }
        const f = new e({ objectMode: !0, highWaterMark: 1, ...n });
        let b = !1;
        return (
          (f._read = function () {
            b ||
              ((b = !0),
              (async function () {
                for (;;) {
                  try {
                    const { value: e, done: t } = c ? await d.next() : d.next();
                    if (t) f.push(null);
                    else {
                      const t = e && "function" == typeof e.then ? await e : e;
                      if (null === t) throw ((b = !1), new u());
                      if (f.push(t)) continue;
                      b = !1;
                    }
                  } catch (e) {
                    f.destroy(e);
                  }
                  break;
                }
              })());
          }),
          (f._destroy = function (e, t) {
            o(
              (async function (e) {
                const t = null != e,
                  n = "function" == typeof d.throw;
                if (t && n) {
                  const { value: t, done: n } = await d.throw(e);
                  if ((await t, n)) return;
                }
                if ("function" == typeof d.return) {
                  const { value: e } = await d.return();
                  await e;
                }
              })(e),
              () => r.nextTick(t, e),
              (n) => r.nextTick(t, n || e)
            );
          }),
          f
        );
      };
    },
    693: (e, t, n) => {
      const { ArrayIsArray: r, ObjectSetPrototypeOf: o } = n(557),
        { EventEmitter: i } = n(178);
      function a(e) {
        i.call(this, e);
      }
      function l(e, t, n) {
        if ("function" == typeof e.prependListener)
          return e.prependListener(t, n);
        e._events && e._events[t]
          ? r(e._events[t])
            ? e._events[t].unshift(n)
            : (e._events[t] = [n, e._events[t]])
          : e.on(t, n);
      }
      o(a.prototype, i.prototype),
        o(a, i),
        (a.prototype.pipe = function (e, t) {
          const n = this;
          function r(t) {
            e.writable && !1 === e.write(t) && n.pause && n.pause();
          }
          function o() {
            n.readable && n.resume && n.resume();
          }
          n.on("data", r),
            e.on("drain", o),
            e._isStdio ||
              (t && !1 === t.end) ||
              (n.on("end", s), n.on("close", u));
          let a = !1;
          function s() {
            a || ((a = !0), e.end());
          }
          function u() {
            a || ((a = !0), "function" == typeof e.destroy && e.destroy());
          }
          function d(e) {
            c(), 0 === i.listenerCount(this, "error") && this.emit("error", e);
          }
          function c() {
            n.removeListener("data", r),
              e.removeListener("drain", o),
              n.removeListener("end", s),
              n.removeListener("close", u),
              n.removeListener("error", d),
              e.removeListener("error", d),
              n.removeListener("end", c),
              n.removeListener("close", c),
              e.removeListener("close", c);
          }
          return (
            l(n, "error", d),
            l(e, "error", d),
            n.on("end", c),
            n.on("close", c),
            e.on("close", c),
            e.emit("pipe", n),
            e
          );
        }),
        (e.exports = { Stream: a, prependListener: l });
    },
    723: (e, t, n) => {
      const r = globalThis.AbortController || n(499).AbortController,
        {
          codes: {
            ERR_INVALID_ARG_TYPE: o,
            ERR_MISSING_ARGS: i,
            ERR_OUT_OF_RANGE: a,
          },
          AbortError: l,
        } = n(994),
        {
          validateAbortSignal: s,
          validateInteger: u,
          validateObject: d,
        } = n(215),
        c = n(557).Symbol("kWeak"),
        { finished: f } = n(355),
        {
          ArrayPrototypePush: b,
          MathFloor: h,
          Number: p,
          NumberIsNaN: y,
          Promise: g,
          PromiseReject: _,
          PromisePrototypeThen: w,
          Symbol: m,
        } = n(557),
        E = m("kEmpty"),
        S = m("kEof");
      function v(e, t) {
        if ("function" != typeof e)
          throw new o("fn", ["Function", "AsyncFunction"], e);
        null != t && d(t, "options"),
          null != (null == t ? void 0 : t.signal) &&
            s(t.signal, "options.signal");
        let n = 1;
        return (
          null != (null == t ? void 0 : t.concurrency) &&
            (n = h(t.concurrency)),
          u(n, "concurrency", 1),
          async function* () {
            var o, i;
            const a = new r(),
              s = this,
              u = [],
              d = a.signal,
              c = { signal: d },
              f = () => a.abort();
            let b, h;
            null != t &&
              null !== (o = t.signal) &&
              void 0 !== o &&
              o.aborted &&
              f(),
              null == t ||
                null === (i = t.signal) ||
                void 0 === i ||
                i.addEventListener("abort", f);
            let p = !1;
            function y() {
              p = !0;
            }
            !(async function () {
              try {
                for await (let t of s) {
                  var r;
                  if (p) return;
                  if (d.aborted) throw new l();
                  try {
                    t = e(t, c);
                  } catch (e) {
                    t = _(e);
                  }
                  t !== E &&
                    ("function" ==
                      typeof (null === (r = t) || void 0 === r
                        ? void 0
                        : r.catch) && t.catch(y),
                    u.push(t),
                    b && (b(), (b = null)),
                    !p &&
                      u.length &&
                      u.length >= n &&
                      (await new g((e) => {
                        h = e;
                      })));
                }
                u.push(S);
              } catch (e) {
                const t = _(e);
                w(t, void 0, y), u.push(t);
              } finally {
                var o;
                (p = !0),
                  b && (b(), (b = null)),
                  null == t ||
                    null === (o = t.signal) ||
                    void 0 === o ||
                    o.removeEventListener("abort", f);
              }
            })();
            try {
              for (;;) {
                for (; u.length > 0; ) {
                  const e = await u[0];
                  if (e === S) return;
                  if (d.aborted) throw new l();
                  e !== E && (yield e), u.shift(), h && (h(), (h = null));
                }
                await new g((e) => {
                  b = e;
                });
              }
            } finally {
              a.abort(), (p = !0), h && (h(), (h = null));
            }
          }.call(this)
        );
      }
      async function R(e, t = void 0) {
        for await (const n of A.call(this, e, t)) return !0;
        return !1;
      }
      function A(e, t) {
        if ("function" != typeof e)
          throw new o("fn", ["Function", "AsyncFunction"], e);
        return v.call(
          this,
          async function (t, n) {
            return (await e(t, n)) ? t : E;
          },
          t
        );
      }
      class T extends i {
        constructor() {
          super("reduce"),
            (this.message =
              "Reduce of an empty stream requires an initial value");
        }
      }
      function I(e) {
        if (((e = p(e)), y(e))) return 0;
        if (e < 0) throw new a("number", ">= 0", e);
        return e;
      }
      (e.exports.streamReturningOperators = {
        asIndexedPairs: function (e = void 0) {
          return (
            null != e && d(e, "options"),
            null != (null == e ? void 0 : e.signal) &&
              s(e.signal, "options.signal"),
            async function* () {
              let t = 0;
              for await (const r of this) {
                var n;
                if (
                  null != e &&
                  null !== (n = e.signal) &&
                  void 0 !== n &&
                  n.aborted
                )
                  throw new l({ cause: e.signal.reason });
                yield [t++, r];
              }
            }.call(this)
          );
        },
        drop: function (e, t = void 0) {
          return (
            null != t && d(t, "options"),
            null != (null == t ? void 0 : t.signal) &&
              s(t.signal, "options.signal"),
            (e = I(e)),
            async function* () {
              var n;
              if (
                null != t &&
                null !== (n = t.signal) &&
                void 0 !== n &&
                n.aborted
              )
                throw new l();
              for await (const n of this) {
                var r;
                if (
                  null != t &&
                  null !== (r = t.signal) &&
                  void 0 !== r &&
                  r.aborted
                )
                  throw new l();
                e-- <= 0 && (yield n);
              }
            }.call(this)
          );
        },
        filter: A,
        flatMap: function (e, t) {
          const n = v.call(this, e, t);
          return async function* () {
            for await (const e of n) yield* e;
          }.call(this);
        },
        map: v,
        take: function (e, t = void 0) {
          return (
            null != t && d(t, "options"),
            null != (null == t ? void 0 : t.signal) &&
              s(t.signal, "options.signal"),
            (e = I(e)),
            async function* () {
              var n;
              if (
                null != t &&
                null !== (n = t.signal) &&
                void 0 !== n &&
                n.aborted
              )
                throw new l();
              for await (const n of this) {
                var r;
                if (
                  null != t &&
                  null !== (r = t.signal) &&
                  void 0 !== r &&
                  r.aborted
                )
                  throw new l();
                if (!(e-- > 0)) return;
                yield n;
              }
            }.call(this)
          );
        },
      }),
        (e.exports.promiseReturningOperators = {
          every: async function (e, t = void 0) {
            if ("function" != typeof e)
              throw new o("fn", ["Function", "AsyncFunction"], e);
            return !(await R.call(this, async (...t) => !(await e(...t)), t));
          },
          forEach: async function (e, t) {
            if ("function" != typeof e)
              throw new o("fn", ["Function", "AsyncFunction"], e);
            for await (const n of v.call(
              this,
              async function (t, n) {
                return await e(t, n), E;
              },
              t
            ));
          },
          reduce: async function (e, t, n) {
            var i;
            if ("function" != typeof e)
              throw new o("reducer", ["Function", "AsyncFunction"], e);
            null != n && d(n, "options"),
              null != (null == n ? void 0 : n.signal) &&
                s(n.signal, "options.signal");
            let a = arguments.length > 1;
            if (
              null != n &&
              null !== (i = n.signal) &&
              void 0 !== i &&
              i.aborted
            ) {
              const e = new l(void 0, { cause: n.signal.reason });
              throw (this.once("error", () => {}), await f(this.destroy(e)), e);
            }
            const u = new r(),
              b = u.signal;
            if (null != n && n.signal) {
              const e = { once: !0, [c]: this };
              n.signal.addEventListener("abort", () => u.abort(), e);
            }
            let h = !1;
            try {
              for await (const r of this) {
                var p;
                if (
                  ((h = !0),
                  null != n &&
                    null !== (p = n.signal) &&
                    void 0 !== p &&
                    p.aborted)
                )
                  throw new l();
                a ? (t = await e(t, r, { signal: b })) : ((t = r), (a = !0));
              }
              if (!h && !a) throw new T();
            } finally {
              u.abort();
            }
            return t;
          },
          toArray: async function (e) {
            null != e && d(e, "options"),
              null != (null == e ? void 0 : e.signal) &&
                s(e.signal, "options.signal");
            const t = [];
            for await (const r of this) {
              var n;
              if (
                null != e &&
                null !== (n = e.signal) &&
                void 0 !== n &&
                n.aborted
              )
                throw new l(void 0, { cause: e.signal.reason });
              b(t, r);
            }
            return t;
          },
          some: R,
          find: async function (e, t) {
            for await (const n of A.call(this, e, t)) return n;
          },
        });
    },
    107: (e, t, n) => {
      const { ObjectSetPrototypeOf: r } = n(557);
      e.exports = i;
      const o = n(744);
      function i(e) {
        if (!(this instanceof i)) return new i(e);
        o.call(this, e);
      }
      r(i.prototype, o.prototype),
        r(i, o),
        (i.prototype._transform = function (e, t, n) {
          n(null, e);
        });
    },
    792: (e, t, n) => {
      const r = n(394),
        { ArrayIsArray: o, Promise: i, SymbolAsyncIterator: a } = n(557),
        l = n(355),
        { once: s } = n(501),
        u = n(701),
        d = n(438),
        {
          aggregateTwoErrors: c,
          codes: {
            ERR_INVALID_ARG_TYPE: f,
            ERR_INVALID_RETURN_VALUE: b,
            ERR_MISSING_ARGS: h,
            ERR_STREAM_DESTROYED: p,
            ERR_STREAM_PREMATURE_CLOSE: y,
          },
          AbortError: g,
        } = n(994),
        { validateFunction: _, validateAbortSignal: w } = n(215),
        {
          isIterable: m,
          isReadable: E,
          isReadableNodeStream: S,
          isNodeStream: v,
        } = n(433),
        R = globalThis.AbortController || n(499).AbortController;
      let A, T;
      function I(e, t, n) {
        let r = !1;
        return (
          e.on("close", () => {
            r = !0;
          }),
          {
            destroy: (t) => {
              r || ((r = !0), u.destroyer(e, t || new p("pipe")));
            },
            cleanup: l(e, { readable: t, writable: n }, (e) => {
              r = !e;
            }),
          }
        );
      }
      function k(e) {
        if (m(e)) return e;
        if (S(e))
          return (async function* (e) {
            T || (T = n(392)), yield* T.prototype[a].call(e);
          })(e);
        throw new f("val", ["Readable", "Iterable", "AsyncIterable"], e);
      }
      async function O(e, t, n, { end: r }) {
        let o,
          a = null;
        const s = (e) => {
            if ((e && (o = e), a)) {
              const e = a;
              (a = null), e();
            }
          },
          u = () =>
            new i((e, t) => {
              o
                ? t(o)
                : (a = () => {
                    o ? t(o) : e();
                  });
            });
        t.on("drain", s);
        const d = l(t, { readable: !1 }, s);
        try {
          t.writableNeedDrain && (await u());
          for await (const n of e) t.write(n) || (await u());
          r && t.end(), await u(), n();
        } catch (e) {
          n(o !== e ? c(o, e) : e);
        } finally {
          d(), t.off("drain", s);
        }
      }
      function P(e, t, i) {
        if ((1 === e.length && o(e[0]) && (e = e[0]), e.length < 2))
          throw new h("streams");
        const a = new R(),
          l = a.signal,
          s = null == i ? void 0 : i.signal,
          u = [];
        function c() {
          N(new g());
        }
        let p, y;
        w(s, "options.signal"), null == s || s.addEventListener("abort", c);
        const _ = [];
        let T,
          P = 0;
        function x(e) {
          N(e, 0 == --P);
        }
        function N(e, n) {
          if (
            (!e || (p && "ERR_STREAM_PREMATURE_CLOSE" !== p.code) || (p = e),
            p || n)
          ) {
            for (; _.length; ) _.shift()(p);
            null == s || s.removeEventListener("abort", c),
              a.abort(),
              n && (p || u.forEach((e) => e()), r.nextTick(t, p, y));
          }
        }
        for (let L = 0; L < e.length; L++) {
          const W = e[L],
            $ = L < e.length - 1,
            C = L > 0,
            F = $ || !1 !== (null == i ? void 0 : i.end),
            U = L === e.length - 1;
          if (v(W)) {
            if (F) {
              const { destroy: B, cleanup: G } = I(W, $, C);
              _.push(B), E(W) && U && u.push(G);
            }
            function j(e) {
              e &&
                "AbortError" !== e.name &&
                "ERR_STREAM_PREMATURE_CLOSE" !== e.code &&
                x(e);
            }
            W.on("error", j),
              E(W) &&
                U &&
                u.push(() => {
                  W.removeListener("error", j);
                });
          }
          if (0 === L)
            if ("function" == typeof W) {
              if (((T = W({ signal: l })), !m(T)))
                throw new b("Iterable, AsyncIterable or Stream", "source", T);
            } else T = m(W) || S(W) ? W : d.from(W);
          else if ("function" == typeof W)
            if (((T = k(T)), (T = W(T, { signal: l })), $)) {
              if (!m(T, !0))
                throw new b("AsyncIterable", `transform[${L - 1}]`, T);
            } else {
              var D;
              A || (A = n(107));
              const H = new A({ objectMode: !0 }),
                V = null === (D = T) || void 0 === D ? void 0 : D.then;
              if ("function" == typeof V)
                P++,
                  V.call(
                    T,
                    (e) => {
                      (y = e),
                        null != e && H.write(e),
                        F && H.end(),
                        r.nextTick(x);
                    },
                    (e) => {
                      H.destroy(e), r.nextTick(x, e);
                    }
                  );
              else {
                if (!m(T, !0))
                  throw new b("AsyncIterable or Promise", "destination", T);
                P++, O(T, H, x, { end: F });
              }
              T = H;
              const { destroy: q, cleanup: Y } = I(T, !1, !0);
              _.push(q), U && u.push(Y);
            }
          else if (v(W)) {
            if (S(T)) {
              P += 2;
              const K = M(T, W, x, { end: F });
              E(W) && U && u.push(K);
            } else {
              if (!m(T))
                throw new f(
                  "val",
                  ["Readable", "Iterable", "AsyncIterable"],
                  T
                );
              P++, O(T, W, x, { end: F });
            }
            T = W;
          } else T = d.from(W);
        }
        return (
          ((null != l && l.aborted) || (null != s && s.aborted)) &&
            r.nextTick(c),
          T
        );
      }
      function M(e, t, n, { end: r }) {
        let o = !1;
        return (
          t.on("close", () => {
            o || n(new y());
          }),
          e.pipe(t, { end: r }),
          r
            ? e.once("end", () => {
                (o = !0), t.end();
              })
            : n(),
          l(e, { readable: !0, writable: !1 }, (t) => {
            const r = e._readableState;
            t &&
            "ERR_STREAM_PREMATURE_CLOSE" === t.code &&
            r &&
            r.ended &&
            !r.errored &&
            !r.errorEmitted
              ? e.once("end", n).once("error", n)
              : n(t);
          }),
          l(t, { readable: !1, writable: !0 }, n)
        );
      }
      e.exports = {
        pipelineImpl: P,
        pipeline: function (...e) {
          return P(
            e,
            s(
              (function (e) {
                return (
                  _(e[e.length - 1], "streams[stream.length - 1]"), e.pop()
                );
              })(e)
            )
          );
        },
      };
    },
    392: (e, t, n) => {
      const r = n(394),
        {
          ArrayPrototypeIndexOf: o,
          NumberIsInteger: i,
          NumberIsNaN: a,
          NumberParseInt: l,
          ObjectDefineProperties: s,
          ObjectKeys: u,
          ObjectSetPrototypeOf: d,
          Promise: c,
          SafeSet: f,
          SymbolAsyncIterator: b,
          Symbol: h,
        } = n(557);
      (e.exports = C), (C.ReadableState = $);
      const { EventEmitter: p } = n(178),
        { Stream: y, prependListener: g } = n(693),
        { Buffer: _ } = n(368),
        { addAbortSignal: w } = n(920),
        m = n(355);
      let E = n(501).debuglog("stream", (e) => {
        E = e;
      });
      const S = n(340),
        v = n(701),
        { getHighWaterMark: R, getDefaultHighWaterMark: A } = n(745),
        {
          aggregateTwoErrors: T,
          codes: {
            ERR_INVALID_ARG_TYPE: I,
            ERR_METHOD_NOT_IMPLEMENTED: k,
            ERR_OUT_OF_RANGE: O,
            ERR_STREAM_PUSH_AFTER_EOF: P,
            ERR_STREAM_UNSHIFT_AFTER_END_EVENT: M,
          },
        } = n(994),
        { validateObject: x } = n(215),
        N = h("kPaused"),
        { StringDecoder: j } = n(888),
        D = n(533);
      d(C.prototype, y.prototype), d(C, y);
      const L = () => {},
        { errorOrDestroy: W } = v;
      function $(e, t, r) {
        "boolean" != typeof r && (r = t instanceof n(438)),
          (this.objectMode = !(!e || !e.objectMode)),
          r &&
            (this.objectMode =
              this.objectMode || !(!e || !e.readableObjectMode)),
          (this.highWaterMark = e
            ? R(this, e, "readableHighWaterMark", r)
            : A(!1)),
          (this.buffer = new S()),
          (this.length = 0),
          (this.pipes = []),
          (this.flowing = null),
          (this.ended = !1),
          (this.endEmitted = !1),
          (this.reading = !1),
          (this.constructed = !0),
          (this.sync = !0),
          (this.needReadable = !1),
          (this.emittedReadable = !1),
          (this.readableListening = !1),
          (this.resumeScheduled = !1),
          (this[N] = null),
          (this.errorEmitted = !1),
          (this.emitClose = !e || !1 !== e.emitClose),
          (this.autoDestroy = !e || !1 !== e.autoDestroy),
          (this.destroyed = !1),
          (this.errored = null),
          (this.closed = !1),
          (this.closeEmitted = !1),
          (this.defaultEncoding = (e && e.defaultEncoding) || "utf8"),
          (this.awaitDrainWriters = null),
          (this.multiAwaitDrain = !1),
          (this.readingMore = !1),
          (this.dataEmitted = !1),
          (this.decoder = null),
          (this.encoding = null),
          e &&
            e.encoding &&
            ((this.decoder = new j(e.encoding)), (this.encoding = e.encoding));
      }
      function C(e) {
        if (!(this instanceof C)) return new C(e);
        const t = this instanceof n(438);
        (this._readableState = new $(e, this, t)),
          e &&
            ("function" == typeof e.read && (this._read = e.read),
            "function" == typeof e.destroy && (this._destroy = e.destroy),
            "function" == typeof e.construct && (this._construct = e.construct),
            e.signal && !t && w(e.signal, this)),
          y.call(this, e),
          v.construct(this, () => {
            this._readableState.needReadable && V(this, this._readableState);
          });
      }
      function F(e, t, n, r) {
        E("readableAddChunk", t);
        const o = e._readableState;
        let i;
        if (
          (o.objectMode ||
            ("string" == typeof t
              ? ((n = n || o.defaultEncoding),
                o.encoding !== n &&
                  (r && o.encoding
                    ? (t = _.from(t, n).toString(o.encoding))
                    : ((t = _.from(t, n)), (n = ""))))
              : t instanceof _
              ? (n = "")
              : y._isUint8Array(t)
              ? ((t = y._uint8ArrayToBuffer(t)), (n = ""))
              : null != t &&
                (i = new I("chunk", ["string", "Buffer", "Uint8Array"], t))),
          i)
        )
          W(e, i);
        else if (null === t)
          (o.reading = !1),
            (function (e, t) {
              if ((E("onEofChunk"), !t.ended)) {
                if (t.decoder) {
                  const e = t.decoder.end();
                  e &&
                    e.length &&
                    (t.buffer.push(e),
                    (t.length += t.objectMode ? 1 : e.length));
                }
                (t.ended = !0),
                  t.sync
                    ? G(e)
                    : ((t.needReadable = !1), (t.emittedReadable = !0), H(e));
              }
            })(e, o);
        else if (o.objectMode || (t && t.length > 0))
          if (r)
            if (o.endEmitted) W(e, new M());
            else {
              if (o.destroyed || o.errored) return !1;
              U(e, o, t, !0);
            }
          else if (o.ended) W(e, new P());
          else {
            if (o.destroyed || o.errored) return !1;
            (o.reading = !1),
              o.decoder && !n
                ? ((t = o.decoder.write(t)),
                  o.objectMode || 0 !== t.length ? U(e, o, t, !1) : V(e, o))
                : U(e, o, t, !1);
          }
        else r || ((o.reading = !1), V(e, o));
        return !o.ended && (o.length < o.highWaterMark || 0 === o.length);
      }
      function U(e, t, n, r) {
        t.flowing && 0 === t.length && !t.sync && e.listenerCount("data") > 0
          ? (t.multiAwaitDrain
              ? t.awaitDrainWriters.clear()
              : (t.awaitDrainWriters = null),
            (t.dataEmitted = !0),
            e.emit("data", n))
          : ((t.length += t.objectMode ? 1 : n.length),
            r ? t.buffer.unshift(n) : t.buffer.push(n),
            t.needReadable && G(e)),
          V(e, t);
      }
      function B(e, t) {
        return e <= 0 || (0 === t.length && t.ended)
          ? 0
          : t.objectMode
          ? 1
          : a(e)
          ? t.flowing && t.length
            ? t.buffer.first().length
            : t.length
          : e <= t.length
          ? e
          : t.ended
          ? t.length
          : 0;
      }
      function G(e) {
        const t = e._readableState;
        E("emitReadable", t.needReadable, t.emittedReadable),
          (t.needReadable = !1),
          t.emittedReadable ||
            (E("emitReadable", t.flowing),
            (t.emittedReadable = !0),
            r.nextTick(H, e));
      }
      function H(e) {
        const t = e._readableState;
        E("emitReadable_", t.destroyed, t.length, t.ended),
          t.destroyed ||
            t.errored ||
            (!t.length && !t.ended) ||
            (e.emit("readable"), (t.emittedReadable = !1)),
          (t.needReadable =
            !t.flowing && !t.ended && t.length <= t.highWaterMark),
          J(e);
      }
      function V(e, t) {
        !t.readingMore &&
          t.constructed &&
          ((t.readingMore = !0), r.nextTick(q, e, t));
      }
      function q(e, t) {
        for (
          ;
          !t.reading &&
          !t.ended &&
          (t.length < t.highWaterMark || (t.flowing && 0 === t.length));

        ) {
          const n = t.length;
          if ((E("maybeReadMore read 0"), e.read(0), n === t.length)) break;
        }
        t.readingMore = !1;
      }
      function Y(e) {
        const t = e._readableState;
        (t.readableListening = e.listenerCount("readable") > 0),
          t.resumeScheduled && !1 === t[N]
            ? (t.flowing = !0)
            : e.listenerCount("data") > 0
            ? e.resume()
            : t.readableListening || (t.flowing = null);
      }
      function K(e) {
        E("readable nexttick read 0"), e.read(0);
      }
      function z(e, t) {
        E("resume", t.reading),
          t.reading || e.read(0),
          (t.resumeScheduled = !1),
          e.emit("resume"),
          J(e),
          t.flowing && !t.reading && e.read(0);
      }
      function J(e) {
        const t = e._readableState;
        for (E("flow", t.flowing); t.flowing && null !== e.read(); );
      }
      function X(e, t) {
        "function" != typeof e.read && (e = C.wrap(e, { objectMode: !0 }));
        const n = (async function* (e, t) {
          let n,
            r = L;
          function o(t) {
            this === e ? (r(), (r = L)) : (r = t);
          }
          e.on("readable", o);
          const i = m(e, { writable: !1 }, (e) => {
            (n = e ? T(n, e) : null), r(), (r = L);
          });
          try {
            for (;;) {
              const t = e.destroyed ? null : e.read();
              if (null !== t) yield t;
              else {
                if (n) throw n;
                if (null === n) return;
                await new c(o);
              }
            }
          } catch (e) {
            throw ((n = T(n, e)), n);
          } finally {
            (!n && !1 === (null == t ? void 0 : t.destroyOnReturn)) ||
            (void 0 !== n && !e._readableState.autoDestroy)
              ? (e.off("readable", o), i())
              : v.destroyer(e, null);
          }
        })(e, t);
        return (n.stream = e), n;
      }
      function Z(e, t) {
        if (0 === t.length) return null;
        let n;
        return (
          t.objectMode
            ? (n = t.buffer.shift())
            : !e || e >= t.length
            ? ((n = t.decoder
                ? t.buffer.join("")
                : 1 === t.buffer.length
                ? t.buffer.first()
                : t.buffer.concat(t.length)),
              t.buffer.clear())
            : (n = t.buffer.consume(e, t.decoder)),
          n
        );
      }
      function Q(e) {
        const t = e._readableState;
        E("endReadable", t.endEmitted),
          t.endEmitted || ((t.ended = !0), r.nextTick(ee, t, e));
      }
      function ee(e, t) {
        if (
          (E("endReadableNT", e.endEmitted, e.length),
          !e.errored && !e.closeEmitted && !e.endEmitted && 0 === e.length)
        )
          if (
            ((e.endEmitted = !0),
            t.emit("end"),
            t.writable && !1 === t.allowHalfOpen)
          )
            r.nextTick(te, t);
          else if (e.autoDestroy) {
            const e = t._writableState;
            (!e || (e.autoDestroy && (e.finished || !1 === e.writable))) &&
              t.destroy();
          }
      }
      function te(e) {
        e.writable && !e.writableEnded && !e.destroyed && e.end();
      }
      let ne;
      function re() {
        return void 0 === ne && (ne = {}), ne;
      }
      (C.prototype.destroy = v.destroy),
        (C.prototype._undestroy = v.undestroy),
        (C.prototype._destroy = function (e, t) {
          t(e);
        }),
        (C.prototype[p.captureRejectionSymbol] = function (e) {
          this.destroy(e);
        }),
        (C.prototype.push = function (e, t) {
          return F(this, e, t, !1);
        }),
        (C.prototype.unshift = function (e, t) {
          return F(this, e, t, !0);
        }),
        (C.prototype.isPaused = function () {
          const e = this._readableState;
          return !0 === e[N] || !1 === e.flowing;
        }),
        (C.prototype.setEncoding = function (e) {
          const t = new j(e);
          (this._readableState.decoder = t),
            (this._readableState.encoding =
              this._readableState.decoder.encoding);
          const n = this._readableState.buffer;
          let r = "";
          for (const e of n) r += t.write(e);
          return (
            n.clear(),
            "" !== r && n.push(r),
            (this._readableState.length = r.length),
            this
          );
        }),
        (C.prototype.read = function (e) {
          E("read", e), void 0 === e ? (e = NaN) : i(e) || (e = l(e, 10));
          const t = this._readableState,
            n = e;
          if (
            (e > t.highWaterMark &&
              (t.highWaterMark = (function (e) {
                if (e > 1073741824) throw new O("size", "<= 1GiB", e);
                return (
                  e--,
                  (e |= e >>> 1),
                  (e |= e >>> 2),
                  (e |= e >>> 4),
                  (e |= e >>> 8),
                  (e |= e >>> 16),
                  ++e
                );
              })(e)),
            0 !== e && (t.emittedReadable = !1),
            0 === e &&
              t.needReadable &&
              ((0 !== t.highWaterMark
                ? t.length >= t.highWaterMark
                : t.length > 0) ||
                t.ended))
          )
            return (
              E("read: emitReadable", t.length, t.ended),
              0 === t.length && t.ended ? Q(this) : G(this),
              null
            );
          if (0 === (e = B(e, t)) && t.ended)
            return 0 === t.length && Q(this), null;
          let r,
            o = t.needReadable;
          if (
            (E("need readable", o),
            (0 === t.length || t.length - e < t.highWaterMark) &&
              ((o = !0), E("length less than watermark", o)),
            t.ended || t.reading || t.destroyed || t.errored || !t.constructed)
          )
            (o = !1), E("reading, ended or constructing", o);
          else if (o) {
            E("do read"),
              (t.reading = !0),
              (t.sync = !0),
              0 === t.length && (t.needReadable = !0);
            try {
              this._read(t.highWaterMark);
            } catch (e) {
              W(this, e);
            }
            (t.sync = !1), t.reading || (e = B(n, t));
          }
          return (
            (r = e > 0 ? Z(e, t) : null),
            null === r
              ? ((t.needReadable = t.length <= t.highWaterMark), (e = 0))
              : ((t.length -= e),
                t.multiAwaitDrain
                  ? t.awaitDrainWriters.clear()
                  : (t.awaitDrainWriters = null)),
            0 === t.length &&
              (t.ended || (t.needReadable = !0), n !== e && t.ended && Q(this)),
            null === r ||
              t.errorEmitted ||
              t.closeEmitted ||
              ((t.dataEmitted = !0), this.emit("data", r)),
            r
          );
        }),
        (C.prototype._read = function (e) {
          throw new k("_read()");
        }),
        (C.prototype.pipe = function (e, t) {
          const n = this,
            o = this._readableState;
          1 === o.pipes.length &&
            (o.multiAwaitDrain ||
              ((o.multiAwaitDrain = !0),
              (o.awaitDrainWriters = new f(
                o.awaitDrainWriters ? [o.awaitDrainWriters] : []
              )))),
            o.pipes.push(e),
            E("pipe count=%d opts=%j", o.pipes.length, t);
          const i =
            (t && !1 === t.end) || e === r.stdout || e === r.stderr ? p : a;
          function a() {
            E("onend"), e.end();
          }
          let l;
          o.endEmitted ? r.nextTick(i) : n.once("end", i),
            e.on("unpipe", function t(r, i) {
              E("onunpipe"),
                r === n &&
                  i &&
                  !1 === i.hasUnpiped &&
                  ((i.hasUnpiped = !0),
                  E("cleanup"),
                  e.removeListener("close", b),
                  e.removeListener("finish", h),
                  l && e.removeListener("drain", l),
                  e.removeListener("error", c),
                  e.removeListener("unpipe", t),
                  n.removeListener("end", a),
                  n.removeListener("end", p),
                  n.removeListener("data", d),
                  (s = !0),
                  l &&
                    o.awaitDrainWriters &&
                    (!e._writableState || e._writableState.needDrain) &&
                    l());
            });
          let s = !1;
          function u() {
            s ||
              (1 === o.pipes.length && o.pipes[0] === e
                ? (E("false write response, pause", 0),
                  (o.awaitDrainWriters = e),
                  (o.multiAwaitDrain = !1))
                : o.pipes.length > 1 &&
                  o.pipes.includes(e) &&
                  (E("false write response, pause", o.awaitDrainWriters.size),
                  o.awaitDrainWriters.add(e)),
              n.pause()),
              l ||
                ((l = (function (e, t) {
                  return function () {
                    const n = e._readableState;
                    n.awaitDrainWriters === t
                      ? (E("pipeOnDrain", 1), (n.awaitDrainWriters = null))
                      : n.multiAwaitDrain &&
                        (E("pipeOnDrain", n.awaitDrainWriters.size),
                        n.awaitDrainWriters.delete(t)),
                      (n.awaitDrainWriters && 0 !== n.awaitDrainWriters.size) ||
                        !e.listenerCount("data") ||
                        e.resume();
                  };
                })(n, e)),
                e.on("drain", l));
          }
          function d(t) {
            E("ondata");
            const n = e.write(t);
            E("dest.write", n), !1 === n && u();
          }
          function c(t) {
            if (
              (E("onerror", t),
              p(),
              e.removeListener("error", c),
              0 === e.listenerCount("error"))
            ) {
              const n = e._writableState || e._readableState;
              n && !n.errorEmitted ? W(e, t) : e.emit("error", t);
            }
          }
          function b() {
            e.removeListener("finish", h), p();
          }
          function h() {
            E("onfinish"), e.removeListener("close", b), p();
          }
          function p() {
            E("unpipe"), n.unpipe(e);
          }
          return (
            n.on("data", d),
            g(e, "error", c),
            e.once("close", b),
            e.once("finish", h),
            e.emit("pipe", n),
            !0 === e.writableNeedDrain
              ? o.flowing && u()
              : o.flowing || (E("pipe resume"), n.resume()),
            e
          );
        }),
        (C.prototype.unpipe = function (e) {
          const t = this._readableState;
          if (0 === t.pipes.length) return this;
          if (!e) {
            const e = t.pipes;
            (t.pipes = []), this.pause();
            for (let t = 0; t < e.length; t++)
              e[t].emit("unpipe", this, { hasUnpiped: !1 });
            return this;
          }
          const n = o(t.pipes, e);
          return (
            -1 === n ||
              (t.pipes.splice(n, 1),
              0 === t.pipes.length && this.pause(),
              e.emit("unpipe", this, { hasUnpiped: !1 })),
            this
          );
        }),
        (C.prototype.on = function (e, t) {
          const n = y.prototype.on.call(this, e, t),
            o = this._readableState;
          return (
            "data" === e
              ? ((o.readableListening = this.listenerCount("readable") > 0),
                !1 !== o.flowing && this.resume())
              : "readable" === e &&
                (o.endEmitted ||
                  o.readableListening ||
                  ((o.readableListening = o.needReadable = !0),
                  (o.flowing = !1),
                  (o.emittedReadable = !1),
                  E("on readable", o.length, o.reading),
                  o.length ? G(this) : o.reading || r.nextTick(K, this))),
            n
          );
        }),
        (C.prototype.addListener = C.prototype.on),
        (C.prototype.removeListener = function (e, t) {
          const n = y.prototype.removeListener.call(this, e, t);
          return "readable" === e && r.nextTick(Y, this), n;
        }),
        (C.prototype.off = C.prototype.removeListener),
        (C.prototype.removeAllListeners = function (e) {
          const t = y.prototype.removeAllListeners.apply(this, arguments);
          return ("readable" !== e && void 0 !== e) || r.nextTick(Y, this), t;
        }),
        (C.prototype.resume = function () {
          const e = this._readableState;
          return (
            e.flowing ||
              (E("resume"),
              (e.flowing = !e.readableListening),
              (function (e, t) {
                t.resumeScheduled ||
                  ((t.resumeScheduled = !0), r.nextTick(z, e, t));
              })(this, e)),
            (e[N] = !1),
            this
          );
        }),
        (C.prototype.pause = function () {
          return (
            E("call pause flowing=%j", this._readableState.flowing),
            !1 !== this._readableState.flowing &&
              (E("pause"),
              (this._readableState.flowing = !1),
              this.emit("pause")),
            (this._readableState[N] = !0),
            this
          );
        }),
        (C.prototype.wrap = function (e) {
          let t = !1;
          e.on("data", (n) => {
            !this.push(n) && e.pause && ((t = !0), e.pause());
          }),
            e.on("end", () => {
              this.push(null);
            }),
            e.on("error", (e) => {
              W(this, e);
            }),
            e.on("close", () => {
              this.destroy();
            }),
            e.on("destroy", () => {
              this.destroy();
            }),
            (this._read = () => {
              t && e.resume && ((t = !1), e.resume());
            });
          const n = u(e);
          for (let t = 1; t < n.length; t++) {
            const r = n[t];
            void 0 === this[r] &&
              "function" == typeof e[r] &&
              (this[r] = e[r].bind(e));
          }
          return this;
        }),
        (C.prototype[b] = function () {
          return X(this);
        }),
        (C.prototype.iterator = function (e) {
          return void 0 !== e && x(e, "options"), X(this, e);
        }),
        s(C.prototype, {
          readable: {
            __proto__: null,
            get() {
              const e = this._readableState;
              return !(
                !e ||
                !1 === e.readable ||
                e.destroyed ||
                e.errorEmitted ||
                e.endEmitted
              );
            },
            set(e) {
              this._readableState && (this._readableState.readable = !!e);
            },
          },
          readableDidRead: {
            __proto__: null,
            enumerable: !1,
            get: function () {
              return this._readableState.dataEmitted;
            },
          },
          readableAborted: {
            __proto__: null,
            enumerable: !1,
            get: function () {
              return !(
                !1 === this._readableState.readable ||
                (!this._readableState.destroyed &&
                  !this._readableState.errored) ||
                this._readableState.endEmitted
              );
            },
          },
          readableHighWaterMark: {
            __proto__: null,
            enumerable: !1,
            get: function () {
              return this._readableState.highWaterMark;
            },
          },
          readableBuffer: {
            __proto__: null,
            enumerable: !1,
            get: function () {
              return this._readableState && this._readableState.buffer;
            },
          },
          readableFlowing: {
            __proto__: null,
            enumerable: !1,
            get: function () {
              return this._readableState.flowing;
            },
            set: function (e) {
              this._readableState && (this._readableState.flowing = e);
            },
          },
          readableLength: {
            __proto__: null,
            enumerable: !1,
            get() {
              return this._readableState.length;
            },
          },
          readableObjectMode: {
            __proto__: null,
            enumerable: !1,
            get() {
              return !!this._readableState && this._readableState.objectMode;
            },
          },
          readableEncoding: {
            __proto__: null,
            enumerable: !1,
            get() {
              return this._readableState ? this._readableState.encoding : null;
            },
          },
          errored: {
            __proto__: null,
            enumerable: !1,
            get() {
              return this._readableState ? this._readableState.errored : null;
            },
          },
          closed: {
            __proto__: null,
            get() {
              return !!this._readableState && this._readableState.closed;
            },
          },
          destroyed: {
            __proto__: null,
            enumerable: !1,
            get() {
              return !!this._readableState && this._readableState.destroyed;
            },
            set(e) {
              this._readableState && (this._readableState.destroyed = e);
            },
          },
          readableEnded: {
            __proto__: null,
            enumerable: !1,
            get() {
              return !!this._readableState && this._readableState.endEmitted;
            },
          },
        }),
        s($.prototype, {
          pipesCount: {
            __proto__: null,
            get() {
              return this.pipes.length;
            },
          },
          paused: {
            __proto__: null,
            get() {
              return !1 !== this[N];
            },
            set(e) {
              this[N] = !!e;
            },
          },
        }),
        (C._fromList = Z),
        (C.from = function (e, t) {
          return D(C, e, t);
        }),
        (C.fromWeb = function (e, t) {
          return re().newStreamReadableFromReadableStream(e, t);
        }),
        (C.toWeb = function (e, t) {
          return re().newReadableStreamFromStreamReadable(e, t);
        }),
        (C.wrap = function (e, t) {
          var n, r;
          return new C({
            objectMode:
              null ===
                (n =
                  null !== (r = e.readableObjectMode) && void 0 !== r
                    ? r
                    : e.objectMode) ||
              void 0 === n ||
              n,
            ...t,
            destroy(t, n) {
              v.destroyer(e, t), n(t);
            },
          }).wrap(e);
        });
    },
    745: (e, t, n) => {
      const { MathFloor: r, NumberIsInteger: o } = n(557),
        { ERR_INVALID_ARG_VALUE: i } = n(994).codes;
      function a(e) {
        return e ? 16 : 16384;
      }
      e.exports = {
        getHighWaterMark: function (e, t, n, l) {
          const s = (function (e, t, n) {
            return null != e.highWaterMark ? e.highWaterMark : t ? e[n] : null;
          })(t, l, n);
          if (null != s) {
            if (!o(s) || s < 0)
              throw new i(l ? `options.${n}` : "options.highWaterMark", s);
            return r(s);
          }
          return a(e.objectMode);
        },
        getDefaultHighWaterMark: a,
      };
    },
    744: (e, t, n) => {
      const { ObjectSetPrototypeOf: r, Symbol: o } = n(557);
      e.exports = u;
      const { ERR_METHOD_NOT_IMPLEMENTED: i } = n(994).codes,
        a = n(438),
        { getHighWaterMark: l } = n(745);
      r(u.prototype, a.prototype), r(u, a);
      const s = o("kCallback");
      function u(e) {
        if (!(this instanceof u)) return new u(e);
        const t = e ? l(this, e, "readableHighWaterMark", !0) : null;
        0 === t &&
          (e = {
            ...e,
            highWaterMark: null,
            readableHighWaterMark: t,
            writableHighWaterMark: e.writableHighWaterMark || 0,
          }),
          a.call(this, e),
          (this._readableState.sync = !1),
          (this[s] = null),
          e &&
            ("function" == typeof e.transform &&
              (this._transform = e.transform),
            "function" == typeof e.flush && (this._flush = e.flush)),
          this.on("prefinish", c);
      }
      function d(e) {
        "function" != typeof this._flush || this.destroyed
          ? (this.push(null), e && e())
          : this._flush((t, n) => {
              t
                ? e
                  ? e(t)
                  : this.destroy(t)
                : (null != n && this.push(n), this.push(null), e && e());
            });
      }
      function c() {
        this._final !== d && d.call(this);
      }
      (u.prototype._final = d),
        (u.prototype._transform = function (e, t, n) {
          throw new i("_transform()");
        }),
        (u.prototype._write = function (e, t, n) {
          const r = this._readableState,
            o = this._writableState,
            i = r.length;
          this._transform(e, t, (e, t) => {
            e
              ? n(e)
              : (null != t && this.push(t),
                o.ended || i === r.length || r.length < r.highWaterMark
                  ? n()
                  : (this[s] = n));
          });
        }),
        (u.prototype._read = function () {
          if (this[s]) {
            const e = this[s];
            (this[s] = null), e();
          }
        });
    },
    433: (e, t, n) => {
      const { Symbol: r, SymbolAsyncIterator: o, SymbolIterator: i } = n(557),
        a = r("kDestroyed"),
        l = r("kIsErrored"),
        s = r("kIsReadable"),
        u = r("kIsDisturbed");
      function d(e, t = !1) {
        var n;
        return !(
          !e ||
          "function" != typeof e.pipe ||
          "function" != typeof e.on ||
          (t &&
            ("function" != typeof e.pause || "function" != typeof e.resume)) ||
          (e._writableState &&
            !1 ===
              (null === (n = e._readableState) || void 0 === n
                ? void 0
                : n.readable)) ||
          (e._writableState && !e._readableState)
        );
      }
      function c(e) {
        var t;
        return !(
          !e ||
          "function" != typeof e.write ||
          "function" != typeof e.on ||
          (e._readableState &&
            !1 ===
              (null === (t = e._writableState) || void 0 === t
                ? void 0
                : t.writable))
        );
      }
      function f(e) {
        return (
          e &&
          (e._readableState ||
            e._writableState ||
            ("function" == typeof e.write && "function" == typeof e.on) ||
            ("function" == typeof e.pipe && "function" == typeof e.on))
        );
      }
      function b(e) {
        if (!f(e)) return null;
        const t = e._writableState,
          n = e._readableState,
          r = t || n;
        return !!(e.destroyed || e[a] || (null != r && r.destroyed));
      }
      function h(e) {
        if (!c(e)) return null;
        if (!0 === e.writableEnded) return !0;
        const t = e._writableState;
        return (
          (null == t || !t.errored) &&
          ("boolean" != typeof (null == t ? void 0 : t.ended) ? null : t.ended)
        );
      }
      function p(e, t) {
        if (!d(e)) return null;
        const n = e._readableState;
        return (
          (null == n || !n.errored) &&
          ("boolean" != typeof (null == n ? void 0 : n.endEmitted)
            ? null
            : !!(
                n.endEmitted ||
                (!1 === t && !0 === n.ended && 0 === n.length)
              ))
        );
      }
      function y(e) {
        return e && null != e[s]
          ? e[s]
          : "boolean" != typeof (null == e ? void 0 : e.readable)
          ? null
          : !b(e) && d(e) && e.readable && !p(e);
      }
      function g(e) {
        return "boolean" != typeof (null == e ? void 0 : e.writable)
          ? null
          : !b(e) && c(e) && e.writable && !h(e);
      }
      function _(e) {
        return (
          "boolean" == typeof e._closed &&
          "boolean" == typeof e._defaultKeepAlive &&
          "boolean" == typeof e._removedConnection &&
          "boolean" == typeof e._removedContLen
        );
      }
      function w(e) {
        return "boolean" == typeof e._sent100 && _(e);
      }
      e.exports = {
        kDestroyed: a,
        isDisturbed: function (e) {
          var t;
          return !(
            !e ||
            !(null !== (t = e[u]) && void 0 !== t
              ? t
              : e.readableDidRead || e.readableAborted)
          );
        },
        kIsDisturbed: u,
        isErrored: function (e) {
          var t, n, r, o, i, a, s, u, d, c;
          return !(
            !e ||
            !(null !==
              (t =
                null !==
                  (n =
                    null !==
                      (r =
                        null !==
                          (o =
                            null !==
                              (i =
                                null !== (a = e[l]) && void 0 !== a
                                  ? a
                                  : e.readableErrored) && void 0 !== i
                              ? i
                              : e.writableErrored) && void 0 !== o
                          ? o
                          : null === (s = e._readableState) || void 0 === s
                          ? void 0
                          : s.errorEmitted) && void 0 !== r
                      ? r
                      : null === (u = e._writableState) || void 0 === u
                      ? void 0
                      : u.errorEmitted) && void 0 !== n
                  ? n
                  : null === (d = e._readableState) || void 0 === d
                  ? void 0
                  : d.errored) && void 0 !== t
              ? t
              : null === (c = e._writableState) || void 0 === c
              ? void 0
              : c.errored)
          );
        },
        kIsErrored: l,
        isReadable: y,
        kIsReadable: s,
        isClosed: function (e) {
          if (!f(e)) return null;
          if ("boolean" == typeof e.closed) return e.closed;
          const t = e._writableState,
            n = e._readableState;
          return "boolean" == typeof (null == t ? void 0 : t.closed) ||
            "boolean" == typeof (null == n ? void 0 : n.closed)
            ? (null == t ? void 0 : t.closed) || (null == n ? void 0 : n.closed)
            : "boolean" == typeof e._closed && _(e)
            ? e._closed
            : null;
        },
        isDestroyed: b,
        isDuplexNodeStream: function (e) {
          return !(
            !e ||
            "function" != typeof e.pipe ||
            !e._readableState ||
            "function" != typeof e.on ||
            "function" != typeof e.write
          );
        },
        isFinished: function (e, t) {
          return f(e)
            ? !(
                !b(e) &&
                ((!1 !== (null == t ? void 0 : t.readable) && y(e)) ||
                  (!1 !== (null == t ? void 0 : t.writable) && g(e)))
              )
            : null;
        },
        isIterable: function (e, t) {
          return (
            null != e &&
            (!0 === t
              ? "function" == typeof e[o]
              : !1 === t
              ? "function" == typeof e[i]
              : "function" == typeof e[o] || "function" == typeof e[i])
          );
        },
        isReadableNodeStream: d,
        isReadableEnded: function (e) {
          if (!d(e)) return null;
          if (!0 === e.readableEnded) return !0;
          const t = e._readableState;
          return (
            !(!t || t.errored) &&
            ("boolean" != typeof (null == t ? void 0 : t.ended)
              ? null
              : t.ended)
          );
        },
        isReadableFinished: p,
        isReadableErrored: function (e) {
          var t, n;
          return f(e)
            ? e.readableErrored
              ? e.readableErrored
              : null !==
                  (t =
                    null === (n = e._readableState) || void 0 === n
                      ? void 0
                      : n.errored) && void 0 !== t
              ? t
              : null
            : null;
        },
        isNodeStream: f,
        isWritable: g,
        isWritableNodeStream: c,
        isWritableEnded: h,
        isWritableFinished: function (e, t) {
          if (!c(e)) return null;
          if (!0 === e.writableFinished) return !0;
          const n = e._writableState;
          return (
            (null == n || !n.errored) &&
            ("boolean" != typeof (null == n ? void 0 : n.finished)
              ? null
              : !!(
                  n.finished ||
                  (!1 === t && !0 === n.ended && 0 === n.length)
                ))
          );
        },
        isWritableErrored: function (e) {
          var t, n;
          return f(e)
            ? e.writableErrored
              ? e.writableErrored
              : null !==
                  (t =
                    null === (n = e._writableState) || void 0 === n
                      ? void 0
                      : n.errored) && void 0 !== t
              ? t
              : null
            : null;
        },
        isServerRequest: function (e) {
          var t;
          return (
            "boolean" == typeof e._consuming &&
            "boolean" == typeof e._dumped &&
            void 0 ===
              (null === (t = e.req) || void 0 === t
                ? void 0
                : t.upgradeOrConnect)
          );
        },
        isServerResponse: w,
        willEmitClose: function (e) {
          if (!f(e)) return null;
          const t = e._writableState,
            n = e._readableState,
            r = t || n;
          return (
            (!r && w(e)) ||
            !!(r && r.autoDestroy && r.emitClose && !1 === r.closed)
          );
        },
      };
    },
    772: (e, t, n) => {
      const r = n(394),
        {
          ArrayPrototypeSlice: o,
          Error: i,
          FunctionPrototypeSymbolHasInstance: a,
          ObjectDefineProperty: l,
          ObjectDefineProperties: s,
          ObjectSetPrototypeOf: u,
          StringPrototypeToLowerCase: d,
          Symbol: c,
          SymbolHasInstance: f,
        } = n(557);
      (e.exports = j), (j.WritableState = x);
      const { EventEmitter: b } = n(178),
        h = n(693).Stream,
        { Buffer: p } = n(368),
        y = n(701),
        { addAbortSignal: g } = n(920),
        { getHighWaterMark: _, getDefaultHighWaterMark: w } = n(745),
        {
          ERR_INVALID_ARG_TYPE: m,
          ERR_METHOD_NOT_IMPLEMENTED: E,
          ERR_MULTIPLE_CALLBACK: S,
          ERR_STREAM_CANNOT_PIPE: v,
          ERR_STREAM_DESTROYED: R,
          ERR_STREAM_ALREADY_FINISHED: A,
          ERR_STREAM_NULL_VALUES: T,
          ERR_STREAM_WRITE_AFTER_END: I,
          ERR_UNKNOWN_ENCODING: k,
        } = n(994).codes,
        { errorOrDestroy: O } = y;
      function P() {}
      u(j.prototype, h.prototype), u(j, h);
      const M = c("kOnFinished");
      function x(e, t, r) {
        "boolean" != typeof r && (r = t instanceof n(438)),
          (this.objectMode = !(!e || !e.objectMode)),
          r &&
            (this.objectMode =
              this.objectMode || !(!e || !e.writableObjectMode)),
          (this.highWaterMark = e
            ? _(this, e, "writableHighWaterMark", r)
            : w(!1)),
          (this.finalCalled = !1),
          (this.needDrain = !1),
          (this.ending = !1),
          (this.ended = !1),
          (this.finished = !1),
          (this.destroyed = !1);
        const o = !(!e || !1 !== e.decodeStrings);
        (this.decodeStrings = !o),
          (this.defaultEncoding = (e && e.defaultEncoding) || "utf8"),
          (this.length = 0),
          (this.writing = !1),
          (this.corked = 0),
          (this.sync = !0),
          (this.bufferProcessing = !1),
          (this.onwrite = $.bind(void 0, t)),
          (this.writecb = null),
          (this.writelen = 0),
          (this.afterWriteTickInfo = null),
          N(this),
          (this.pendingcb = 0),
          (this.constructed = !0),
          (this.prefinished = !1),
          (this.errorEmitted = !1),
          (this.emitClose = !e || !1 !== e.emitClose),
          (this.autoDestroy = !e || !1 !== e.autoDestroy),
          (this.errored = null),
          (this.closed = !1),
          (this.closeEmitted = !1),
          (this[M] = []);
      }
      function N(e) {
        (e.buffered = []),
          (e.bufferedIndex = 0),
          (e.allBuffers = !0),
          (e.allNoop = !0);
      }
      function j(e) {
        const t = this instanceof n(438);
        if (!t && !a(j, this)) return new j(e);
        (this._writableState = new x(e, this, t)),
          e &&
            ("function" == typeof e.write && (this._write = e.write),
            "function" == typeof e.writev && (this._writev = e.writev),
            "function" == typeof e.destroy && (this._destroy = e.destroy),
            "function" == typeof e.final && (this._final = e.final),
            "function" == typeof e.construct && (this._construct = e.construct),
            e.signal && g(e.signal, this)),
          h.call(this, e),
          y.construct(this, () => {
            const e = this._writableState;
            e.writing || B(this, e), H(this, e);
          });
      }
      function D(e, t, n, o) {
        const i = e._writableState;
        if ("function" == typeof n) (o = n), (n = i.defaultEncoding);
        else {
          if (n) {
            if ("buffer" !== n && !p.isEncoding(n)) throw new k(n);
          } else n = i.defaultEncoding;
          "function" != typeof o && (o = P);
        }
        if (null === t) throw new T();
        if (!i.objectMode)
          if ("string" == typeof t)
            !1 !== i.decodeStrings && ((t = p.from(t, n)), (n = "buffer"));
          else if (t instanceof p) n = "buffer";
          else {
            if (!h._isUint8Array(t))
              throw new m("chunk", ["string", "Buffer", "Uint8Array"], t);
            (t = h._uint8ArrayToBuffer(t)), (n = "buffer");
          }
        let a;
        return (
          i.ending ? (a = new I()) : i.destroyed && (a = new R("write")),
          a
            ? (r.nextTick(o, a), O(e, a, !0), a)
            : (i.pendingcb++,
              (function (e, t, n, r, o) {
                const i = t.objectMode ? 1 : n.length;
                t.length += i;
                const a = t.length < t.highWaterMark;
                return (
                  a || (t.needDrain = !0),
                  t.writing || t.corked || t.errored || !t.constructed
                    ? (t.buffered.push({ chunk: n, encoding: r, callback: o }),
                      t.allBuffers && "buffer" !== r && (t.allBuffers = !1),
                      t.allNoop && o !== P && (t.allNoop = !1))
                    : ((t.writelen = i),
                      (t.writecb = o),
                      (t.writing = !0),
                      (t.sync = !0),
                      e._write(n, r, t.onwrite),
                      (t.sync = !1)),
                  a && !t.errored && !t.destroyed
                );
              })(e, i, t, n, o))
        );
      }
      function L(e, t, n, r, o, i, a) {
        (t.writelen = r),
          (t.writecb = a),
          (t.writing = !0),
          (t.sync = !0),
          t.destroyed
            ? t.onwrite(new R("write"))
            : n
            ? e._writev(o, t.onwrite)
            : e._write(o, i, t.onwrite),
          (t.sync = !1);
      }
      function W(e, t, n, r) {
        --t.pendingcb, r(n), U(t), O(e, n);
      }
      function $(e, t) {
        const n = e._writableState,
          o = n.sync,
          i = n.writecb;
        "function" == typeof i
          ? ((n.writing = !1),
            (n.writecb = null),
            (n.length -= n.writelen),
            (n.writelen = 0),
            t
              ? (t.stack,
                n.errored || (n.errored = t),
                e._readableState &&
                  !e._readableState.errored &&
                  (e._readableState.errored = t),
                o ? r.nextTick(W, e, n, t, i) : W(e, n, t, i))
              : (n.buffered.length > n.bufferedIndex && B(e, n),
                o
                  ? null !== n.afterWriteTickInfo &&
                    n.afterWriteTickInfo.cb === i
                    ? n.afterWriteTickInfo.count++
                    : ((n.afterWriteTickInfo = {
                        count: 1,
                        cb: i,
                        stream: e,
                        state: n,
                      }),
                      r.nextTick(C, n.afterWriteTickInfo))
                  : F(e, n, 1, i)))
          : O(e, new S());
      }
      function C({ stream: e, state: t, count: n, cb: r }) {
        return (t.afterWriteTickInfo = null), F(e, t, n, r);
      }
      function F(e, t, n, r) {
        for (
          !t.ending &&
          !e.destroyed &&
          0 === t.length &&
          t.needDrain &&
          ((t.needDrain = !1), e.emit("drain"));
          n-- > 0;

        )
          t.pendingcb--, r();
        t.destroyed && U(t), H(e, t);
      }
      function U(e) {
        if (e.writing) return;
        for (let n = e.bufferedIndex; n < e.buffered.length; ++n) {
          var t;
          const { chunk: r, callback: o } = e.buffered[n],
            i = e.objectMode ? 1 : r.length;
          (e.length -= i),
            o(null !== (t = e.errored) && void 0 !== t ? t : new R("write"));
        }
        const n = e[M].splice(0);
        for (let t = 0; t < n.length; t++) {
          var r;
          n[t](null !== (r = e.errored) && void 0 !== r ? r : new R("end"));
        }
        N(e);
      }
      function B(e, t) {
        if (t.corked || t.bufferProcessing || t.destroyed || !t.constructed)
          return;
        const { buffered: n, bufferedIndex: r, objectMode: i } = t,
          a = n.length - r;
        if (!a) return;
        let l = r;
        if (((t.bufferProcessing = !0), a > 1 && e._writev)) {
          t.pendingcb -= a - 1;
          const r = t.allNoop
              ? P
              : (e) => {
                  for (let t = l; t < n.length; ++t) n[t].callback(e);
                },
            i = t.allNoop && 0 === l ? n : o(n, l);
          (i.allBuffers = t.allBuffers), L(e, t, !0, t.length, i, "", r), N(t);
        } else {
          do {
            const { chunk: r, encoding: o, callback: a } = n[l];
            (n[l++] = null), L(e, t, !1, i ? 1 : r.length, r, o, a);
          } while (l < n.length && !t.writing);
          l === n.length
            ? N(t)
            : l > 256
            ? (n.splice(0, l), (t.bufferedIndex = 0))
            : (t.bufferedIndex = l);
        }
        t.bufferProcessing = !1;
      }
      function G(e) {
        return (
          e.ending &&
          !e.destroyed &&
          e.constructed &&
          0 === e.length &&
          !e.errored &&
          0 === e.buffered.length &&
          !e.finished &&
          !e.writing &&
          !e.errorEmitted &&
          !e.closeEmitted
        );
      }
      function H(e, t, n) {
        G(t) &&
          ((function (e, t) {
            t.prefinished ||
              t.finalCalled ||
              ("function" != typeof e._final || t.destroyed
                ? ((t.prefinished = !0), e.emit("prefinish"))
                : ((t.finalCalled = !0),
                  (function (e, t) {
                    let n = !1;
                    function o(o) {
                      if (n) O(e, null != o ? o : S());
                      else if (((n = !0), t.pendingcb--, o)) {
                        const n = t[M].splice(0);
                        for (let e = 0; e < n.length; e++) n[e](o);
                        O(e, o, t.sync);
                      } else
                        G(t) &&
                          ((t.prefinished = !0),
                          e.emit("prefinish"),
                          t.pendingcb++,
                          r.nextTick(V, e, t));
                    }
                    (t.sync = !0), t.pendingcb++;
                    try {
                      e._final(o);
                    } catch (e) {
                      o(e);
                    }
                    t.sync = !1;
                  })(e, t)));
          })(e, t),
          0 === t.pendingcb &&
            (n
              ? (t.pendingcb++,
                r.nextTick(
                  (e, t) => {
                    G(t) ? V(e, t) : t.pendingcb--;
                  },
                  e,
                  t
                ))
              : G(t) && (t.pendingcb++, V(e, t))));
      }
      function V(e, t) {
        t.pendingcb--, (t.finished = !0);
        const n = t[M].splice(0);
        for (let e = 0; e < n.length; e++) n[e]();
        if ((e.emit("finish"), t.autoDestroy)) {
          const t = e._readableState;
          (!t || (t.autoDestroy && (t.endEmitted || !1 === t.readable))) &&
            e.destroy();
        }
      }
      (x.prototype.getBuffer = function () {
        return o(this.buffered, this.bufferedIndex);
      }),
        l(x.prototype, "bufferedRequestCount", {
          __proto__: null,
          get() {
            return this.buffered.length - this.bufferedIndex;
          },
        }),
        l(j, f, {
          __proto__: null,
          value: function (e) {
            return (
              !!a(this, e) || (this === j && e && e._writableState instanceof x)
            );
          },
        }),
        (j.prototype.pipe = function () {
          O(this, new v());
        }),
        (j.prototype.write = function (e, t, n) {
          return !0 === D(this, e, t, n);
        }),
        (j.prototype.cork = function () {
          this._writableState.corked++;
        }),
        (j.prototype.uncork = function () {
          const e = this._writableState;
          e.corked && (e.corked--, e.writing || B(this, e));
        }),
        (j.prototype.setDefaultEncoding = function (e) {
          if (("string" == typeof e && (e = d(e)), !p.isEncoding(e)))
            throw new k(e);
          return (this._writableState.defaultEncoding = e), this;
        }),
        (j.prototype._write = function (e, t, n) {
          if (!this._writev) throw new E("_write()");
          this._writev([{ chunk: e, encoding: t }], n);
        }),
        (j.prototype._writev = null),
        (j.prototype.end = function (e, t, n) {
          const o = this._writableState;
          let a;
          if (
            ("function" == typeof e
              ? ((n = e), (e = null), (t = null))
              : "function" == typeof t && ((n = t), (t = null)),
            null != e)
          ) {
            const n = D(this, e, t);
            n instanceof i && (a = n);
          }
          return (
            o.corked && ((o.corked = 1), this.uncork()),
            a ||
              (o.errored || o.ending
                ? o.finished
                  ? (a = new A("end"))
                  : o.destroyed && (a = new R("end"))
                : ((o.ending = !0), H(this, o, !0), (o.ended = !0))),
            "function" == typeof n &&
              (a || o.finished ? r.nextTick(n, a) : o[M].push(n)),
            this
          );
        }),
        s(j.prototype, {
          closed: {
            __proto__: null,
            get() {
              return !!this._writableState && this._writableState.closed;
            },
          },
          destroyed: {
            __proto__: null,
            get() {
              return !!this._writableState && this._writableState.destroyed;
            },
            set(e) {
              this._writableState && (this._writableState.destroyed = e);
            },
          },
          writable: {
            __proto__: null,
            get() {
              const e = this._writableState;
              return !(
                !e ||
                !1 === e.writable ||
                e.destroyed ||
                e.errored ||
                e.ending ||
                e.ended
              );
            },
            set(e) {
              this._writableState && (this._writableState.writable = !!e);
            },
          },
          writableFinished: {
            __proto__: null,
            get() {
              return !!this._writableState && this._writableState.finished;
            },
          },
          writableObjectMode: {
            __proto__: null,
            get() {
              return !!this._writableState && this._writableState.objectMode;
            },
          },
          writableBuffer: {
            __proto__: null,
            get() {
              return this._writableState && this._writableState.getBuffer();
            },
          },
          writableEnded: {
            __proto__: null,
            get() {
              return !!this._writableState && this._writableState.ending;
            },
          },
          writableNeedDrain: {
            __proto__: null,
            get() {
              const e = this._writableState;
              return !!e && !e.destroyed && !e.ending && e.needDrain;
            },
          },
          writableHighWaterMark: {
            __proto__: null,
            get() {
              return this._writableState && this._writableState.highWaterMark;
            },
          },
          writableCorked: {
            __proto__: null,
            get() {
              return this._writableState ? this._writableState.corked : 0;
            },
          },
          writableLength: {
            __proto__: null,
            get() {
              return this._writableState && this._writableState.length;
            },
          },
          errored: {
            __proto__: null,
            enumerable: !1,
            get() {
              return this._writableState ? this._writableState.errored : null;
            },
          },
          writableAborted: {
            __proto__: null,
            enumerable: !1,
            get: function () {
              return !(
                !1 === this._writableState.writable ||
                (!this._writableState.destroyed &&
                  !this._writableState.errored) ||
                this._writableState.finished
              );
            },
          },
        });
      const q = y.destroy;
      let Y;
      function K() {
        return void 0 === Y && (Y = {}), Y;
      }
      (j.prototype.destroy = function (e, t) {
        const n = this._writableState;
        return (
          !n.destroyed &&
            (n.bufferedIndex < n.buffered.length || n[M].length) &&
            r.nextTick(U, n),
          q.call(this, e, t),
          this
        );
      }),
        (j.prototype._undestroy = y.undestroy),
        (j.prototype._destroy = function (e, t) {
          t(e);
        }),
        (j.prototype[b.captureRejectionSymbol] = function (e) {
          this.destroy(e);
        }),
        (j.fromWeb = function (e, t) {
          return K().newStreamWritableFromWritableStream(e, t);
        }),
        (j.toWeb = function (e) {
          return K().newWritableStreamFromStreamWritable(e);
        });
    },
    215: (e, t, n) => {
      const {
          ArrayIsArray: r,
          ArrayPrototypeIncludes: o,
          ArrayPrototypeJoin: i,
          ArrayPrototypeMap: a,
          NumberIsInteger: l,
          NumberIsNaN: s,
          NumberMAX_SAFE_INTEGER: u,
          NumberMIN_SAFE_INTEGER: d,
          NumberParseInt: c,
          ObjectPrototypeHasOwnProperty: f,
          RegExpPrototypeExec: b,
          String: h,
          StringPrototypeToUpperCase: p,
          StringPrototypeTrim: y,
        } = n(557),
        {
          hideStackFrames: g,
          codes: {
            ERR_SOCKET_BAD_PORT: _,
            ERR_INVALID_ARG_TYPE: w,
            ERR_INVALID_ARG_VALUE: m,
            ERR_OUT_OF_RANGE: E,
            ERR_UNKNOWN_SIGNAL: S,
          },
        } = n(994),
        { normalizeEncoding: v } = n(501),
        { isAsyncFunction: R, isArrayBufferView: A } = n(501).types,
        T = {},
        I = /^[0-7]+$/,
        k = g((e, t, n = d, r = u) => {
          if ("number" != typeof e) throw new w(t, "number", e);
          if (!l(e)) throw new E(t, "an integer", e);
          if (e < n || e > r) throw new E(t, `>= ${n} && <= ${r}`, e);
        }),
        O = g((e, t, n = -2147483648, r = 2147483647) => {
          if ("number" != typeof e) throw new w(t, "number", e);
          if (!l(e)) throw new E(t, "an integer", e);
          if (e < n || e > r) throw new E(t, `>= ${n} && <= ${r}`, e);
        }),
        P = g((e, t, n = !1) => {
          if ("number" != typeof e) throw new w(t, "number", e);
          if (!l(e)) throw new E(t, "an integer", e);
          const r = n ? 1 : 0,
            o = 4294967295;
          if (e < r || e > o) throw new E(t, `>= ${r} && <= ${o}`, e);
        });
      function M(e, t) {
        if ("string" != typeof e) throw new w(t, "string", e);
      }
      const x = g((e, t, n) => {
        if (!o(n, e)) {
          const r = i(
            a(n, (e) => ("string" == typeof e ? `'${e}'` : h(e))),
            ", "
          );
          throw new m(t, e, "must be one of: " + r);
        }
      });
      function N(e, t, n) {
        return null != e && f(e, t) ? e[t] : n;
      }
      const j = g((e, t, n = null) => {
          const o = N(n, "allowArray", !1),
            i = N(n, "allowFunction", !1);
          if (
            (!N(n, "nullable", !1) && null === e) ||
            (!o && r(e)) ||
            ("object" != typeof e && (!i || "function" != typeof e))
          )
            throw new w(t, "Object", e);
        }),
        D = g((e, t, n = 0) => {
          if (!r(e)) throw new w(t, "Array", e);
          if (e.length < n) throw new m(t, e, `must be longer than ${n}`);
        }),
        L = g((e, t = "buffer") => {
          if (!A(e)) throw new w(t, ["Buffer", "TypedArray", "DataView"], e);
        }),
        W = g((e, t) => {
          if (
            void 0 !== e &&
            (null === e || "object" != typeof e || !("aborted" in e))
          )
            throw new w(t, "AbortSignal", e);
        }),
        $ = g((e, t) => {
          if ("function" != typeof e) throw new w(t, "Function", e);
        }),
        C = g((e, t) => {
          if ("function" != typeof e || R(e)) throw new w(t, "Function", e);
        }),
        F = g((e, t) => {
          if (void 0 !== e) throw new w(t, "undefined", e);
        });
      e.exports = {
        isInt32: function (e) {
          return e === (0 | e);
        },
        isUint32: function (e) {
          return e === e >>> 0;
        },
        parseFileMode: function (e, t, n) {
          if ((void 0 === e && (e = n), "string" == typeof e)) {
            if (null === b(I, e))
              throw new m(
                t,
                e,
                "must be a 32-bit unsigned integer or an octal string"
              );
            e = c(e, 8);
          }
          return P(e, t), e;
        },
        validateArray: D,
        validateBoolean: function (e, t) {
          if ("boolean" != typeof e) throw new w(t, "boolean", e);
        },
        validateBuffer: L,
        validateEncoding: function (e, t) {
          const n = v(t),
            r = e.length;
          if ("hex" === n && r % 2 != 0)
            throw new m("encoding", t, `is invalid for data of length ${r}`);
        },
        validateFunction: $,
        validateInt32: O,
        validateInteger: k,
        validateNumber: function (e, t, n = void 0, r) {
          if ("number" != typeof e) throw new w(t, "number", e);
          if (
            (null != n && e < n) ||
            (null != r && e > r) ||
            ((null != n || null != r) && s(e))
          )
            throw new E(
              t,
              `${null != n ? `>= ${n}` : ""}${
                null != n && null != r ? " && " : ""
              }${null != r ? `<= ${r}` : ""}`,
              e
            );
        },
        validateObject: j,
        validateOneOf: x,
        validatePlainFunction: C,
        validatePort: function (e, t = "Port", n = !0) {
          if (
            ("number" != typeof e && "string" != typeof e) ||
            ("string" == typeof e && 0 === y(e).length) ||
            +e != +e >>> 0 ||
            e > 65535 ||
            (0 === e && !n)
          )
            throw new _(t, e, n);
          return 0 | e;
        },
        validateSignalName: function (e, t = "signal") {
          if ((M(e, t), void 0 === T[e])) {
            if (void 0 !== T[p(e)])
              throw new S(e + " (signals must use all capital letters)");
            throw new S(e);
          }
        },
        validateString: M,
        validateUint32: P,
        validateUndefined: F,
        validateUnion: function (e, t, n) {
          if (!o(n, e)) throw new w(t, `('${i(n, "|")}')`, e);
        },
        validateAbortSignal: W,
      };
    },
    994: (e, t, n) => {
      const { format: r, inspect: o, AggregateError: i } = n(501),
        a = globalThis.AggregateError || i,
        l = Symbol("kIsNodeError"),
        s = [
          "string",
          "function",
          "number",
          "object",
          "Function",
          "Object",
          "boolean",
          "bigint",
          "symbol",
        ],
        u = /^([A-Z][a-z0-9]*)+$/,
        d = {};
      function c(e, t) {
        if (!e) throw new d.ERR_INTERNAL_ASSERTION(t);
      }
      function f(e) {
        let t = "",
          n = e.length;
        const r = "-" === e[0] ? 1 : 0;
        for (; n >= r + 4; n -= 3) t = `_${e.slice(n - 3, n)}${t}`;
        return `${e.slice(0, n)}${t}`;
      }
      function b(e, t, n) {
        n || (n = Error);
        class o extends n {
          constructor(...n) {
            super(
              (function (e, t, n) {
                if ("function" == typeof t)
                  return (
                    c(
                      t.length <= n.length,
                      `Code: ${e}; The provided arguments length (${n.length}) does not match the required ones (${t.length}).`
                    ),
                    t(...n)
                  );
                const o = (t.match(/%[dfijoOs]/g) || []).length;
                return (
                  c(
                    o === n.length,
                    `Code: ${e}; The provided arguments length (${n.length}) does not match the required ones (${o}).`
                  ),
                  0 === n.length ? t : r(t, ...n)
                );
              })(e, t, n)
            );
          }
          toString() {
            return `${this.name} [${e}]: ${this.message}`;
          }
        }
        Object.defineProperties(o.prototype, {
          name: {
            value: n.name,
            writable: !0,
            enumerable: !1,
            configurable: !0,
          },
          toString: {
            value() {
              return `${this.name} [${e}]: ${this.message}`;
            },
            writable: !0,
            enumerable: !1,
            configurable: !0,
          },
        }),
          (o.prototype.code = e),
          (o.prototype[l] = !0),
          (d[e] = o);
      }
      function h(e) {
        const t = "__node_internal_" + e.name;
        return Object.defineProperty(e, "name", { value: t }), e;
      }
      class p extends Error {
        constructor(e = "The operation was aborted", t = void 0) {
          if (void 0 !== t && "object" != typeof t)
            throw new d.ERR_INVALID_ARG_TYPE("options", "Object", t);
          super(e, t), (this.code = "ABORT_ERR"), (this.name = "AbortError");
        }
      }
      b("ERR_ASSERTION", "%s", Error),
        b(
          "ERR_INVALID_ARG_TYPE",
          (e, t, n) => {
            c("string" == typeof e, "'name' must be a string"),
              Array.isArray(t) || (t = [t]);
            let r = "The ";
            e.endsWith(" argument")
              ? (r += `${e} `)
              : (r += `"${e}" ${e.includes(".") ? "property" : "argument"} `),
              (r += "must be ");
            const i = [],
              a = [],
              l = [];
            for (const e of t)
              c(
                "string" == typeof e,
                "All expected entries have to be of type string"
              ),
                s.includes(e)
                  ? i.push(e.toLowerCase())
                  : u.test(e)
                  ? a.push(e)
                  : (c(
                      "object" !== e,
                      'The value "object" should be written as "Object"'
                    ),
                    l.push(e));
            if (a.length > 0) {
              const e = i.indexOf("object");
              -1 !== e && (i.splice(i, e, 1), a.push("Object"));
            }
            if (i.length > 0) {
              switch (i.length) {
                case 1:
                  r += `of type ${i[0]}`;
                  break;
                case 2:
                  r += `one of type ${i[0]} or ${i[1]}`;
                  break;
                default: {
                  const e = i.pop();
                  r += `one of type ${i.join(", ")}, or ${e}`;
                }
              }
              (a.length > 0 || l.length > 0) && (r += " or ");
            }
            if (a.length > 0) {
              switch (a.length) {
                case 1:
                  r += `an instance of ${a[0]}`;
                  break;
                case 2:
                  r += `an instance of ${a[0]} or ${a[1]}`;
                  break;
                default: {
                  const e = a.pop();
                  r += `an instance of ${a.join(", ")}, or ${e}`;
                }
              }
              l.length > 0 && (r += " or ");
            }
            switch (l.length) {
              case 0:
                break;
              case 1:
                l[0].toLowerCase() !== l[0] && (r += "an "), (r += `${l[0]}`);
                break;
              case 2:
                r += `one of ${l[0]} or ${l[1]}`;
                break;
              default: {
                const e = l.pop();
                r += `one of ${l.join(", ")}, or ${e}`;
              }
            }
            if (null == n) r += `. Received ${n}`;
            else if ("function" == typeof n && n.name)
              r += `. Received function ${n.name}`;
            else if ("object" == typeof n) {
              var d;
              null !== (d = n.constructor) && void 0 !== d && d.name
                ? (r += `. Received an instance of ${n.constructor.name}`)
                : (r += `. Received ${o(n, { depth: -1 })}`);
            } else {
              let e = o(n, { colors: !1 });
              e.length > 25 && (e = `${e.slice(0, 25)}...`),
                (r += `. Received type ${typeof n} (${e})`);
            }
            return r;
          },
          TypeError
        ),
        b(
          "ERR_INVALID_ARG_VALUE",
          (e, t, n = "is invalid") => {
            let r = o(t);
            return (
              r.length > 128 && (r = r.slice(0, 128) + "..."),
              `The ${
                e.includes(".") ? "property" : "argument"
              } '${e}' ${n}. Received ${r}`
            );
          },
          TypeError
        ),
        b(
          "ERR_INVALID_RETURN_VALUE",
          (e, t, n) => {
            var r;
            return `Expected ${e} to be returned from the "${t}" function but got ${
              null != n &&
              null !== (r = n.constructor) &&
              void 0 !== r &&
              r.name
                ? `instance of ${n.constructor.name}`
                : "type " + typeof n
            }.`;
          },
          TypeError
        ),
        b(
          "ERR_MISSING_ARGS",
          (...e) => {
            let t;
            c(e.length > 0, "At least one arg needs to be specified");
            const n = e.length;
            switch (
              ((e = (Array.isArray(e) ? e : [e])
                .map((e) => `"${e}"`)
                .join(" or ")),
              n)
            ) {
              case 1:
                t += `The ${e[0]} argument`;
                break;
              case 2:
                t += `The ${e[0]} and ${e[1]} arguments`;
                break;
              default: {
                const n = e.pop();
                t += `The ${e.join(", ")}, and ${n} arguments`;
              }
            }
            return `${t} must be specified`;
          },
          TypeError
        ),
        b(
          "ERR_OUT_OF_RANGE",
          (e, t, n) => {
            let r;
            return (
              c(t, 'Missing "range" argument'),
              Number.isInteger(n) && Math.abs(n) > 2 ** 32
                ? (r = f(String(n)))
                : "bigint" == typeof n
                ? ((r = String(n)),
                  (n > 2n ** 32n || n < -(2n ** 32n)) && (r = f(r)),
                  (r += "n"))
                : (r = o(n)),
              `The value of "${e}" is out of range. It must be ${t}. Received ${r}`
            );
          },
          RangeError
        ),
        b("ERR_MULTIPLE_CALLBACK", "Callback called multiple times", Error),
        b(
          "ERR_METHOD_NOT_IMPLEMENTED",
          "The %s method is not implemented",
          Error
        ),
        b(
          "ERR_STREAM_ALREADY_FINISHED",
          "Cannot call %s after a stream was finished",
          Error
        ),
        b("ERR_STREAM_CANNOT_PIPE", "Cannot pipe, not readable", Error),
        b(
          "ERR_STREAM_DESTROYED",
          "Cannot call %s after a stream was destroyed",
          Error
        ),
        b(
          "ERR_STREAM_NULL_VALUES",
          "May not write null values to stream",
          TypeError
        ),
        b("ERR_STREAM_PREMATURE_CLOSE", "Premature close", Error),
        b("ERR_STREAM_PUSH_AFTER_EOF", "stream.push() after EOF", Error),
        b(
          "ERR_STREAM_UNSHIFT_AFTER_END_EVENT",
          "stream.unshift() after end event",
          Error
        ),
        b("ERR_STREAM_WRITE_AFTER_END", "write after end", Error),
        b("ERR_UNKNOWN_ENCODING", "Unknown encoding: %s", TypeError),
        (e.exports = {
          AbortError: p,
          aggregateTwoErrors: h(function (e, t) {
            if (e && t && e !== t) {
              if (Array.isArray(t.errors)) return t.errors.push(e), t;
              const n = new a([t, e], t.message);
              return (n.code = t.code), n;
            }
            return e || t;
          }),
          hideStackFrames: h,
          codes: d,
        });
    },
    557: (e) => {
      e.exports = {
        ArrayIsArray: (e) => Array.isArray(e),
        ArrayPrototypeIncludes: (e, t) => e.includes(t),
        ArrayPrototypeIndexOf: (e, t) => e.indexOf(t),
        ArrayPrototypeJoin: (e, t) => e.join(t),
        ArrayPrototypeMap: (e, t) => e.map(t),
        ArrayPrototypePop: (e, t) => e.pop(t),
        ArrayPrototypePush: (e, t) => e.push(t),
        ArrayPrototypeSlice: (e, t, n) => e.slice(t, n),
        Error,
        FunctionPrototypeCall: (e, t, ...n) => e.call(t, ...n),
        FunctionPrototypeSymbolHasInstance: (e, t) =>
          Function.prototype[Symbol.hasInstance].call(e, t),
        MathFloor: Math.floor,
        Number,
        NumberIsInteger: Number.isInteger,
        NumberIsNaN: Number.isNaN,
        NumberMAX_SAFE_INTEGER: Number.MAX_SAFE_INTEGER,
        NumberMIN_SAFE_INTEGER: Number.MIN_SAFE_INTEGER,
        NumberParseInt: Number.parseInt,
        ObjectDefineProperties: (e, t) => Object.defineProperties(e, t),
        ObjectDefineProperty: (e, t, n) => Object.defineProperty(e, t, n),
        ObjectGetOwnPropertyDescriptor: (e, t) =>
          Object.getOwnPropertyDescriptor(e, t),
        ObjectKeys: (e) => Object.keys(e),
        ObjectSetPrototypeOf: (e, t) => Object.setPrototypeOf(e, t),
        Promise,
        PromisePrototypeCatch: (e, t) => e.catch(t),
        PromisePrototypeThen: (e, t, n) => e.then(t, n),
        PromiseReject: (e) => Promise.reject(e),
        ReflectApply: Reflect.apply,
        RegExpPrototypeTest: (e, t) => e.test(t),
        SafeSet: Set,
        String,
        StringPrototypeSlice: (e, t, n) => e.slice(t, n),
        StringPrototypeToLowerCase: (e) => e.toLowerCase(),
        StringPrototypeToUpperCase: (e) => e.toUpperCase(),
        StringPrototypeTrim: (e) => e.trim(),
        Symbol,
        SymbolAsyncIterator: Symbol.asyncIterator,
        SymbolHasInstance: Symbol.hasInstance,
        SymbolIterator: Symbol.iterator,
        TypedArrayPrototypeSet: (e, t, n) => e.set(t, n),
        Uint8Array,
      };
    },
    501: (e, t, n) => {
      const r = n(368),
        o = Object.getPrototypeOf(async function () {}).constructor,
        i = globalThis.Blob || r.Blob,
        a =
          void 0 !== i
            ? function (e) {
                return e instanceof i;
              }
            : function (e) {
                return !1;
              };
      class l extends Error {
        constructor(e) {
          if (!Array.isArray(e))
            throw new TypeError(
              "Expected input to be an Array, got " + typeof e
            );
          let t = "";
          for (let n = 0; n < e.length; n++) t += `    ${e[n].stack}\n`;
          super(t), (this.name = "AggregateError"), (this.errors = e);
        }
      }
      (e.exports = {
        AggregateError: l,
        kEmptyObject: Object.freeze({}),
        once(e) {
          let t = !1;
          return function (...n) {
            t || ((t = !0), e.apply(this, n));
          };
        },
        createDeferredPromise: function () {
          let e, t;
          return {
            promise: new Promise((n, r) => {
              (e = n), (t = r);
            }),
            resolve: e,
            reject: t,
          };
        },
        promisify: (e) =>
          new Promise((t, n) => {
            e((e, ...r) => (e ? n(e) : t(...r)));
          }),
        debuglog: () => function () {},
        format: (e, ...t) =>
          e.replace(/%([sdifj])/g, function (...[e, n]) {
            const r = t.shift();
            return "f" === n
              ? r.toFixed(6)
              : "j" === n
              ? JSON.stringify(r)
              : "s" === n && "object" == typeof r
              ? `${
                  r.constructor !== Object ? r.constructor.name : ""
                } {}`.trim()
              : r.toString();
          }),
        inspect(e) {
          switch (typeof e) {
            case "string":
              if (e.includes("'")) {
                if (!e.includes('"')) return `"${e}"`;
                if (!e.includes("`") && !e.includes("${")) return `\`${e}\``;
              }
              return `'${e}'`;
            case "number":
              return isNaN(e) ? "NaN" : Object.is(e, -0) ? String(e) : e;
            case "bigint":
              return `${String(e)}n`;
            case "boolean":
            case "undefined":
              return String(e);
            case "object":
              return "{}";
          }
        },
        types: {
          isAsyncFunction: (e) => e instanceof o,
          isArrayBufferView: (e) => ArrayBuffer.isView(e),
        },
        isBlob: a,
      }),
        (e.exports.promisify.custom = Symbol.for(
          "nodejs.util.promisify.custom"
        ));
    },
    957: (e, t, n) => {
      const { Buffer: r } = n(368),
        { ObjectDefineProperty: o, ObjectKeys: i, ReflectApply: a } = n(557),
        {
          promisify: { custom: l },
        } = n(501),
        { streamReturningOperators: s, promiseReturningOperators: u } = n(723),
        {
          codes: { ERR_ILLEGAL_CONSTRUCTOR: d },
        } = n(994),
        c = n(262),
        { pipeline: f } = n(792),
        { destroyer: b } = n(701),
        h = n(355),
        p = n(523),
        y = n(433),
        g = (e.exports = n(693).Stream);
      (g.isDisturbed = y.isDisturbed),
        (g.isErrored = y.isErrored),
        (g.isReadable = y.isReadable),
        (g.Readable = n(392));
      for (const m of i(s)) {
        const E = s[m];
        function _(...e) {
          if (new.target) throw d();
          return g.Readable.from(a(E, this, e));
        }
        o(_, "name", { __proto__: null, value: E.name }),
          o(_, "length", { __proto__: null, value: E.length }),
          o(g.Readable.prototype, m, {
            __proto__: null,
            value: _,
            enumerable: !1,
            configurable: !0,
            writable: !0,
          });
      }
      for (const S of i(u)) {
        const v = u[S];
        function _(...e) {
          if (new.target) throw d();
          return a(v, this, e);
        }
        o(_, "name", { __proto__: null, value: v.name }),
          o(_, "length", { __proto__: null, value: v.length }),
          o(g.Readable.prototype, S, {
            __proto__: null,
            value: _,
            enumerable: !1,
            configurable: !0,
            writable: !0,
          });
      }
      (g.Writable = n(772)),
        (g.Duplex = n(438)),
        (g.Transform = n(744)),
        (g.PassThrough = n(107)),
        (g.pipeline = f);
      const { addAbortSignal: w } = n(920);
      (g.addAbortSignal = w),
        (g.finished = h),
        (g.destroy = b),
        (g.compose = c),
        o(g, "promises", {
          __proto__: null,
          configurable: !0,
          enumerable: !0,
          get: () => p,
        }),
        o(f, l, { __proto__: null, enumerable: !0, get: () => p.pipeline }),
        o(h, l, { __proto__: null, enumerable: !0, get: () => p.finished }),
        (g.Stream = g),
        (g._isUint8Array = function (e) {
          return e instanceof Uint8Array;
        }),
        (g._uint8ArrayToBuffer = function (e) {
          return r.from(e.buffer, e.byteOffset, e.byteLength);
        });
    },
    523: (e, t, n) => {
      const { ArrayPrototypePop: r, Promise: o } = n(557),
        { isIterable: i, isNodeStream: a } = n(433),
        { pipelineImpl: l } = n(792),
        { finished: s } = n(355);
      e.exports = {
        finished: s,
        pipeline: function (...e) {
          return new o((t, n) => {
            let o, s;
            const u = e[e.length - 1];
            if (u && "object" == typeof u && !a(u) && !i(u)) {
              const t = r(e);
              (o = t.signal), (s = t.end);
            }
            l(
              e,
              (e, r) => {
                e ? n(e) : t(r);
              },
              { signal: o, end: s }
            );
          });
        },
      };
    },
    499: (e, t, n) => {
      Object.defineProperty(t, "__esModule", { value: !0 });
      var r = n(911);
      class o extends r.EventTarget {
        constructor() {
          throw (
            (super(),
            new TypeError("AbortSignal cannot be constructed directly"))
          );
        }
        get aborted() {
          const e = i.get(this);
          if ("boolean" != typeof e)
            throw new TypeError(
              "Expected 'this' to be an 'AbortSignal' object, but got " +
                (null === this ? "null" : typeof this)
            );
          return e;
        }
      }
      r.defineEventAttribute(o.prototype, "abort");
      const i = new WeakMap();
      Object.defineProperties(o.prototype, { aborted: { enumerable: !0 } }),
        "function" == typeof Symbol &&
          "symbol" == typeof Symbol.toStringTag &&
          Object.defineProperty(o.prototype, Symbol.toStringTag, {
            configurable: !0,
            value: "AbortSignal",
          });
      class a {
        constructor() {
          l.set(
            this,
            (function () {
              const e = Object.create(o.prototype);
              return r.EventTarget.call(e), i.set(e, !1), e;
            })()
          );
        }
        get signal() {
          return s(this);
        }
        abort() {
          var e;
          (e = s(this)),
            !1 === i.get(e) &&
              (i.set(e, !0), e.dispatchEvent({ type: "abort" }));
        }
      }
      const l = new WeakMap();
      function s(e) {
        const t = l.get(e);
        if (null == t)
          throw new TypeError(
            "Expected 'this' to be an 'AbortController' object, but got " +
              (null === e ? "null" : typeof e)
          );
        return t;
      }
      Object.defineProperties(a.prototype, {
        signal: { enumerable: !0 },
        abort: { enumerable: !0 },
      }),
        "function" == typeof Symbol &&
          "symbol" == typeof Symbol.toStringTag &&
          Object.defineProperty(a.prototype, Symbol.toStringTag, {
            configurable: !0,
            value: "AbortController",
          }),
        (t.AbortController = a),
        (t.AbortSignal = o),
        (t.default = a),
        (e.exports = a),
        (e.exports.AbortController = e.exports.default = a),
        (e.exports.AbortSignal = o);
    },
    911: (e, t) => {
      Object.defineProperty(t, "__esModule", { value: !0 });
      const n = new WeakMap(),
        r = new WeakMap();
      function o(e) {
        const t = n.get(e);
        return (
          console.assert(
            null != t,
            "'this' is expected an Event object, but got",
            e
          ),
          t
        );
      }
      function i(e) {
        null == e.passiveListener
          ? e.event.cancelable &&
            ((e.canceled = !0),
            "function" == typeof e.event.preventDefault &&
              e.event.preventDefault())
          : "undefined" != typeof console &&
            "function" == typeof console.error &&
            console.error(
              "Unable to preventDefault inside passive event listener invocation.",
              e.passiveListener
            );
      }
      function a(e, t) {
        n.set(this, {
          eventTarget: e,
          event: t,
          eventPhase: 2,
          currentTarget: e,
          canceled: !1,
          stopped: !1,
          immediateStopped: !1,
          passiveListener: null,
          timeStamp: t.timeStamp || Date.now(),
        }),
          Object.defineProperty(this, "isTrusted", {
            value: !1,
            enumerable: !0,
          });
        const r = Object.keys(t);
        for (let e = 0; e < r.length; ++e) {
          const t = r[e];
          t in this || Object.defineProperty(this, t, l(t));
        }
      }
      function l(e) {
        return {
          get() {
            return o(this).event[e];
          },
          set(t) {
            o(this).event[e] = t;
          },
          configurable: !0,
          enumerable: !0,
        };
      }
      function s(e) {
        return {
          value() {
            const t = o(this).event;
            return t[e].apply(t, arguments);
          },
          configurable: !0,
          enumerable: !0,
        };
      }
      function u(e) {
        if (null == e || e === Object.prototype) return a;
        let t = r.get(e);
        return (
          null == t &&
            ((t = (function (e, t) {
              const n = Object.keys(t);
              if (0 === n.length) return e;
              function r(t, n) {
                e.call(this, t, n);
              }
              r.prototype = Object.create(e.prototype, {
                constructor: { value: r, configurable: !0, writable: !0 },
              });
              for (let o = 0; o < n.length; ++o) {
                const i = n[o];
                if (!(i in e.prototype)) {
                  const e =
                    "function" ==
                    typeof Object.getOwnPropertyDescriptor(t, i).value;
                  Object.defineProperty(r.prototype, i, e ? s(i) : l(i));
                }
              }
              return r;
            })(u(Object.getPrototypeOf(e)), e)),
            r.set(e, t)),
          t
        );
      }
      function d(e) {
        return o(e).immediateStopped;
      }
      function c(e, t) {
        o(e).passiveListener = t;
      }
      (a.prototype = {
        get type() {
          return o(this).event.type;
        },
        get target() {
          return o(this).eventTarget;
        },
        get currentTarget() {
          return o(this).currentTarget;
        },
        composedPath() {
          const e = o(this).currentTarget;
          return null == e ? [] : [e];
        },
        get NONE() {
          return 0;
        },
        get CAPTURING_PHASE() {
          return 1;
        },
        get AT_TARGET() {
          return 2;
        },
        get BUBBLING_PHASE() {
          return 3;
        },
        get eventPhase() {
          return o(this).eventPhase;
        },
        stopPropagation() {
          const e = o(this);
          (e.stopped = !0),
            "function" == typeof e.event.stopPropagation &&
              e.event.stopPropagation();
        },
        stopImmediatePropagation() {
          const e = o(this);
          (e.stopped = !0),
            (e.immediateStopped = !0),
            "function" == typeof e.event.stopImmediatePropagation &&
              e.event.stopImmediatePropagation();
        },
        get bubbles() {
          return Boolean(o(this).event.bubbles);
        },
        get cancelable() {
          return Boolean(o(this).event.cancelable);
        },
        preventDefault() {
          i(o(this));
        },
        get defaultPrevented() {
          return o(this).canceled;
        },
        get composed() {
          return Boolean(o(this).event.composed);
        },
        get timeStamp() {
          return o(this).timeStamp;
        },
        get srcElement() {
          return o(this).eventTarget;
        },
        get cancelBubble() {
          return o(this).stopped;
        },
        set cancelBubble(e) {
          if (!e) return;
          const t = o(this);
          (t.stopped = !0),
            "boolean" == typeof t.event.cancelBubble &&
              (t.event.cancelBubble = !0);
        },
        get returnValue() {
          return !o(this).canceled;
        },
        set returnValue(e) {
          e || i(o(this));
        },
        initEvent() {},
      }),
        Object.defineProperty(a.prototype, "constructor", {
          value: a,
          configurable: !0,
          writable: !0,
        }),
        "undefined" != typeof window &&
          void 0 !== window.Event &&
          (Object.setPrototypeOf(a.prototype, window.Event.prototype),
          r.set(window.Event.prototype, a));
      const f = new WeakMap(),
        b = 3;
      function h(e) {
        return null !== e && "object" == typeof e;
      }
      function p(e) {
        const t = f.get(e);
        if (null == t)
          throw new TypeError(
            "'this' is expected an EventTarget object, but got another value."
          );
        return t;
      }
      function y(e, t) {
        Object.defineProperty(
          e,
          `on${t}`,
          (function (e) {
            return {
              get() {
                let t = p(this).get(e);
                for (; null != t; ) {
                  if (t.listenerType === b) return t.listener;
                  t = t.next;
                }
                return null;
              },
              set(t) {
                "function" == typeof t || h(t) || (t = null);
                const n = p(this);
                let r = null,
                  o = n.get(e);
                for (; null != o; )
                  o.listenerType === b
                    ? null !== r
                      ? (r.next = o.next)
                      : null !== o.next
                      ? n.set(e, o.next)
                      : n.delete(e)
                    : (r = o),
                    (o = o.next);
                if (null !== t) {
                  const o = {
                    listener: t,
                    listenerType: b,
                    passive: !1,
                    once: !1,
                    next: null,
                  };
                  null === r ? n.set(e, o) : (r.next = o);
                }
              },
              configurable: !0,
              enumerable: !0,
            };
          })(t)
        );
      }
      function g(e) {
        function t() {
          _.call(this);
        }
        t.prototype = Object.create(_.prototype, {
          constructor: { value: t, configurable: !0, writable: !0 },
        });
        for (let n = 0; n < e.length; ++n) y(t.prototype, e[n]);
        return t;
      }
      function _() {
        if (!(this instanceof _)) {
          if (1 === arguments.length && Array.isArray(arguments[0]))
            return g(arguments[0]);
          if (arguments.length > 0) {
            const e = new Array(arguments.length);
            for (let t = 0; t < arguments.length; ++t) e[t] = arguments[t];
            return g(e);
          }
          throw new TypeError("Cannot call a class as a function");
        }
        f.set(this, new Map());
      }
      (_.prototype = {
        addEventListener(e, t, n) {
          if (null == t) return;
          if ("function" != typeof t && !h(t))
            throw new TypeError(
              "'listener' should be a function or an object."
            );
          const r = p(this),
            o = h(n),
            i = (o ? Boolean(n.capture) : Boolean(n)) ? 1 : 2,
            a = {
              listener: t,
              listenerType: i,
              passive: o && Boolean(n.passive),
              once: o && Boolean(n.once),
              next: null,
            };
          let l = r.get(e);
          if (void 0 === l) return void r.set(e, a);
          let s = null;
          for (; null != l; ) {
            if (l.listener === t && l.listenerType === i) return;
            (s = l), (l = l.next);
          }
          s.next = a;
        },
        removeEventListener(e, t, n) {
          if (null == t) return;
          const r = p(this),
            o = (h(n) ? Boolean(n.capture) : Boolean(n)) ? 1 : 2;
          let i = null,
            a = r.get(e);
          for (; null != a; ) {
            if (a.listener === t && a.listenerType === o)
              return void (null !== i
                ? (i.next = a.next)
                : null !== a.next
                ? r.set(e, a.next)
                : r.delete(e));
            (i = a), (a = a.next);
          }
        },
        dispatchEvent(e) {
          if (null == e || "string" != typeof e.type)
            throw new TypeError('"event.type" should be a string.');
          const t = p(this),
            n = e.type;
          let r = t.get(n);
          if (null == r) return !0;
          const i = (function (e, t) {
            return new (u(Object.getPrototypeOf(t)))(e, t);
          })(this, e);
          let a = null;
          for (; null != r; ) {
            if (
              (r.once
                ? null !== a
                  ? (a.next = r.next)
                  : null !== r.next
                  ? t.set(n, r.next)
                  : t.delete(n)
                : (a = r),
              c(i, r.passive ? r.listener : null),
              "function" == typeof r.listener)
            )
              try {
                r.listener.call(this, i);
              } catch (e) {
                "undefined" != typeof console &&
                  "function" == typeof console.error &&
                  console.error(e);
              }
            else
              r.listenerType !== b &&
                "function" == typeof r.listener.handleEvent &&
                r.listener.handleEvent(i);
            if (d(i)) break;
            r = r.next;
          }
          return (
            c(i, null),
            (function (e, t) {
              o(e).eventPhase = 0;
            })(i),
            (function (e, t) {
              o(e).currentTarget = null;
            })(i),
            !i.defaultPrevented
          );
        },
      }),
        Object.defineProperty(_.prototype, "constructor", {
          value: _,
          configurable: !0,
          writable: !0,
        }),
        "undefined" != typeof window &&
          void 0 !== window.EventTarget &&
          Object.setPrototypeOf(_.prototype, window.EventTarget.prototype),
        (t.defineEventAttribute = y),
        (t.EventTarget = _),
        (t.default = _),
        (e.exports = _),
        (e.exports.EventTarget = e.exports.default = _),
        (e.exports.defineEventAttribute = y);
    },
    368: (t) => {
      t.exports = e;
    },
    178: (e) => {
      e.exports = t;
    },
    394: (e) => {
      e.exports = n;
    },
    888: (e) => {
      e.exports = r;
    },
  },
  i = {};
function a(e) {
  var t = i[e];
  if (void 0 !== t) return t.exports;
  var n = (i[e] = { exports: {} });
  return o[e](n, n.exports, a), n.exports;
}
var l = {};
(() => {
  var e = l;
  const t = a(957),
    n = t.Readable.destroy;
  (e.CT = t._isUint8Array),
    (e.q8 = t.isDisturbed),
    (e.o6 = t.isErrored),
    (e.FA = t.isReadable),
    (e.$M = t.Readable),
    (e.cK = t.Writable),
    (e.$P = t.Duplex),
    (e.wx = t.Transform),
    (e.ze = t.PassThrough),
    (e.iy = t.addAbortSignal),
    (e.V3 = t.finished),
    (e.ob = t.destroy),
    (e.ob = n),
    (e.EU = t.pipeline),
    (e.qC = t.compose);
})();
var d = l.$M;

/**
 * Utility function for constructing a multipart form in Node.js.
 *
 * @packageDocumentation
 * @internal
 */
/**
 * @internal
 */
async function toForm(fields) {
    const form = new Multipart();
    for (const key of Object.keys(fields)) {
        let value = fields[key];
        if (value === undefined)
            continue;
        if (!(value instanceof d) &&
            !(value instanceof global.Buffer) &&
            (typeof value === "object" || typeof value === "function")) {
            value = JSON.stringify(value);
        }
        form.append(key, value);
    }
    const body = await form.buffer();
    const headers = form.getHeaders();
    delete headers["transfer-encoding"];
    return { body, headers };
}

/**
 * Represents an arbitrary route relative to an ArangoDB database.
 */
class Route {
    /**
     * @internal
     */
    constructor(db, path = "", headers = {}) {
        if (!path)
            path = "";
        else if (path.charAt(0) !== "/")
            path = `/${path}`;
        this._db = db;
        this._path = path;
        this._headers = headers;
    }
    /**
     * Creates a new route relative to this route that inherits any of its default
     * HTTP headers.
     *
     * @param path - Path relative to this route.
     * @param headers - Additional headers that will be sent with each request.
     *
     * @example
     * ```js
     * const db = new Database();
     * const foxx = db.route("/my-foxx-service");
     * const users = foxx.route("/users");
     * ```
     */
    route(path, headers) {
        if (!path)
            path = "";
        else if (path.charAt(0) !== "/")
            path = `/${path}`;
        return new Route(this._db, this._path + path, {
            ...this._headers,
            ...headers,
        });
    }
    /**
     * Performs an arbitrary HTTP request relative to this route and returns the
     * server response.
     *
     * @param options - Options for performing the request.
     *
     * @example
     * ```js
     * const db = new Database();
     * const foxx = db.route("/my-foxx-service");
     * const res = await foxx.request({
     *   method: "POST",
     *   path: "/users",
     *   body: {
     *     username: "admin",
     *     password: "hunter2"
     *   }
     * });
     * ```
     */
    request(options) {
        const opts = { ...options };
        if (!opts.path || opts.path === "/")
            opts.path = "";
        else if (!this._path || opts.path.charAt(0) === "/")
            opts.path = opts.path;
        else
            opts.path = `/${opts.path}`;
        opts.basePath = this._path;
        opts.headers = { ...this._headers, ...opts.headers };
        opts.method = opts.method ? opts.method.toUpperCase() : "GET";
        return this._db.request(opts, false);
    }
    delete(...args) {
        const path = typeof args[0] === "string" ? args.shift() : undefined;
        const [qs, headers] = args;
        return this.request({ method: "DELETE", path, qs, headers });
    }
    get(...args) {
        const path = typeof args[0] === "string" ? args.shift() : undefined;
        const [qs, headers] = args;
        return this.request({ method: "GET", path, qs, headers });
    }
    head(...args) {
        const path = typeof args[0] === "string" ? args.shift() : undefined;
        const [qs, headers] = args;
        return this.request({ method: "HEAD", path, qs, headers });
    }
    patch(...args) {
        const path = typeof args[0] === "string" ? args.shift() : undefined;
        const [body, qs, headers] = args;
        return this.request({ method: "PATCH", path, body, qs, headers });
    }
    post(...args) {
        const path = typeof args[0] === "string" ? args.shift() : undefined;
        const [body, qs, headers] = args;
        return this.request({ method: "POST", path, body, qs, headers });
    }
    put(...args) {
        const path = typeof args[0] === "string" ? args.shift() : undefined;
        const [body, qs, headers] = args;
        return this.request({ method: "PUT", path, body, qs, headers });
    }
}

/**
 * Represents a streaming transaction in a {@link database.Database}.
 */
class Transaction {
    /**
     * @internal
     */
    constructor(db, id) {
        this._db = db;
        this._id = id;
    }
    /**
     * @internal
     *
     * Indicates that this object represents an ArangoDB transaction.
     */
    get isArangoTransaction() {
        return true;
    }
    /**
     * Unique identifier of this transaction.
     *
     * See {@link database.Database#transaction}.
     */
    get id() {
        return this._id;
    }
    /**
     * Checks whether the transaction exists.
     *
     * @example
     * ```js
     * const db = new Database();
     * const trx = db.transaction("some-transaction");
     * const result = await trx.exists();
     * // result indicates whether the transaction exists
     * ```
     */
    async exists() {
        try {
            await this.get();
            return true;
        }
        catch (err) {
            if (isArangoError(err) && err.errorNum === TRANSACTION_NOT_FOUND) {
                return false;
            }
            throw err;
        }
    }
    /**
     * Retrieves general information about the transaction.
     *
     * @example
     * ```js
     * const db = new Database();
     * const col = db.collection("some-collection");
     * const trx = db.beginTransaction(col);
     * await trx.step(() => col.save({ hello: "world" }));
     * const info = await trx.get();
     * // the transaction exists
     * ```
     */
    get() {
        return this._db.request({
            path: `/_api/transaction/${encodeURIComponent(this.id)}`,
        }, (res) => res.body.result);
    }
    /**
     * Attempts to commit the transaction to the databases.
     *
     * @param options - Options for comitting the transaction.
     *
     * @example
     * ```js
     * const db = new Database();
     * const col = db.collection("some-collection");
     * const trx = db.beginTransaction(col);
     * await trx.step(() => col.save({ hello: "world" }));
     * const result = await trx.commit();
     * // result indicates the updated transaction status
     * ```
     */
    commit(options = {}) {
        const { allowDirtyRead = undefined } = options;
        return this._db.request({
            method: "PUT",
            path: `/_api/transaction/${encodeURIComponent(this.id)}`,
            allowDirtyRead,
        }, (res) => res.body.result);
    }
    /**
     * Attempts to abort the transaction to the databases.
     *
     * @param options - Options for aborting the transaction.
     *
     * @example
     * ```js
     * const db = new Database();
     * const col = db.collection("some-collection");
     * const trx = db.beginTransaction(col);
     * await trx.step(() => col.save({ hello: "world" }));
     * const result = await trx.abort();
     * // result indicates the updated transaction status
     * ```
     */
    abort(options = {}) {
        const { allowDirtyRead = undefined } = options;
        return this._db.request({
            method: "DELETE",
            path: `/_api/transaction/${encodeURIComponent(this.id)}`,
            allowDirtyRead,
        }, (res) => res.body.result);
    }
    /**
     * Executes the given function locally as a single step of the transaction.
     *
     * @param T - Type of the callback's returned promise.
     * @param callback - Callback function returning a promise.
     *
     * **Warning**: The callback function should wrap a single call of an async
     * arangojs method (e.g. a method on a `Collection` object of a collection
     * that is involved in the transaction or the `db.query` method).
     * If the callback function is async, only the first promise-returning (or
     * async) method call will be executed as part of the transaction. See the
     * examples below for how to avoid common mistakes when using this method.
     *
     * **Note**: Avoid defining the callback as an async function if possible
     * as arangojs will throw an error if the callback did not return a promise.
     * Async functions will return an empty promise by default, making it harder
     * to notice if you forgot to return something from the callback.
     *
     * **Note**: Although almost anything can be wrapped in a callback and passed
     * to this method, that does not guarantee ArangoDB can actually do it in a
     * transaction. Refer to the ArangoDB documentation if you are unsure whether
     * a given operation can be executed as part of a transaction. Generally any
     * modification or retrieval of data is eligible but modifications of
     * collections or databases are not.
     *
     * @example
     * ```js
     * const db = new Database();
     * const vertices = db.collection("vertices");
     * const edges = db.collection("edges");
     * const trx = await db.beginTransaction({ write: [vertices, edges] });
     *
     * // The following code will be part of the transaction
     * const left = await trx.step(() => vertices.save({ label: "left" }));
     * const right = await trx.step(() => vertices.save({ label: "right" }));
     *
     * // Results from preceding actions can be used normally
     * await trx.step(() => edges.save({
     *   _from: left._id,
     *   _to: right._id,
     *   data: "potato"
     * }));
     *
     * // Transaction must be committed for changes to take effected
     * // Always call either trx.commit or trx.abort to end a transaction
     * await trx.commit();
     * ```
     *
     * @example
     * ```js
     * // BAD! If the callback is an async function it must only use await once!
     * await trx.step(async () => {
     *   await collection.save(data);
     *   await collection.save(moreData); // WRONG
     * });
     *
     * // BAD! Callback function must use only one arangojs call!
     * await trx.step(() => {
     *  return collection.save(data)
     *    .then(() => collection.save(moreData)); // WRONG
     * });
     *
     * // BETTER: Wrap every arangojs method call that should be part of the
     * // transaction in a separate `trx.step` call
     * await trx.step(() => collection.save(data));
     * await trx.step(() => collection.save(moreData));
     * ```
     *
     * @example
     * ```js
     * // BAD! If the callback is an async function it must not await before
     * // calling an arangojs method!
     * await trx.step(async () => {
     *   await doSomethingElse();
     *   return collection.save(data); // WRONG
     * });
     *
     * // BAD! Any arangojs inside the callback must not happen inside a promise
     * // method!
     * await trx.step(() => {
     *   return doSomethingElse()
     *     .then(() => collection.save(data)); // WRONG
     * });
     *
     * // BETTER: Perform any async logic needed outside the `trx.step` call
     * await doSomethingElse();
     * await trx.step(() => collection.save(data));
     *
     * // OKAY: You can perform async logic in the callback after the arangojs
     * // method call as long as it does not involve additional arangojs method
     * // calls, but this makes it easy to make mistakes later
     * await trx.step(async () => {
     *   await collection.save(data);
     *   await doSomethingDifferent(); // no arangojs method calls allowed
     * });
     * ```
     *
     * @example
     * ```js
     * // BAD! The callback should not use any functions that themselves use any
     * // arangojs methods!
     * async function saveSomeData() {
     *   await collection.save(data);
     *   await collection.save(moreData);
     * }
     * await trx.step(() => saveSomeData()); // WRONG
     *
     * // BETTER: Pass the transaction to functions that need to call arangojs
     * // methods inside a transaction
     * async function saveSomeData(trx) {
     *   await trx.step(() => collection.save(data));
     *   await trx.step(() => collection.save(moreData));
     * }
     * await saveSomeData(); // no `trx.step` call needed
     * ```
     *
     * @example
     * ```js
     * // BAD! You must wait for the promise to resolve (or await on the
     * // `trx.step` call) before calling `trx.step` again!
     * trx.step(() => collection.save(data)); // WRONG
     * await trx.step(() => collection.save(moreData));
     *
     * // BAD! The trx.step callback can not make multiple calls to async arangojs
     * // methods, not even using Promise.all!
     * await trx.step(() => Promise.all([ // WRONG
     *   collection.save(data),
     *   collection.save(moreData),
     * ]));
     *
     * // BAD! Multiple `trx.step` calls can not run in parallel!
     * await Promise.all([ // WRONG
     *   trx.step(() => collection.save(data)),
     *   trx.step(() => collection.save(moreData)),
     * ]));
     *
     * // BETTER: Always call `trx.step` sequentially, one after the other
     * await trx.step(() => collection.save(data));
     * await trx.step(() => collection.save(moreData));
     *
     * // OKAY: The then callback can be used if async/await is not available
     * trx.step(() => collection.save(data))
     *   .then(() => trx.step(() => collection.save(moreData)));
     * ```
     *
     * @example
     * ```js
     * // BAD! The callback will return an empty promise that resolves before
     * // the inner arangojs method call has even talked to ArangoDB!
     * await trx.step(async () => {
     *   collection.save(data); // WRONG
     * });
     *
     * // BETTER: Use an arrow function so you don't forget to return
     * await trx.step(() => collection.save(data));
     *
     * // OKAY: Remember to always return when using a function body
     * await trx.step(() => {
     *   return collection.save(data); // easy to forget!
     * });
     *
     * // OKAY: You do not have to use arrow functions but it helps
     * await trx.step(function () {
     *   return collection.save(data);
     * });
     * ```
     *
     * @example
     * ```js
     * // BAD! You can not pass promises instead of a callback!
     * await trx.step(collection.save(data)); // WRONG
     *
     * // BETTER: Wrap the code in a function and pass the function instead
     * await trx.step(() => collection.save(data));
     * ```
     *
     * @example
     * ```js
     * // WORSE: Calls to non-async arangojs methods don't need to be performed
     * // as part of a transaction
     * const collection = await trx.step(() => db.collection("my-documents"));
     *
     * // BETTER: If an arangojs method is not async and doesn't return promises,
     * // call it without `trx.step`
     * const collection = db.collection("my-documents");
     * ```
     */
    step(callback) {
        const conn = this._db._connection;
        conn.setTransactionId(this.id);
        try {
            const promise = callback();
            if (!promise) {
                throw new Error("Transaction callback was not an async function or did not return a promise!");
            }
            return Promise.resolve(promise);
        }
        finally {
            conn.clearTransactionId();
        }
    }
}

/**
 * Indicates whether the given value represents a {@link Database}.
 *
 * @param database - A value that might be a database.
 */
function isArangoDatabase(database) {
    return Boolean(database && database.isArangoDatabase);
}
/**
 * @internal
 */
function coerceTransactionCollections(collections) {
    if (typeof collections === "string") {
        return { write: [collections] };
    }
    if (Array.isArray(collections)) {
        return { write: collections.map(collectionToString) };
    }
    if (isArangoCollection(collections)) {
        return { write: collectionToString(collections) };
    }
    const cols = {};
    if (collections) {
        if (collections.allowImplicit !== undefined) {
            cols.allowImplicit = collections.allowImplicit;
        }
        if (collections.read) {
            cols.read = Array.isArray(collections.read)
                ? collections.read.map(collectionToString)
                : collectionToString(collections.read);
        }
        if (collections.write) {
            cols.write = Array.isArray(collections.write)
                ? collections.write.map(collectionToString)
                : collectionToString(collections.write);
        }
        if (collections.exclusive) {
            cols.exclusive = Array.isArray(collections.exclusive)
                ? collections.exclusive.map(collectionToString)
                : collectionToString(collections.exclusive);
        }
    }
    return cols;
}
/**
 * An object representing a single ArangoDB database. All arangojs collections,
 * cursors, analyzers and so on are linked to a `Database` object.
 */
class Database {
    constructor(configOrDatabase = {}, name) {
        this._analyzers = new Map();
        this._collections = new Map();
        this._graphs = new Map();
        this._views = new Map();
        if (isArangoDatabase(configOrDatabase)) {
            const connection = configOrDatabase._connection;
            const databaseName = (name || configOrDatabase.name).normalize("NFC");
            this._connection = connection;
            this._name = databaseName;
            const database = connection.database(databaseName);
            if (database)
                return database;
        }
        else {
            const config = configOrDatabase;
            const { databaseName, ...options } = typeof config === "string" || Array.isArray(config)
                ? { databaseName: name, url: config }
                : config;
            this._connection = new Connection(options);
            this._name = databaseName?.normalize("NFC") || "_system";
        }
    }
    //#region misc
    /**
     * @internal
     *
     * Indicates that this object represents an ArangoDB database.
     */
    get isArangoDatabase() {
        return true;
    }
    /**
     * Name of the ArangoDB database this instance represents.
     */
    get name() {
        return this._name;
    }
    /**
     * Fetches version information from the ArangoDB server.
     *
     * @param details - If set to `true`, additional information about the
     * ArangoDB server will be available as the `details` property.
     *
     * @example
     * ```js
     * const db = new Database();
     * const version = await db.version();
     * // the version object contains the ArangoDB version information.
     * // license: "community" or "enterprise"
     * // version: ArangoDB version number
     * // server: description of the server
     * ```
     */
    version(details) {
        return this.request({
            method: "GET",
            path: "/_api/version",
            qs: { details },
        });
    }
    /**
     * Returns a new {@link route.Route} instance for the given path (relative to the
     * database) that can be used to perform arbitrary HTTP requests.
     *
     * @param path - The database-relative URL of the route. Defaults to the
     * database API root.
     * @param headers - Default headers that should be sent with each request to
     * the route.
     *
     * @example
     * ```js
     * const db = new Database();
     * const myFoxxService = db.route("my-foxx-service");
     * const response = await myFoxxService.post("users", {
     *   username: "admin",
     *   password: "hunter2"
     * });
     * // response.body is the result of
     * // POST /_db/_system/my-foxx-service/users
     * // with JSON request body '{"username": "admin", "password": "hunter2"}'
     * ```
     */
    route(path, headers) {
        return new Route(this, path, headers);
    }
    request({ absolutePath = false, basePath, ...opts }, transform = (res) => res.body) {
        if (!absolutePath) {
            basePath = `/_db/${encodeURIComponent(this._name)}${basePath || ""}`;
        }
        return this._connection.request({ basePath, ...opts }, transform || undefined);
    }
    /**
     * Updates the URL list by requesting a list of all coordinators in the
     * cluster and adding any endpoints not initially specified in the
     * {@link connection.Config}.
     *
     * For long-running processes communicating with an ArangoDB cluster it is
     * recommended to run this method periodically (e.g. once per hour) to make
     * sure new coordinators are picked up correctly and can be used for
     * fail-over or load balancing.
     *
     * @param overwrite - If set to `true`, the existing host list will be
     * replaced instead of extended.
     *
     * @example
     * ```js
     * const db = new Database();
     * const interval = setInterval(
     *   () => db.acquireHostList(),
     *   5 * 60 * 1000 // every 5 minutes
     * );
     *
     * // later
     * clearInterval(interval);
     * system.close();
     * ```
     */
    async acquireHostList(overwrite = false) {
        const urls = await this.request({ path: "/_api/cluster/endpoints" }, (res) => res.body.endpoints.map((endpoint) => endpoint.endpoint));
        if (urls.length > 0) {
            if (overwrite)
                this._connection.setHostList(urls);
            else
                this._connection.addToHostList(urls);
        }
    }
    /**
     * Closes all active connections of this database instance.
     *
     * Can be used to clean up idling connections during longer periods of
     * inactivity.
     *
     * **Note**: This method currently has no effect in the browser version of
     * arangojs.
     *
     * @example
     * ```js
     * const db = new Database();
     * const sessions = db.collection("sessions");
     * // Clean up expired sessions once per hour
     * setInterval(async () => {
     *   await db.query(aql`
     *     FOR session IN ${sessions}
     *     FILTER session.expires < DATE_NOW()
     *     REMOVE session IN ${sessions}
     *   `);
     *   // Making sure to close the connections because they're no longer used
     *   system.close();
     * }, 1000 * 60 * 60);
     * ```
     */
    close() {
        this._connection.close();
    }
    async waitForPropagation({ basePath, ...request }, timeout) {
        await this._connection.waitForPropagation({
            ...request,
            basePath: `/_db/${encodeURIComponent(this._name)}${basePath || ""}`,
        }, timeout);
    }
    /**
     * Methods for accessing the server-reported queue times of the mostly
     * recently received responses.
     */
    get queueTime() {
        return this._connection.queueTime;
    }
    /**
     * Sets the limit for the number of values of the most recently received
     * server-reported queue times that can be accessed using
     * {@link Database#queueTime}.
     *
     * @param responseQueueTimeSamples - Number of values to maintain.
     */
    setResponseQueueTimeSamples(responseQueueTimeSamples) {
        this._connection.setResponseQueueTimeSamples(responseQueueTimeSamples);
    }
    //#endregion
    //#region auth
    /**
     * Updates the underlying connection's `authorization` header to use Basic
     * authentication with the given `username` and `password`, then returns
     * itself.
     *
     * @param username - The username to authenticate with.
     * @param password - The password to authenticate with.
     *
     * @example
     * ```js
     * const db = new Database();
     * db.useBasicAuth("admin", "hunter2");
     * // with the username "admin" and password "hunter2".
     * ```
     */
    useBasicAuth(username = "root", password = "") {
        this._connection.setBasicAuth({ username, password });
        return this;
    }
    /**
     * Updates the underlying connection's `authorization` header to use Bearer
     * authentication with the given authentication `token`, then returns itself.
     *
     * @param token - The token to authenticate with.
     *
     * @example
     * ```js
     * const db = new Database();
     * db.useBearerAuth("keyboardcat");
     * // The database instance now uses Bearer authentication.
     * ```
     */
    useBearerAuth(token) {
        this._connection.setBearerAuth({ token });
        return this;
    }
    /**
     * Validates the given database credentials and exchanges them for an
     * authentication token, then uses the authentication token for future
     * requests and returns it.
     *
     * @param username - The username to authenticate with.
     * @param password - The password to authenticate with.
     *
     * @example
     * ```js
     * const db = new Database();
     * await db.login("admin", "hunter2");
     * // with an authentication token for the "admin" user.
     * ```
     */
    login(username = "root", password = "") {
        return this.request({
            method: "POST",
            path: "/_open/auth",
            body: { username, password },
        }, (res) => {
            this.useBearerAuth(res.body.jwt);
            return res.body.jwt;
        });
    }
    //#endregion
    //#region databases
    /**
     * Creates a new `Database` instance for the given `databaseName` that
     * shares this database's connection pool.
     *
     * See also {@link Database:constructor}.
     *
     * @param databaseName - Name of the database.
     *
     * @example
     * ```js
     * const systemDb = new Database();
     * const myDb = system.database("my_database");
     * ```
     */
    database(databaseName) {
        return new Database(this, databaseName);
    }
    /**
     * Fetches the database description for the active database from the server.
     *
     * @example
     * ```js
     * const db = new Database();
     * const info = await db.get();
     * // the database exists
     * ```
     */
    get() {
        return this.request({ path: "/_api/database/current" }, (res) => res.body.result);
    }
    /**
     * Checks whether the database exists.
     *
     * @example
     * ```js
     * const db = new Database();
     * const result = await db.exists();
     * // result indicates whether the database exists
     * ```
     */
    async exists() {
        try {
            await this.get();
            return true;
        }
        catch (err) {
            if (isArangoError(err) && err.errorNum === DATABASE_NOT_FOUND) {
                return false;
            }
            throw err;
        }
    }
    createDatabase(databaseName, usersOrOptions = {}) {
        const { users, ...options } = Array.isArray(usersOrOptions)
            ? { users: usersOrOptions }
            : usersOrOptions;
        return this.request({
            method: "POST",
            path: "/_api/database",
            body: { name: databaseName.normalize("NFC"), users, options },
        }, () => this.database(databaseName));
    }
    /**
     * Fetches all databases from the server and returns an array of their names.
     *
     * See also {@link Database#databases} and
     * {@link Database#listUserDatabases}.
     *
     * @example
     * ```js
     * const db = new Database();
     * const names = await db.listDatabases();
     * // databases is an array of database names
     * ```
     */
    listDatabases() {
        return this.request({ path: "/_api/database" }, (res) => res.body.result);
    }
    /**
     * Fetches all databases accessible to the active user from the server and
     * returns an array of their names.
     *
     * See also {@link Database#userDatabases} and
     * {@link Database#listDatabases}.
     *
     * @example
     * ```js
     * const db = new Database();
     * const names = await db.listUserDatabases();
     * // databases is an array of database names
     * ```
     */
    listUserDatabases() {
        return this.request({ path: "/_api/database/user" }, (res) => res.body.result);
    }
    /**
     * Fetches all databases from the server and returns an array of `Database`
     * instances for those databases.
     *
     * See also {@link Database#listDatabases} and
     * {@link Database#userDatabases}.
     *
     * @example
     * ```js
     * const db = new Database();
     * const names = await db.databases();
     * // databases is an array of databases
     * ```
     */
    databases() {
        return this.request({ path: "/_api/database" }, (res) => res.body.result.map((databaseName) => this.database(databaseName)));
    }
    /**
     * Fetches all databases accessible to the active user from the server and
     * returns an array of `Database` instances for those databases.
     *
     * See also {@link Database#listUserDatabases} and
     * {@link Database#databases}.
     *
     * @example
     * ```js
     * const db = new Database();
     * const names = await db.userDatabases();
     * // databases is an array of databases
     * ```
     */
    userDatabases() {
        return this.request({ path: "/_api/database/user" }, (res) => res.body.result.map((databaseName) => this.database(databaseName)));
    }
    /**
     * Deletes the database with the given `databaseName` from the server.
     *
     * @param databaseName - Name of the database to delete.
     *
     * @example
     * ```js
     * const db = new Database();
     * await db.dropDatabase("mydb");
     * // database "mydb" no longer exists
     * ```
     */
    dropDatabase(databaseName) {
        databaseName = databaseName.normalize("NFC");
        return this.request({
            method: "DELETE",
            path: `/_api/database/${encodeURIComponent(databaseName)}`,
        }, (res) => res.body.result);
    }
    //#endregion
    //#region collections
    /**
     * Returns a `Collection` instance for the given collection name.
     *
     * In TypeScript the collection implements both the
     * {@link collection.DocumentCollection} and {@link collection.EdgeCollection}
     * interfaces and can be cast to either type to enforce a stricter API.
     *
     * @param T - Type to use for document data. Defaults to `any`.
     * @param collectionName - Name of the edge collection.
     *
     * @example
     * ```js
     * const db = new Database();
     * const collection = db.collection("potatoes");
     * ```
     *
     * @example
     * ```ts
     * interface Person {
     *   name: string;
     * }
     * const db = new Database();
     * const persons = db.collection<Person>("persons");
     * ```
     *
     * @example
     * ```ts
     * interface Person {
     *   name: string;
     * }
     * interface Friend {
     *   startDate: number;
     *   endDate?: number;
     * }
     * const db = new Database();
     * const documents = db.collection("persons") as DocumentCollection<Person>;
     * const edges = db.collection("friends") as EdgeCollection<Friend>;
     * ```
     */
    collection(collectionName) {
        collectionName = collectionName.normalize("NFC");
        if (!this._collections.has(collectionName)) {
            this._collections.set(collectionName, new Collection(this, collectionName));
        }
        return this._collections.get(collectionName);
    }
    async createCollection(collectionName, options) {
        const collection = this.collection(collectionName);
        await collection.create(options);
        return collection;
    }
    /**
     * Creates a new edge collection with the given `collectionName` and
     * `options`, then returns an {@link collection.EdgeCollection} instance for the new
     * edge collection.
     *
     * This is a convenience method for calling {@link Database#createCollection}
     * with `options.type` set to `EDGE_COLLECTION`.
     *
     * @param T - Type to use for edge document data. Defaults to `any`.
     * @param collectionName - Name of the new collection.
     * @param options - Options for creating the collection.
     *
     * @example
     * ```js
     * const db = new Database();
     * const edges = db.createEdgeCollection("friends");
     * ```
     *
     * @example
     * ```ts
     * interface Friend {
     *   startDate: number;
     *   endDate?: number;
     * }
     * const db = new Database();
     * const edges = db.createEdgeCollection<Friend>("friends");
     * ```
     */
    async createEdgeCollection(collectionName, options) {
        return this.createCollection(collectionName, {
            ...options,
            type: CollectionType.EDGE_COLLECTION,
        });
    }
    /**
     * Renames the collection `collectionName` to `newName`.
     *
     * Additionally removes any stored `Collection` instance for
     * `collectionName` from the `Database` instance's internal cache.
     *
     * **Note**: Renaming collections may not be supported when ArangoDB is
     * running in a cluster configuration.
     *
     * @param collectionName - Current name of the collection.
     * @param newName - The new name of the collection.
     */
    async renameCollection(collectionName, newName) {
        collectionName = collectionName.normalize("NFC");
        const result = await this.request({
            method: "PUT",
            path: `/_api/collection/${encodeURIComponent(collectionName)}/rename`,
            body: { name: newName.normalize("NFC") },
        });
        this._collections.delete(collectionName);
        return result;
    }
    /**
     * Fetches all collections from the database and returns an array of
     * collection descriptions.
     *
     * See also {@link Database#collections}.
     *
     * @param excludeSystem - Whether system collections should be excluded.
     *
     * @example
     * ```js
     * const db = new Database();
     * const collections = await db.listCollections();
     * // collections is an array of collection descriptions
     * // not including system collections
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * const collections = await db.listCollections(false);
     * // collections is an array of collection descriptions
     * // including system collections
     * ```
     */
    listCollections(excludeSystem = true) {
        return this.request({
            path: "/_api/collection",
            qs: { excludeSystem },
        }, (res) => res.body.result);
    }
    /**
     * Fetches all collections from the database and returns an array of
     * `Collection` instances.
     *
     * In TypeScript these instances implement both the
     * {@link collection.DocumentCollection} and {@link collection.EdgeCollection}
     * interfaces and can be cast to either type to enforce a stricter API.
     *
     * See also {@link Database#listCollections}.
     *
     * @param excludeSystem - Whether system collections should be excluded.
     *
     * @example
     * ```js
     * const db = new Database();
     * const collections = await db.collections();
     * // collections is an array of DocumentCollection and EdgeCollection
     * // instances not including system collections
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * const collections = await db.collections(false);
     * // collections is an array of DocumentCollection and EdgeCollection
     * // instances including system collections
     * ```
     */
    async collections(excludeSystem = true) {
        const collections = await this.listCollections(excludeSystem);
        return collections.map((data) => this.collection(data.name));
    }
    //#endregion
    //#region graphs
    /**
     * Returns a {@link graph.Graph} instance representing the graph with the given
     * `graphName`.
     *
     * @param graphName - Name of the graph.
     *
     * @example
     * ```js
     * const db = new Database();
     * const graph = db.graph("some-graph");
     * ```
     */
    graph(graphName) {
        graphName = graphName.normalize("NFC");
        if (!this._graphs.has(graphName)) {
            this._graphs.set(graphName, new Graph(this, graphName));
        }
        return this._graphs.get(graphName);
    }
    /**
     * Creates a graph with the given `graphName` and `edgeDefinitions`, then
     * returns a {@link graph.Graph} instance for the new graph.
     *
     * @param graphName - Name of the graph to be created.
     * @param edgeDefinitions - An array of edge definitions.
     * @param options - An object defining the properties of the graph.
     */
    async createGraph(graphName, edgeDefinitions, options) {
        const graph = this.graph(graphName.normalize("NFC"));
        await graph.create(edgeDefinitions, options);
        return graph;
    }
    /**
     * Fetches all graphs from the database and returns an array of graph
     * descriptions.
     *
     * See also {@link Database#graphs}.
     *
     * @example
     * ```js
     * const db = new Database();
     * const graphs = await db.listGraphs();
     * // graphs is an array of graph descriptions
     * ```
     */
    listGraphs() {
        return this.request({ path: "/_api/gharial" }, (res) => res.body.graphs);
    }
    /**
     * Fetches all graphs from the database and returns an array of {@link graph.Graph}
     * instances for those graphs.
     *
     * See also {@link Database#listGraphs}.
     *
     * @example
     * ```js
     * const db = new Database();
     * const graphs = await db.graphs();
     * // graphs is an array of Graph instances
     * ```
     */
    async graphs() {
        const graphs = await this.listGraphs();
        return graphs.map((data) => this.graph(data._key));
    }
    //#endregion
    //#region views
    /**
     * Returns a {@link view.View} instance for the given `viewName`.
     *
     * @param viewName - Name of the ArangoSearch or SearchAlias View.
     *
     * @example
     * ```js
     * const db = new Database();
     * const view = db.view("potatoes");
     * ```
     */
    view(viewName) {
        viewName = viewName.normalize("NFC");
        if (!this._views.has(viewName)) {
            this._views.set(viewName, new View(this, viewName));
        }
        return this._views.get(viewName);
    }
    /**
     * Creates a new View with the given `viewName` and `options`, then returns a
     * {@link view.View} instance for the new View.
     *
     * @param viewName - Name of the View.
     * @param options - An object defining the properties of the View.
     *
     * @example
     * ```js
     * const db = new Database();
     * const view = await db.createView("potatoes", { type: "arangosearch" });
     * // the ArangoSearch View "potatoes" now exists
     * ```
     */
    async createView(viewName, options) {
        const view = this.view(viewName.normalize("NFC"));
        await view.create(options);
        return view;
    }
    /**
     * Renames the view `viewName` to `newName`.
     *
     * Additionally removes any stored {@link view.View} instance for `viewName` from
     * the `Database` instance's internal cache.
     *
     * **Note**: Renaming views may not be supported when ArangoDB is running in
     * a cluster configuration.
     *
     * @param viewName - Current name of the view.
     * @param newName - The new name of the view.
     */
    async renameView(viewName, newName) {
        viewName = viewName.normalize("NFC");
        const result = await this.request({
            method: "PUT",
            path: `/_api/view/${encodeURIComponent(viewName)}/rename`,
            body: { name: newName.normalize("NFC") },
        });
        this._views.delete(viewName);
        return result;
    }
    /**
     * Fetches all Views from the database and returns an array of View
     * descriptions.
     *
     * See also {@link Database#views}.
     *
     * @example
     * ```js
     * const db = new Database();
     *
     * const views = await db.listViews();
     * // views is an array of View descriptions
     * ```
     */
    listViews() {
        return this.request({ path: "/_api/view" }, (res) => res.body.result);
    }
    /**
     * Fetches all Views from the database and returns an array of
     * {@link view.View} instances
     * for the Views.
     *
     * See also {@link Database#listViews}.
     *
     * @example
     * ```js
     * const db = new Database();
     * const views = await db.views();
     * // views is an array of ArangoSearch View instances
     * ```
     */
    async views() {
        const views = await this.listViews();
        return views.map((data) => this.view(data.name));
    }
    //#endregion
    //#region analyzers
    /**
     * Returns an {@link analyzer.Analyzer} instance representing the Analyzer with the
     * given `analyzerName`.
     *
     * @example
     * ```js
     * const db = new Database();
     * const analyzer = db.analyzer("some-analyzer");
     * const info = await analyzer.get();
     * ```
     */
    analyzer(analyzerName) {
        analyzerName = analyzerName.normalize("NFC");
        if (!this._analyzers.has(analyzerName)) {
            this._analyzers.set(analyzerName, new Analyzer(this, analyzerName));
        }
        return this._analyzers.get(analyzerName);
    }
    /**
     * Creates a new Analyzer with the given `analyzerName` and `options`, then
     * returns an {@link analyzer.Analyzer} instance for the new Analyzer.
     *
     * @param analyzerName - Name of the Analyzer.
     * @param options - An object defining the properties of the Analyzer.
     *
     * @example
     * ```js
     * const db = new Database();
     * const analyzer = await db.createAnalyzer("potatoes", { type: "identity" });
     * // the identity Analyzer "potatoes" now exists
     * ```
     */
    async createAnalyzer(analyzerName, options) {
        const analyzer = this.analyzer(analyzerName);
        await analyzer.create(options);
        return analyzer;
    }
    /**
     * Fetches all Analyzers visible in the database and returns an array of
     * Analyzer descriptions.
     *
     * See also {@link Database#analyzers}.
     *
     * @example
     * ```js
     * const db = new Database();
     * const analyzers = await db.listAnalyzers();
     * // analyzers is an array of Analyzer descriptions
     * ```
     */
    listAnalyzers() {
        return this.request({ path: "/_api/analyzer" }, (res) => res.body.result);
    }
    /**
     * Fetches all Analyzers visible in the database and returns an array of
     * {@link analyzer.Analyzer} instances for those Analyzers.
     *
     * See also {@link Database#listAnalyzers}.
     *
     * @example
     * ```js
     * const db = new Database();
     * const analyzers = await db.analyzers();
     * // analyzers is an array of Analyzer instances
     * ```
     */
    async analyzers() {
        const analyzers = await this.listAnalyzers();
        return analyzers.map((data) => this.analyzer(data.name));
    }
    //#endregion
    //#region users
    /**
     * Fetches all ArangoDB users visible to the authenticated user and returns
     * an array of user objects.
     *
     * @example
     * ```js
     * const db = new Database();
     * const users = await db.listUsers();
     * // users is an array of user objects
     * ```
     */
    listUsers() {
        return this.request({
            absolutePath: true,
            path: "/_api/user",
        });
    }
    /**
     * Fetches the user data of a single ArangoDB user.
     *
     * @param username - Name of the ArangoDB user to fetch.
     *
     * @example
     * ```js
     * const db = new Database();
     * const user = await db.getUser("steve");
     * // user is the user object for the user named "steve"
     * ```
     */
    getUser(username) {
        return this.request({
            absolutePath: true,
            path: `/_api/user/${encodeURIComponent(username)}`,
        });
    }
    createUser(username, options) {
        if (typeof options === "string") {
            options = { passwd: options };
        }
        return this.request({
            absolutePath: true,
            method: "POST",
            path: "/_api/user",
            body: { user: username, ...options },
        }, (res) => res.body);
    }
    updateUser(username, options) {
        if (typeof options === "string") {
            options = { passwd: options };
        }
        return this.request({
            absolutePath: true,
            method: "PATCH",
            path: `/api/user/${encodeURIComponent(username)}`,
            body: options,
        }, (res) => res.body);
    }
    /**
     * Replaces the ArangoDB user's option with the new options.
     *
     * @param username - Name of the ArangoDB user to modify.
     * @param options - New options to replace the user's existing options.
     *
     * @example
     * ```js
     * const db = new Database();
     * const user = await db.replaceUser("steve", { passwd: "", active: false });
     * // The user "steve" has been set to inactive with an empty password
     * ```
     */
    replaceUser(username, options) {
        if (typeof options === "string") {
            options = { passwd: options };
        }
        return this.request({
            absolutePath: true,
            method: "PUT",
            path: `/api/user/${encodeURIComponent(username)}`,
            body: options,
        }, (res) => res.body);
    }
    /**
     * Removes the ArangoDB user with the given username from the server.
     *
     * @param username - Name of the ArangoDB user to remove.
     *
     * @example
     * ```js
     * const db = new Database();
     * await db.removeUser("steve");
     * // The user "steve" has been removed
     * ```
     */
    removeUser(username) {
        return this.request({
            absolutePath: true,
            method: "DELETE",
            path: `/_api/user/${encodeURIComponent(username)}`,
        }, (res) => res.body);
    }
    /**
     * Fetches the given ArangoDB user's access level for the database, or the
     * given collection in the given database.
     *
     * @param username - Name of the ArangoDB user to fetch the access level for.
     * @param database - Database to fetch the access level for.
     * @param collection - Collection to fetch the access level for.
     *
     * @example
     * ```js
     * const db = new Database();
     * const accessLevel = await db.getUserAccessLevel("steve");
     * // The access level of the user "steve" has been fetched for the current
     * // database.
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * const accessLevel = await db.getUserAccessLevel("steve", {
     *   database: "staging"
     * });
     * // The access level of the user "steve" has been fetched for the "staging"
     * // database.
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * const accessLevel = await db.getUserAccessLevel("steve", {
     *   collection: "pokemons"
     * });
     * // The access level of the user "steve" has been fetched for the
     * // "pokemons" collection in the current database.
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * const accessLevel = await db.getUserAccessLevel("steve", {
     *   database: "staging",
     *   collection: "pokemons"
     * });
     * // The access level of the user "steve" has been fetched for the
     * // "pokemons" collection in the "staging" database.
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * const staging = db.database("staging");
     * const accessLevel = await db.getUserAccessLevel("steve", {
     *   database: staging
     * });
     * // The access level of the user "steve" has been fetched for the "staging"
     * // database.
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * const staging = db.database("staging");
     * const accessLevel = await db.getUserAccessLevel("steve", {
     *   collection: staging.collection("pokemons")
     * });
     * // The access level of the user "steve" has been fetched for the
     * // "pokemons" collection in database "staging".
     * ```
     */
    getUserAccessLevel(username, { database, collection }) {
        const databaseName = isArangoDatabase(database)
            ? database.name
            : database?.normalize("NFC") ??
                (isArangoCollection(collection)
                    ? collection._db.name
                    : this._name);
        const suffix = collection
            ? `/${encodeURIComponent(isArangoCollection(collection)
                ? collection.name
                : collection.normalize("NFC"))}`
            : "";
        return this.request({
            absolutePath: true,
            path: `/_api/user/${encodeURIComponent(username)}/database/${encodeURIComponent(databaseName)}${suffix}`,
        }, (res) => res.body.result);
    }
    /**
     * Sets the given ArangoDB user's access level for the database, or the
     * given collection in the given database.
     *
     * @param username - Name of the ArangoDB user to set the access level for.
     * @param database - Database to set the access level for.
     * @param collection - Collection to set the access level for.
     * @param grant - Access level to set for the given user.
     *
     * @example
     * ```js
     * const db = new Database();
     * await db.setUserAccessLevel("steve", { grant: "rw" });
     * // The user "steve" now has read-write access to the current database.
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * await db.setUserAccessLevel("steve", {
     *   database: "staging",
     *   grant: "rw"
     * });
     * // The user "steve" now has read-write access to the "staging" database.
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * await db.setUserAccessLevel("steve", {
     *   collection: "pokemons",
     *   grant: "rw"
     * });
     * // The user "steve" now has read-write access to the "pokemons" collection
     * // in the current database.
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * await db.setUserAccessLevel("steve", {
     *   database: "staging",
     *   collection: "pokemons",
     *   grant: "rw"
     * });
     * // The user "steve" now has read-write access to the "pokemons" collection
     * // in the "staging" database.
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * const staging = db.database("staging");
     * await db.setUserAccessLevel("steve", {
     *   database: staging,
     *   grant: "rw"
     * });
     * // The user "steve" now has read-write access to the "staging" database.
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * const staging = db.database("staging");
     * await db.setUserAccessLevel("steve", {
     *   collection: staging.collection("pokemons"),
     *   grant: "rw"
     * });
     * // The user "steve" now has read-write access to the "pokemons" collection
     * // in database "staging".
     * ```
     */
    setUserAccessLevel(username, { database, collection, grant, }) {
        const databaseName = isArangoDatabase(database)
            ? database.name
            : database?.normalize("NFC") ??
                (isArangoCollection(collection)
                    ? collection._db.name
                    : this._name);
        const suffix = collection
            ? `/${encodeURIComponent(isArangoCollection(collection)
                ? collection.name
                : collection.normalize("NFC"))}`
            : "";
        return this.request({
            absolutePath: true,
            method: "PUT",
            path: `/_api/user/${encodeURIComponent(username)}/database/${encodeURIComponent(databaseName)}${suffix}`,
            body: { grant },
        }, (res) => res.body);
    }
    /**
     * Clears the given ArangoDB user's access level for the database, or the
     * given collection in the given database.
     *
     * @param username - Name of the ArangoDB user to clear the access level for.
     * @param database - Database to clear the access level for.
     * @param collection - Collection to clear the access level for.
     *
     * @example
     * ```js
     * const db = new Database();
     * await db.clearUserAccessLevel("steve");
     * // The access level of the user "steve" has been cleared for the current
     * // database.
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * await db.clearUserAccessLevel("steve", { database: "staging" });
     * // The access level of the user "steve" has been cleared for the "staging"
     * // database.
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * await db.clearUserAccessLevel("steve", { collection: "pokemons" });
     * // The access level of the user "steve" has been cleared for the
     * // "pokemons" collection in the current database.
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * await db.clearUserAccessLevel("steve", {
     *   database: "staging",
     *   collection: "pokemons"
     * });
     * // The access level of the user "steve" has been cleared for the
     * // "pokemons" collection in the "staging" database.
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * const staging = db.database("staging");
     * await db.clearUserAccessLevel("steve", { database: staging });
     * // The access level of the user "steve" has been cleared for the "staging"
     * // database.
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * const staging = db.database("staging");
     * await db.clearUserAccessLevel("steve", {
     *   collection: staging.collection("pokemons")
     * });
     * // The access level of the user "steve" has been cleared for the
     * // "pokemons" collection in database "staging".
     * ```
     */
    clearUserAccessLevel(username, { database, collection }) {
        const databaseName = isArangoDatabase(database)
            ? database.name
            : database?.normalize("NFC") ??
                (isArangoCollection(collection)
                    ? collection._db.name
                    : this._name);
        const suffix = collection
            ? `/${encodeURIComponent(isArangoCollection(collection)
                ? collection.name
                : collection.normalize("NFC"))}`
            : "";
        return this.request({
            absolutePath: true,
            method: "DELETE",
            path: `/_api/user/${encodeURIComponent(username)}/database/${encodeURIComponent(databaseName)}${suffix}`,
        }, (res) => res.body);
    }
    getUserDatabases(username, full) {
        return this.request({
            absolutePath: true,
            path: `/_api/user/${encodeURIComponent(username)}/database`,
            qs: { full },
        }, (res) => res.body.result);
    }
    executeTransaction(collections, action, options = {}) {
        const { allowDirtyRead = undefined, ...opts } = options;
        return this.request({
            method: "POST",
            path: "/_api/transaction",
            allowDirtyRead,
            body: {
                collections: coerceTransactionCollections(collections),
                action,
                ...opts,
            },
        }, (res) => res.body.result);
    }
    /**
     * Returns a {@link transaction.Transaction} instance for an existing streaming
     * transaction with the given `id`.
     *
     * See also {@link Database#beginTransaction}.
     *
     * @param id - The `id` of an existing stream transaction.
     *
     * @example
     * ```js
     * const trx1 = await db.beginTransaction(collections);
     * const id = trx1.id;
     * // later
     * const trx2 = db.transaction(id);
     * await trx2.commit();
     * ```
     */
    transaction(transactionId) {
        return new Transaction(this, transactionId);
    }
    beginTransaction(collections, options = {}) {
        const { allowDirtyRead = undefined, ...opts } = options;
        return this.request({
            method: "POST",
            path: "/_api/transaction/begin",
            allowDirtyRead,
            body: {
                collections: coerceTransactionCollections(collections),
                ...opts,
            },
        }, (res) => new Transaction(this, res.body.result.id));
    }
    /**
     * Fetches all active transactions from the database and returns an array of
     * transaction descriptions.
     *
     * See also {@link Database#transactions}.
     *
     * @example
     * ```js
     * const db = new Database();
     * const transactions = await db.listTransactions();
     * // transactions is an array of transaction descriptions
     * ```
     */
    listTransactions() {
        return this._connection.request({ path: "/_api/transaction" }, (res) => res.body.transactions);
    }
    /**
     * Fetches all active transactions from the database and returns an array of
     * {@link transaction.Transaction} instances for those transactions.
     *
     * See also {@link Database#listTransactions}.
     *
     * @example
     * ```js
     * const db = new Database();
     * const transactions = await db.transactions();
     * // transactions is an array of transactions
     * ```
     */
    async transactions() {
        const transactions = await this.listTransactions();
        return transactions.map((data) => this.transaction(data.id));
    }
    query(query, bindVars, options = {}) {
        if (isAqlQuery(query)) {
            options = bindVars ?? {};
            bindVars = query.bindVars;
            query = query.query;
        }
        else if (isAqlLiteral(query)) {
            query = query.toAQL();
        }
        const { allowDirtyRead, retryOnConflict, count, batchSize, cache, memoryLimit, ttl, timeout, ...opts } = options;
        return this.request({
            method: "POST",
            path: "/_api/cursor",
            body: {
                query,
                bindVars,
                count,
                batchSize,
                cache,
                memoryLimit,
                ttl,
                options: opts,
            },
            allowDirtyRead,
            retryOnConflict,
            timeout,
        }, (res) => new BatchedArrayCursor(this, res.body, res.arangojsHostUrl, allowDirtyRead).items);
    }
    explain(query, bindVars, options) {
        if (isAqlQuery(query)) {
            options = bindVars;
            bindVars = query.bindVars;
            query = query.query;
        }
        else if (isAqlLiteral(query)) {
            query = query.toAQL();
        }
        return this.request({
            method: "POST",
            path: "/_api/explain",
            body: { query, bindVars, options },
        });
    }
    /**
     * Parses the given query and returns the result.
     *
     * See the {@link aql!aql} template string handler for information about how
     * to create a query string without manually defining bind parameters nor
     * having to worry about escaping variables.
     *
     * @param query - An AQL query string or an object containing an AQL query
     * string and bind parameters, e.g. the object returned from an {@link aql!aql}
     * template string.
     *
     * @example
     * ```js
     * const db = new Database();
     * const collection = db.collection("some-collection");
     * const ast = await db.parse(aql`
     *   FOR doc IN ${collection}
     *   FILTER doc.flavor == "strawberry"
     *   RETURN doc._key
     * `);
     * ```
     */
    parse(query) {
        if (isAqlQuery(query)) {
            query = query.query;
        }
        else if (isAqlLiteral(query)) {
            query = query.toAQL();
        }
        return this.request({
            method: "POST",
            path: "/_api/query",
            body: { query },
        });
    }
    /**
     * Fetches the available optimizer rules.
     *
     * @example
     * ```js
     * const db = new Database();
     * const rules = await db.queryRules();
     * for (const rule of rules) {
     *   console.log(rule.name);
     * }
     * ```
     */
    queryRules() {
        return this.request({
            path: "/_api/query/rules",
        });
    }
    queryTracking(options) {
        return this.request(options
            ? {
                method: "PUT",
                path: "/_api/query/properties",
                body: options,
            }
            : {
                method: "GET",
                path: "/_api/query/properties",
            });
    }
    /**
     * Fetches a list of information for all currently running queries.
     *
     * See also {@link Database#listSlowQueries} and {@link Database#killQuery}.
     *
     * @example
     * ```js
     * const db = new Database();
     * const queries = await db.listRunningQueries();
     * ```
     */
    listRunningQueries() {
        return this.request({
            method: "GET",
            path: "/_api/query/current",
        });
    }
    /**
     * Fetches a list of information for all recent slow queries.
     *
     * See also {@link Database#listRunningQueries} and
     * {@link Database#clearSlowQueries}.
     *
     * @example
     * ```js
     * const db = new Database();
     * const queries = await db.listSlowQueries();
     * // Only works if slow query tracking is enabled
     * ```
     */
    listSlowQueries() {
        return this.request({
            method: "GET",
            path: "/_api/query/slow",
        });
    }
    /**
     * Clears the list of recent slow queries.
     *
     * See also {@link Database#listSlowQueries}.
     *
     * @example
     * ```js
     * const db = new Database();
     * await db.clearSlowQueries();
     * // Slow query list is now cleared
     * ```
     */
    clearSlowQueries() {
        return this.request({
            method: "DELETE",
            path: "/_api/query/slow",
        }, () => undefined);
    }
    /**
     * Kills a running query with the given `queryId`.
     *
     * See also {@link Database#listRunningQueries}.
     *
     * @param queryId - The ID of a currently running query.
     *
     * @example
     * ```js
     * const db = new Database();
     * const queries = await db.listRunningQueries();
     * await Promise.all(queries.map(
     *   async (query) => {
     *     if (query.state === "executing") {
     *       await db.killQuery(query.id);
     *     }
     *   }
     * ));
     * ```
     */
    killQuery(queryId) {
        return this.request({
            method: "DELETE",
            path: `/_api/query/${encodeURIComponent(queryId)}`,
        }, () => undefined);
    }
    //#endregion
    //#region functions
    /**
     * Fetches a list of all AQL user functions registered with the database.
     *
     * @example
     * ```js
     * const db = new Database();
     * const functions = await db.listFunctions();
     * const names = functions.map(fn => fn.name);
     * ```
     */
    listFunctions() {
        return this.request({ path: "/_api/aqlfunction" }, (res) => res.body.result);
    }
    /**
     * Creates an AQL user function with the given _name_ and _code_ if it does
     * not already exist or replaces it if a function with the same name already
     * existed.
     *
     * @param name - A valid AQL function name. The function name must consist
     * of at least two alphanumeric identifiers separated with double colons.
     * @param code - A string evaluating to a JavaScript function (not a
     * JavaScript function object).
     * @param isDeterministic - If set to `true`, the function is expected to
     * always return the same result for equivalent inputs. This option currently
     * has no effect but may allow for optimizations in the future.
     *
     * @example
     * ```js
     * const db = new Database();
     * await db.createFunction(
     *   "ACME::ACCOUNTING::CALCULATE_VAT",
     *   "(price) => price * 0.19"
     * );
     * // Use the new function in an AQL query with template handler:
     * const cursor = await db.query(aql`
     *   FOR product IN products
     *   RETURN MERGE(
     *     { vat: ACME::ACCOUNTING::CALCULATE_VAT(product.price) },
     *     product
     *   )
     * `);
     * // cursor is a cursor for the query result
     * ```
     */
    createFunction(name, code, isDeterministic = false) {
        return this.request({
            method: "POST",
            path: "/_api/aqlfunction",
            body: { name, code, isDeterministic },
        });
    }
    /**
     * Deletes the AQL user function with the given name from the database.
     *
     * @param name - The name of the user function to drop.
     * @param group - If set to `true`, all functions with a name starting with
     * `name` will be deleted, otherwise only the function with the exact name
     * will be deleted.
     *
     * @example
     * ```js
     * const db = new Database();
     * await db.dropFunction("ACME::ACCOUNTING::CALCULATE_VAT");
     * // the function no longer exists
     * ```
     */
    dropFunction(name, group = false) {
        return this.request({
            method: "DELETE",
            path: `/_api/aqlfunction/${encodeURIComponent(name)}`,
            qs: { group },
        });
    }
    //#endregion
    //#region services
    /**
     * Fetches a list of all installed service.
     *
     * @param excludeSystem - Whether system services should be excluded.
     *
     * @example
     * ```js
     * const db = new Database();
     * const services = await db.listServices();
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * const services = await db.listServices(false); // all services
     * ```
     */
    listServices(excludeSystem = true) {
        return this.request({
            path: "/_api/foxx",
            qs: { excludeSystem },
        });
    }
    /**
     * Installs a new service.
     *
     * @param mount - The service's mount point, relative to the database.
     * @param source - The service bundle to install.
     * @param options - Options for installing the service.
     *
     * @example
     * ```js
     * const db = new Database();
     * // Using a node.js file stream as source
     * const source = fs.createReadStream("./my-foxx-service.zip");
     * const info = await db.installService("/hello", source);
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * // Using a node.js Buffer as source
     * const source = fs.readFileSync("./my-foxx-service.zip");
     * const info = await db.installService("/hello", source);
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * // Using a File (Blob) from a browser file input
     * const element = document.getElementById("my-file-input");
     * const source = element.files[0];
     * const info = await db.installService("/hello", source);
     * ```
     */
    async installService(mount, source, options = {}) {
        const { configuration, dependencies, ...qs } = options;
        const req = await toForm({
            configuration,
            dependencies,
            source,
        });
        return await this.request({
            ...req,
            method: "POST",
            path: "/_api/foxx",
            isBinary: true,
            qs: { ...qs, mount },
        });
    }
    /**
     * Replaces an existing service with a new service by completely removing the
     * old service and installing a new service at the same mount point.
     *
     * @param mount - The service's mount point, relative to the database.
     * @param source - The service bundle to install.
     * @param options - Options for replacing the service.
     *
     * @example
     * ```js
     * const db = new Database();
     * // Using a node.js file stream as source
     * const source = fs.createReadStream("./my-foxx-service.zip");
     * const info = await db.replaceService("/hello", source);
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * // Using a node.js Buffer as source
     * const source = fs.readFileSync("./my-foxx-service.zip");
     * const info = await db.replaceService("/hello", source);
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * // Using a File (Blob) from a browser file input
     * const element = document.getElementById("my-file-input");
     * const source = element.files[0];
     * const info = await db.replaceService("/hello", source);
     * ```
     */
    async replaceService(mount, source, options = {}) {
        const { configuration, dependencies, ...qs } = options;
        const req = await toForm({
            configuration,
            dependencies,
            source,
        });
        return await this.request({
            ...req,
            method: "PUT",
            path: "/_api/foxx/service",
            isBinary: true,
            qs: { ...qs, mount },
        });
    }
    /**
     * Replaces an existing service with a new service while retaining the old
     * service's configuration and dependencies.
     *
     * @param mount - The service's mount point, relative to the database.
     * @param source - The service bundle to install.
     * @param options - Options for upgrading the service.
     *
     * @example
     * ```js
     * const db = new Database();
     * // Using a node.js file stream as source
     * const source = fs.createReadStream("./my-foxx-service.zip");
     * const info = await db.upgradeService("/hello", source);
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * // Using a node.js Buffer as source
     * const source = fs.readFileSync("./my-foxx-service.zip");
     * const info = await db.upgradeService("/hello", source);
     * ```
     *
     * @example
     * ```js
     * const db = new Database();
     * // Using a File (Blob) from a browser file input
     * const element = document.getElementById("my-file-input");
     * const source = element.files[0];
     * const info = await db.upgradeService("/hello", source);
     * ```
     */
    async upgradeService(mount, source, options = {}) {
        const { configuration, dependencies, ...qs } = options;
        const req = await toForm({
            configuration,
            dependencies,
            source,
        });
        return await this.request({
            ...req,
            method: "PATCH",
            path: "/_api/foxx/service",
            isBinary: true,
            qs: { ...qs, mount },
        });
    }
    /**
     * Completely removes a service from the database.
     *
     * @param mount - The service's mount point, relative to the database.
     * @param options - Options for uninstalling the service.
     *
     * @example
     * ```js
     * const db = new Database();
     * await db.uninstallService("/my-foxx");
     * ```
     */
    uninstallService(mount, options) {
        return this.request({
            method: "DELETE",
            path: "/_api/foxx/service",
            qs: { ...options, mount },
        }, () => undefined);
    }
    /**
     * Retrieves information about a mounted service.
     *
     * @param mount - The service's mount point, relative to the database.
     *
     * @example
     * ```js
     * const db = new Database();
     * const info = await db.getService("/my-service");
     * // info contains detailed information about the service
     * ```
     */
    getService(mount) {
        return this.request({
            path: "/_api/foxx/service",
            qs: { mount },
        });
    }
    getServiceConfiguration(mount, minimal = false) {
        return this.request({
            path: "/_api/foxx/configuration",
            qs: { mount, minimal },
        });
    }
    replaceServiceConfiguration(mount, cfg, minimal = false) {
        return this.request({
            method: "PUT",
            path: "/_api/foxx/configuration",
            body: cfg,
            qs: { mount, minimal },
        });
    }
    updateServiceConfiguration(mount, cfg, minimal = false) {
        return this.request({
            method: "PATCH",
            path: "/_api/foxx/configuration",
            body: cfg,
            qs: { mount, minimal },
        });
    }
    getServiceDependencies(mount, minimal = false) {
        return this.request({
            path: "/_api/foxx/dependencies",
            qs: { mount, minimal },
        });
    }
    replaceServiceDependencies(mount, deps, minimal = false) {
        return this.request({
            method: "PUT",
            path: "/_api/foxx/dependencies",
            body: deps,
            qs: { mount, minimal },
        });
    }
    updateServiceDependencies(mount, deps, minimal = false) {
        return this.request({
            method: "PATCH",
            path: "/_api/foxx/dependencies",
            body: deps,
            qs: { mount, minimal },
        });
    }
    /**
     * Enables or disables development mode for the given service.
     *
     * @param mount - The service's mount point, relative to the database.
     * @param enabled - Whether development mode should be enabled or disabled.
     *
     * @example
     * ```js
     * const db = new Database();
     * await db.setServiceDevelopmentMode("/my-service", true);
     * // the service is now in development mode
     * await db.setServiceDevelopmentMode("/my-service", false);
     * // the service is now in production mode
     * ```
     */
    setServiceDevelopmentMode(mount, enabled = true) {
        return this.request({
            method: enabled ? "POST" : "DELETE",
            path: "/_api/foxx/development",
            qs: { mount },
        });
    }
    /**
     * Retrieves a list of scripts defined in the service manifest's "scripts"
     * section mapped to their human readable representations.
     *
     * @param mount - The service's mount point, relative to the database.
     *
     * @example
     * ```js
     * const db = new Database();
     * const scripts = await db.listServiceScripts("/my-service");
     * for (const [name, title] of Object.entries(scripts)) {
     *   console.log(`${name}: ${title}`);
     * }
     * ```
     */
    listServiceScripts(mount) {
        return this.request({
            path: "/_api/foxx/scripts",
            qs: { mount },
        });
    }
    /**
     * Executes a service script and retrieves its result exposed as
     * `module.exports` (if any).
     *
     * @param mount - The service's mount point, relative to the database.
     * @param name - Name of the service script to execute as defined in the
     * service manifest.
     * @param params - Arbitrary value that will be exposed to the script as
     * `argv[0]` in the service context (e.g. `module.context.argv[0]`).
     * Must be serializable to JSON.
     *
     * @example
     * ```js
     * const db = new Database();
     * const result = await db.runServiceScript(
     *   "/my-service",
     *   "create-user",
     *   {
     *     username: "service_admin",
     *     password: "hunter2"
     *   }
     * );
     * ```
     */
    runServiceScript(mount, name, params) {
        return this.request({
            method: "POST",
            path: `/_api/foxx/scripts/${encodeURIComponent(name)}`,
            body: params,
            qs: { mount },
        });
    }
    runServiceTests(mount, options) {
        return this.request({
            method: "POST",
            path: "/_api/foxx/tests",
            qs: {
                ...options,
                mount,
            },
        });
    }
    /**
     * Retrieves the text content of the service's `README` or `README.md` file.
     *
     * Returns `undefined` if no such file could be found.
     *
     * @param mount - The service's mount point, relative to the database.
     *
     * @example
     * ```js
     * const db = new Database();
     * const readme = await db.getServiceReadme("/my-service");
     * if (readme !== undefined) console.log(readme);
     * else console.warn(`No README found.`)
     * ```
     */
    getServiceReadme(mount) {
        return this.request({
            path: "/_api/foxx/readme",
            qs: { mount },
        });
    }
    /**
     * Retrieves an Open API compatible Swagger API description object for the
     * service installed at the given mount point.
     *
     * @param mount - The service's mount point, relative to the database.
     *
     * @example
     * ```js
     * const db = new Database();
     * const spec = await db.getServiceDocumentation("/my-service");
     * // spec is a Swagger API description of the service
     * ```
     */
    getServiceDocumentation(mount) {
        return this.request({
            path: "/_api/foxx/swagger",
            qs: { mount },
        });
    }
    /**
     * Retrieves a zip bundle containing the service files.
     *
     * Returns a `Buffer` in node.js or `Blob` in the browser.
     *
     * @param mount - The service's mount point, relative to the database.
     *
     * @example
     * ```js
     * const db = new Database();
     * const serviceBundle = await db.downloadService("/my-foxx");
     * ```
     */
    downloadService(mount) {
        return this.request({
            method: "POST",
            path: "/_api/foxx/download",
            qs: { mount },
            expectBinary: true,
        });
    }
    /**
     * Writes all locally available services to the database and updates any
     * service bundles missing in the database.
     *
     * @param replace - If set to `true`, outdated services will also be
     * committed. This can be used to solve some consistency problems when
     * service bundles are missing in the database or were deleted manually.
     *
     * @example
     * ```js
     * await db.commitLocalServiceState();
     * // all services available on the coordinator have been written to the db
     * ```
     *
     * @example
     * ```js
     * await db.commitLocalServiceState(true);
     * // all service conflicts have been resolved in favor of this coordinator
     * ```
     */
    commitLocalServiceState(replace = false) {
        return this.request({
            method: "POST",
            path: "/_api/foxx/commit",
            qs: { replace },
        }, () => undefined);
    }
}

function arangojs(config, name) {
    if (typeof config === "string" || Array.isArray(config)) {
        const url = config;
        return new Database(url, name);
    }
    return new Database(config);
}

export { Database, aql, arangojs, arangojs as default };
//# sourceMappingURL=arangojs.js.map
