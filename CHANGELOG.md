# (2023-6-4)

## 1.0.0

Based on [orangojs]（ https://github.com/arangodb/arangojs ）Adapt the original library version 8.3.1 to run on OpenHarmony, while retaining its existing usage and features.