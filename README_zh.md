# arangojs

## 介绍

[@ohos/arangojs](https://github.com/arangodb/arangojs)是适用于 openharmony 环境的 ArangoDB 数据库 javascript 版驱动。本库基于 [arangojs](https://github.com/iamkun/dayjs)原库 v8.3.1 版本进行适配。

## 下载安装
```shell
ohpm install @ohos/arangojs
```

> OpenHarmony ohpm 环境配置等更多内容，请参考[如何安装 OpenHarmony ohpm 包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)

## 使用说明

### 基本使用示例

1.连接数据库

```javascript
import { Database } from "@ohos/arangojs";

const system = new Database({
  url: "http://127.0.0.1:8529",
});

// The credentials can be swapped at any time
db.useBasicAuth("admin", "maplesyrup");
```

2.创建数据库

```javascript
const name = "testDB" + Date.now();
await system.createDatabase(name);
```

3.删除数据库

```javascript
await system.dropDatabase(name);
```

4.获取数据库列表

```javascript
await system.databases();
```

5.切换数据库

```javascript
const db = system.database("database_name");
```

6.创建文档模型数据表

```javascript
await system.createCollection("collection_name");
```

7.创建图表模型数据表

```javascript
await system.createEdgeCollection("edge_collection_name");
```

## 接口说明

|                                                                        方法名                                                                         |             接口描述              |
|:--------------------------------------------------------------------------------------------------------------------------------------------------:|:-----------------------------:|
|                                       createDatabase(databaseName: string, options?: CreateDatabaseOptions)                                        |             创建数据库             |
|                                                         dropDatabase(databaseName: string)                                                         |             删除数据库             |
|                                                                    databases()                                                                     |            获取数据库列表            |
|                                                              database(dbName:string)                                                               |           切换数据库            |
|                        createCollection(collectionName: string, options?: CreateCollectionOptions & {type: CollectionType})                        |         创建文档模型数据表          |
|                                  createEdgeCollection(collectionName: string, options?: CreateCollectionOptions)                                   |           创建图表模型数据表             |

## 约束与限制

在下述版本验证通过：

- DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release(5.0.0.66)

- DevEco Studio: 4.0.3.112 ，OpenHarmony SDK: API10（4.0.7.3 Beta1）。

## 贡献代码

使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/arangojs/issues)
非常欢迎发 [PR](https://gitee.com/openharmony-sig/arangojs/pulls) 。

## 开源协议

本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/arangojs/blob/master/LICENSE) ，请自由地享受和参与开源。
