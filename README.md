# arangojs

## Introduction

[@ohos/arangojs](https://github.com/arangodb/arangojs) is ArangoDB JavaScript driver for OpenHarmony. This library is adapted from [arangojs](https://github.com/iamkun/dayjs) v8.3.1.

## How to Install
```shell
ohpm install @ohos/arangojs
```

For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## How to Use

### Basic Usage Example

1. Connect to a database.

```javascript
import { Database } from "@ohos/arangojs";

const system = new Database({
  url: "http://127.0.0.1:8529",
});

// The credentials can be swapped at any time
db.useBasicAuth("admin", "maplesyrup");
```

2. Create a database.

```javascript
const name = "testDB" + Date.now();
await system.createDatabase(name);
```

3. Delete a database.

```javascript
await system.dropDatabase(name);
```

4. Obtain all databases available on the server.

```javascript
await system.databases();
```

5. Select the database to work with.

```javascript
const db = system.database("database_name");
```

6. Create a document collection.

```javascript
await system.createCollection("collection_name");
```

7. Create an edge collection.

```javascript
await system.createEdgeCollection("edge_collection_name");
```

## Available APIs

|                                                                        API                                                                        |             Description             |
|:--------------------------------------------------------------------------------------------------------------------------------------------------|:-----------------------------|
|                                       createDatabase(databaseName: string, options?: CreateDatabaseOptions)                                        |             Creates a database.            |
|                                                         dropDatabase(databaseName: string)                                                         |             Deletes a database.            |
|                                                                    databases()                                                                     |           Obtains all databases available on the server.           |
|                                                              database(dbName:string)                                                               |           Selects a database to work with.           |
|                        createCollection(collectionName: string, options?: CreateCollectionOptions & {type: CollectionType})                        |         Creates a document collection.         |
|                                  createEdgeCollection(collectionName: string, options?: CreateCollectionOptions)                                   |           Creates an edge collection.            |

## Constraints

This project has been verified in the following version:

- DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release(5.0.0.66)

- DevEco Studio: 4.0.3.112, OpenHarmony SDK: API10 (4.0.7.3 Beta1)

## How to Contribute

If you find any problem during the use, submit an [Issue](https://gitee.com/openharmony-sig/arangojs/issues) or a [PR](https://gitee.com/openharmony-sig/arangojs/pulls) to us.

## License

This project is licensed under [Apache License 2.0](https://gitee.com/openharmony-sig/arangojs/blob/master/LICENSE).
